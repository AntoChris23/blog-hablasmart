/**
 * ServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package serviceWsdl;

public interface ServicePortType extends java.rmi.Remote {

    /**
     * Para Inseretar en objetos
     */
    public java.lang.String insertar(java.lang.String USUARIO, java.lang.String PASSWORD, java.lang.String nombres, java.lang.String apellidos, java.lang.String direccion, java.lang.String documento, java.lang.String email, java.lang.String tipodoc, java.lang.String barrio, java.lang.String celular, java.lang.String ciudad, java.lang.String estrato, java.lang.String tel, java.lang.String planVoz, java.lang.String proInternet, java.lang.String proTV, java.lang.String planHd, java.lang.String svasInternet, java.lang.String svastv, java.lang.String decodificador, java.lang.String totalPedido, java.lang.String totalConIVA, java.lang.String codigoLb, java.lang.String codigoBa, java.lang.String codigoPlanTv, java.lang.String tipo_mx) throws java.rmi.RemoteException;
}
