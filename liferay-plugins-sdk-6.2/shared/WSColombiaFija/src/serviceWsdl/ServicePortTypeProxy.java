package serviceWsdl;

public class ServicePortTypeProxy implements serviceWsdl.ServicePortType {
  private String _endpoint = null;
  private serviceWsdl.ServicePortType servicePortType = null;
  
  public ServicePortTypeProxy() {
    _initServicePortTypeProxy();
  }
  
  public ServicePortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicePortTypeProxy();
  }
  
  private void _initServicePortTypeProxy() {
    try {
      servicePortType = (new serviceWsdl.ServiceLocator()).getservicePort();
      if (servicePortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servicePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servicePortType != null)
      ((javax.xml.rpc.Stub)servicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public serviceWsdl.ServicePortType getServicePortType() {
    if (servicePortType == null)
      _initServicePortTypeProxy();
    return servicePortType;
  }
  
  public java.lang.String insertar(java.lang.String USUARIO, java.lang.String PASSWORD, java.lang.String nombres, java.lang.String apellidos, java.lang.String direccion, java.lang.String documento, java.lang.String email, java.lang.String tipodoc, java.lang.String barrio, java.lang.String celular, java.lang.String ciudad, java.lang.String estrato, java.lang.String tel, java.lang.String planVoz, java.lang.String proInternet, java.lang.String proTV, java.lang.String planHd, java.lang.String svasInternet, java.lang.String svastv, java.lang.String decodificador, java.lang.String totalPedido, java.lang.String totalConIVA, java.lang.String codigoLb, java.lang.String codigoBa, java.lang.String codigoPlanTv, java.lang.String tipo_mx) throws java.rmi.RemoteException{
    if (servicePortType == null)
      _initServicePortTypeProxy();
    return servicePortType.insertar(USUARIO, PASSWORD, nombres, apellidos, direccion, documento, email, tipodoc, barrio, celular, ciudad, estrato, tel, planVoz, proInternet, proTV, planHd, svasInternet, svastv, decodificador, totalPedido, totalConIVA, codigoLb, codigoBa, codigoPlanTv, tipo_mx);
  }
  
  
}