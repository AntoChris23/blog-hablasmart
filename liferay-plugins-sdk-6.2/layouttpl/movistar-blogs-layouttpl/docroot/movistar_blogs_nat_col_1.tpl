<section class="nat-blog-section" id="main-content" role="main">
<div class="container">
		<div class="portlet-layout nat-row">
			<div class="portlet-column portlet-column-only nat-col--12" id="column-1">
				$processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")
			</div>
		</div>
	</div>
</section>