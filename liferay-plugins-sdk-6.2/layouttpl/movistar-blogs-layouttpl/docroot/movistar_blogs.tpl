<section class="nat-blog-section--full" id="main-content" role="main">
	<div class="container">
		<div class="portlet-layout nat-row">
			<div class="portlet-column portlet-column-first nat-col--lg-8" id="column-1">
				$processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
			</div>
			<div class="portlet-column portlet-column-last nat-col--lg-4" id="column-2">
				$processor.processColumn("column-2", "portlet-column-content portlet-column-content-last")
			</div>
		</div>
	</div>
</section>