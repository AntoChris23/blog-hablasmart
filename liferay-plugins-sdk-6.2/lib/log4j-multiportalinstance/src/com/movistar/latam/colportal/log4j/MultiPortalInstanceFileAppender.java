/*
 * 
 */
package com.movistar.latam.colportal.log4j;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.spi.ErrorHandler;
import org.apache.log4j.spi.LoggingEvent;


/**
 * MultiPortalInstanceFileAppender:.
 * 
 * @author VASS
 * @version 1.0
 */
public class MultiPortalInstanceFileAppender extends RollingFileAppender {

	/** Name. */
	protected String name;

	/** Prefix. */
	private String prefix;

	/** Suffix. */
	private String suffix;

	/** Appender map. */
	private final Map<String, Map<String, RollingFileAppender>> appenderMap = new HashMap<String, Map<String, RollingFileAppender>>();

	/* (non-Javadoc)
	 * @see org.apache.log4j.AppenderSkeleton#clearFilters()
	 */
	@Override
	public void clearFilters() {
		if (appenderMap != null) {
			for (final Map<String, RollingFileAppender> dateMap : appenderMap
					.values()) {
				for (final RollingFileAppender appender : dateMap.values()) {
					appender.clearFilters();
				}
			}
		} else {
			super.clearFilters();
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.log4j.WriterAppender#close()
	 */
	@Override
	public void close() {
		if (appenderMap != null) {
			for (final Map<String, RollingFileAppender> dateMap : appenderMap
					.values()) {
				for (final RollingFileAppender appender : dateMap.values()) {
					appender.close();
				}
			}
		} else {
			super.close();
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.log4j.AppenderSkeleton#doAppend(org.apache.log4j.spi.LoggingEvent)
	 */
	@Override
	public void doAppend(final LoggingEvent event) {
		// SELECT RIGHT portalInstance FRON EVENT MDC
		String portalInstance = (String) event.getMDC("portalInstance");
		if (portalInstance != null) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String currentDate = dateFormat.format(cal.getTime());
			final FileAppender appender = getAppender(portalInstance,
					currentDate);
			if (appender != null) {
				appender.doAppend(event);
			}
		} else {
			super.doAppend(event);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.log4j.Appender#setErrorHandler(org.apache.log4j.spi.ErrorHandler
	 * )
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.log4j.Appender#getErrorHandler()
	 */
	@Override
	public ErrorHandler getErrorHandler() {
		return super.getErrorHandler();
	}

	/* (non-Javadoc)
	 * @see org.apache.log4j.AppenderSkeleton#getLayout()
	 */
	@Override
	public Layout getLayout() {
		return this.layout;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.log4j.Appender#requiresLayout()
	 */
	@Override
	public boolean requiresLayout() {
		return true;
	}

	/**
	 * Obtiene appender.
	 * 
	 * @param portalInstance
	 *            portal instance
	 * @param currentDate
	 *            current date
	 * @return Appender
	 */
	private RollingFileAppender getAppender(final String portalInstance,
			final String currentDate) {
		RollingFileAppender appender = null;
		if (appenderMap.containsKey(portalInstance)) {
			Map<String, RollingFileAppender> dateMap = appenderMap
					.get(portalInstance);
			if (dateMap.containsKey(currentDate)) {
				appender = dateMap.get(currentDate);
			} else {
				appender = addAppender(portalInstance, currentDate);
			}
		} else {
			appender = addAppender(portalInstance, currentDate);
		}
		return appender;
	}

	/**
	 * Añade appender.
	 * 
	 * @param portalInstance
	 *            portal instance
	 * @param currentDate
	 *            current date
	 * @return rolling file appender
	 */
	private RollingFileAppender addAppender(final String portalInstance,
			final String currentDate) {

		final String prefix = getPrefix();
		final String suffix = getSuffix();

		String newFileName = prefix + "_" + portalInstance + "_" + currentDate
				+ suffix;
		if (portalInstance.length() == 0) {
			newFileName = super.getFile();
		}

		try {
			final RollingFileAppender fileAppender = new RollingFileAppender(
					layout, newFileName, super.fileAppend);
			fileAppender.setBufferedIO(bufferedIO);
			fileAppender.setBufferSize(bufferSize);
			fileAppender.setEncoding(super.getEncoding());
			fileAppender.setErrorHandler(getErrorHandler());
			fileAppender.setImmediateFlush(super.getImmediateFlush());
			fileAppender.setMaxBackupIndex(super.getMaxBackupIndex());
			fileAppender.setMaximumFileSize(super.getMaximumFileSize());
			fileAppender.setName(newFileName);
			fileAppender.setThreshold(threshold);

			Map<String, RollingFileAppender> map = new HashMap<String, RollingFileAppender>();
			map.put(currentDate, fileAppender);

			appenderMap.put(portalInstance, map);
			return fileAppender;
		} catch (final IOException e) {
			e.printStackTrace(System.out);
			return null;
		}
	}

	/**
	 * Obtiene prefix.
	 * 
	 * @return Prefix
	 */
	private String getPrefix() {
		if (prefix == null) {
			intPrefixAndSuffix();
		}
		return prefix;
	}

	/**
	 * Obtiene suffix.
	 * 
	 * @return Suffix
	 */
	private String getSuffix() {
		if (suffix == null) {
			intPrefixAndSuffix();
		}
		return suffix;
	}

	/**
	 * Int prefix and suffix.
	 */
	private synchronized void intPrefixAndSuffix() {
		prefix = fileName;
		suffix = "";
		if (!fileName.endsWith(".log")) {
			return;
		}

		prefix = fileName.substring(0, fileName.indexOf(".log"));
		suffix = ".log";
	}
}
