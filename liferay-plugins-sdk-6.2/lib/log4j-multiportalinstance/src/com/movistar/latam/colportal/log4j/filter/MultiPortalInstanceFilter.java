/*
 * 
 */
package com.movistar.latam.colportal.log4j.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.MDC;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.util.PortalUtil;


/**
 * MultiPortalInstanceFilter:.
 * 
 * @author VASS
 * @version 1.0
 */
public class MultiPortalInstanceFilter implements Filter {

	/** _log. */
	private final Log _log = LogFactoryUtil
			.getLog(MultiPortalInstanceFilter.class);

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		_log.debug("Llamando a MultiPortalInstanceFilter.destroy()");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		try {
			Company company = PortalUtil
					.getCompany((HttpServletRequest) servletRequest);
			if (company != null) {
				MDC.put("portalInstance", company.getVirtualHostname());
			}
		} catch (PortalException | SystemException e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error obteniendo company", e);
			}
		} finally {
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) {
		_log.debug("Llamando a MultiPortalInstanceFilter.init(" + filterConfig
				+ ")");
	}

}
