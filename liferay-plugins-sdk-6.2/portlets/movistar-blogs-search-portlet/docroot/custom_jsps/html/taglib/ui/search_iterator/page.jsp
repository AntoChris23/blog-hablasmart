<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */
--%>

<%@ include file="/html/taglib/init.jsp" %>

<%
SearchContainer searchContainer = (SearchContainer)request.getAttribute("liferay-ui:search:searchContainer");

boolean paginate = GetterUtil.getBoolean((String)request.getAttribute("liferay-ui:search-iterator:paginate"));
String type = (String)request.getAttribute("liferay-ui:search:type");

String id = searchContainer.getId(request, namespace);

int end = searchContainer.getEnd();
int total = searchContainer.getTotal();
List resultRows = searchContainer.getResultRows();
List<String> headerNames = searchContainer.getHeaderNames();
List<String> normalizedHeaderNames = searchContainer.getNormalizedHeaderNames();
Map orderableHeaders = searchContainer.getOrderableHeaders();
String emptyResultsMessage = searchContainer.getEmptyResultsMessage();
RowChecker rowChecker = searchContainer.getRowChecker();

if (end > total) {
	end = total;
}

if (rowChecker != null) {
	if (headerNames != null) {
		headerNames.add(0, rowChecker.getAllRowsCheckBox());

		normalizedHeaderNames.add(0, "rowChecker");
	}
}

String url = StringPool.BLANK;

PortletURL iteratorURL = searchContainer.getIteratorURL();

if (iteratorURL != null) {
	url = iteratorURL.toString();
	url = HttpUtil.removeParameter(url, namespace + searchContainer.getOrderByColParam());
	url = HttpUtil.removeParameter(url, namespace + searchContainer.getOrderByTypeParam());
}

List<String> primaryKeys = new ArrayList<String>();

int sortColumnIndex = -1;
%>

<c:if test="<%= resultRows.isEmpty() && (emptyResultsMessage != null) %>">
	<c:if test='<%= !"blog".equals(type) %>'>
	<div class="alert alert-info">
		<%= LanguageUtil.get(pageContext, emptyResultsMessage) %>
	</div>
	</c:if>
	<c:if test='<%= "blog".equals(type) %>'>
	<div class="nat-page-result-error">
	    <div class="page-result-error__wrapper">
	        <div class="page-result-error__content">
	            <div class="description">
	                <p>Tambi&eacute;n puedes probar:</p>
	                <ul>
	                    <li>Asegur&aacute;ndote de que todas las palabras est&eacute;n escritas correctamente.</li>
	                    <li>Probando diferentes palabras clave.</li>
	                    <li>Probando palabras clave m&aacute;s generales.</li>
	                </ul>
	            </div>
	            <p class="return--home">
	            	<a id="urlSearchNotFoundBack" href="#" title="Volver a la pantalla de inicio">
	                    <svg class="return--home__icon svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 19">
	                    <title>Regresar al inicio</title>
	                    <path fill="currentColor" d="M4.47526 9.89209V17.3114C4.47526 17.4533 4.49477 17.5449 4.51265 17.5976C4.52932 17.6467 4.54435 17.6624 4.54566 17.6638C4.54612 17.6643 4.55315 17.6726 4.58183 17.6834C4.61471 17.6958 4.68346 17.7137 4.80176 17.7137H15.0216C15.1399 17.7137 15.2087 17.6958 15.2415 17.6834C15.271 17.6723 15.2775 17.664 15.2777 17.6638C15.2786 17.6629 15.2938 17.6475 15.3107 17.5976C15.3286 17.5449 15.3481 17.4533 15.3481 17.3114V9.89259H17.4247L15.3481 7.58927V2.24366H13.7656V5.8339L9.92276 1.57137L2.40104 9.89209H4.47526ZM12.7421 1.349C12.7582 1.32537 12.7763 1.30301 12.7961 1.28215C12.8934 1.17975 13.0254 1.12223 13.163 1.12223H15.9502C16.0184 1.12216 16.0859 1.13624 16.1489 1.16365C16.2119 1.19106 16.2692 1.23127 16.3174 1.28197C16.3656 1.33268 16.4039 1.3929 16.43 1.45917C16.4561 1.52545 16.4696 1.59649 16.4696 1.66824V7.15839L19.1143 10.092C19.1842 10.1694 19.231 10.2667 19.2489 10.3719C19.2668 10.477 19.2549 10.5854 19.2149 10.6835C19.1748 10.7817 19.1083 10.8653 19.0235 10.924C18.9387 10.9828 18.8394 11.0141 18.7379 11.014H16.4696V17.3114C16.4696 18.2796 15.9416 18.8351 15.0216 18.8351H4.80176C3.88173 18.8351 3.35382 18.2796 3.35382 17.3114V11.0135H1.08544C0.983887 11.0136 0.884553 10.9822 0.799755 10.9234C0.714957 10.8646 0.648422 10.7809 0.608398 10.6827C0.568375 10.5845 0.556623 10.4761 0.574599 10.3709C0.592575 10.2657 0.639488 10.1684 0.709523 10.091L9.54768 0.313999C9.59616 0.260425 9.65448 0.217777 9.7191 0.188646C9.78372 0.159514 9.8533 0.144505 9.9236 0.144531C9.99405 0.144567 10.0638 0.1597 10.1285 0.189008C10.1932 0.218316 10.2515 0.261185 10.3 0.315002L12.6441 2.91518V1.66824C12.6441 1.55292 12.6788 1.44136 12.7421 1.349Z"/>
	                   </svg>Volver a la pantalla de inicio
	               </a>
	           </p>
	        </div>
	    </div>
	    <picture class="page-result-error__picture">
	        <source media="(min-width:992px)" srcset="/movistar-blogs-theme/images/background/page-result-error-desktop.png">
	        <source media="(min-width:600px)" srcset="/movistar-blogs-theme/images/background/page-result-error-tablet.png">
	        <img src="/movistar-blogs-theme/images/background/page-result-error-mobile.png" alt="page-result-error" title="Sin Resultados">
	    </picture>
	</div>
	</c:if>
</c:if>

<div class="lfr-search-container <%= resultRows.isEmpty() ? "hide" : StringPool.BLANK %>">
	<c:if test="<%= PropsValues.SEARCH_CONTAINER_SHOW_PAGINATION_TOP && (resultRows.size() > 10) && paginate %>">
		<div class="taglib-search-iterator-page-iterator-top">
			<liferay-ui:search-paginator id='<%= id + "PageIteratorTop" %>' searchContainer="<%= searchContainer %>" type="<%= type %>" />
		</div>
	</c:if>

	<div id="<%= namespace + id %>SearchContainer">
		<table class="table table-bordered table-hover table-striped">

		<c:if test="<%= headerNames != null %>">
			<thead class="table-columns">
				<tr>

				<%
				for (int i = 0; i < headerNames.size(); i++) {
					String headerName = headerNames.get(i);

					String normalizedHeaderName = null;

					if (i < normalizedHeaderNames.size()) {
						normalizedHeaderName = normalizedHeaderNames.get(i);
					}

					if (Validator.isNull(normalizedHeaderName)) {
						normalizedHeaderName = String.valueOf(i +1);
					}

					String orderKey = null;
					String orderByType = null;
					boolean orderCurrentHeader = false;

					if (orderableHeaders != null) {
						orderKey = (String)orderableHeaders.get(headerName);

						if (orderKey != null) {
							orderByType = searchContainer.getOrderByType();

							if (orderKey.equals(searchContainer.getOrderByCol())) {
								orderCurrentHeader = true;
							}
						}
					}

					String cssClass = StringPool.BLANK;

					if (headerNames.size() == 1) {
						cssClass = "only";
					}
					else if (i == 0) {
						cssClass = "table-first-header";
					}
					else if (i == (headerNames.size() - 1)) {
						cssClass = "table-last-header";
					}

					if (orderCurrentHeader) {
						cssClass += " table-sortable-column table-sorted";

						if (HtmlUtil.escapeAttribute(orderByType).equals("desc")) {
							cssClass += " table-sorted-desc";
						}

						sortColumnIndex = i;

						if (orderByType.equals("asc")) {
							orderByType = "desc";
						}
						else {
							orderByType = "asc";
						}
					}
				%>

					<th class="<%= cssClass %>" id="<%= namespace + id %>_col-<%= normalizedHeaderName %>"

						<%--

						// Minimize the width of the first column if and only if
						// it is a row checker.

						--%>

						<c:if test="<%= (rowChecker != null) && (i == 0) %>">
							width="1%"
						</c:if>
					>

						<c:if test="<%= orderKey != null %>">
							<div class="table-sort-liner">

								<%
								String orderByJS = searchContainer.getOrderByJS();
								%>

								<c:choose>
									<c:when test="<%= Validator.isNull(orderByJS) %>">

										<%
										url = HttpUtil.setParameter(url, namespace + searchContainer.getOrderByColParam(), orderKey);
										url = HttpUtil.setParameter(url, namespace + searchContainer.getOrderByTypeParam(), orderByType);
										%>

										<a href="<%= url %>">
									</c:when>
									<c:otherwise>
										<a href="<%= StringUtil.replace(orderByJS, new String[] { "orderKey", "orderByType" }, new String[] { orderKey, orderByType }) %>">
									</c:otherwise>
								</c:choose>
						</c:if>

							<%
							String headerNameValue = null;

							if ((rowChecker == null) || (i > 0)) {
								headerNameValue = LanguageUtil.get(pageContext, HtmlUtil.escape(headerName));
							}
							else {
								headerNameValue = headerName;
							}
							%>

							<c:choose>
								<c:when test="<%= Validator.isNull(headerNameValue) %>">
									<%= StringPool.NBSP %>
								</c:when>
								<c:otherwise>
									<%= headerNameValue %>
								</c:otherwise>
							</c:choose>

						<c:if test="<%= orderKey != null %>">
									<span class="table-sort-indicator"></span>
								</a>
							</div>
						</c:if>
					</th>

				<%
				}
				%>

				</tr>
			</thead>
		</c:if>

		<tbody class="table-data">

		<c:if test="<%= resultRows.isEmpty() && (emptyResultsMessage != null) %>">
			<tr>
				<td class="table-cell">
					<%= LanguageUtil.get(pageContext, emptyResultsMessage) %>
				</td>
			</tr>
		</c:if>

		<%
		boolean allRowsIsChecked = true;

		for (int i = 0; i < resultRows.size(); i++) {
			ResultRow row = (ResultRow)resultRows.get(i);

			primaryKeys.add(HtmlUtil.escape(row.getPrimaryKey()));

			request.setAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW, row);

			List entries = row.getEntries();

			boolean rowIsChecked = false;

			if (rowChecker != null) {
				rowIsChecked = rowChecker.isChecked(row.getObject());

				boolean rowIsDisabled = rowChecker.isDisabled(row.getObject());

				if (!rowIsChecked) {
					allRowsIsChecked = false;
				}

				TextSearchEntry textSearchEntry = new TextSearchEntry();

				textSearchEntry.setAlign(rowChecker.getAlign());
				textSearchEntry.setColspan(rowChecker.getColspan());
				textSearchEntry.setCssClass(rowChecker.getCssClass());
				textSearchEntry.setName(rowChecker.getRowCheckBox(request, rowIsChecked, rowIsDisabled, row.getPrimaryKey()));
				textSearchEntry.setValign(rowChecker.getValign());

				row.addSearchEntry(0, textSearchEntry);
			}

			request.setAttribute("liferay-ui:search-container-row:rowId", id.concat(StringPool.UNDERLINE.concat(row.getRowId())));

			Map<String, Object> data = row.getData();
		%>

			<tr class="<%= GetterUtil.getString(row.getClassName()) %> <%= rowIsChecked ? "info" : StringPool.BLANK %>" <%= AUIUtil.buildData(data) %>>

			<%
			for (int j = 0; j < entries.size(); j++) {
				SearchEntry entry = (SearchEntry)entries.get(j);

				String normalizedHeaderName = null;

				if ((normalizedHeaderNames != null) && (j < normalizedHeaderNames.size())) {
					normalizedHeaderName = normalizedHeaderNames.get(j);
				}

				if (Validator.isNull(normalizedHeaderName)) {
					normalizedHeaderName = String.valueOf(j + 1);
				}

				entry.setIndex(j);

				request.setAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW_ENTRY, entry);

				String columnClassName = entry.getCssClass();

				if (entries.size() == 1) {
					columnClassName += " only";
				}
				else if (j == 0) {
					columnClassName += " first";
				}
				else if ((j + 1) == entries.size()) {
					columnClassName += " last";
				}

				if (j == sortColumnIndex) {
					columnClassName += " table-sortable-column";
				}
			%>

				<td class="table-cell <%= columnClassName %>">

					<%
					entry.print(pageContext);
					%>

				</td>

			<%
			}
			%>

			</tr>

		<%
			request.removeAttribute("liferay-ui:search-container-row:rowId");
		}
		%>

		<c:if test="<%= headerNames != null %>">
			<tr class="lfr-template">

				<%
				for (int i = 0; i < headerNames.size(); i++) {
				%>

					<td class="table-cell"></td>

				<%
				}
				%>

			</tr>
		</c:if>

		</tbody>
		</table>
	</div>

	<c:if test="<%= PropsValues.SEARCH_CONTAINER_SHOW_PAGINATION_BOTTOM && paginate %>">
		<div class="taglib-search-iterator-page-iterator-bottom">
			<liferay-ui:search-paginator id='<%= id + "PageIteratorBottom" %>' searchContainer="<%= searchContainer %>" type="<%= type %>" />
		</div>
	</c:if>
</div>

<c:if test="<%= (rowChecker != null) && !resultRows.isEmpty() && Validator.isNotNull(rowChecker.getAllRowsId()) && allRowsIsChecked %>">
	<aui:script>
		document.<%= rowChecker.getFormName() %>.<%= rowChecker.getAllRowsId() %>.checked = true;
	</aui:script>
</c:if>

<c:if test="<%= Validator.isNotNull(id) %>">
	<input id="<%= namespace + id %>PrimaryKeys" name="<%= namespace + id %>PrimaryKeys" type="hidden" value="<%= StringUtil.merge(primaryKeys) %>" />

	<aui:script use="liferay-search-container">
		new Liferay.SearchContainer(
			{
				classNameHover: '<%= _CLASS_NAME_HOVER %>',
				hover: <%= searchContainer.isHover() %>,
				id: '<%= namespace + id %>',
				rowClassNameAlternate: '<%= _ROW_CLASS_NAME_ALTERNATE %>',
				rowClassNameAlternateHover: '<%= _ROW_CLASS_NAME_ALTERNATE_HOVER %>',
				rowClassNameBody: '<%= _ROW_CLASS_NAME_BODY %>',
				rowClassNameBodyHover: '<%= _ROW_CLASS_NAME_BODY %>'
			}
		).render();
	</aui:script>
</c:if>

<%!
private static final String _CLASS_NAME_HOVER = "hover";

private static final String _ROW_CLASS_NAME_ALTERNATE = "";

private static final String _ROW_CLASS_NAME_ALTERNATE_HOVER = "-hover";

private static final String _ROW_CLASS_NAME_BODY = "";
%>