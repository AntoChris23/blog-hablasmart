<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */
--%>

<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>

<%@ include file="/html/portlet/blogs/init.jsp" %>

<%!
static final boolean EDITOR_INLINE_EDITING_ENABLED = GetterUtil.getBoolean(com.liferay.portal.util.PropsUtil.get("editor.inline.editing.enabled"));
%>

<%
SearchContainer searchContainer = (SearchContainer)request.getAttribute("view_entry_content.jsp-searchContainer");

BlogsEntry entry = (BlogsEntry)request.getAttribute("view_entry_content.jsp-entry");

AssetEntry assetEntry = (AssetEntry)request.getAttribute("view_entry_content.jsp-assetEntry");

String resourceTypeColumn = "resource-type";
String resourceUrlColumn = "resource-url";
String resourceUrlTabletColumn = "resource-url-tablet";
String resourceUrlMobileColumn = "resource-url-mobile";
String resourceUrlYoutubeColumn = "resource-url-youtube";
String cssNotePageColumn = "css-note-page";
String footerContentWebIdColumn = "footer-content-web-id";

String resourceType = StringPool.BLANK;
String resourceUrl = StringPool.BLANK;
String resourceUrlTablet = StringPool.BLANK;
String resourceUrlMobile = StringPool.BLANK;
String resourceUrlYoutube = StringPool.BLANK;
String cssNotePage = StringPool.BLANK;
String footerContentWebId = StringPool.BLANK;
String contentHtml = StringPool.BLANK;
%>

<c:choose>
	<c:when test="<%= BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.VIEW) && (entry.isVisible() || (entry.getUserId() == user.getUserId()) || BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.UPDATE)) %>">
		<section class="nat-note-page" id="<portlet:namespace /><%= entry.getEntryId() %>">
		<div class="container">
			<%
				String strutsAction = ParamUtil.getString(request, "struts_action");
			%>
			<c:if test='<%= strutsAction.equals("/blogs/view_entry") %>'>
			<h1 class="note-page__title"><%= StringUtil.shorten(entry.getTitle(), 105) %></h1>
			<%
				//Leyendo Expandos
				if(entry.getExpandoBridge().hasAttribute(resourceTypeColumn)){
					String arrayResorType[] = (String[])entry.getExpandoBridge().getAttribute(resourceTypeColumn);
					if(arrayResorType.length > 0){
						resourceType = arrayResorType[0];
					}
				}
				if(entry.getExpandoBridge().hasAttribute(resourceUrlColumn)){
					resourceUrl = (String) entry.getExpandoBridge().getAttribute(resourceUrlColumn);
				}
				if(entry.getExpandoBridge().hasAttribute(resourceUrlTabletColumn)){
					resourceUrlTablet = (String) entry.getExpandoBridge().getAttribute(resourceUrlTabletColumn);
					if(Validator.isBlank(resourceUrlTablet))
						resourceUrlTablet = resourceUrl;
				}
				if(entry.getExpandoBridge().hasAttribute(resourceUrlMobileColumn)){
					resourceUrlMobile = (String) entry.getExpandoBridge().getAttribute(resourceUrlMobileColumn);
					if(Validator.isBlank(resourceUrlMobile))
						resourceUrlMobile = resourceUrl;
				}
				if(entry.getExpandoBridge().hasAttribute(resourceUrlYoutubeColumn)){
					resourceUrlYoutube = (String) entry.getExpandoBridge().getAttribute(resourceUrlYoutubeColumn);
				}
				if(entry.getExpandoBridge().hasAttribute(cssNotePageColumn)){
					cssNotePage = (String) entry.getExpandoBridge().getAttribute(cssNotePageColumn);
				}
				if(entry.getExpandoBridge().hasAttribute(footerContentWebIdColumn)){
					footerContentWebId = (String) entry.getExpandoBridge().getAttribute(footerContentWebIdColumn);
				}
				//Contenido Web
				long jaGroup = themeDisplay.getScopeGroupId();
				if (themeDisplay.getScopeGroup().hasStagingGroup()) {
					jaGroup = themeDisplay.getScopeGroup().getStagingGroup().getGroupId();
				}
				if(!Validator.isBlank(footerContentWebId)){
					try{
						JournalArticle journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(jaGroup, footerContentWebId);
						contentHtml = JournalContentUtil.getContent(journalArticle.getGroupId(), journalArticle.getArticleId(), WebKeys.WINDOW_STATE, themeDisplay.getLanguageId(), themeDisplay);
					}catch(Exception ex){
						contentHtml = StringPool.BLANK;
					}
				}
			%>
			</c:if>
			<div class="entry-content">
				<c:if test="<%= !entry.isApproved() %>">
					<h3>
						<liferay-ui:message key='<%= entry.isPending() ? "pending-approval" : WorkflowConstants.getStatusLabel(entry.getStatus()) %>' />
					</h3>
				</c:if>

				<portlet:renderURL var="viewEntryURL">
					<portlet:param name="struts_action" value="/blogs/view_entry" />
					<portlet:param name="redirect" value="<%= currentURL %>" />
					<portlet:param name="urlTitle" value="<%= entry.getUrlTitle() %>" />
				</portlet:renderURL>

				<c:if test='<%= !strutsAction.equals("/blogs/view_entry") %>'>
					<div class="entry-title">
						<h2><aui:a href="<%= viewEntryURL %>"><%= HtmlUtil.escape(entry.getTitle()) %></aui:a></h2>
					</div>
					
					<div class="entry-date">
						<span class="hide-accessible"><liferay-ui:message key="published-date" /></span>
	
						<%= dateFormatDateTime.format(entry.getDisplayDate()) %>
					</div>
				</c:if>
			</div>
			<portlet:renderURL var="bookmarkURL" windowState="<%= WindowState.NORMAL.toString() %>">
				<portlet:param name="struts_action" value="/blogs/view_entry" />
				<portlet:param name="urlTitle" value="<%= entry.getUrlTitle() %>" />
			</portlet:renderURL>

			<c:if test='<%= enableSocialBookmarks && socialBookmarksDisplayPosition.equals("top") %>'>
				<liferay-ui:social-bookmarks
					displayStyle="<%= socialBookmarksDisplayStyle %>"
					target="_blank"
					title="<%= entry.getTitle() %>"
					types="<%= socialBookmarksTypes %>"
					url="<%= PortalUtil.getCanonicalURL(bookmarkURL.toString(), themeDisplay, layout) %>"
				/>
			</c:if>
			<c:if test="<%= BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.DELETE) || BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.PERMISSIONS) || BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.UPDATE) %>">
				<ul class="edit-actions entry icons-container lfr-meta-actions">
					<c:if test="<%= BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.UPDATE) %>">
						<li class="edit-entry">
							<portlet:renderURL var="editEntryURL">
								<portlet:param name="struts_action" value="/blogs/edit_entry" />
								<portlet:param name="redirect" value="<%= currentURL %>" />
								<portlet:param name="backURL" value="<%= currentURL %>" />
								<portlet:param name="entryId" value="<%= String.valueOf(entry.getEntryId()) %>" />
							</portlet:renderURL>

							<liferay-ui:icon
								image="edit"
								label="<%= true %>"
								url="<%= editEntryURL %>"
							/>
						</li>
					</c:if>

					<c:if test="<%= showEditEntryPermissions && BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.PERMISSIONS) %>">
						<li class="edit-entry-permissions">
							<liferay-security:permissionsURL
								modelResource="<%= BlogsEntry.class.getName() %>"
								modelResourceDescription="<%= entry.getTitle() %>"
								resourceGroupId="<%= String.valueOf(entry.getGroupId()) %>"
								resourcePrimKey="<%= String.valueOf(entry.getEntryId()) %>"
								var="permissionsEntryURL"
								windowState="<%= LiferayWindowState.POP_UP.toString() %>"
							/>

							<liferay-ui:icon
								image="permissions"
								label="<%= true %>"
								method="get"
								url="<%= permissionsEntryURL %>"
								useDialog="<%= true %>"
							/>
						</li>
					</c:if>

					<c:if test="<%= BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.DELETE) %>">
						<li class="delete-entry">
							<portlet:renderURL var="viewURL">
								<portlet:param name="struts_action" value="/blogs/view" />
							</portlet:renderURL>

							<portlet:actionURL var="deleteEntryURL">
								<portlet:param name="struts_action" value="/blogs/edit_entry" />
								<portlet:param name="<%= Constants.CMD %>" value="<%= TrashUtil.isTrashEnabled(scopeGroupId) ? Constants.MOVE_TO_TRASH : Constants.DELETE %>" />
								<portlet:param name="redirect" value="<%= viewURL %>" />
								<portlet:param name="entryId" value="<%= String.valueOf(entry.getEntryId()) %>" />
							</portlet:actionURL>

							<liferay-ui:icon-delete
								label="<%= true %>"
								trash="<%= TrashUtil.isTrashEnabled(scopeGroupId) %>"
								url="<%= deleteEntryURL %>"
							/>
						</li>
					</c:if>
				</ul>
			</c:if>

			<div class="note-page__box <%=cssNotePage %>">
				<c:choose>
					<c:when test='<%= displayStyle.equals(BlogsUtil.DISPLAY_STYLE_ABSTRACT) && !strutsAction.equals("/blogs/view_entry") %>'>
						<c:if test="<%= entry.isSmallImage() %>">
							<div class="asset-small-image">
								<img alt="" class="asset-small-image" src="<%= HtmlUtil.escape(entry.getEntryImageURL(themeDisplay)) %>" width="150" />
							</div>
						</c:if>

						<%
						String summary = entry.getDescription();

						if (Validator.isNull(summary)) {
							summary = entry.getContent();
						}
						%>

						<%= StringUtil.shorten(HtmlUtil.stripHtml(summary), pageAbstractLength) %>

						<br />

						<aui:a href="<%= viewEntryURL %>"><liferay-ui:message arguments='<%= new Object[] {"hide-accessible", HtmlUtil.escape(entry.getTitle())} %>' key="read-more-x-about-x" /> &raquo;</aui:a>
					</c:when>
					<c:when test='<%= displayStyle.equals(BlogsUtil.DISPLAY_STYLE_FULL_CONTENT) || strutsAction.equals("/blogs/view_entry") %>'>

						<%
						String entryContentId = "blogs-entry-content-" + entry.getEntryId();

						boolean inlineEditEnabled = PropsValues.EDITOR_INLINE_EDITING_ENABLED && BlogsEntryPermission.contains(permissionChecker, entry, ActionKeys.UPDATE) && BrowserSnifferUtil.isRtf(request) && !WorkflowDefinitionLinkLocalServiceUtil.hasWorkflowDefinitionLink(themeDisplay.getCompanyId(), scopeGroupId, BlogsEntry.class.getName());
						//Validando HTML de resources
						if(strutsAction.equals("/blogs/view_entry")){
							if(!Validator.isBlank(resourceUrl) && Validator.equals("Imagen", resourceType)) {
						%>
						<div class="box__image">
			                <div class="image__media">
			                    <picture class="media__picture">
			                        <source media="(min-width:1024px)" srcset="<%= resourceUrl %>">
			                        <source media="(min-width:600px)" srcset="<%= resourceUrlTablet %>">
			                        <img src="<%= resourceUrlMobile %>" alt="<%= entry.getTitle() %>" title="<%= entry.getTitle() %>">
			                    </picture>
			                </div>
			            </div>
						<% 	} else if(!Validator.isBlank(resourceUrl) && Validator.equals("Video", resourceType)) {%>
						<div class="box__image">
			                <a class="image__media image__media--video modal-trigger" data-videourl="<%= resourceUrlYoutube %>" href="#modalSearchVideo">
			                    <picture class="media__picture">
			                        <source media="(min-width:1024px)" srcset="<%= resourceUrl %>">
			                        <source media="(min-width:600px)" srcset="<%= resourceUrlTablet %>">
			                        <img src="<%= resourceUrlMobile %>" alt="<%= entry.getTitle() %>" title="<%= entry.getTitle() %>">
			                    </picture>
			                    <span class="media__play">
			                        <svg class="svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23 25">
			                        <title>Play</title>
			                        <path fill="currentColor" d="M0.760986 2.48024C0.760986 0.950588 2.4083 -0.012839 3.74151 0.737091L21.3437 10.6383C22.703 11.403 22.703 13.36 21.3437 14.1246L3.74151 24.0259C2.4083 24.7758 0.760986 23.8124 0.760986 22.2827V2.48024Z"/>
			                        </svg>
			                    </span>
			                </a>
			            </div>
						<% 	} 
						}
						%>

						<div id="<%= entryContentId %>" <%= (inlineEditEnabled && Validator.equals(GetterUtil.getString(SessionClicks.get(request, "liferay_toggle_controls", "visible")), "visible")) ? "class=\"lfr-editable\" contenteditable=\"true\" spellcheck=\"false\"" : "class=\"box__description\"" %>>
							<%= entry.getContent() %>
						</div>
						<c:if test='<%= !strutsAction.equals("/blogs/view_entry") %>'>
						<liferay-ui:custom-attributes-available className="<%= BlogsEntry.class.getName() %>">
							<liferay-ui:custom-attribute-list
								className="<%= BlogsEntry.class.getName() %>"
								classPK="<%= entry.getEntryId() %>"
								editable="<%= false %>"
								label="<%= true %>"
							/>
						</liferay-ui:custom-attributes-available>
						</c:if>
						<c:if test="<%= inlineEditEnabled %>">
							<portlet:actionURL var="updateEntryContent">
								<portlet:param name="struts_action" value="/blogs/edit_entry" />
								<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.UPDATE_CONTENT %>" />
								<portlet:param name="entryId" value="<%= String.valueOf(entry.getEntryId()) %>" />
							</portlet:actionURL>

							<liferay-ui:input-editor
								editorImpl="ckeditor"
								inlineEdit="<%= true %>"
								inlineEditSaveURL="<%= updateEntryContent %>"
								name="<%= entryContentId %>"
							/>
						</c:if>
					</c:when>
					<c:when test='<%= displayStyle.equals(BlogsUtil.DISPLAY_STYLE_TITLE) && !strutsAction.equals("/blogs/view_entry") %>'>
						<aui:a href="<%= viewEntryURL %>"><liferay-ui:message arguments='<%= new Object[] {"hide-accessible", HtmlUtil.escape(entry.getTitle())} %>' key="read-more-x-about-x" /> &raquo;</aui:a>
					</c:when>
				</c:choose>
			</div>
			<c:if test="<%= false %>">
			<div class="entry-footer">
				<div class="entry-author">
					<liferay-ui:message key="written-by" /> <%= HtmlUtil.escape(PortalUtil.getUserName(entry)) %>
				</div>

				<div class="stats">
					<c:if test="<%= assetEntry != null %>">
						<span class="view-count">
							<c:choose>
								<c:when test="<%= assetEntry.getViewCount() == 1 %>">
									<%= assetEntry.getViewCount() %> <liferay-ui:message key="view" />,
								</c:when>
								<c:when test="<%= assetEntry.getViewCount() > 1 %>">
									<%= assetEntry.getViewCount() %> <liferay-ui:message key="views" />,
								</c:when>
							</c:choose>
						</span>
					</c:if>

					<c:if test="<%= enableComments %>">
						<span class="comments">

							<%
							long classNameId = PortalUtil.getClassNameId(BlogsEntry.class.getName());

							int messagesCount = MBMessageLocalServiceUtil.getDiscussionMessagesCount(classNameId, entry.getEntryId(), WorkflowConstants.STATUS_APPROVED);
							%>

							<c:choose>
								<c:when test='<%= strutsAction.equals("/blogs/view_entry") %>'>
									<%= messagesCount %> <liferay-ui:message key='<%= (messagesCount == 1) ? "comment" : "comments" %>' />
								</c:when>
								<c:otherwise>
									<aui:a href='<%= PropsValues.PORTLET_URL_ANCHOR_ENABLE ? viewEntryURL : viewEntryURL + StringPool.POUND + "blogsCommentsPanelContainer" %>'><%= messagesCount %> <liferay-ui:message key='<%= (messagesCount == 1) ? "comment" : "comments" %>' /></aui:a>
								</c:otherwise>
							</c:choose>
						</span>
					</c:if>
				</div>

				<c:if test="<%= enableFlags %>">
					<liferay-ui:flags
						className="<%= BlogsEntry.class.getName() %>"
						classPK="<%= entry.getEntryId() %>"
						contentTitle="<%= entry.getTitle() %>"
						reportedUserId="<%= entry.getUserId() %>"
					/>
				</c:if>

				<span class="entry-categories">
					<liferay-ui:asset-categories-summary
						className="<%= BlogsEntry.class.getName() %>"
						classPK="<%= entry.getEntryId() %>"
						portletURL="<%= renderResponse.createRenderURL() %>"
					/>
				</span>

				<span class="entry-tags">
					<liferay-ui:asset-tags-summary
						className="<%= BlogsEntry.class.getName() %>"
						classPK="<%= entry.getEntryId() %>"
						portletURL="<%= renderResponse.createRenderURL() %>"
					/>
				</span>

				<c:if test='<%= displayStyle.equals(BlogsUtil.DISPLAY_STYLE_FULL_CONTENT) || strutsAction.equals("/blogs/view_entry") %>'>
					<c:if test="<%= enableRelatedAssets %>">
						<div class="entry-links">
							<liferay-ui:asset-links
								assetEntryId="<%= (assetEntry != null) ? assetEntry.getEntryId() : 0 %>"
								className="<%= BlogsEntry.class.getName() %>"
								classPK="<%= entry.getEntryId() %>"
							/>
						</div>
					</c:if>

					<c:if test='<%= enableSocialBookmarks && socialBookmarksDisplayPosition.equals("bottom") %>'>
						<liferay-ui:social-bookmarks
							contentId="<%= String.valueOf(entry.getEntryId()) %>"
							displayStyle="<%= socialBookmarksDisplayStyle %>"
							target="_blank"
							title="<%= entry.getTitle() %>"
							types="<%= socialBookmarksTypes %>"
							url="<%= PortalUtil.getCanonicalURL(bookmarkURL.toString(), themeDisplay, layout) %>"
						/>
					</c:if>

					<c:if test="<%= enableRatings %>">
						<liferay-ui:ratings
							className="<%= BlogsEntry.class.getName() %>"
							classPK="<%= entry.getEntryId() %>"
						/>
					</c:if>
				</c:if>
			</div>
			</c:if>
		</div>
		</section>
		<c:if test='<%= !Validator.isBlank(contentHtml) %>'>
			<%= contentHtml %>
		</c:if>
	</c:when>
	<c:otherwise>

		<%
		if (searchContainer != null) {
			searchContainer.setTotal(searchContainer.getTotal() - 1);
		}
		%>

	</c:otherwise>
</c:choose>