package com.movistar.site.blogs.view;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.service.AssetEntryServiceUtil;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.blogs.service.BlogsEntryLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.journalcontent.util.JournalContentUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.movistar.site.blogs.view.util.ViewBlogPreferencesUtil;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

/**
 * Portlet implementation class ViewBlogPortlet
 */
public class ViewBlogPortlet extends MVCPortlet {
 
	/** _log. */
	private static Log _log = LogFactoryUtil.getLog(ViewBlogPortlet.class);
	
	private final String resourceTypeColumn = "resource-type";
	private final String resourceUrlColumn = "resource-url";
	private final String resourceUrlTabletColumn = "resource-url-tablet";
	private final String resourceUrlMobileColumn = "resource-url-mobile";
	private final String resourceUrlYoutubeColumn = "resource-url-youtube";
	private final String cssNotePageColumn = "css-note-page";
	private final String footerContentWebIdColumn = "footer-content-web-id";
	
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String sBlogId = ViewBlogPreferencesUtil.getBlogId(renderRequest.getPreferences());
		BlogsEntry blogsEntry = null;
		try {
			long blogId = Long.parseLong(sBlogId);
			blogsEntry = BlogsEntryLocalServiceUtil.getBlogsEntry(blogId);
			
			ServiceContext serviceContext = ServiceContextFactory.getInstance(renderRequest);
			long scopeGroupId = serviceContext.getScopeGroupId();
			
			if(blogsEntry.getGroupId() != scopeGroupId){
				blogsEntry = BlogsEntryLocalServiceUtil.getBlogsEntryByUuidAndGroupId(blogsEntry.getUuid(), scopeGroupId);
			}
			
			AssetEntryServiceUtil.incrementViewCounter(BlogsEntry.class.getName(), blogsEntry.getEntryId());
			String resourceType = StringPool.BLANK;
			String resourceUrl = StringPool.BLANK;
			String resourceUrlTablet = StringPool.BLANK;
			String resourceUrlMobile = StringPool.BLANK;
			String resourceUrlYoutube = StringPool.BLANK;
			String cssNotePage = StringPool.BLANK;
			String footerContentWebId = StringPool.BLANK;
			String contentHtml = StringPool.BLANK;
			
			if(blogsEntry.getExpandoBridge().hasAttribute(resourceTypeColumn)){
				String arrayResorType[] = (String[])blogsEntry.getExpandoBridge().getAttribute(resourceTypeColumn);
				if(arrayResorType.length > 0){
					resourceType = arrayResorType[0];
				}
			}
			if(blogsEntry.getExpandoBridge().hasAttribute(resourceUrlColumn)){
				resourceUrl = (String) blogsEntry.getExpandoBridge().getAttribute(resourceUrlColumn);
			}
			if(blogsEntry.getExpandoBridge().hasAttribute(resourceUrlTabletColumn)){
				resourceUrlTablet = (String) blogsEntry.getExpandoBridge().getAttribute(resourceUrlTabletColumn);
				if(Validator.isBlank(resourceUrlTablet))
					resourceUrlTablet = resourceUrl;
			}
			if(blogsEntry.getExpandoBridge().hasAttribute(resourceUrlMobileColumn)){
				resourceUrlMobile = (String) blogsEntry.getExpandoBridge().getAttribute(resourceUrlMobileColumn);
				if(Validator.isBlank(resourceUrlMobile))
					resourceUrlMobile = resourceUrl;
			}
			if(blogsEntry.getExpandoBridge().hasAttribute(resourceUrlYoutubeColumn)){
				resourceUrlYoutube = (String) blogsEntry.getExpandoBridge().getAttribute(resourceUrlYoutubeColumn);
			}
			if(blogsEntry.getExpandoBridge().hasAttribute(cssNotePageColumn)){
				cssNotePage = (String) blogsEntry.getExpandoBridge().getAttribute(cssNotePageColumn);
			}
			if(blogsEntry.getExpandoBridge().hasAttribute(footerContentWebIdColumn)){
				footerContentWebId = (String) blogsEntry.getExpandoBridge().getAttribute(footerContentWebIdColumn);
				ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
				long jaGroup = themeDisplay.getScopeGroupId();
				if (themeDisplay.getScopeGroup().hasStagingGroup()) {
					jaGroup = themeDisplay.getScopeGroup().getStagingGroup().getGroupId();
				}
				if(!Validator.isBlank(footerContentWebId)){
					try{
						JournalArticle journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(jaGroup, footerContentWebId);
						contentHtml = JournalContentUtil.getContent(journalArticle.getGroupId(), journalArticle.getArticleId(), WebKeys.WINDOW_STATE, themeDisplay.getLanguageId(), themeDisplay);
					}catch(Exception ex){
						contentHtml = StringPool.BLANK;
					}
				}
			}
			
			renderRequest.setAttribute("resourceType", resourceType);
			renderRequest.setAttribute("resourceUrl", resourceUrl);
			renderRequest.setAttribute("resourceUrlTablet", resourceUrlTablet);
			renderRequest.setAttribute("resourceUrlMobile", resourceUrlMobile);
			renderRequest.setAttribute("resourceUrlYoutube", resourceUrlYoutube);
			renderRequest.setAttribute("cssNotePage", cssNotePage);
			renderRequest.setAttribute("contentHtml", contentHtml);
		} catch (PortalException ex) {
			_log.error("doView - PortalException: " + ex.getMessage());
		} catch (SystemException ex) {
			_log.error("doView - SystemException: " + ex.getMessage());
		} catch (NumberFormatException ex) {
			_log.error("doView - NumberFormatException: " + ex.getMessage());
		}
		
		
		renderRequest.setAttribute("blogsEntry", blogsEntry);
		super.doView(renderRequest, renderResponse);
	}
}
