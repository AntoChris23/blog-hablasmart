package com.movistar.site.blogs.view.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

import javax.portlet.PortletPreferences;

public class ViewBlogPreferencesUtil {
	/** _log. */
	private static Log _log = LogFactoryUtil.getLog(ViewBlogPreferencesUtil.class);
	
	/**
	 * Obtiene rest url.
	 * 
	 * @param preferences
	 *            preferences
	 * @return Rest url
	 */
	public static String getBlogId(PortletPreferences preferences) {
		String default_ = StringPool.BLANK;
		return preferences.getValue("blogId", default_);
	}
}
