<%@ include file="/html/init.jsp" %>
<%
String currentURL = PortalUtil.getCurrentURL(request);
Group group = themeDisplay.getScopeGroup();
boolean isWindowStateNormal = portletRequest.getWindowState().equals(WindowState.NORMAL);

String keywords = ParamUtil.getString(request, "keywords");
long groupId = ParamUtil.getLong(request, "groupId");

if(!isWindowStateNormal) {
	String redirect = ParamUtil.getString(request, "redirect");
	
	if (Validator.isNotNull(redirect)) {
		portletDisplay.setURLBack(redirect);
	}
	
	PortletURL portletURL = PortletURLUtil.getCurrent(renderRequest, renderResponse);

	request.setAttribute("blogs.search.view.jsp-portletURL", portletURL);
	request.setAttribute("blogs.search.view.jsp-returnToFullPageURL", portletDisplay.getURLBack());
%>

<%@ include file="/html/search/main_search.jspf" %>

<%} %>
<liferay-portlet:renderURL varImpl="portletURL" windowState="maximized">
	<portlet:param name="mvcPath" value="/html/search/view.jsp"/>
	<portlet:param name="redirect" value="<%= currentURL %>"/>
</liferay-portlet:renderURL>
<form action="<%= portletURL.toString() %>" method="get" name="<portlet:namespace />fm">
	<liferay-portlet:renderURLParams varImpl="portletURL" />
	<input name="<portlet:namespace />groupId" type="hidden" value="<%= group.getGroupId() %>" />
	<input name="keywords" type="hidden" value="<%= HtmlUtil.escapeAttribute(keywords) %>" />
</form>
<aui:script>
	function doSearch(e){
		if(e.which == 10 || e.which == 13) {
			initSearch();
			return false;
		}
		return true;
	}
	
	function initSearch() {
		var inputSearch = document.getElementById('inputSearch').value;
		inputSearch = inputSearch.replace(/^\s+|\s+$/, '');
		if (inputSearch != '') {
			document.<portlet:namespace />fm.keywords.value = inputSearch;
			submitForm(document.<portlet:namespace />fm);
		}
	}
	<% if(!isWindowStateNormal) {%>
	AUI().ready( function() {
		document.getElementById('inputSearch').value = document.<portlet:namespace />fm.keywords.value;
		var urlSearchNotFoundBack = document.getElementById('urlSearchNotFoundBack');
		if(urlSearchNotFoundBack != null){
			urlSearchNotFoundBack.href = document.getElementById('urlHomeLink').href;
		}
	});
	<% } %>
</aui:script>
