<%@ include file="/html/init.jsp" %>
<%
PortletURL currentURLObj = PortletURLUtil.getCurrent(liferayPortletRequest, liferayPortletResponse);
String currentURL = currentURLObj.toString();

boolean viewInContext = true;

ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

Document document = (Document)row.getObject();

String className = document.get(Field.ENTRY_CLASS_NAME);

String resourceTypeColumn = "resource-type";
String resourceUrlColumn = "resource-url";
String resourceUrlTabletColumn = "resource-url-tablet";
String resourceUrlMobileColumn = "resource-url-mobile";
String resourceUrlYoutubeColumn = "resource-url-youtube";
String pageUrlLayoutColumn = "page-url-layout";

String resourceType = StringPool.BLANK;
String resourceUrl = StringPool.BLANK;
String resourceUrlTablet = StringPool.BLANK;
String resourceUrlMobile = StringPool.BLANK;
String resourceUrlYoutube = StringPool.BLANK;
String pageUrlLayout = StringPool.BLANK;

String entryTitle = null;
String entrySummary = null;
String downloadURL = null;
String returnToFullPageURL = (String)request.getAttribute("blogs.search.view.jsp-returnToFullPageURL");
PortletURL viewFullContentURL = null;
String viewURL = null;

AssetRendererFactory assetRendererFactory = AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(className);

AssetRenderer assetRenderer = null;

boolean inheritRedirect = false;

if (assetRendererFactory != null) {
	long classPK = GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));

	long resourcePrimKey = GetterUtil.getLong(document.get(Field.ROOT_ENTRY_CLASS_PK));

	if (resourcePrimKey > 0) {
		classPK = resourcePrimKey;
	}
	
	AssetEntry assetEntry = AssetEntryLocalServiceUtil.getEntry(className, classPK);

	assetRenderer = assetRendererFactory.getAssetRenderer(classPK);
	
	if(BlogsEntry.class.getName().equals(assetRenderer.getClassName())){
		BlogsEntry blogsEntry = BlogsEntryLocalServiceUtil.getBlogsEntry(assetEntry.getClassPK());
		if(blogsEntry.getExpandoBridge().hasAttribute(resourceTypeColumn)){
			String arrayResorType[] = (String[])blogsEntry.getExpandoBridge().getAttribute(resourceTypeColumn);
			if(arrayResorType.length > 0){
				resourceType = arrayResorType[0];
			}
		}
		if(blogsEntry.getExpandoBridge().hasAttribute(resourceUrlColumn)){
			resourceUrl = (String) blogsEntry.getExpandoBridge().getAttribute(resourceUrlColumn);
		}
		if(blogsEntry.getExpandoBridge().hasAttribute(resourceUrlTabletColumn)){
			resourceUrlTablet = (String) blogsEntry.getExpandoBridge().getAttribute(resourceUrlTabletColumn);
			if(Validator.isBlank(resourceUrlTablet))
				resourceUrlTablet = resourceUrl;
		}
		if(blogsEntry.getExpandoBridge().hasAttribute(resourceUrlMobileColumn)){
			resourceUrlMobile = (String) blogsEntry.getExpandoBridge().getAttribute(resourceUrlMobileColumn);
			if(Validator.isBlank(resourceUrlMobile))
				resourceUrlMobile = resourceUrl;
		}
		if(blogsEntry.getExpandoBridge().hasAttribute(resourceUrlYoutubeColumn)){
			resourceUrlYoutube = (String) blogsEntry.getExpandoBridge().getAttribute(resourceUrlYoutubeColumn);
		}
		if(blogsEntry.getExpandoBridge().hasAttribute(pageUrlLayoutColumn)){
			pageUrlLayout = (String) blogsEntry.getExpandoBridge().getAttribute(pageUrlLayoutColumn);
		}
	}else if(JournalArticle.class.getName().equals(assetRenderer.getClassName())){
		JournalArticleResource journalArticleResourceObj = JournalArticleResourceLocalServiceUtil.getJournalArticleResource(assetEntry.getClassPK());
		JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(assetRenderer.getGroupId(), journalArticleResourceObj.getArticleId());
		if(journalArticle.getExpandoBridge().hasAttribute(resourceTypeColumn)){
			String arrayResorType[] = (String[])journalArticle.getExpandoBridge().getAttribute(resourceTypeColumn);
			if(arrayResorType.length > 0){
				resourceType = arrayResorType[0];
			}
		}
		if(journalArticle.getExpandoBridge().hasAttribute(resourceUrlColumn)){
			resourceUrl = (String) journalArticle.getExpandoBridge().getAttribute(resourceUrlColumn);
		}
		if(journalArticle.getExpandoBridge().hasAttribute(resourceUrlTabletColumn)){
			resourceUrlTablet = (String) journalArticle.getExpandoBridge().getAttribute(resourceUrlTabletColumn);
			if(Validator.isBlank(resourceUrlTablet))
				resourceUrlTablet = resourceUrl;
		}
		if(journalArticle.getExpandoBridge().hasAttribute(resourceUrlMobileColumn)){
			resourceUrlMobile = (String) journalArticle.getExpandoBridge().getAttribute(resourceUrlMobileColumn);
			if(Validator.isBlank(resourceUrlMobile))
				resourceUrlMobile = resourceUrl;
		}
		if(journalArticle.getExpandoBridge().hasAttribute(resourceUrlYoutubeColumn)){
			resourceUrlYoutube = (String) journalArticle.getExpandoBridge().getAttribute(resourceUrlYoutubeColumn);
		}
	}
	downloadURL = assetRenderer.getURLDownload(themeDisplay);
	System.out.println("currentURL: " + currentURL);
	String pathBase = currentURL.split(themeDisplay.getLayout().getFriendlyURL() + "\\?")[0];
	System.out.println("pathBase: " + pathBase);
	viewURL = pathBase + pageUrlLayout;
	
	viewFullContentURL = _getViewFullContentURL(request, themeDisplay, PortletKeys.ASSET_PUBLISHER, document);

	viewFullContentURL.setParameter("struts_action", "/asset_publisher/view_content");

	if (Validator.isNotNull(returnToFullPageURL)) {
		viewFullContentURL.setParameter("returnToFullPageURL", returnToFullPageURL);
	}

	viewFullContentURL.setParameter("assetEntryId", String.valueOf(assetEntry.getEntryId()));
	viewFullContentURL.setParameter("type", assetRendererFactory.getType());

	if (Validator.isNotNull(assetRenderer.getUrlTitle())) {
		if ((assetRenderer.getGroupId() > 0) && (assetRenderer.getGroupId() != scopeGroupId)) {
			viewFullContentURL.setParameter("groupId", String.valueOf(assetRenderer.getGroupId()));
		}

		viewFullContentURL.setParameter("urlTitle", assetRenderer.getUrlTitle());
	}

	if (viewInContext || !assetEntry.isVisible()) {
		inheritRedirect = true;

		String viewFullContentURLString = viewFullContentURL.toString();

		viewFullContentURLString = HttpUtil.setParameter(viewFullContentURLString, "redirect", currentURL);

		//viewURL = assetRenderer.getURLViewInContext(liferayPortletRequest, liferayPortletResponse, viewFullContentURLString);
	}
	else {
		//viewURL = viewFullContentURL.toString();
	}
	
} else {
	String portletId = document.get(Field.PORTLET_ID);

	viewFullContentURL = _getViewFullContentURL(request, themeDisplay, portletId, document);

	if (Validator.isNotNull(returnToFullPageURL)) {
		viewFullContentURL.setParameter("returnToFullPageURL", returnToFullPageURL);
	}

	viewURL = viewFullContentURL.toString();
}

Indexer indexer = IndexerRegistryUtil.getIndexer(className);

Summary summary = null;

if (indexer != null) {
	String snippet = document.get(Field.SNIPPET);

	summary = indexer.getSummary(document, locale, snippet, viewFullContentURL);

	entryTitle = summary.getTitle();
	entrySummary = summary.getContent();
}
else if (assetRenderer != null) {
	entryTitle = assetRenderer.getTitle(locale);
	entrySummary = assetRenderer.getSearchSummary(locale);
}

if ((assetRendererFactory == null) && viewInContext) {
	viewURL = viewFullContentURL.toString();
}

//viewURL = _checkViewURL(themeDisplay, viewURL, currentURL, inheritRedirect);

String[] queryTerms = (String[])request.getAttribute("blogs.search.view.jsp-queryTerms");

PortletURL portletURL = (PortletURL)request.getAttribute("blogs.search.view.jsp-portletURL");
%>

<div class="nat-page-result__item">
  <div class="cardBox cardBox--result">
  	<% if(!Validator.isBlank(resourceUrl) && Validator.equals("Imagen", resourceType)) {%>
  	<div class="cardBox__image">
        <a class="image__media" href="<%= viewURL %>" target="_blank" tabindex="0" title="<%= entryTitle %>">
            <picture class="media__picture">
                <source media="(min-width:1024px)" srcset="<%= resourceUrl %>">
                <source media="(min-width:600px)" srcset="<%= resourceUrlTablet %>">
                <img class="media__thumbnail" src="<%= resourceUrlMobile %>" alt="<%= entryTitle %>" title="<%= entryTitle %>">
            </picture>
        </a>
    </div>
    <% } else if(!Validator.isBlank(resourceUrl) && Validator.equals("Video", resourceType)) {%>
    <div class="cardBox__image">
        <a class="image__media image__media--video modal-trigger" data-videourl="<%= resourceUrlYoutube %>" href="#modalSearchVideo" tabindex="0" title="<%= entryTitle %>">
            <picture class="media__picture">
                <source media="(min-width:1024px)" srcset="<%= resourceUrl %>">
                <source media="(min-width:600px)" srcset="<%= resourceUrlTablet %>">
                <img class="media__thumbnail" src="<%= resourceUrlMobile %>" alt="<%= entryTitle %>" title="<%= entryTitle %>">
            </picture>
            <span class="media__play">
                <svg class="svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23 25">
                    <title>Play</title>
                    <path fill="currentColor" d="M0.760986 2.48024C0.760986 0.950588 2.4083 -0.012839 3.74151 0.737091L21.3437 10.6383C22.703 11.403 22.703 13.36 21.3437 14.1246L3.74151 24.0259C2.4083 24.7758 0.760986 23.8124 0.760986 22.2827V2.48024Z"/>
                </svg>
            </span>
        </a>
    </div>
    <% } %>
	<div class="cardBox__content">
	<%
	String[] assetCategoryIds = document.getValues(Field.ASSET_CATEGORY_IDS);
	%>
        <div class="cardBox__info">
            <span class="info__date">
                <fmt:formatDate value="<%= assetRenderer.getDisplayDate() %>" type="date" pattern="MMMM dd, yyyy"/>
            </span>
            <c:if test="<%= Validator.isNotNull(assetCategoryIds[0]) %>">
                <%
                Locale assetCategoryLocale = locale;

                if (locale != summary.getLocale()) {
                    assetCategoryLocale = summary.getLocale();
                }

                for (int i = 0; i < assetCategoryIds.length; i++) {
                    long assetCategoryId = GetterUtil.getLong(assetCategoryIds[i]);

                    AssetCategory assetCategory = null;

                    try {
                        assetCategory = AssetCategoryLocalServiceUtil.getCategory(assetCategoryId);
                    }
                    catch (NoSuchCategoryException nsce) {
                    }

                    if ((assetCategory == null) || !permissionChecker.hasPermission(assetCategory.getGroupId(), assetCategory.getModelClassName(), assetCategory.getPrimaryKey(), ActionKeys.VIEW)) {
                        continue;
                    }

                    AssetVocabulary assetVocabulary = AssetVocabularyLocalServiceUtil.getVocabulary(assetCategory.getVocabularyId());

                    PortletURL categoryURL = PortletURLUtil.clone(portletURL, renderResponse);

                    categoryURL.setParameter(Field.ASSET_CATEGORY_IDS, String.valueOf(assetCategory.getCategoryId()));
                %>
                    <a class="info__category" href="<%= assetCategory.getDescription(assetCategoryLocale) %>">
                        <%= _buildAssetCategoryPath(assetCategory, assetCategoryLocale) %>
                    </a>
                <%
                }
                %>
            </c:if>
        </div>
        <h3 class="cardBox__title">
            <a class="title__link" href="<%= viewURL %>">
                <c:if test="<%= assetRenderer != null %>">
                    <img alt="" src="<%= assetRenderer.getIconPath(renderRequest) %>" />
                </c:if>

                <%= StringUtil.highlight(HtmlUtil.escape(entryTitle), queryTerms) %>
            </a>

            <c:if test="<%= Validator.isNotNull(downloadURL) %>">
                <liferay-ui:icon image="../arrows/01_down" label="<%= false %>" message='<%= LanguageUtil.format(pageContext, "download-x", HtmlUtil.escape(entryTitle)) %>' url="<%= downloadURL %>" />
            </c:if>
        </h3>
        <c:if test="<%= Validator.isNotNull(entrySummary) || Validator.isNotNull(assetCategoryIds[0]) %>">
            <div class="cardBox__text">
                <c:if test="<%= Validator.isNotNull(entrySummary) %>">
                    <p>
                        <%= StringUtil.highlight(HtmlUtil.escape(entrySummary), queryTerms) %>
                    </p>
                </c:if>
            </div>
        </c:if>
	</div>
  </div>
</div>