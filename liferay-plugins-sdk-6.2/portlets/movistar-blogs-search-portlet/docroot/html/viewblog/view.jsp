<%@ include file="/html/init.jsp"%>
<%@page import="com.movistar.site.blogs.view.ViewBlogPortlet"%>
<c:if test="${blogsEntry ne null}">
	<%
		BlogsEntry blogsEntry = (BlogsEntry)renderRequest.getAttribute("blogsEntry");
		String resourceType = (String)renderRequest.getAttribute("resourceType");
		String resourceUrl = (String)renderRequest.getAttribute("resourceUrl");
	%>
	<section class="nat-note-page" id="<portlet:namespace />${blogsEntry.getEntryId()}">
		<div class="container">
			<h1 class="note-page__title"><%= StringUtil.shorten(blogsEntry.getTitle(), 105) %></h1>
			<div class="note-page__box ${cssNotePage}">
				<%
				if(!Validator.isBlank(resourceUrl) && Validator.equals("Imagen", resourceType)) {
				%>
					<div class="box__image">
			        	<div class="image__media">
			                <picture class="media__picture">
			                    <source media="(min-width:1024px)" srcset="${resourceUrl}">
			                    <source media="(min-width:600px)" srcset="${resourceUrlTablet}">
			                    <img src="${resourceUrlMobile}" alt="${blogsEntry.getTitle()}" title="${blogsEntry.getTitle()}">
			                </picture>
			            </div>
			        </div>
				<% 	} else if(!Validator.isBlank(resourceUrl) && Validator.equals("Video", resourceType)) {%>
					<div class="box__image">
			                <a class="image__media image__media--video modal-trigger" data-videourl="${resourceUrlYoutube}" href="#modalSearchVideo">
			                    <picture class="media__picture">
			                        <source media="(min-width:1024px)" srcset="${resourceUrl}">
			                        <source media="(min-width:600px)" srcset="${resourceUrlTablet}">
			                        <img src="${resourceUrlMobile}" alt="${blogsEntry.getTitle()}" title="${blogsEntry.getTitle()}">
			                    </picture>
			                    <span class="media__play">
			                        <svg class="svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23 25">
			                        <title>Play</title>
			                        <path fill="currentColor" d="M0.760986 2.48024C0.760986 0.950588 2.4083 -0.012839 3.74151 0.737091L21.3437 10.6383C22.703 11.403 22.703 13.36 21.3437 14.1246L3.74151 24.0259C2.4083 24.7758 0.760986 23.8124 0.760986 22.2827V2.48024Z"/>
			                        </svg>
			                    </span>
			                </a>
			       </div>
				 <% } %>
				 <div class="box__description">
					${blogsEntry.getContent()}
				 </div>
			</div>
		</div>
	</section>
	${contentHtml}
</c:if>
<c:if test="${blogsEntry eq null}">
	<div class="alert alert-error">Blog ID incorrecto</div>
</c:if>