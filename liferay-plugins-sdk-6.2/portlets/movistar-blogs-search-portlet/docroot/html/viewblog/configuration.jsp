<%@ include file="/html/init.jsp"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="com.movistar.site.blogs.view.util.ViewBlogPreferencesUtil"%>

<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />
<%
	String blogId = ParamUtil.getString(request,
			"preferences--blogId--",
			ViewBlogPreferencesUtil.getBlogId(portletPreferences));
%>

<aui:form action="<%=configurationURL%>" method="post"
	name="fm-view-blog">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />
	<aui:row>
		<aui:column cssClass="span12">
			<aui:input name="preferences--blogId--" required="true"
				showRequiredLabel="false" label="viewBlogLabelId"
				value="<%=blogId%>" ignoreRequestValue="true"
				helpMessage="Ejem.: 123" />
		</aui:column>
	</aui:row>
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>