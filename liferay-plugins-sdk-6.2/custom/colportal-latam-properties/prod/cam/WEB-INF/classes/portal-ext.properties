include-and-override=portal-ext.prod.cam.properties
include-and-override=portal-${easyconf:webId}.properties

##
## Spring
##

    #
    # Input a list of comma delimited Spring configurations. These will be
    # loaded after the bean definitions specified in the contextConfigLocation
    # parameter in web.xml.
    #
    # Note that there is a special case for hibernate-spring.xml and
    # jpa-spring.xml. Even though both files are specified, only one will
    # actually load at runtime based on the property "persistence.provider".
    #
    spring.configs=\
        META-INF/base-spring.xml,\
        \
        META-INF/hibernate-spring.xml,\
        META-INF/infrastructure-spring.xml,\
        META-INF/management-spring.xml,\
        \
        META-INF/util-spring.xml,\
        \
        META-INF/jpa-spring.xml,\
        \
        META-INF/executor-spring.xml,\
        \
        META-INF/audit-spring.xml,\
        META-INF/cluster-spring.xml,\
        META-INF/editor-spring.xml,\
        META-INF/jcr-spring.xml,\
        META-INF/ldap-spring.xml,\
        META-INF/messaging-core-spring.xml,\
        META-INF/messaging-misc-spring.xml,\
        META-INF/mobile-device-spring.xml,\
        META-INF/notifications-spring.xml,\
        META-INF/poller-spring.xml,\
        META-INF/rules-spring.xml,\
        META-INF/scheduler-spring.xml,\
        META-INF/search-spring.xml,\
        META-INF/workflow-spring.xml,\
        \
        META-INF/counter-spring.xml,\
        META-INF/mail-spring.xml,\
        META-INF/portal-spring.xml,\
        META-INF/portlet-container-spring.xml,\
        META-INF/staging-spring.xml,\
        META-INF/virtual-layouts-spring.xml,\
        \
        META-INF/monitoring-spring.xml,\
        \
        #META-INF/dynamic-data-source-spring.xml,\
        META-INF/shard-data-source-spring.xml,\
        #META-INF/memcached-spring.xml,\
        \
        classpath*:META-INF/ext-spring.xml


##
## Languages and Time Zones
##

	locales=es,en

	#
	# Specify the locales that are enabled by default.
	#
	locales.enabled=es,en
	
	#
    # This sets the default time zone of the portal.
    #
	company.default.time.zone=UTC-6

    #
    # The portal can authenticate users based on their email address, screen
    # name, or user id.
    #
    company.security.auth.type=screenName

    #
    # Set this to true to ensure users login with https. If this is set to true
    # and you want your HTTP session to contain your credentials after logging
    # in, then the property "session.enable.phishing.protection" must be set to
    # false or your credentials will only be available in the HTTPS session.
    #
    company.security.auth.requires.https=false
    
	#
    # Set this to true to allow users to autocomplete the login form based on
    # their previously entered values.
    #
    company.security.login.form.autocomplete=false

    #
    # Set this to true to allow users to ask the portal to send them their
    # password.
    #
    company.security.send.password=false

    #
    # Set this to true to allow users to ask the portal to send them a password
    # reset link.
    #
    company.security.send.password.reset.link=false

    #
    # Set this to true to allow strangers to create accounts and register
    # themselves on the portal.
    #
    company.security.strangers=false

    #
    # Set this to true if strangers can create accounts with email addresses
    # that match the company mail suffix. This property is not used unless
    # "company.security.strangers" is also set to true.
    #
    company.security.strangers.with.mx=false

##
## OpenID
##

    #
    # Set this to true to enable OpenId authentication. If set to true, then the
    # property "auto.login.hooks" must contain a reference to the class
    # com.liferay.portal.security.auth.OpenIdAutoLogin.
    #
    open.id.auth.enabled=false
    
##
##
##
	#
    # Set this to true to invalidate the session when a user logs into the
    # portal. This helps prevent phishing. Set this to false if you need the
    # guest user and the authenticated user to have the same session.
    #
    # Set this to false if the property "company.security.auth.requires.https"
    # is set to true and you want to maintain the same credentials across HTTP
    # and HTTPS sessions.
    #
    session.enable.phishing.protection=false

##
## Lucene Search
##

    #
    # Set the location of the spell checker dictionary files for specific
    # locales. Specify a comma delimited list of files if a locale has more than
    # one dictionary file. A group id (e.g. 123456) can also be added to specify
    # a dictionary that only applies to one group.
    #
    index.search.spell.checker.dictionary[en]=com/liferay/portal/search/lucene/dependencies/spellchecker/en_US.txt
    index.search.spell.checker.dictionary[es]=com/liferay/portal/search/lucene/dependencies/spellchecker/es_ES.txt

    #
    # Specify the locales supported by the spell checker. This is used by
    # the search engine's spell checker when determining whether to execute
    # spell checking for a particular language.
    #
    index.search.spell.checker.supported.locales=es,en
	
##
## Enable Service Locator for velocity templates
##
	velocity.engine.restricted.variables=
	
	
##
## Dynamic Data Mapping Portlet
##

    #
    # Set the name of template language which will be used by default.
    #
    dynamic.data.mapping.template.language.default=vm