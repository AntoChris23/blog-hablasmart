/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the DecoValor service. Represents a row in the &quot;homephone_DecoValor&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link com.movistar.latam.colportal.homephone.model.impl.DecoValorModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link com.movistar.latam.colportal.homephone.model.impl.DecoValorImpl}.
 * </p>
 *
 * @author vass
 * @see DecoValor
 * @see com.movistar.latam.colportal.homephone.model.impl.DecoValorImpl
 * @see com.movistar.latam.colportal.homephone.model.impl.DecoValorModelImpl
 * @generated
 */
public interface DecoValorModel extends BaseModel<DecoValor> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a deco valor model instance should use the {@link DecoValor} interface instead.
	 */

	/**
	 * Returns the primary key of this deco valor.
	 *
	 * @return the primary key of this deco valor
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this deco valor.
	 *
	 * @param primaryKey the primary key of this deco valor
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the ID of this deco valor.
	 *
	 * @return the ID of this deco valor
	 */
	public long getId();

	/**
	 * Sets the ID of this deco valor.
	 *
	 * @param id the ID of this deco valor
	 */
	public void setId(long id);

	/**
	 * Returns the id deco of this deco valor.
	 *
	 * @return the id deco of this deco valor
	 */
	public long getIdDeco();

	/**
	 * Sets the id deco of this deco valor.
	 *
	 * @param idDeco the id deco of this deco valor
	 */
	public void setIdDeco(long idDeco);

	/**
	 * Returns the nombre of this deco valor.
	 *
	 * @return the nombre of this deco valor
	 */
	@AutoEscape
	public String getNombre();

	/**
	 * Sets the nombre of this deco valor.
	 *
	 * @param nombre the nombre of this deco valor
	 */
	public void setNombre(String nombre);

	/**
	 * Returns the divipola of this deco valor.
	 *
	 * @return the divipola of this deco valor
	 */
	public long getDivipola();

	/**
	 * Sets the divipola of this deco valor.
	 *
	 * @param divipola the divipola of this deco valor
	 */
	public void setDivipola(long divipola);

	/**
	 * Returns the valor of this deco valor.
	 *
	 * @return the valor of this deco valor
	 */
	public long getValor();

	/**
	 * Sets the valor of this deco valor.
	 *
	 * @param valor the valor of this deco valor
	 */
	public void setValor(long valor);

	/**
	 * Returns the fecha of this deco valor.
	 *
	 * @return the fecha of this deco valor
	 */
	public Date getFecha();

	/**
	 * Sets the fecha of this deco valor.
	 *
	 * @param fecha the fecha of this deco valor
	 */
	public void setFecha(Date fecha);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(DecoValor decoValor);

	@Override
	public int hashCode();

	@Override
	public CacheModel<DecoValor> toCacheModel();

	@Override
	public DecoValor toEscapedModel();

	@Override
	public DecoValor toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}