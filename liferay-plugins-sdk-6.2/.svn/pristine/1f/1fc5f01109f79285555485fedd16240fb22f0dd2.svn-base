/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.movistar.latam.colportal.homephone.model.SvaTipo;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing SvaTipo in entity cache.
 *
 * @author vass
 * @see SvaTipo
 * @generated
 */
public class SvaTipoCacheModel implements CacheModel<SvaTipo>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{id=");
		sb.append(id);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", fecha=");
		sb.append(fecha);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public SvaTipo toEntityModel() {
		SvaTipoImpl svaTipoImpl = new SvaTipoImpl();

		svaTipoImpl.setId(id);

		if (nombre == null) {
			svaTipoImpl.setNombre(StringPool.BLANK);
		}
		else {
			svaTipoImpl.setNombre(nombre);
		}

		if (fecha == Long.MIN_VALUE) {
			svaTipoImpl.setFecha(null);
		}
		else {
			svaTipoImpl.setFecha(new Date(fecha));
		}

		svaTipoImpl.resetOriginalValues();

		return svaTipoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		nombre = objectInput.readUTF();
		fecha = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		objectOutput.writeLong(fecha);
	}

	public long id;
	public String nombre;
	public long fecha;
}