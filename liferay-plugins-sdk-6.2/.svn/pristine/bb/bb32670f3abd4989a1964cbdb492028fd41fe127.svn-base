/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author vass
 * @generated
 */
public class SolicitudSoap implements Serializable {
	public static SolicitudSoap toSoapModel(Solicitud model) {
		SolicitudSoap soapModel = new SolicitudSoap();

		soapModel.setId(model.getId());
		soapModel.setNombres(model.getNombres());
		soapModel.setApellidos(model.getApellidos());
		soapModel.setDireccion(model.getDireccion());
		soapModel.setDocumento(model.getDocumento());
		soapModel.setEmail(model.getEmail());
		soapModel.setTipodoc(model.getTipodoc());
		soapModel.setBarrio(model.getBarrio());
		soapModel.setCelular(model.getCelular());
		soapModel.setTelefono(model.getTelefono());
		soapModel.setDivipola(model.getDivipola());
		soapModel.setEstrato(model.getEstrato());
		soapModel.setPlanLB(model.getPlanLB());
		soapModel.setPlanBA(model.getPlanBA());
		soapModel.setPlanTV(model.getPlanTV());
		soapModel.setPlanHD(model.getPlanHD());
		soapModel.setSvaInternet(model.getSvaInternet());
		soapModel.setSvaTelevision(model.getSvaTelevision());
		soapModel.setDecodificador(model.getDecodificador());
		soapModel.setTotalPedido(model.getTotalPedido());
		soapModel.setTotalConIVA(model.getTotalConIVA());
		soapModel.setCodigoLB(model.getCodigoLB());
		soapModel.setCodigoBA(model.getCodigoBA());
		soapModel.setCodigoPlanTV(model.getCodigoPlanTV());
		soapModel.setTipoMX(model.getTipoMX());

		return soapModel;
	}

	public static SolicitudSoap[] toSoapModels(Solicitud[] models) {
		SolicitudSoap[] soapModels = new SolicitudSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SolicitudSoap[][] toSoapModels(Solicitud[][] models) {
		SolicitudSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SolicitudSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SolicitudSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SolicitudSoap[] toSoapModels(List<Solicitud> models) {
		List<SolicitudSoap> soapModels = new ArrayList<SolicitudSoap>(models.size());

		for (Solicitud model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SolicitudSoap[soapModels.size()]);
	}

	public SolicitudSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getNombres() {
		return _nombres;
	}

	public void setNombres(String nombres) {
		_nombres = nombres;
	}

	public String getApellidos() {
		return _apellidos;
	}

	public void setApellidos(String apellidos) {
		_apellidos = apellidos;
	}

	public String getDireccion() {
		return _direccion;
	}

	public void setDireccion(String direccion) {
		_direccion = direccion;
	}

	public String getDocumento() {
		return _documento;
	}

	public void setDocumento(String documento) {
		_documento = documento;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public long getTipodoc() {
		return _tipodoc;
	}

	public void setTipodoc(long tipodoc) {
		_tipodoc = tipodoc;
	}

	public String getBarrio() {
		return _barrio;
	}

	public void setBarrio(String barrio) {
		_barrio = barrio;
	}

	public String getCelular() {
		return _celular;
	}

	public void setCelular(String celular) {
		_celular = celular;
	}

	public String getTelefono() {
		return _telefono;
	}

	public void setTelefono(String telefono) {
		_telefono = telefono;
	}

	public long getDivipola() {
		return _divipola;
	}

	public void setDivipola(long divipola) {
		_divipola = divipola;
	}

	public Long getEstrato() {
		return _estrato;
	}

	public void setEstrato(Long estrato) {
		_estrato = estrato;
	}

	public long getPlanLB() {
		return _planLB;
	}

	public void setPlanLB(long planLB) {
		_planLB = planLB;
	}

	public String getPlanBA() {
		return _planBA;
	}

	public void setPlanBA(String planBA) {
		_planBA = planBA;
	}

	public String getPlanTV() {
		return _planTV;
	}

	public void setPlanTV(String planTV) {
		_planTV = planTV;
	}

	public String getPlanHD() {
		return _planHD;
	}

	public void setPlanHD(String planHD) {
		_planHD = planHD;
	}

	public String getSvaInternet() {
		return _svaInternet;
	}

	public void setSvaInternet(String svaInternet) {
		_svaInternet = svaInternet;
	}

	public String getSvaTelevision() {
		return _svaTelevision;
	}

	public void setSvaTelevision(String svaTelevision) {
		_svaTelevision = svaTelevision;
	}

	public String getDecodificador() {
		return _decodificador;
	}

	public void setDecodificador(String decodificador) {
		_decodificador = decodificador;
	}

	public float getTotalPedido() {
		return _totalPedido;
	}

	public void setTotalPedido(float totalPedido) {
		_totalPedido = totalPedido;
	}

	public float getTotalConIVA() {
		return _totalConIVA;
	}

	public void setTotalConIVA(float totalConIVA) {
		_totalConIVA = totalConIVA;
	}

	public long getCodigoLB() {
		return _codigoLB;
	}

	public void setCodigoLB(long codigoLB) {
		_codigoLB = codigoLB;
	}

	public long getCodigoBA() {
		return _codigoBA;
	}

	public void setCodigoBA(long codigoBA) {
		_codigoBA = codigoBA;
	}

	public long getCodigoPlanTV() {
		return _codigoPlanTV;
	}

	public void setCodigoPlanTV(long codigoPlanTV) {
		_codigoPlanTV = codigoPlanTV;
	}

	public long getTipoMX() {
		return _tipoMX;
	}

	public void setTipoMX(long tipoMX) {
		_tipoMX = tipoMX;
	}

	private long _id;
	private String _nombres;
	private String _apellidos;
	private String _direccion;
	private String _documento;
	private String _email;
	private long _tipodoc;
	private String _barrio;
	private String _celular;
	private String _telefono;
	private long _divipola;
	private Long _estrato;
	private long _planLB;
	private String _planBA;
	private String _planTV;
	private String _planHD;
	private String _svaInternet;
	private String _svaTelevision;
	private String _decodificador;
	private float _totalPedido;
	private float _totalConIVA;
	private long _codigoLB;
	private long _codigoBA;
	private long _codigoPlanTV;
	private long _tipoMX;
}