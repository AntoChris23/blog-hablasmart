/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.base;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.bean.IdentifiableBean;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.model.PersistedModel;
import com.liferay.portal.service.BaseLocalServiceImpl;
import com.liferay.portal.service.PersistedModelLocalServiceRegistryUtil;
import com.liferay.portal.service.persistence.UserPersistence;

import com.movistar.latam.colportal.homephone.model.Deco;
import com.movistar.latam.colportal.homephone.service.DecoLocalService;
import com.movistar.latam.colportal.homephone.service.persistence.BAPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.CiudadPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.DecoPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.DecoValorPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.DepartamentoPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.EstratoPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.LBPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.MatrizPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.SolicitudPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.SvaPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.SvaTipoPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.SvaValorPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.SvaXBaPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.TipoMxPersistence;
import com.movistar.latam.colportal.homephone.service.persistence.TvPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the deco local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link com.movistar.latam.colportal.homephone.service.impl.DecoLocalServiceImpl}.
 * </p>
 *
 * @author vass
 * @see com.movistar.latam.colportal.homephone.service.impl.DecoLocalServiceImpl
 * @see com.movistar.latam.colportal.homephone.service.DecoLocalServiceUtil
 * @generated
 */
public abstract class DecoLocalServiceBaseImpl extends BaseLocalServiceImpl
	implements DecoLocalService, IdentifiableBean {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link com.movistar.latam.colportal.homephone.service.DecoLocalServiceUtil} to access the deco local service.
	 */

	/**
	 * Adds the deco to the database. Also notifies the appropriate model listeners.
	 *
	 * @param deco the deco
	 * @return the deco that was added
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Deco addDeco(Deco deco) throws SystemException {
		deco.setNew(true);

		return decoPersistence.update(deco);
	}

	/**
	 * Creates a new deco with the primary key. Does not add the deco to the database.
	 *
	 * @param id the primary key for the new deco
	 * @return the new deco
	 */
	@Override
	public Deco createDeco(long id) {
		return decoPersistence.create(id);
	}

	/**
	 * Deletes the deco with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the deco
	 * @return the deco that was removed
	 * @throws PortalException if a deco with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Deco deleteDeco(long id) throws PortalException, SystemException {
		return decoPersistence.remove(id);
	}

	/**
	 * Deletes the deco from the database. Also notifies the appropriate model listeners.
	 *
	 * @param deco the deco
	 * @return the deco that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Deco deleteDeco(Deco deco) throws SystemException {
		return decoPersistence.remove(deco);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(Deco.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return decoPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.DecoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return decoPersistence.findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.DecoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return decoPersistence.findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery)
		throws SystemException {
		return decoPersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection) throws SystemException {
		return decoPersistence.countWithDynamicQuery(dynamicQuery, projection);
	}

	@Override
	public Deco fetchDeco(long id) throws SystemException {
		return decoPersistence.fetchByPrimaryKey(id);
	}

	/**
	 * Returns the deco with the primary key.
	 *
	 * @param id the primary key of the deco
	 * @return the deco
	 * @throws PortalException if a deco with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Deco getDeco(long id) throws PortalException, SystemException {
		return decoPersistence.findByPrimaryKey(id);
	}

	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException, SystemException {
		return decoPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the decos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.DecoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of decos
	 * @param end the upper bound of the range of decos (not inclusive)
	 * @return the range of decos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Deco> getDecos(int start, int end) throws SystemException {
		return decoPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of decos.
	 *
	 * @return the number of decos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int getDecosCount() throws SystemException {
		return decoPersistence.countAll();
	}

	/**
	 * Updates the deco in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param deco the deco
	 * @return the deco that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Deco updateDeco(Deco deco) throws SystemException {
		return decoPersistence.update(deco);
	}

	/**
	 * Returns the b a local service.
	 *
	 * @return the b a local service
	 */
	public com.movistar.latam.colportal.homephone.service.BALocalService getBALocalService() {
		return baLocalService;
	}

	/**
	 * Sets the b a local service.
	 *
	 * @param baLocalService the b a local service
	 */
	public void setBALocalService(
		com.movistar.latam.colportal.homephone.service.BALocalService baLocalService) {
		this.baLocalService = baLocalService;
	}

	/**
	 * Returns the b a persistence.
	 *
	 * @return the b a persistence
	 */
	public BAPersistence getBAPersistence() {
		return baPersistence;
	}

	/**
	 * Sets the b a persistence.
	 *
	 * @param baPersistence the b a persistence
	 */
	public void setBAPersistence(BAPersistence baPersistence) {
		this.baPersistence = baPersistence;
	}

	/**
	 * Returns the ciudad local service.
	 *
	 * @return the ciudad local service
	 */
	public com.movistar.latam.colportal.homephone.service.CiudadLocalService getCiudadLocalService() {
		return ciudadLocalService;
	}

	/**
	 * Sets the ciudad local service.
	 *
	 * @param ciudadLocalService the ciudad local service
	 */
	public void setCiudadLocalService(
		com.movistar.latam.colportal.homephone.service.CiudadLocalService ciudadLocalService) {
		this.ciudadLocalService = ciudadLocalService;
	}

	/**
	 * Returns the ciudad persistence.
	 *
	 * @return the ciudad persistence
	 */
	public CiudadPersistence getCiudadPersistence() {
		return ciudadPersistence;
	}

	/**
	 * Sets the ciudad persistence.
	 *
	 * @param ciudadPersistence the ciudad persistence
	 */
	public void setCiudadPersistence(CiudadPersistence ciudadPersistence) {
		this.ciudadPersistence = ciudadPersistence;
	}

	/**
	 * Returns the deco local service.
	 *
	 * @return the deco local service
	 */
	public com.movistar.latam.colportal.homephone.service.DecoLocalService getDecoLocalService() {
		return decoLocalService;
	}

	/**
	 * Sets the deco local service.
	 *
	 * @param decoLocalService the deco local service
	 */
	public void setDecoLocalService(
		com.movistar.latam.colportal.homephone.service.DecoLocalService decoLocalService) {
		this.decoLocalService = decoLocalService;
	}

	/**
	 * Returns the deco persistence.
	 *
	 * @return the deco persistence
	 */
	public DecoPersistence getDecoPersistence() {
		return decoPersistence;
	}

	/**
	 * Sets the deco persistence.
	 *
	 * @param decoPersistence the deco persistence
	 */
	public void setDecoPersistence(DecoPersistence decoPersistence) {
		this.decoPersistence = decoPersistence;
	}

	/**
	 * Returns the deco valor local service.
	 *
	 * @return the deco valor local service
	 */
	public com.movistar.latam.colportal.homephone.service.DecoValorLocalService getDecoValorLocalService() {
		return decoValorLocalService;
	}

	/**
	 * Sets the deco valor local service.
	 *
	 * @param decoValorLocalService the deco valor local service
	 */
	public void setDecoValorLocalService(
		com.movistar.latam.colportal.homephone.service.DecoValorLocalService decoValorLocalService) {
		this.decoValorLocalService = decoValorLocalService;
	}

	/**
	 * Returns the deco valor persistence.
	 *
	 * @return the deco valor persistence
	 */
	public DecoValorPersistence getDecoValorPersistence() {
		return decoValorPersistence;
	}

	/**
	 * Sets the deco valor persistence.
	 *
	 * @param decoValorPersistence the deco valor persistence
	 */
	public void setDecoValorPersistence(
		DecoValorPersistence decoValorPersistence) {
		this.decoValorPersistence = decoValorPersistence;
	}

	/**
	 * Returns the departamento local service.
	 *
	 * @return the departamento local service
	 */
	public com.movistar.latam.colportal.homephone.service.DepartamentoLocalService getDepartamentoLocalService() {
		return departamentoLocalService;
	}

	/**
	 * Sets the departamento local service.
	 *
	 * @param departamentoLocalService the departamento local service
	 */
	public void setDepartamentoLocalService(
		com.movistar.latam.colportal.homephone.service.DepartamentoLocalService departamentoLocalService) {
		this.departamentoLocalService = departamentoLocalService;
	}

	/**
	 * Returns the departamento persistence.
	 *
	 * @return the departamento persistence
	 */
	public DepartamentoPersistence getDepartamentoPersistence() {
		return departamentoPersistence;
	}

	/**
	 * Sets the departamento persistence.
	 *
	 * @param departamentoPersistence the departamento persistence
	 */
	public void setDepartamentoPersistence(
		DepartamentoPersistence departamentoPersistence) {
		this.departamentoPersistence = departamentoPersistence;
	}

	/**
	 * Returns the estrato local service.
	 *
	 * @return the estrato local service
	 */
	public com.movistar.latam.colportal.homephone.service.EstratoLocalService getEstratoLocalService() {
		return estratoLocalService;
	}

	/**
	 * Sets the estrato local service.
	 *
	 * @param estratoLocalService the estrato local service
	 */
	public void setEstratoLocalService(
		com.movistar.latam.colportal.homephone.service.EstratoLocalService estratoLocalService) {
		this.estratoLocalService = estratoLocalService;
	}

	/**
	 * Returns the estrato persistence.
	 *
	 * @return the estrato persistence
	 */
	public EstratoPersistence getEstratoPersistence() {
		return estratoPersistence;
	}

	/**
	 * Sets the estrato persistence.
	 *
	 * @param estratoPersistence the estrato persistence
	 */
	public void setEstratoPersistence(EstratoPersistence estratoPersistence) {
		this.estratoPersistence = estratoPersistence;
	}

	/**
	 * Returns the l b local service.
	 *
	 * @return the l b local service
	 */
	public com.movistar.latam.colportal.homephone.service.LBLocalService getLBLocalService() {
		return lbLocalService;
	}

	/**
	 * Sets the l b local service.
	 *
	 * @param lbLocalService the l b local service
	 */
	public void setLBLocalService(
		com.movistar.latam.colportal.homephone.service.LBLocalService lbLocalService) {
		this.lbLocalService = lbLocalService;
	}

	/**
	 * Returns the l b persistence.
	 *
	 * @return the l b persistence
	 */
	public LBPersistence getLBPersistence() {
		return lbPersistence;
	}

	/**
	 * Sets the l b persistence.
	 *
	 * @param lbPersistence the l b persistence
	 */
	public void setLBPersistence(LBPersistence lbPersistence) {
		this.lbPersistence = lbPersistence;
	}

	/**
	 * Returns the matriz local service.
	 *
	 * @return the matriz local service
	 */
	public com.movistar.latam.colportal.homephone.service.MatrizLocalService getMatrizLocalService() {
		return matrizLocalService;
	}

	/**
	 * Sets the matriz local service.
	 *
	 * @param matrizLocalService the matriz local service
	 */
	public void setMatrizLocalService(
		com.movistar.latam.colportal.homephone.service.MatrizLocalService matrizLocalService) {
		this.matrizLocalService = matrizLocalService;
	}

	/**
	 * Returns the matriz persistence.
	 *
	 * @return the matriz persistence
	 */
	public MatrizPersistence getMatrizPersistence() {
		return matrizPersistence;
	}

	/**
	 * Sets the matriz persistence.
	 *
	 * @param matrizPersistence the matriz persistence
	 */
	public void setMatrizPersistence(MatrizPersistence matrizPersistence) {
		this.matrizPersistence = matrizPersistence;
	}

	/**
	 * Returns the solicitud local service.
	 *
	 * @return the solicitud local service
	 */
	public com.movistar.latam.colportal.homephone.service.SolicitudLocalService getSolicitudLocalService() {
		return solicitudLocalService;
	}

	/**
	 * Sets the solicitud local service.
	 *
	 * @param solicitudLocalService the solicitud local service
	 */
	public void setSolicitudLocalService(
		com.movistar.latam.colportal.homephone.service.SolicitudLocalService solicitudLocalService) {
		this.solicitudLocalService = solicitudLocalService;
	}

	/**
	 * Returns the solicitud persistence.
	 *
	 * @return the solicitud persistence
	 */
	public SolicitudPersistence getSolicitudPersistence() {
		return solicitudPersistence;
	}

	/**
	 * Sets the solicitud persistence.
	 *
	 * @param solicitudPersistence the solicitud persistence
	 */
	public void setSolicitudPersistence(
		SolicitudPersistence solicitudPersistence) {
		this.solicitudPersistence = solicitudPersistence;
	}

	/**
	 * Returns the sva local service.
	 *
	 * @return the sva local service
	 */
	public com.movistar.latam.colportal.homephone.service.SvaLocalService getSvaLocalService() {
		return svaLocalService;
	}

	/**
	 * Sets the sva local service.
	 *
	 * @param svaLocalService the sva local service
	 */
	public void setSvaLocalService(
		com.movistar.latam.colportal.homephone.service.SvaLocalService svaLocalService) {
		this.svaLocalService = svaLocalService;
	}

	/**
	 * Returns the sva persistence.
	 *
	 * @return the sva persistence
	 */
	public SvaPersistence getSvaPersistence() {
		return svaPersistence;
	}

	/**
	 * Sets the sva persistence.
	 *
	 * @param svaPersistence the sva persistence
	 */
	public void setSvaPersistence(SvaPersistence svaPersistence) {
		this.svaPersistence = svaPersistence;
	}

	/**
	 * Returns the sva tipo local service.
	 *
	 * @return the sva tipo local service
	 */
	public com.movistar.latam.colportal.homephone.service.SvaTipoLocalService getSvaTipoLocalService() {
		return svaTipoLocalService;
	}

	/**
	 * Sets the sva tipo local service.
	 *
	 * @param svaTipoLocalService the sva tipo local service
	 */
	public void setSvaTipoLocalService(
		com.movistar.latam.colportal.homephone.service.SvaTipoLocalService svaTipoLocalService) {
		this.svaTipoLocalService = svaTipoLocalService;
	}

	/**
	 * Returns the sva tipo persistence.
	 *
	 * @return the sva tipo persistence
	 */
	public SvaTipoPersistence getSvaTipoPersistence() {
		return svaTipoPersistence;
	}

	/**
	 * Sets the sva tipo persistence.
	 *
	 * @param svaTipoPersistence the sva tipo persistence
	 */
	public void setSvaTipoPersistence(SvaTipoPersistence svaTipoPersistence) {
		this.svaTipoPersistence = svaTipoPersistence;
	}

	/**
	 * Returns the sva valor local service.
	 *
	 * @return the sva valor local service
	 */
	public com.movistar.latam.colportal.homephone.service.SvaValorLocalService getSvaValorLocalService() {
		return svaValorLocalService;
	}

	/**
	 * Sets the sva valor local service.
	 *
	 * @param svaValorLocalService the sva valor local service
	 */
	public void setSvaValorLocalService(
		com.movistar.latam.colportal.homephone.service.SvaValorLocalService svaValorLocalService) {
		this.svaValorLocalService = svaValorLocalService;
	}

	/**
	 * Returns the sva valor persistence.
	 *
	 * @return the sva valor persistence
	 */
	public SvaValorPersistence getSvaValorPersistence() {
		return svaValorPersistence;
	}

	/**
	 * Sets the sva valor persistence.
	 *
	 * @param svaValorPersistence the sva valor persistence
	 */
	public void setSvaValorPersistence(SvaValorPersistence svaValorPersistence) {
		this.svaValorPersistence = svaValorPersistence;
	}

	/**
	 * Returns the sva x ba local service.
	 *
	 * @return the sva x ba local service
	 */
	public com.movistar.latam.colportal.homephone.service.SvaXBaLocalService getSvaXBaLocalService() {
		return svaXBaLocalService;
	}

	/**
	 * Sets the sva x ba local service.
	 *
	 * @param svaXBaLocalService the sva x ba local service
	 */
	public void setSvaXBaLocalService(
		com.movistar.latam.colportal.homephone.service.SvaXBaLocalService svaXBaLocalService) {
		this.svaXBaLocalService = svaXBaLocalService;
	}

	/**
	 * Returns the sva x ba persistence.
	 *
	 * @return the sva x ba persistence
	 */
	public SvaXBaPersistence getSvaXBaPersistence() {
		return svaXBaPersistence;
	}

	/**
	 * Sets the sva x ba persistence.
	 *
	 * @param svaXBaPersistence the sva x ba persistence
	 */
	public void setSvaXBaPersistence(SvaXBaPersistence svaXBaPersistence) {
		this.svaXBaPersistence = svaXBaPersistence;
	}

	/**
	 * Returns the tipo mx local service.
	 *
	 * @return the tipo mx local service
	 */
	public com.movistar.latam.colportal.homephone.service.TipoMxLocalService getTipoMxLocalService() {
		return tipoMxLocalService;
	}

	/**
	 * Sets the tipo mx local service.
	 *
	 * @param tipoMxLocalService the tipo mx local service
	 */
	public void setTipoMxLocalService(
		com.movistar.latam.colportal.homephone.service.TipoMxLocalService tipoMxLocalService) {
		this.tipoMxLocalService = tipoMxLocalService;
	}

	/**
	 * Returns the tipo mx persistence.
	 *
	 * @return the tipo mx persistence
	 */
	public TipoMxPersistence getTipoMxPersistence() {
		return tipoMxPersistence;
	}

	/**
	 * Sets the tipo mx persistence.
	 *
	 * @param tipoMxPersistence the tipo mx persistence
	 */
	public void setTipoMxPersistence(TipoMxPersistence tipoMxPersistence) {
		this.tipoMxPersistence = tipoMxPersistence;
	}

	/**
	 * Returns the tv local service.
	 *
	 * @return the tv local service
	 */
	public com.movistar.latam.colportal.homephone.service.TvLocalService getTvLocalService() {
		return tvLocalService;
	}

	/**
	 * Sets the tv local service.
	 *
	 * @param tvLocalService the tv local service
	 */
	public void setTvLocalService(
		com.movistar.latam.colportal.homephone.service.TvLocalService tvLocalService) {
		this.tvLocalService = tvLocalService;
	}

	/**
	 * Returns the tv persistence.
	 *
	 * @return the tv persistence
	 */
	public TvPersistence getTvPersistence() {
		return tvPersistence;
	}

	/**
	 * Sets the tv persistence.
	 *
	 * @param tvPersistence the tv persistence
	 */
	public void setTvPersistence(TvPersistence tvPersistence) {
		this.tvPersistence = tvPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public com.liferay.portal.service.UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(
		com.liferay.portal.service.UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		Class<?> clazz = getClass();

		_classLoader = clazz.getClassLoader();

		PersistedModelLocalServiceRegistryUtil.register("com.movistar.latam.colportal.homephone.model.Deco",
			decoLocalService);
	}

	public void destroy() {
		PersistedModelLocalServiceRegistryUtil.unregister(
			"com.movistar.latam.colportal.homephone.model.Deco");
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	@Override
	public String getBeanIdentifier() {
		return _beanIdentifier;
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	@Override
	public void setBeanIdentifier(String beanIdentifier) {
		_beanIdentifier = beanIdentifier;
	}

	@Override
	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		Thread currentThread = Thread.currentThread();

		ClassLoader contextClassLoader = currentThread.getContextClassLoader();

		if (contextClassLoader != _classLoader) {
			currentThread.setContextClassLoader(_classLoader);
		}

		try {
			return _clpInvoker.invokeMethod(name, parameterTypes, arguments);
		}
		finally {
			if (contextClassLoader != _classLoader) {
				currentThread.setContextClassLoader(contextClassLoader);
			}
		}
	}

	protected Class<?> getModelClass() {
		return Deco.class;
	}

	protected String getModelClassName() {
		return Deco.class.getName();
	}

	/**
	 * Performs an SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) throws SystemException {
		try {
			DataSource dataSource = decoPersistence.getDataSource();

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql, new int[0]);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = com.movistar.latam.colportal.homephone.service.BALocalService.class)
	protected com.movistar.latam.colportal.homephone.service.BALocalService baLocalService;
	@BeanReference(type = BAPersistence.class)
	protected BAPersistence baPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.CiudadLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.CiudadLocalService ciudadLocalService;
	@BeanReference(type = CiudadPersistence.class)
	protected CiudadPersistence ciudadPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.DecoLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.DecoLocalService decoLocalService;
	@BeanReference(type = DecoPersistence.class)
	protected DecoPersistence decoPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.DecoValorLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.DecoValorLocalService decoValorLocalService;
	@BeanReference(type = DecoValorPersistence.class)
	protected DecoValorPersistence decoValorPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.DepartamentoLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.DepartamentoLocalService departamentoLocalService;
	@BeanReference(type = DepartamentoPersistence.class)
	protected DepartamentoPersistence departamentoPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.EstratoLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.EstratoLocalService estratoLocalService;
	@BeanReference(type = EstratoPersistence.class)
	protected EstratoPersistence estratoPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.LBLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.LBLocalService lbLocalService;
	@BeanReference(type = LBPersistence.class)
	protected LBPersistence lbPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.MatrizLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.MatrizLocalService matrizLocalService;
	@BeanReference(type = MatrizPersistence.class)
	protected MatrizPersistence matrizPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.SolicitudLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.SolicitudLocalService solicitudLocalService;
	@BeanReference(type = SolicitudPersistence.class)
	protected SolicitudPersistence solicitudPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.SvaLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.SvaLocalService svaLocalService;
	@BeanReference(type = SvaPersistence.class)
	protected SvaPersistence svaPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.SvaTipoLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.SvaTipoLocalService svaTipoLocalService;
	@BeanReference(type = SvaTipoPersistence.class)
	protected SvaTipoPersistence svaTipoPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.SvaValorLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.SvaValorLocalService svaValorLocalService;
	@BeanReference(type = SvaValorPersistence.class)
	protected SvaValorPersistence svaValorPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.SvaXBaLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.SvaXBaLocalService svaXBaLocalService;
	@BeanReference(type = SvaXBaPersistence.class)
	protected SvaXBaPersistence svaXBaPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.TipoMxLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.TipoMxLocalService tipoMxLocalService;
	@BeanReference(type = TipoMxPersistence.class)
	protected TipoMxPersistence tipoMxPersistence;
	@BeanReference(type = com.movistar.latam.colportal.homephone.service.TvLocalService.class)
	protected com.movistar.latam.colportal.homephone.service.TvLocalService tvLocalService;
	@BeanReference(type = TvPersistence.class)
	protected TvPersistence tvPersistence;
	@BeanReference(type = com.liferay.counter.service.CounterLocalService.class)
	protected com.liferay.counter.service.CounterLocalService counterLocalService;
	@BeanReference(type = com.liferay.portal.service.ResourceLocalService.class)
	protected com.liferay.portal.service.ResourceLocalService resourceLocalService;
	@BeanReference(type = com.liferay.portal.service.UserLocalService.class)
	protected com.liferay.portal.service.UserLocalService userLocalService;
	@BeanReference(type = com.liferay.portal.service.UserService.class)
	protected com.liferay.portal.service.UserService userService;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private String _beanIdentifier;
	private ClassLoader _classLoader;
	private DecoLocalServiceClpInvoker _clpInvoker = new DecoLocalServiceClpInvoker();
}