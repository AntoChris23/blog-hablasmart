/*
 * 
 */
package com.movistar.latam.colportal.hook.init.util;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;


/**
 * RoleUtil:.
 * 
 * @author VASS
 * @version 1.0
 */
public class RoleUtil {

	/** _log. */
	private static Log _log = LogFactoryUtil.getLog(RoleUtil.class);

	/** _LEGACY_TYPE_COMMUNITY_LABEL. */
	private static final String _LEGACY_TYPE_COMMUNITY_LABEL = "community";

	/** PORTAL_CONTENT_EDITOR. */
	public static final String PORTAL_CONTENT_EDITOR = "Portal Content Editor";
	
	/** PORTAL_CONTENT_PUBLISHER. */
	public static final String PORTAL_CONTENT_PUBLISHER = "Portal Content Publisher";
	
	/** PORTAL_CONTENT_REVIEWER. */
	public static final String PORTAL_CONTENT_REVIEWER = "Portal Content Reviewer";

	/**
	 * Añade role.
	 * 
	 * @param companyId
	 *            company id
	 * @param userId
	 *            user id
	 * @param name
	 *            name
	 * @param description
	 *            description
	 * @param roleType
	 *            role type
	 * @param permissions
	 *            permissions
	 * @return role
	 * @throws Exception
	 *             exception
	 */
	public static Role addRole(long companyId, long userId, String name,
			String description, int roleType, Map<String, String[]> permissions)
			throws Exception {

		Role role = null;

		role = RoleLocalServiceUtil.fetchRole(companyId, name);
		if (role == null || role != null && role.getType() != roleType) {
			Map<Locale, String> titleMap = new HashMap<Locale, String>();
			titleMap.put(LocaleUtil.getDefault(), name);

			Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
			descriptionMap.put(LocaleUtil.getDefault(), description);

			role = RoleLocalServiceUtil.addRole(userId, Role.class.getName(),
					0, name, titleMap, descriptionMap, roleType, null, null);
			if (permissions != null) {
				for (Entry<String, String[]> entry : permissions.entrySet()) {
					setRolePermissions(role, entry.getKey(), entry.getValue());
				}
			}
		}
		return role;
	}

	/**
	 * Añade group role.
	 * 
	 * @param userGroupIds
	 *            user group ids
	 * @param role
	 *            role
	 * @throws SystemException
	 *             system exception
	 */
	public static void addGroupRole(long[] userGroupIds, Role role)
			throws SystemException {
		for (long userGroupId : userGroupIds) {
			// TODO
		}
	}

	/**
	 * Asigna role permissions.
	 * 
	 * @param role
	 *            role
	 * @param name
	 *            name
	 * @param actionIds
	 *            action ids
	 * @throws Exception
	 *             exception
	 */
	protected static void setRolePermissions(Role role, String name,
			String[] actionIds) throws Exception {
		long roleId = role.getRoleId();
		long companyId = role.getCompanyId();
		int scope = ResourceConstants.SCOPE_COMPANY;
		String primKey = String.valueOf(companyId);
		ResourcePermissionLocalServiceUtil.setResourcePermissions(companyId,
				name, scope, primKey, roleId, actionIds);
	}

	/**
	 * Obtiene role type.
	 * 
	 * @param roleType
	 *            role type
	 * @return Role type
	 */
	public static int getRoleType(String roleType) {
		if (roleType.equals(RoleConstants.TYPE_ORGANIZATION_LABEL)) {
			return RoleConstants.TYPE_ORGANIZATION;
		} else if (roleType.equals(RoleConstants.TYPE_SITE_LABEL)
				|| roleType.equals(_LEGACY_TYPE_COMMUNITY_LABEL)) {

			return RoleConstants.TYPE_SITE;
		} else {
			return RoleConstants.TYPE_REGULAR;
		}
	}

}
