// Variables con los colores utilizados en la web
$blanco: #ffffff;
$azul1: #10456f;
$azul2: #10628a;
$azul3: #1889ab;
$azul4: #41bbd0;
$azul4-40: lighten($azul4, 40%);
$azul5: #B3E4EC;
$azul6: #89c4d2;
$verde1: #177c30;
$verde2: #2d9534;
$verde3: #64ba3b;
$purpura: #72288b;
$amarillo: #e5a60d;
$rosa: #e4007b;
$gris: #999999;
$gris2: #f0f2f5;
$gris3:#3c4349;
$gris4:#666;

/* Desktop - Diseño 980 */
@media (max-width: 1199px) {

// AUI
.aui {
  // HEADER
  header {
	margin-bottom: 5px;
    // NAVIGATION
    #navigation {
      .navbar-inner {
        max-width:940px;
        padding:0;
        .brand {
          width:180px;
          padding:14px 0;
        }
        div.nav-collapse {
          width:580px;

          .nav {
            overflow:visible;
            li {
              padding:23px 15px;
              .dropdown-menu {
                top:66px;
              }
              .dropdown-content {
                .arrow-down {
                  top:48px;
                }
              }
            }
          }
        }
        .buscador_form {
          width:180px;
          padding:14px 0;
          &.open {
            margin-top:10px;
            .search_close {
              background-position:right 2px;
              padding:14px 0 22px;
              font-family:'Movistar_textregular';
              width:100%;
            }
          }
          .container-buscador input {
            width:108px;
            font-size:14px;
          }
        }
      }
    }
    // END NAVIGATION

    // BUSCADOR
    .contenedor-buscador {
      .container-fluid {
        .contenedor-buscador-main {
          .box-results {
            [class*='span'] {padding-left:10px;}
          }
          .box_form {
            .formulario-layer-buscador {
              .input-layer-buscador {
                width:85%;
                font-size:16px;
              }
              .submit-layer-buscador {
                width:15%;
                padding:0 20px;
              }
            }
          }
        }
      }
    }
    // END BUSCADOR

  .breadcrumbs {
      .breadcrumb{
        padding: 5px 2px;
      } // breadcrumb
  } // breadcrumbs


  }
  // END HEADER
  
  // Medidas genericas de los modulos
  .modulo-f, .modulo-f .banner{
    height: 240px!important;
  }
  .modulo-e, .modulo-e .banner, .modulo-e .banner .item{
    height: 365px!important;
  }
  .modulo-i, .modulo-i .banner{
    height: 110px!important;
  }

}
// END AUI
  
}


/* Desktop - Diseño 768 */
@media (max-width: 979px) {

// AUI
.aui {

  a.btn, button.btn, span.btn{width: auto;}
  // HEADER
  header {
	margin-bottom: 5px;
    // NAVIGATION
    #navigation {

      .navbar-inner {
        max-width:738px;
        padding:0;
        .brand {
          width:150px;
          padding:10px 0;
        }
        div.nav-collapse {
          width:438px;
        }
        .buscador_form{
          width:150px;
          padding:3px 0;
        }
      }

      div {
        &.nav-collapse, &.nav-collapse.collapse {
          height:auto;
          overflow:visible;
          .nav {
            font-size:16px;
            height:auto;
            li {
              padding:16px;
              .dropdown-menu {
                width:94%;
                top: 51px;
                margin-left:-47%;
                box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
                .item-submenu1 {
                  h5{font-size:16px;}
                  .sec-child-menu{
                    a {
                      &:link, &:visited {font-size: 14px;}
                    }
                  }
                  &.no-has-children {
                    a {
                      &:link, &:visited {font-size: 14px;}
                    }
                  }
                }
              }
              .dropdown-content {
                .arrow-down {
                  top:39px;
                }
              }
            }
          }
        }
    }

      .btn-navbar {display:none}

      .buscador_form {
        &.open {
          margin-top:7px;
          .search_close {padding:14px 0 10px;}
        }
      }
    }
    // END NAVIGATION

    // BUSCADOR
    .contenedor-buscador {
      .container-fluid {               
        .container-fluid {
          padding:0 10px 17px;
        }
        .contenedor-buscador-main{                  
          top:0;                 
          .box_results {
            > div{
              padding-left:10px;
              .ico_plus{float:right; cursor:pointer;}
            }
          }
        }                
        .box_form{
          > .span12 {margin-bottom:17px;}
          .formulario-layer-buscador{
            .input-layer-buscador {
              width:83%;
            }
            .submit-layer-buscador {
              width:17%;
              padding:3px 30px;
              text-align:left;
            }
          }                
        }
      }
    }
    // BUSCADOR

    .breadcrumbs {
        .breadcrumb{
          padding: 5px 2px;
        } // breadcrumb
    } // breadcrumbs

  }
  // END HEADER

  // Medidas genericas de los modulos
  .modulo-f, .modulo-f .banner{
    height: 200px!important;
  }
  .modulo-f.xs-modulo-f, .modulo-f.xs-modulo-f .banner{
    height: 300px!important;
  }
  .modulo-e, .modulo-e .banner{
    height: 300px!important;
  }

}
// END AUI

}

/* Desktop - Diseño 480 estatico */
@media (max-width: 767px) {

// AUI
.aui {

  // HEADER
  header {
    margin-bottom: 5px;
    //NAVIGATION
    .navegacion-secundaria{
      .menu_usuario{
        padding-left: 5px;
      }
      .menu_mi_movistar{
        padding-right: 5px;
      }
    }
    #navigation {
      height:45px;
      position:relative;
      .navbar-inner {
        padding:right;
        max-width:480px;

        .brand {
          padding:7px 0;
          margin-left: 5px;
        }

        .buscador_form {display:none}

        .btn_search {
          display:block;
          background:url(../images/latam/ico_search_mobile.png) no-repeat 0 0 transparent;
          width:45px;
          height: 45px;
          float:right;
          border:none;
          border-radius:0;
          margin:0;
          padding:0;
          outline: none;
          &.open{background:url(../images/latam/ico_search_mobile_open.png) no-repeat 0 0 transparent;}
        }

        .btn-navbar{
          font-family: "Movistar_headlineregular";
          outline: none;
          display:block;
          float:right;
          width:auto;
          border:none;
          background:url(../images/latam/ico_arrow_white.png) no-repeat 75px 21px $azul3;
          box-shadow: none;
          padding:12px 30px 13px 20px;
          margin:0;
          border-radius:0;
          font-size:18px;
          &.collapsed{background-color: $azul2;}
        }

        div {
          &.nav-collapse, &.nav-collapse.collapse{
            z-index:100;
            clear:both;
            width:100%;
            background:$azul3;
            position:absolute;
            top: 45px;
            left: 0px;
            overflow: hidden;
            height:0;
            border-bottom:1px solid $blanco;
            .nav {
              text-align:left;
              position:relative;
              top:0;
              left:0;
              font-size: 18px;
              height:0;

              .col-1,.col-2, .col-3, .col-4{
                  .dropdown-menu{
                    .item-submenu1{width:100%}
                  }
              }

              li {
                float: left;
                width:100%;
                border-top:1px solid #fff;
                padding: 0px;
                background:none;
                cursor: pointer;

                .dropdown-content {
                  .arrow-down {
                    background: url(../images/latam/ico_arrow_white_right.png) no-repeat 86% bottom;
                    float: right;
                    height: 20px;
                    width: 30%;
                    position:static; 
                    display: block;
                    &.down{background: url(../images/latam/ico_arrow_white.png) no-repeat 86% bottom;}  
                  }
                }

                &.open_mov{
                  .arrow-down{background: url(../images/latam/ico_arrow_white.png) no-repeat 86% bottom;}
                }

                &:first-child{border:none;}

                a{
                  &:link, &:visited{
                    padding:6px 20px;
                    float: left;
                  }
                  &:hover{text-decoration:none;}
                }

                // SUBMENUS
                .dropdown-menu {
                  background:$azul5;
                  padding:0;
                  position:relative!important; 
                  left:0!important; 
                  display:none; 
                  min-width:inherit;
                  float: left;
                  width:100%;
                  top:0!important;
                  margin:0!important;
                  border:none;
                  .item-submenu1{
                    border-top:1px solid $azul3;
                    float: left;
                    width:100%;
                    padding:0; 
                    margin:0;

                    h5{
                      margin:0;
                      font-size: 16px;
                      padding:5px 20px;
                      position:static;
                      background: url("../images/latam/ico_arrow_blue4_small_right.png") no-repeat 96% center;
                      &.down{background: url("../images/latam/ico_arrow_blue4_small.png") no-repeat 96% center;}
                    }

                    .sec-child-menu{
                      float: left;
                      width:100%;
                      padding:0;
                      display: none;                        
                      a {
                        &:link, &:visited{background:none;}
                      }
                      .item-submenu2{
                        border:none;
                        padding:0;                          
                        a{
                          padding:5px 20px;
                          display: block;
                          width:100%;
                        }
                      }                        
                    }

                    &:first-child{border:none;}

                    &.no-has-children{
                      border-top:1px solid $azul3;
                      float: left;
                      width:100%!important;
                      padding:0; 
                      margin:0;
                      a{
                        &:link, &:visited{
                          font-size: 16px;
                          font-weight: bold;
                          color: $azul1;
                        }
                      }
                    }

                    &.open_mov2{
                      h5{background: url('../images/latam/ico_arrow_blue4_small.png') no-repeat 96% center;}
                    }                    
                  }
                }

                &.open {
                  .dropdown-content {
                    .arrow-down{display:block;}
                  }
                  .dropdown-menu{
                      display: none;
                      li.item-submenu1{display:block;}
                  }
                }

                &.open_mov{
                  a{
                    &.dropdown-toggle:link, &.dropdown-toggle:visited{
                      font-family:'Movistar_headlineregular'; 
                      color:$azul5; 
                      font-weight:bold;
                    }
                  }
                }
                // END SUBMENUS

              }              
            }
          } 
        }
      }
    }
    //END NAVIGATION

    // NAVEGACION SECUNDARIA
    .navegacion-secundaria{
      .list-nav-sec{
        display: block;
        font-weight: bold;
        font-size: 14px;
        color:$azul2;
        cursor:pointer;
        span{
          background: url("../images/latam/ico_arrow_blue4_right.png") no-repeat right center;
          padding-right: 18px;
          &.down{
              background: url("../images/latam/ico_arrow_blue4.png") no-repeat right center;
          }
        }
      }
      .menu_usuario {
        position:relative;
        .tipo-usuario {
          display: none;
          position:absolute; 
          padding:10px;
          border:1px solid $azul5;
          background:$blanco;
          z-index: 1000;
          cursor: pointer;
          width:120px;
          top:27px;
          left: 0;
          li{
            float: none;
            background: none;
            padding: 0 0 5px 0px;
          }             
        }
      }
      .menu_mi_movistar {
        position:relative;
        .tipo-mimovistar {
          display: none;
          position:absolute;
          border:1px solid $azul5;
          background:$blanco;
          z-index: 1000;
          cursor: pointer;
          width:141px;
          top:27px;
          right: 0;
          li {
            float:left;
            width:100%;
            padding:10px;
            .mimovistar {
              width:100%;
              display:block;
              padding:0 10px 0 0;
              text-align:left;
            }
            &.li_recarga {
              padding:0;
            .recarga {
              margin:0;
              width:100%;
              background-position:100px center;
              padding:7px 53px 7px 16px;
            }             
            }
          }
        }
      }
    }
    // END NAVEGACION SECUNDARIA
  
    // BUSCADOR
    .contenedor-buscador {
      .container-fluid {

        .contenedor-buscador-main {  
          top:-5px;
             
          .box_form {
            > .span12 {margin-bottom:14px;}
            .formulario-layer-buscador {
              padding:7px 14px;      
              .input-layer-buscador {font-size:14px; width:80%;}
              .submit-layer-buscador {
                background:$azul5;
                color:$azul1;
                box-shadow:none;
                text-shadow:none;
                padding:3px 0;
                text-align:right;
                width:20%;
              }
            }
          }

          .box_results [class*='span'] {
            width:100%!important;
            padding:0;
            margin-top:10px;
            &:first-child {margin-top:0;}
            > div {
              .resultados-busqueda {
                height:auto;
                max-height:220px;
              }
            }
          }          
        }

        .container-fluid {
          padding:0 14px 14px;
        }
      }
    }
    //END BUSCADOR

    .breadcrumbs {
      .breadcrumb{
        padding: 5px 2px;
      } // breadcrumb
  } // breadcrumbs

  }
  // END HEADER

  // FOOTER
  #footer {
    position:relative;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 15px;

    .row-fluid {
      .redes-sociales {
       	width:50%!important;
      	span {
        	display:none; 
      	}         
      } // redes-sociales
    
    .ligas {
    padding-top: 0;
        width:100%!important;
      }
      .logo-telefonica {
      padding-top: 0;
      position:absolute;
      top:20px;
      right: 10px;
      }
      
    }
  }


  // Medidas genericas de los modulos
  .modulo-f, .modulo-f .banner{
    height: 170px!important;
  }
  .home .modulo-f, .home .modulo-f .banner{
    height: 425px !important;
  }
  .modulo-e, .modulo-e .banner, .modulo-e .banner .item{
    height: 300px!important;
  }
  .modulo-i, .modulo-i .banner{
    height: auto!important;
  }

}
// END AUI

}


