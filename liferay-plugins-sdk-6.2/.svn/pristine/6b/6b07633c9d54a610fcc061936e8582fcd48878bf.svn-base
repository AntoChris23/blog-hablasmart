/*
 * 
 */
package com.movistar.latam.colportal.hook.portlet.layoutsadmin.action;

import com.liferay.portal.NoSuchGroupException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutConstants;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.movistar.latam.colportal.hook.portlet.layoutsadmin.JsonUtil;
import com.movistar.latam.colportal.hook.portlet.layoutsadmin.Sitemap;
import com.movistar.latam.colportal.hook.portlet.layoutsadmin.SitemapNode;
import com.movistar.latam.colportal.hook.portlet.layoutsadmin.SyncPropsUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.glassfish.jersey.client.ClientProperties;

/**
 * SynchronizeLayoutsAction:.
 * 
 * @author VASS
 * @version 1.0
 */
public class SynchronizeLayoutsAction extends BaseStrutsAction {

	/** _log. */
	private static Log _log = LogFactoryUtil
			.getLog(SynchronizeLayoutsAction.class);

	/**
	 * StatusSync:.
	 * 
	 * @author VASS
	 * @version 1.0
	 */
	private enum StatusSync {

		/** Ok. */
		OK,
		/** Error. */
		ERROR,
		/** Warn. */
		WARN;
	}

	/**
	 * CatalogVersion:.
	 * 
	 * @author VASS
	 * @version 1.0
	 */
	private enum CatalogVersion {

		/** Staged. */
		STAGED("Staged"),
		/** Online. */
		ONLINE("Online");

		/** Key. */
		private String key;

		/**
		 * Instancia catalog version.
		 * 
		 * @param key
		 *            key
		 */
		private CatalogVersion(String key) {
			this.key = key;
		}

		/**
		 * Obtiene key.
		 * 
		 * @return Key
		 */
		public String getKey() {
			return key;
		}
	}

	/**
	 * SitemapType:.
	 * 
	 * @author VASS
	 * @version 1.0
	 */
	private enum SitemapType {

		/** Menu navigation. */
		MENU_NAVIGATION("menu-navigation"),
		/** Footer navigation. */
		FOOTER_NAVIGATION("footer-navigation"),
		/** Footer social. */
		FOOTER_SOCIAL("footer-social"),
		/** Portal links */
		HEADER_NAVIGATION("header-navigation");

		/** Key. */
		private String key;

		/**
		 * Instancia sitemap type.
		 * 
		 * @param key
		 *            key
		 */
		private SitemapType(String key) {
			this.key = key;
		}

		/**
		 * Obtiene key.
		 * 
		 * @return Key
		 */
		public String getKey() {
			return key;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.liferay.portal.kernel.struts.BaseStrutsAction#execute(javax.servlet
	 * .http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		boolean staging = ParamUtil.getBoolean(request, "staging", false);
		long groupId = ParamUtil.getLong(request, "groupId", 0);

		ThemeDisplay themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);
		if (_log.isInfoEnabled()) {
			_log.info("Sincronizando sitemap");
		}
		Map<StatusSync, List<String>> synchronizeSitemaps = synchronizeSitemaps(
				groupId, themeDisplay, staging);
		PrintWriter writer = response.getWriter();
		StringBuilder messageBuilder = getMessagesHTML(themeDisplay,
				synchronizeSitemaps);
		writer.write(messageBuilder.toString());
		return super.execute(request, response);
	}

	/**
	 * Obtiene messages html.
	 * 
	 * @param themeDisplay
	 *            theme display
	 * @param synchronizeSitemaps
	 *            synchronize sitemaps
	 * @return Messages html
	 */
	private StringBuilder getMessagesHTML(ThemeDisplay themeDisplay,
			Map<StatusSync, List<String>> synchronizeSitemaps) {
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("<div id=\"sync-messages\">");
		for (Entry<StatusSync, List<String>> entries : synchronizeSitemaps
				.entrySet()) {
			String alertClass = "alert-success";
			if (entries.getKey().equals(StatusSync.ERROR)) {
				alertClass = "alert-error";
			}
			for (String message : entries.getValue()) {
				messageBuilder.append("<div class=\"alert " + alertClass
						+ "\">"
						+ LanguageUtil.get(themeDisplay.getLocale(), message)
						+ "</div>");
			}
		}
		messageBuilder.append("</div>");
		return messageBuilder;
	}

	/**
	 * Construye sitemap node.
	 * 
	 * @param layout
	 *            layout
	 * @return sitemap node
	 */
	private SitemapNode buildSitemapNode(Layout layout) {
		SitemapNode sitemapNode = new SitemapNode();

		if (layout.getType().equals("shop")
				|| layout.getType().equals(LayoutConstants.TYPE_URL)) {
			sitemapNode.setFriendlyURL(layout.getTypeSettingsProperty("url"));
		} else {
			sitemapNode.setFriendlyURL(layout.getFriendlyURL());
		}

		sitemapNode.setNameMap(layout.getNameMap());
		sitemapNode.setTitleMap(layout.getTitleMap());
		sitemapNode.setPriority(layout.getPriority());
		sitemapNode.setTarget(layout.getTarget());

		if (layout.getIconImage()) {
			StringBuilder imageUrl = new StringBuilder(
					"/image/layout_icon?img_id=").append(layout
					.getIconImageId());
			sitemapNode.setImageURL(imageUrl.toString());
		}
		return sitemapNode;
	}

	/**
	 * Synchronize sitemaps.
	 * 
	 * @param themeDisplay
	 *            theme display
	 * @param staging
	 *            staging
	 * @return map
	 * @throws SystemException
	 *             system exception
	 * @throws PortalException
	 *             portal exception
	 */
	private Map<StatusSync, List<String>> synchronizeSitemaps(long groupId,
			ThemeDisplay themeDisplay, boolean staging) throws SystemException,
			PortalException {

		Locale locale = themeDisplay.getLocale();
		// Si no tiene staging cogemos la estructura de live
		Group stagingGroup = null;
		Long stagingGroupId = null;
		List<Layout> layouts = new ArrayList<Layout>();
		if (staging == true) {
			try {
				if (GroupLocalServiceUtil.hasStagingGroup(groupId)) {
					stagingGroup = GroupLocalServiceUtil
							.getStagingGroup(groupId);
					stagingGroupId = staging ? stagingGroup.getGroupId()
							: groupId;
				}
			} catch (NoSuchGroupException e) {
				if (_log.isErrorEnabled()) {
					_log.error("Error getting staging", e);
				}
			}
			layouts = LayoutLocalServiceUtil.getLayouts(stagingGroupId, false,
					0);
		} else {
			layouts = LayoutLocalServiceUtil.getLayouts(groupId, false, 0);
		}

		Map<StatusSync, List<String>> statusMessages = new HashMap<SynchronizeLayoutsAction.StatusSync, List<String>>();

		Sitemap menuSitemap = new Sitemap();
		Sitemap footerSitemap = new Sitemap();
		Sitemap rrssSitemap = new Sitemap();
		Sitemap headerSitemap = buildHeaderSitemap(themeDisplay);

		for (Layout layout : layouts) {
			if (!layout.isHidden()) {
				if (!layout.getName(locale).equalsIgnoreCase("home")) {
					SitemapNode sitemapNode = buildSitemapNode(layout);
					for (Layout child : layout.getChildren()) {
						if (!child.isHidden()) {
							SitemapNode childNode = buildSitemapNode(child);
							sitemapNode.getChildren().add(childNode);
							if (child.hasChildren()
									&& child.getType()
											.equalsIgnoreCase("group")) {
								for (Layout thirdChild : child.getChildren()) {
									if (!thirdChild.isHidden()) {
										childNode.getChildren().add(
												buildSitemapNode(thirdChild));
									}
								}
							}
						}
					}
					if (layout.getName(locale).equalsIgnoreCase("footer")) {
						for (Layout child : layout.getChildren()) {
							if (!child.isHidden()) {
								SitemapNode childNode = buildSitemapNode(child);
								footerSitemap.getNodes().add(childNode);
							}
						}
					} else if (layout.getName(locale).equalsIgnoreCase("rrss")) {
						for (Layout child : layout.getChildren()) {
							if (!child.isHidden()) {
								SitemapNode childNode = buildSitemapNode(child);
								rrssSitemap.getNodes().add(childNode);
							}
						}
					} else {
						menuSitemap.getNodes().add(sitemapNode);
					}
				}
			}
		}

		CatalogVersion catalogVersion = staging ? CatalogVersion.STAGED
				: CatalogVersion.ONLINE;

		boolean synchronizeHeader = synchronize(menuSitemap, catalogVersion,
				SitemapType.MENU_NAVIGATION);
		if (synchronizeHeader) {
			addMessage(statusMessages, StatusSync.OK,
					"menu-navigation-ok-message");
		} else {
			addMessage(statusMessages, StatusSync.ERROR,
					"menu-navigation-error-message");
		}

		boolean synchronizeFooter = synchronize(footerSitemap, catalogVersion,
				SitemapType.FOOTER_NAVIGATION);
		if (synchronizeFooter) {
			addMessage(statusMessages, StatusSync.OK,
					"footer-navigation-ok-message");
		} else {
			addMessage(statusMessages, StatusSync.ERROR,
					"footer-navigation-error-message");
		}

		boolean synchronizeFooterSocial = synchronize(rrssSitemap,
				catalogVersion, SitemapType.FOOTER_SOCIAL);
		if (synchronizeFooterSocial) {
			addMessage(statusMessages, StatusSync.OK,
					"footer-social-ok-message");
		} else {
			addMessage(statusMessages, StatusSync.ERROR,
					"footer-social-error-message");
		}

		boolean synchronizeHeaderNavigation = synchronize(headerSitemap,
				catalogVersion, SitemapType.HEADER_NAVIGATION);
		if (synchronizeHeaderNavigation) {
			addMessage(statusMessages, StatusSync.OK,
					"header-navigation-ok-message");
		} else {
			addMessage(statusMessages, StatusSync.ERROR,
					"headeer-navigation-error-message");
		}

		return statusMessages;
	}

	private Sitemap buildHeaderSitemap(ThemeDisplay themeDisplay) {
		Sitemap headerSitemap = new Sitemap();
		SitemapNode particularesNode = new SitemapNode();
		Locale localeES = new Locale("es");
		Locale localeEN = new Locale("en");

		Map<Locale, String> particualesNameMap = new HashMap<Locale, String>();
		particualesNameMap.put(localeES,
				LanguageUtil.get(localeES, "site.persons"));
		particualesNameMap.put(localeEN,
				LanguageUtil.get(localeEN, "site.persons"));

		Map<Locale, String> particualesTitleMap = new HashMap<Locale, String>();
		particualesTitleMap.put(localeES,
				LanguageUtil.get(localeES, "site.business"));
		particualesTitleMap.put(localeEN,
				LanguageUtil.get(localeEN, "site.business"));

		particularesNode.setNameMap(particualesNameMap);
		particularesNode.setTitleMap(particualesTitleMap);
		particularesNode.setPriority(0);
		particularesNode.setFriendlyURL("/");
		headerSitemap.getNodes().add(particularesNode);

		String businessUrl = themeDisplay
				.getThemeSetting("navigation-business-url");
		if (businessUrl != null && !businessUrl.isEmpty()) {
			SitemapNode businessNode = new SitemapNode();
			Map<Locale, String> businessNameMap = new HashMap<Locale, String>();
			businessNameMap.put(localeES,
					LanguageUtil.get(localeES, "site.business"));
			businessNameMap.put(localeEN,
					LanguageUtil.get(localeEN, "site.business"));

			Map<Locale, String> businessTitleMap = new HashMap<Locale, String>();
			businessTitleMap.put(localeES,
					LanguageUtil.get(localeES, "site.business"));
			businessTitleMap.put(localeEN,
					LanguageUtil.get(localeEN, "site.business"));

			businessNode.setNameMap(businessNameMap);
			businessNode.setTitleMap(businessTitleMap);
			businessNode.setPriority(1);
			businessNode.setFriendlyURL(businessUrl);
			headerSitemap.getNodes().add(businessNode);
		}

		String companiesUrl = themeDisplay
				.getThemeSetting("navigation-companies-url");
		if (companiesUrl != null && !companiesUrl.isEmpty()) {
			SitemapNode companiesNode = new SitemapNode();
			Map<Locale, String> companiesNameMap = new HashMap<Locale, String>();
			companiesNameMap.put(localeES,
					LanguageUtil.get(localeES, "site.companies"));
			companiesNameMap.put(localeEN,
					LanguageUtil.get(localeEN, "site.companies"));

			Map<Locale, String> companiesTitleMap = new HashMap<Locale, String>();
			companiesTitleMap.put(localeES,
					LanguageUtil.get(localeES, "site.companies"));
			companiesTitleMap.put(localeEN,
					LanguageUtil.get(localeEN, "site.companies"));

			companiesNode.setNameMap(companiesNameMap);
			companiesNode.setTitleMap(companiesTitleMap);
			companiesNode.setPriority(2);
			companiesNode.setFriendlyURL(companiesUrl);
			headerSitemap.getNodes().add(companiesNode);
		}
		return headerSitemap;
	}

	/**
	 * Añade message.
	 * 
	 * @param statusMessages
	 *            status messages
	 * @param status
	 *            status
	 * @param messageKey
	 *            message key
	 */
	private void addMessage(Map<StatusSync, List<String>> statusMessages,
			StatusSync status, String messageKey) {
		if (statusMessages.containsKey(status)) {
			statusMessages.get(status).add(messageKey);
		} else {
			List<String> messages = new ArrayList<String>();
			messages.add(messageKey);
			statusMessages.put(status, messages);
		}
	}

	/**
	 * Synchronize.
	 * 
	 * @param sitemap
	 *            sitemap
	 * @param catalogVersion
	 *            catalog version
	 * @param sitemapType
	 *            sitemap type
	 * @return true, si es correcto. false en caso contrario
	 */
	private boolean synchronize(Sitemap sitemap, CatalogVersion catalogVersion,
			SitemapType sitemapType) {
		boolean status = false;
		try {
			String json = JsonUtil.fromObjectToJson(sitemap);

			if (json != null && !json.isEmpty()) {
				if (_log.isDebugEnabled()) {
					_log.debug(sitemapType.name() + " json: " + json);
				}
				Client client = ClientBuilder.newClient();
				String restBaseURL = PrefsPropsUtil
						.getString(SyncPropsUtil.SITEMAP_SYNC_REST_URL);
				String catalogId = PrefsPropsUtil
						.getString(SyncPropsUtil.SITEMAP_SYNC_CATALOG_ID);
				Integer connectTimeout = PrefsPropsUtil
						.getInteger(SyncPropsUtil.SITEMAP_SYNC_CONNECT_TIMEOUT);
				Integer readTimeout = PrefsPropsUtil
						.getInteger(SyncPropsUtil.SITEMAP_SYNC_READ_TIMEOUT);
				client.property(ClientProperties.CONNECT_TIMEOUT,
						connectTimeout);
				client.property(ClientProperties.READ_TIMEOUT, readTimeout);
				if (restBaseURL != null && !restBaseURL.isEmpty()) {
					WebTarget target = client
							.target(restBaseURL)
							.path("/catalogs/{catalogId}/{catalogVersion}/{siteMap}")
							.resolveTemplate("catalogId", catalogId)
							.resolveTemplate("catalogVersion",
									catalogVersion.getKey())
							.resolveTemplate("siteMap", sitemapType.getKey());
					if (_log.isDebugEnabled()) {
						_log.debug("Sync URL: " + target.getUri().toString());
					}
					Invocation.Builder request = target.request();
					Invocation putRequest = request.buildPut(Entity.entity(
							json, "application/json; charset=UTF-8"));

					Response response = putRequest.invoke();
					if (response.getStatus() == 200) {
						status = true;
					}
				}
			}
		} catch (JsonGenerationException | JsonMappingException e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error parseando json: ", e);
			}
			status = false;
		} catch (IOException e) {
			if (_log.isWarnEnabled()) {
				_log.warn("Error de conexión: ", e);
			}
			status = false;
		} catch (SystemException e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error leyendo propiedad ", e);
			}
			status = false;
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error de conexión ", e);
			}
			status = false;
		}
		return status;
	}
}
