/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.movistar.latam.colportal.homephone.model.Sva;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Sva in entity cache.
 *
 * @author vass
 * @see Sva
 * @generated
 */
public class SvaCacheModel implements CacheModel<Sva>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{id=");
		sb.append(id);
		sb.append(", idSvaTipo=");
		sb.append(idSvaTipo);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", fecha=");
		sb.append(fecha);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Sva toEntityModel() {
		SvaImpl svaImpl = new SvaImpl();

		svaImpl.setId(id);
		svaImpl.setIdSvaTipo(idSvaTipo);

		if (nombre == null) {
			svaImpl.setNombre(StringPool.BLANK);
		}
		else {
			svaImpl.setNombre(nombre);
		}

		if (fecha == Long.MIN_VALUE) {
			svaImpl.setFecha(null);
		}
		else {
			svaImpl.setFecha(new Date(fecha));
		}

		svaImpl.resetOriginalValues();

		return svaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		idSvaTipo = objectInput.readLong();
		nombre = objectInput.readUTF();
		fecha = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);
		objectOutput.writeLong(idSvaTipo);

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		objectOutput.writeLong(fecha);
	}

	public long id;
	public long idSvaTipo;
	public String nombre;
	public long fecha;
}