/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.movistar.latam.colportal.homephone.model.LB;

/**
 * The persistence interface for the l b service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see LBPersistenceImpl
 * @see LBUtil
 * @generated
 */
public interface LBPersistence extends BasePersistence<LB> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LBUtil} to access the l b persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the l b where nombre = &#63; or throws a {@link com.movistar.latam.colportal.homephone.NoSuchLBException} if it could not be found.
	*
	* @param nombre the nombre
	* @return the matching l b
	* @throws com.movistar.latam.colportal.homephone.NoSuchLBException if a matching l b could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.LB findByNombre(
		java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchLBException;

	/**
	* Returns the l b where nombre = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param nombre the nombre
	* @return the matching l b, or <code>null</code> if a matching l b could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.LB fetchByNombre(
		java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the l b where nombre = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param nombre the nombre
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching l b, or <code>null</code> if a matching l b could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.LB fetchByNombre(
		java.lang.String nombre, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the l b where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	* @return the l b that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.LB removeByNombre(
		java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchLBException;

	/**
	* Returns the number of l bs where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching l bs
	* @throws SystemException if a system exception occurred
	*/
	public int countByNombre(java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the l b in the entity cache if it is enabled.
	*
	* @param lb the l b
	*/
	public void cacheResult(com.movistar.latam.colportal.homephone.model.LB lb);

	/**
	* Caches the l bs in the entity cache if it is enabled.
	*
	* @param lbs the l bs
	*/
	public void cacheResult(
		java.util.List<com.movistar.latam.colportal.homephone.model.LB> lbs);

	/**
	* Creates a new l b with the primary key. Does not add the l b to the database.
	*
	* @param id the primary key for the new l b
	* @return the new l b
	*/
	public com.movistar.latam.colportal.homephone.model.LB create(long id);

	/**
	* Removes the l b with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the l b
	* @return the l b that was removed
	* @throws com.movistar.latam.colportal.homephone.NoSuchLBException if a l b with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.LB remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchLBException;

	public com.movistar.latam.colportal.homephone.model.LB updateImpl(
		com.movistar.latam.colportal.homephone.model.LB lb)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the l b with the primary key or throws a {@link com.movistar.latam.colportal.homephone.NoSuchLBException} if it could not be found.
	*
	* @param id the primary key of the l b
	* @return the l b
	* @throws com.movistar.latam.colportal.homephone.NoSuchLBException if a l b with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.LB findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchLBException;

	/**
	* Returns the l b with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the l b
	* @return the l b, or <code>null</code> if a l b with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.LB fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the l bs.
	*
	* @return the l bs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.LB> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the l bs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.LBModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of l bs
	* @param end the upper bound of the range of l bs (not inclusive)
	* @return the range of l bs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.LB> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the l bs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.LBModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of l bs
	* @param end the upper bound of the range of l bs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of l bs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.LB> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the l bs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of l bs.
	*
	* @return the number of l bs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}