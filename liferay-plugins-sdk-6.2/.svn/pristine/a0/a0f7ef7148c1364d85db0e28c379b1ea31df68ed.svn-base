/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.movistar.latam.colportal.homephone.model.Matriz;

/**
 * The persistence interface for the matriz service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see MatrizPersistenceImpl
 * @see MatrizUtil
 * @generated
 */
public interface MatrizPersistence extends BasePersistence<Matriz> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MatrizUtil} to access the matriz persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the matrizs where divipola = &#63;.
	*
	* @param divipola the divipola
	* @return the matching matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findByCiudad(
		long divipola)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the matrizs where divipola = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MatrizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param divipola the divipola
	* @param start the lower bound of the range of matrizs
	* @param end the upper bound of the range of matrizs (not inclusive)
	* @return the range of matching matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findByCiudad(
		long divipola, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the matrizs where divipola = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MatrizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param divipola the divipola
	* @param start the lower bound of the range of matrizs
	* @param end the upper bound of the range of matrizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findByCiudad(
		long divipola, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first matriz in the ordered set where divipola = &#63;.
	*
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching matriz
	* @throws com.movistar.latam.colportal.homephone.NoSuchMatrizException if a matching matriz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz findByCiudad_First(
		long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMatrizException;

	/**
	* Returns the first matriz in the ordered set where divipola = &#63;.
	*
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching matriz, or <code>null</code> if a matching matriz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz fetchByCiudad_First(
		long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last matriz in the ordered set where divipola = &#63;.
	*
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching matriz
	* @throws com.movistar.latam.colportal.homephone.NoSuchMatrizException if a matching matriz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz findByCiudad_Last(
		long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMatrizException;

	/**
	* Returns the last matriz in the ordered set where divipola = &#63;.
	*
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching matriz, or <code>null</code> if a matching matriz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz fetchByCiudad_Last(
		long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the matrizs before and after the current matriz in the ordered set where divipola = &#63;.
	*
	* @param id the primary key of the current matriz
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next matriz
	* @throws com.movistar.latam.colportal.homephone.NoSuchMatrizException if a matriz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz[] findByCiudad_PrevAndNext(
		long id, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMatrizException;

	/**
	* Removes all the matrizs where divipola = &#63; from the database.
	*
	* @param divipola the divipola
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCiudad(long divipola)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of matrizs where divipola = &#63;.
	*
	* @param divipola the divipola
	* @return the number of matching matrizs
	* @throws SystemException if a system exception occurred
	*/
	public int countByCiudad(long divipola)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the matrizs where divipola = &#63; and estrato = &#63;.
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @return the matching matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findBySearchMatriz(
		long divipola, java.lang.String estrato)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the matrizs where divipola = &#63; and estrato = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MatrizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @param start the lower bound of the range of matrizs
	* @param end the upper bound of the range of matrizs (not inclusive)
	* @return the range of matching matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findBySearchMatriz(
		long divipola, java.lang.String estrato, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the matrizs where divipola = &#63; and estrato = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MatrizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @param start the lower bound of the range of matrizs
	* @param end the upper bound of the range of matrizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findBySearchMatriz(
		long divipola, java.lang.String estrato, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first matriz in the ordered set where divipola = &#63; and estrato = &#63;.
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching matriz
	* @throws com.movistar.latam.colportal.homephone.NoSuchMatrizException if a matching matriz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz findBySearchMatriz_First(
		long divipola, java.lang.String estrato,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMatrizException;

	/**
	* Returns the first matriz in the ordered set where divipola = &#63; and estrato = &#63;.
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching matriz, or <code>null</code> if a matching matriz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz fetchBySearchMatriz_First(
		long divipola, java.lang.String estrato,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last matriz in the ordered set where divipola = &#63; and estrato = &#63;.
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching matriz
	* @throws com.movistar.latam.colportal.homephone.NoSuchMatrizException if a matching matriz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz findBySearchMatriz_Last(
		long divipola, java.lang.String estrato,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMatrizException;

	/**
	* Returns the last matriz in the ordered set where divipola = &#63; and estrato = &#63;.
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching matriz, or <code>null</code> if a matching matriz could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz fetchBySearchMatriz_Last(
		long divipola, java.lang.String estrato,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the matrizs before and after the current matriz in the ordered set where divipola = &#63; and estrato = &#63;.
	*
	* @param id the primary key of the current matriz
	* @param divipola the divipola
	* @param estrato the estrato
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next matriz
	* @throws com.movistar.latam.colportal.homephone.NoSuchMatrizException if a matriz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz[] findBySearchMatriz_PrevAndNext(
		long id, long divipola, java.lang.String estrato,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMatrizException;

	/**
	* Removes all the matrizs where divipola = &#63; and estrato = &#63; from the database.
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @throws SystemException if a system exception occurred
	*/
	public void removeBySearchMatriz(long divipola, java.lang.String estrato)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of matrizs where divipola = &#63; and estrato = &#63;.
	*
	* @param divipola the divipola
	* @param estrato the estrato
	* @return the number of matching matrizs
	* @throws SystemException if a system exception occurred
	*/
	public int countBySearchMatriz(long divipola, java.lang.String estrato)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the matriz in the entity cache if it is enabled.
	*
	* @param matriz the matriz
	*/
	public void cacheResult(
		com.movistar.latam.colportal.homephone.model.Matriz matriz);

	/**
	* Caches the matrizs in the entity cache if it is enabled.
	*
	* @param matrizs the matrizs
	*/
	public void cacheResult(
		java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> matrizs);

	/**
	* Creates a new matriz with the primary key. Does not add the matriz to the database.
	*
	* @param id the primary key for the new matriz
	* @return the new matriz
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz create(long id);

	/**
	* Removes the matriz with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the matriz
	* @return the matriz that was removed
	* @throws com.movistar.latam.colportal.homephone.NoSuchMatrizException if a matriz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMatrizException;

	public com.movistar.latam.colportal.homephone.model.Matriz updateImpl(
		com.movistar.latam.colportal.homephone.model.Matriz matriz)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the matriz with the primary key or throws a {@link com.movistar.latam.colportal.homephone.NoSuchMatrizException} if it could not be found.
	*
	* @param id the primary key of the matriz
	* @return the matriz
	* @throws com.movistar.latam.colportal.homephone.NoSuchMatrizException if a matriz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMatrizException;

	/**
	* Returns the matriz with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the matriz
	* @return the matriz, or <code>null</code> if a matriz with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Matriz fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the matrizs.
	*
	* @return the matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the matrizs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MatrizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of matrizs
	* @param end the upper bound of the range of matrizs (not inclusive)
	* @return the range of matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the matrizs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MatrizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of matrizs
	* @param end the upper bound of the range of matrizs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matrizs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Matriz> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the matrizs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of matrizs.
	*
	* @return the number of matrizs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}