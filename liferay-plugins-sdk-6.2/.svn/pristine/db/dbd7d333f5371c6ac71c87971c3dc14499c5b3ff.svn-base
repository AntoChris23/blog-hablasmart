/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author vass
 * @generated
 */
public class MatrizSoap implements Serializable {
	public static MatrizSoap toSoapModel(Matriz model) {
		MatrizSoap soapModel = new MatrizSoap();

		soapModel.setId(model.getId());
		soapModel.setDivipola(model.getDivipola());
		soapModel.setCodigoAtis(model.getCodigoAtis());
		soapModel.setEstrato(model.getEstrato());
		soapModel.setTipoMX(model.getTipoMX());
		soapModel.setPlanLB(model.getPlanLB());
		soapModel.setCodigoLB(model.getCodigoLB());
		soapModel.setValorLB(model.getValorLB());
		soapModel.setPlanBA(model.getPlanBA());
		soapModel.setCodigoBA(model.getCodigoBA());
		soapModel.setValorBA(model.getValorBA());
		soapModel.setSubsidioBA(model.getSubsidioBA());
		soapModel.setPlanTV(model.getPlanTV());
		soapModel.setCodigoTV(model.getCodigoTV());
		soapModel.setValorTV(model.getValorTV());
		soapModel.setTotalValor(model.getTotalValor());
		soapModel.setTiemPromDes(model.getTiemPromDes());
		soapModel.setDescuento(model.getDescuento());
		soapModel.setAplicaHD(model.getAplicaHD());

		return soapModel;
	}

	public static MatrizSoap[] toSoapModels(Matriz[] models) {
		MatrizSoap[] soapModels = new MatrizSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MatrizSoap[][] toSoapModels(Matriz[][] models) {
		MatrizSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MatrizSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MatrizSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MatrizSoap[] toSoapModels(List<Matriz> models) {
		List<MatrizSoap> soapModels = new ArrayList<MatrizSoap>(models.size());

		for (Matriz model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MatrizSoap[soapModels.size()]);
	}

	public MatrizSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public long getDivipola() {
		return _divipola;
	}

	public void setDivipola(long divipola) {
		_divipola = divipola;
	}

	public String getCodigoAtis() {
		return _codigoAtis;
	}

	public void setCodigoAtis(String codigoAtis) {
		_codigoAtis = codigoAtis;
	}

	public String getEstrato() {
		return _estrato;
	}

	public void setEstrato(String estrato) {
		_estrato = estrato;
	}

	public String getTipoMX() {
		return _tipoMX;
	}

	public void setTipoMX(String tipoMX) {
		_tipoMX = tipoMX;
	}

	public String getPlanLB() {
		return _planLB;
	}

	public void setPlanLB(String planLB) {
		_planLB = planLB;
	}

	public String getCodigoLB() {
		return _codigoLB;
	}

	public void setCodigoLB(String codigoLB) {
		_codigoLB = codigoLB;
	}

	public long getValorLB() {
		return _valorLB;
	}

	public void setValorLB(long valorLB) {
		_valorLB = valorLB;
	}

	public String getPlanBA() {
		return _planBA;
	}

	public void setPlanBA(String planBA) {
		_planBA = planBA;
	}

	public String getCodigoBA() {
		return _codigoBA;
	}

	public void setCodigoBA(String codigoBA) {
		_codigoBA = codigoBA;
	}

	public long getValorBA() {
		return _valorBA;
	}

	public void setValorBA(long valorBA) {
		_valorBA = valorBA;
	}

	public long getSubsidioBA() {
		return _subsidioBA;
	}

	public void setSubsidioBA(long subsidioBA) {
		_subsidioBA = subsidioBA;
	}

	public String getPlanTV() {
		return _planTV;
	}

	public void setPlanTV(String planTV) {
		_planTV = planTV;
	}

	public String getCodigoTV() {
		return _codigoTV;
	}

	public void setCodigoTV(String codigoTV) {
		_codigoTV = codigoTV;
	}

	public long getValorTV() {
		return _valorTV;
	}

	public void setValorTV(long valorTV) {
		_valorTV = valorTV;
	}

	public long getTotalValor() {
		return _totalValor;
	}

	public void setTotalValor(long totalValor) {
		_totalValor = totalValor;
	}

	public long getTiemPromDes() {
		return _tiemPromDes;
	}

	public void setTiemPromDes(long tiemPromDes) {
		_tiemPromDes = tiemPromDes;
	}

	public float getDescuento() {
		return _descuento;
	}

	public void setDescuento(float descuento) {
		_descuento = descuento;
	}

	public String getAplicaHD() {
		return _aplicaHD;
	}

	public void setAplicaHD(String aplicaHD) {
		_aplicaHD = aplicaHD;
	}

	private long _id;
	private long _divipola;
	private String _codigoAtis;
	private String _estrato;
	private String _tipoMX;
	private String _planLB;
	private String _codigoLB;
	private long _valorLB;
	private String _planBA;
	private String _codigoBA;
	private long _valorBA;
	private long _subsidioBA;
	private String _planTV;
	private String _codigoTV;
	private long _valorTV;
	private long _totalValor;
	private long _tiemPromDes;
	private float _descuento;
	private String _aplicaHD;
}