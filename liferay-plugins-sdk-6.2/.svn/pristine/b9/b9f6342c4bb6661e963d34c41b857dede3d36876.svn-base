/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.movistar.latam.colportal.homephone.model.SvaValor;

import java.util.List;

/**
 * The persistence utility for the sva valor service. This utility wraps {@link SvaValorPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see SvaValorPersistence
 * @see SvaValorPersistenceImpl
 * @generated
 */
public class SvaValorUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(SvaValor svaValor) {
		getPersistence().clearCache(svaValor);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SvaValor> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SvaValor> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SvaValor> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static SvaValor update(SvaValor svaValor) throws SystemException {
		return getPersistence().update(svaValor);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static SvaValor update(SvaValor svaValor,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(svaValor, serviceContext);
	}

	/**
	* Returns all the sva valors where idSva = &#63; and divipola = &#63;.
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @return the matching sva valors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.SvaValor> findBySvaByCiudad(
		long idSva, long divipola)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySvaByCiudad(idSva, divipola);
	}

	/**
	* Returns a range of all the sva valors where idSva = &#63; and divipola = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.SvaValorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @param start the lower bound of the range of sva valors
	* @param end the upper bound of the range of sva valors (not inclusive)
	* @return the range of matching sva valors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.SvaValor> findBySvaByCiudad(
		long idSva, long divipola, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySvaByCiudad(idSva, divipola, start, end);
	}

	/**
	* Returns an ordered range of all the sva valors where idSva = &#63; and divipola = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.SvaValorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @param start the lower bound of the range of sva valors
	* @param end the upper bound of the range of sva valors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sva valors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.SvaValor> findBySvaByCiudad(
		long idSva, long divipola, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBySvaByCiudad(idSva, divipola, start, end,
			orderByComparator);
	}

	/**
	* Returns the first sva valor in the ordered set where idSva = &#63; and divipola = &#63;.
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sva valor
	* @throws com.movistar.latam.colportal.homephone.NoSuchSvaValorException if a matching sva valor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor findBySvaByCiudad_First(
		long idSva, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchSvaValorException {
		return getPersistence()
				   .findBySvaByCiudad_First(idSva, divipola, orderByComparator);
	}

	/**
	* Returns the first sva valor in the ordered set where idSva = &#63; and divipola = &#63;.
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sva valor, or <code>null</code> if a matching sva valor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor fetchBySvaByCiudad_First(
		long idSva, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySvaByCiudad_First(idSva, divipola, orderByComparator);
	}

	/**
	* Returns the last sva valor in the ordered set where idSva = &#63; and divipola = &#63;.
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sva valor
	* @throws com.movistar.latam.colportal.homephone.NoSuchSvaValorException if a matching sva valor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor findBySvaByCiudad_Last(
		long idSva, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchSvaValorException {
		return getPersistence()
				   .findBySvaByCiudad_Last(idSva, divipola, orderByComparator);
	}

	/**
	* Returns the last sva valor in the ordered set where idSva = &#63; and divipola = &#63;.
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sva valor, or <code>null</code> if a matching sva valor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor fetchBySvaByCiudad_Last(
		long idSva, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySvaByCiudad_Last(idSva, divipola, orderByComparator);
	}

	/**
	* Returns the sva valors before and after the current sva valor in the ordered set where idSva = &#63; and divipola = &#63;.
	*
	* @param id the primary key of the current sva valor
	* @param idSva the id sva
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sva valor
	* @throws com.movistar.latam.colportal.homephone.NoSuchSvaValorException if a sva valor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor[] findBySvaByCiudad_PrevAndNext(
		long id, long idSva, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchSvaValorException {
		return getPersistence()
				   .findBySvaByCiudad_PrevAndNext(id, idSva, divipola,
			orderByComparator);
	}

	/**
	* Removes all the sva valors where idSva = &#63; and divipola = &#63; from the database.
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBySvaByCiudad(long idSva, long divipola)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBySvaByCiudad(idSva, divipola);
	}

	/**
	* Returns the number of sva valors where idSva = &#63; and divipola = &#63;.
	*
	* @param idSva the id sva
	* @param divipola the divipola
	* @return the number of matching sva valors
	* @throws SystemException if a system exception occurred
	*/
	public static int countBySvaByCiudad(long idSva, long divipola)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBySvaByCiudad(idSva, divipola);
	}

	/**
	* Caches the sva valor in the entity cache if it is enabled.
	*
	* @param svaValor the sva valor
	*/
	public static void cacheResult(
		com.movistar.latam.colportal.homephone.model.SvaValor svaValor) {
		getPersistence().cacheResult(svaValor);
	}

	/**
	* Caches the sva valors in the entity cache if it is enabled.
	*
	* @param svaValors the sva valors
	*/
	public static void cacheResult(
		java.util.List<com.movistar.latam.colportal.homephone.model.SvaValor> svaValors) {
		getPersistence().cacheResult(svaValors);
	}

	/**
	* Creates a new sva valor with the primary key. Does not add the sva valor to the database.
	*
	* @param id the primary key for the new sva valor
	* @return the new sva valor
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor create(
		long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the sva valor with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the sva valor
	* @return the sva valor that was removed
	* @throws com.movistar.latam.colportal.homephone.NoSuchSvaValorException if a sva valor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor remove(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchSvaValorException {
		return getPersistence().remove(id);
	}

	public static com.movistar.latam.colportal.homephone.model.SvaValor updateImpl(
		com.movistar.latam.colportal.homephone.model.SvaValor svaValor)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(svaValor);
	}

	/**
	* Returns the sva valor with the primary key or throws a {@link com.movistar.latam.colportal.homephone.NoSuchSvaValorException} if it could not be found.
	*
	* @param id the primary key of the sva valor
	* @return the sva valor
	* @throws com.movistar.latam.colportal.homephone.NoSuchSvaValorException if a sva valor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchSvaValorException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the sva valor with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the sva valor
	* @return the sva valor, or <code>null</code> if a sva valor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.SvaValor fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the sva valors.
	*
	* @return the sva valors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.SvaValor> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the sva valors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.SvaValorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sva valors
	* @param end the upper bound of the range of sva valors (not inclusive)
	* @return the range of sva valors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.SvaValor> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the sva valors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.SvaValorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sva valors
	* @param end the upper bound of the range of sva valors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of sva valors
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.SvaValor> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the sva valors from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of sva valors.
	*
	* @return the number of sva valors
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static SvaValorPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (SvaValorPersistence)PortletBeanLocatorUtil.locate(com.movistar.latam.colportal.homephone.service.ClpSerializer.getServletContextName(),
					SvaValorPersistence.class.getName());

			ReferenceRegistry.registerReference(SvaValorUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(SvaValorPersistence persistence) {
	}

	private static SvaValorPersistence _persistence;
}