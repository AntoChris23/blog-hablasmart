/*
 * 
 */
package com.movistar.latam.colportal.homephone.portlet;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.captcha.Captcha;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Http;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.movistar.latam.colportal.homephone.model.Solicitud;
import com.movistar.latam.colportal.homephone.model.impl.SolicitudImpl;
import com.movistar.latam.colportal.homephone.service.EstratoLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.LBLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.SolicitudLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.SvaLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.TipoMxLocalServiceUtil;
import com.movistar.latam.colportal.homephone.serviceWsdl.ServicePortTypeProxy;
import com.movistar.latam.colportal.homephone.util.HomePhoneMailUtil;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Enumeration;

import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;

// TODO: Auto-generated Javadoc
/**
 * HomePhonePortlet:.
 * 
 * @author VASS
 * @version 1.0
 */
public class HomePhonePortlet extends MVCPortlet {
	
	/** Log. */
	private static Log log = LogFactoryUtil.getLog(HomePhonePortlet.class);
	
	/** Captcha. */
	Captcha captcha;
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#serveResource(javax.portlet.ResourceRequest, javax.portlet.ResourceResponse)
	 */
	@Override
	public void serveResource(ResourceRequest resourceRequest,
    		ResourceResponse resourceResponse) throws IOException, PortletException {
    	try {
    		String jspPage = (String)resourceRequest.getParameter("jspPage");
    		Boolean CaptchaOK = false;
    		
    		if(jspPage != null){
    			
    			//Validamos el captcha
    			String enteredCaptchaText = resourceRequest.getParameter("param1");
    			
    			PortletSession session = resourceRequest.getPortletSession();
  		        String captchaText = (String)getCaptchaValueFromSession(session);
  		        
  		        HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);

    		    if (Validator.isNull(captchaText)) {
    		    	//Hay recaptcha
        			String reCaptchaChallenge = resourceRequest.getParameter("param2");
        			
        			Http.Options options = new Http.Options();

        			options.addPart("challenge", reCaptchaChallenge);

        			try {
        				options.addPart(
        					"privatekey",PrefsPropsUtil.getString("captcha.engine.recaptcha.key.private", PropsUtil.get("captcha.engine.recaptcha.key.private")));
        			}
        			catch (SystemException se) {
        			}
//        		    
        			options.addPart("remoteip", request.getRemoteAddr());
        			options.addPart("response", enteredCaptchaText);
        			options.setLocation(
        				HttpUtil.protocolize(
        						PropsUtil.get("captcha.engine.recaptcha.url.verify"),
        					request.isSecure()));
        			options.setPost(true);

        			String content = null;

        			try {
        				content = HttpUtil.URLtoString(options);
        			}
        			catch (IOException ioe) {
        				//_log.error(ioe, ioe);

        				throw new CaptchaTextException();
        			}
     
        			if (content == null) {
        				log.error("reCAPTCHA did not return a result");

        				throw new CaptchaTextException();
        			}

        			String[] messages = content.split("\r?\n");

        			if (messages.length < 1) {
        				log.error("reCAPTCHA did not return a valid result: " + content);

        				throw new CaptchaTextException();
        			}

        			CaptchaOK =  GetterUtil.getBoolean(messages[0]);
    		    	
    		    	
	    		    resourceRequest.setAttribute("checkCaptchaResult", CaptchaOK);
    		           
		        }else if (!captchaText.equals(enteredCaptchaText)) {
				        
		        	resourceRequest.setAttribute("checkCaptchaResult", false);
				    
		        }else{
		        	
			        resourceRequest.setAttribute("checkCaptchaResult", true);
					
			    }
    		    include(jspPage, resourceRequest, resourceResponse);
    			
    		}else{
    			captcha = com.liferay.portal.kernel.captcha.CaptchaUtil.getCaptcha();
        		
        		captcha.serveImage(resourceRequest, resourceResponse);
    		}
    		
    		
    	} catch (Exception e) {
    		log.error(e.getMessage());
    	}
    }
	
	
	/**
	 * Añade una nueva solicitud a la base de datos.
	 * 
	 * @param actionRequest
	 *            action request
	 * @param actionResponse
	 *            action response
	 * @throws IOException
	 *             ha ocurrido una excepción de I/O.
	 */
	public void addSolicitud(
	            ActionRequest actionRequest, ActionResponse actionResponse)
	        throws IOException {
		 
		 try {
			Solicitud solicitud  = solicitudFromRequest(actionRequest);
			JSONObject jsonObj = null;
			
			Boolean solicitudOK = false;
			
			//Se almacena en local la solicitud para tenerla registrada
			SolicitudLocalServiceUtil.addSolicitud(solicitud);
			
			PortletPreferences preferences = actionRequest.getPreferences();
			
			//Si se almacena correctamente en local se realiza la llamada al web service telefonía fija
			String responseWS = callWSColombiaFija(solicitud, preferences);
			
			if(responseWS.contains("codError")){
			
				jsonObj = new JSONObject(responseWS);
				
				if(jsonObj.get("codError") != null){
					Integer codError = (Integer) jsonObj.get("codError");
					if(codError == 0){
						solicitudOK = true;
					}
					
				}
				
			}
				
			if(solicitudOK){
				if (log.isDebugEnabled()) {
					log.debug("Llamada a webService OK:" + responseWS);
				}
				//Si no devuelve error la llamada al webService se envían las notificaciones
				sendConfirmatioEmail(solicitud, preferences);
							
				SessionMessages.add(actionRequest, "solicitud-added");
				sendRedirect(actionRequest, actionResponse);
			}else{
				
				log.error("Llamada a webService KO: " + responseWS);
				
				SessionErrors.add(actionRequest, "solicitud-error");
				sendRedirect(actionRequest, actionResponse);
			}
				
		} catch (SystemException e) {
			
			SessionErrors.add(actionRequest, "solicitud-error");
			sendRedirect(actionRequest, actionResponse);
			log.error("Error almacenando en BD");
			log.error(e.getMessage());
		} catch (JSONException e) {

			SessionErrors.add(actionRequest, "solicitud-error");
			sendRedirect(actionRequest, actionResponse);
			log.error("Error en el JSON del WS");
			log.error(e.getMessage());
		}catch (Exception e) {
			
			log.error("Error almacenando en DB");
			log.error(e.getMessage());
			SessionErrors.add(actionRequest, "solicitud-error");
			sendRedirect(actionRequest, actionResponse);
			
		}
		 

	}
	
	/**
	 * Realiza la llamada al Web Service con los datos de la solicitud.
	 * 
	 * @param solicitud
	 *            solicitud
	 * @param preferences
	 *            preferences
	 * @return string
	 */
	private String callWSColombiaFija(Solicitud solicitud, PortletPreferences preferences){
		
		String WSUrl =  HomePhoneMailUtil.getWSUrl(preferences);
		String WSUser =  HomePhoneMailUtil.getWSUser(preferences);
		String WSPassword =  HomePhoneMailUtil.getWSPassword(preferences);
		
		ServicePortTypeProxy servicePortTypeProxy = new ServicePortTypeProxy(WSUrl);
		String response="";
		try {
			response = servicePortTypeProxy.insertar(
					WSUser, 
					WSPassword, 
					solicitud.getNombres(), 
					solicitud.getApellidos(), 
					solicitud.getDireccion(), 
					solicitud.getDocumento(), 
					solicitud.getEmail(), 
					String.valueOf(solicitud.getTipodoc()), 
					solicitud.getBarrio(), 
					solicitud.getCelular(), 
					String.valueOf(solicitud.getDivipola()),
					String.valueOf(solicitud.getEstrato()), 
					String.valueOf(solicitud.getTelefono()), 
					String.valueOf(solicitud.getPlanLB()), 
					solicitud.getPlanBA(), 
					solicitud.getPlanTV(), 
					solicitud.getPlanHD(), 
					solicitud.getSvaInternet(), 
					solicitud.getSvaTelevision(), 
					solicitud.getDecodificador(), 
					String.valueOf(solicitud.getTotalPedido()), 
					String.valueOf(solicitud.getTotalConIVA()), 
					String.valueOf(solicitud.getCodigoLB()), 
					String.valueOf(solicitud.getCodigoBA()), 
					String.valueOf(solicitud.getCodigoPlanTV()), 
					String.valueOf(solicitud.getTipoMX()));
		
		} catch (RemoteException e) {
			log.error(e.getMessage());
		}
								
		return response;
	}
	 
	/**
	 * Método que envía un correo de confirmación del pedido.
	 * 
	 * @param solicitud
	 *            solicitud
	 * @param preferences
	 *            preferences
	 */
	private void sendConfirmatioEmail(Solicitud solicitud, PortletPreferences preferences){
	
		try{
		
			 InternetAddress fromAddress = new InternetAddress(HomePhoneMailUtil.getEmailFromAddress(preferences), HomePhoneMailUtil.getEmailFromName(preferences)); // from address
			 InternetAddress toAddress = new InternetAddress(HomePhoneMailUtil.getEmailToAddress(preferences));  // to address
			 InternetAddress userAddress = new InternetAddress(solicitud.getEmail());  // to address
			 
			 String body = HomePhoneMailUtil.replaceBody(HomePhoneMailUtil.getEmailBody(preferences), solicitud);
			 String subject = HomePhoneMailUtil.getEmailSubject(preferences);
			 
			 MailMessage mailMessage = new MailMessage();
	         mailMessage.setTo(userAddress);
	         mailMessage.setBCC(toAddress);
	         mailMessage.setFrom(fromAddress);
	         mailMessage.setSubject(subject);
	         mailMessage.setBody(body);
	         mailMessage.setHTMLFormat(true);
	        
	         MailServiceUtil.sendEmail(mailMessage); // Sending message
		}catch (Exception e) {
	            log.error("Error al enviar el mail de confirmación", e);
	         }
			
		} 
	

	 
	 /**
	 * Método que recupera los valores de la solicitud del formulario.
	 * 
	 * @param request
	 *            request
	 * @return solicitud
	 */
	private Solicitud solicitudFromRequest(PortletRequest request) {
	
		SolicitudImpl solicitud = new SolicitudImpl();
		
		String nombres = ParamUtil.getString(request, "nombre");
		String apellidos = ParamUtil.getString(request, "apellido");
		String direccion = ParamUtil.getString(request, "direccion");
		String documento = ParamUtil.getString(request, "documento");
		String email = ParamUtil.getString(request, "email");
		Long idTipoDocumento = ParamUtil.getLong(request, "idTipoDocumento");
		String barrio = ParamUtil.getString(request, "barrio");
		String celular = ParamUtil.getString(request, "celular");
		Long ciudadSel = ParamUtil.getLong(request, "divipolaSel");
		//Buscamos el id del estrato
		Long estrato = EstratoLocalServiceUtil.findByNombre(ParamUtil.getString(request, "estratoSel")).getId();
		String telefono = ParamUtil.getString(request, "telefono");
		//Buscamos el id de plan voz
		String planLBSel = ParamUtil.getString(request, "planLBSel");
		long idPlanLB = 0;
		if(Validator.isNotNull(planLBSel)){
			idPlanLB = LBLocalServiceUtil.findByNombre(planLBSel).getId();
		}
				
		String planBASel = ParamUtil.getString(request, "planBASel");
		if(Validator.isNull(planBASel)){
			planBASel = "NA";
		}
		
		String planTVSel = ParamUtil.getString(request, "planTVSel");
		if(Validator.isNull(planTVSel)){
			planTVSel = "NA";
		}
		

		String planHDSel = ParamUtil.getString(request, "planHDSel");
		if(Validator.isNull(planHDSel)){
			planHDSel = "NA";
		}
		
		String svaInternetSel = ParamUtil.getString(request, "svaInternetSel");
		if(Validator.isNull(planBASel) || Validator.isNull(svaInternetSel)){
			svaInternetSel = "NA";
		}
		
		String svaTelevisionSel = ParamUtil.getString(request, "svaTelevisionSel");
		if(Validator.isNull(planTVSel) || Validator.isNull(svaTelevisionSel)){
			svaTelevisionSel = "NA";
		}else{
			//Obtenemos los nombres de los svaTV
			    
			String[] svaTVArray = svaTelevisionSel.split(",");
			try {
				svaTelevisionSel = SvaLocalServiceUtil.getSva(Long.valueOf(svaTVArray[0])).getNombre();
				for(int i=1; i < svaTVArray.length; i++){
					svaTelevisionSel = svaTelevisionSel + "," + SvaLocalServiceUtil.getSva(Long.valueOf(svaTVArray[i])).getNombre();
				}
			} catch (NumberFormatException | PortalException | SystemException e) {
				log.error(e.getMessage());
			}
			
			
		}
		
		String decodificadorSel = ParamUtil.getString(request, "decodificadorSel");
		if(Validator.isNull(decodificadorSel) || Validator.isNull(svaTelevisionSel)){
			decodificadorSel = "NA";
		}
		
	
		long totalPedido = ParamUtil.getLong(request, "totalPedido");
		//long totalConIVA = ParamUtil.getLong(request, "totalConIva");
		
		long codigoLB = ParamUtil.getLong(request, "codigoLB");
		long codigoBA = ParamUtil.getLong(request, "codigoBA");
		long codigoTV = ParamUtil.getLong(request, "codigoTV");
		
		String tipoMXSel = ParamUtil.getString(request, "tipoMXSel");
		
		//Buscamos el idTipoMX
		long idTipoMX = 0;
		if(Validator.isNotNull(tipoMXSel)){
			idTipoMX = TipoMxLocalServiceUtil.findByNombre(tipoMXSel).getId();
		}
		
				 		
		solicitud.setNombres(nombres);
		solicitud.setApellidos(apellidos);
		solicitud.setDireccion(direccion);
		solicitud.setDocumento(documento);
		solicitud.setEmail(email);
		solicitud.setTipodoc(idTipoDocumento);
		solicitud.setBarrio(barrio);
		solicitud.setCelular(celular);
		solicitud.setDivipola(ciudadSel);
		solicitud.setEstrato(estrato);
		solicitud.setTelefono(telefono);
		solicitud.setPlanLB(idPlanLB);
		solicitud.setPlanBA(planBASel);
		solicitud.setPlanTV(planTVSel);
		solicitud.setPlanHD(planHDSel);
		solicitud.setSvaInternet(svaInternetSel);
		solicitud.setSvaTelevision(svaTelevisionSel);
		solicitud.setDecodificador(decodificadorSel);
		solicitud.setTotalPedido(totalPedido);
		solicitud.setTotalConIVA(totalPedido);
		solicitud.setCodigoLB(codigoLB);
		solicitud.setCodigoBA(codigoBA);
		solicitud.setCodigoPlanTV(codigoTV);
		solicitud.setTipoMX(idTipoMX);
				
		return solicitud;
	}

	/**
	 * Método que recupera de la sesión el valor del captcha del formulario.
	 * 
	 * @param session
	 *            session
	 * @return Captcha value from session
	 */
	private String getCaptchaValueFromSession(PortletSession session) {
        Enumeration<String> atNames = session.getAttributeNames();
        while (atNames.hasMoreElements()) {
            String name = atNames.nextElement();
            if (name.contains("CAPTCHA_TEXT")) {
                return (String) session.getAttribute(name);
            }
        }
        return null;
    }
	
}
