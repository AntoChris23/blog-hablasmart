/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.movistar.latam.colportal.homephone.model.Mercado;

import java.util.List;

/**
 * The persistence utility for the mercado service. This utility wraps {@link MercadoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see MercadoPersistence
 * @see MercadoPersistenceImpl
 * @generated
 */
public class MercadoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Mercado mercado) {
		getPersistence().clearCache(mercado);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Mercado> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Mercado> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Mercado> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static Mercado update(Mercado mercado) throws SystemException {
		return getPersistence().update(mercado);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static Mercado update(Mercado mercado, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(mercado, serviceContext);
	}

	/**
	* Caches the mercado in the entity cache if it is enabled.
	*
	* @param mercado the mercado
	*/
	public static void cacheResult(
		com.movistar.latam.colportal.homephone.model.Mercado mercado) {
		getPersistence().cacheResult(mercado);
	}

	/**
	* Caches the mercados in the entity cache if it is enabled.
	*
	* @param mercados the mercados
	*/
	public static void cacheResult(
		java.util.List<com.movistar.latam.colportal.homephone.model.Mercado> mercados) {
		getPersistence().cacheResult(mercados);
	}

	/**
	* Creates a new mercado with the primary key. Does not add the mercado to the database.
	*
	* @param id the primary key for the new mercado
	* @return the new mercado
	*/
	public static com.movistar.latam.colportal.homephone.model.Mercado create(
		long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the mercado with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the mercado
	* @return the mercado that was removed
	* @throws com.movistar.latam.colportal.homephone.NoSuchMercadoException if a mercado with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.Mercado remove(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMercadoException {
		return getPersistence().remove(id);
	}

	public static com.movistar.latam.colportal.homephone.model.Mercado updateImpl(
		com.movistar.latam.colportal.homephone.model.Mercado mercado)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(mercado);
	}

	/**
	* Returns the mercado with the primary key or throws a {@link com.movistar.latam.colportal.homephone.NoSuchMercadoException} if it could not be found.
	*
	* @param id the primary key of the mercado
	* @return the mercado
	* @throws com.movistar.latam.colportal.homephone.NoSuchMercadoException if a mercado with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.Mercado findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchMercadoException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the mercado with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the mercado
	* @return the mercado, or <code>null</code> if a mercado with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.Mercado fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the mercados.
	*
	* @return the mercados
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.Mercado> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the mercados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MercadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mercados
	* @param end the upper bound of the range of mercados (not inclusive)
	* @return the range of mercados
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.Mercado> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the mercados.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MercadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mercados
	* @param end the upper bound of the range of mercados (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of mercados
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.Mercado> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the mercados from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of mercados.
	*
	* @return the number of mercados
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static MercadoPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (MercadoPersistence)PortletBeanLocatorUtil.locate(com.movistar.latam.colportal.homephone.service.ClpSerializer.getServletContextName(),
					MercadoPersistence.class.getName());

			ReferenceRegistry.registerReference(MercadoUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(MercadoPersistence persistence) {
	}

	private static MercadoPersistence _persistence;
}