/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Departamento}.
 * </p>
 *
 * @author vass
 * @see Departamento
 * @generated
 */
public class DepartamentoWrapper implements Departamento,
	ModelWrapper<Departamento> {
	public DepartamentoWrapper(Departamento departamento) {
		_departamento = departamento;
	}

	@Override
	public Class<?> getModelClass() {
		return Departamento.class;
	}

	@Override
	public String getModelClassName() {
		return Departamento.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("nombre", getNombre());
		attributes.put("fecha", getFecha());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Date fecha = (Date)attributes.get("fecha");

		if (fecha != null) {
			setFecha(fecha);
		}
	}

	/**
	* Returns the primary key of this departamento.
	*
	* @return the primary key of this departamento
	*/
	@Override
	public long getPrimaryKey() {
		return _departamento.getPrimaryKey();
	}

	/**
	* Sets the primary key of this departamento.
	*
	* @param primaryKey the primary key of this departamento
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_departamento.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this departamento.
	*
	* @return the ID of this departamento
	*/
	@Override
	public long getId() {
		return _departamento.getId();
	}

	/**
	* Sets the ID of this departamento.
	*
	* @param id the ID of this departamento
	*/
	@Override
	public void setId(long id) {
		_departamento.setId(id);
	}

	/**
	* Returns the nombre of this departamento.
	*
	* @return the nombre of this departamento
	*/
	@Override
	public java.lang.String getNombre() {
		return _departamento.getNombre();
	}

	/**
	* Sets the nombre of this departamento.
	*
	* @param nombre the nombre of this departamento
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_departamento.setNombre(nombre);
	}

	/**
	* Returns the fecha of this departamento.
	*
	* @return the fecha of this departamento
	*/
	@Override
	public java.util.Date getFecha() {
		return _departamento.getFecha();
	}

	/**
	* Sets the fecha of this departamento.
	*
	* @param fecha the fecha of this departamento
	*/
	@Override
	public void setFecha(java.util.Date fecha) {
		_departamento.setFecha(fecha);
	}

	@Override
	public boolean isNew() {
		return _departamento.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_departamento.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _departamento.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_departamento.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _departamento.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _departamento.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_departamento.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _departamento.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_departamento.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_departamento.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_departamento.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DepartamentoWrapper((Departamento)_departamento.clone());
	}

	@Override
	public int compareTo(
		com.movistar.latam.colportal.homephone.model.Departamento departamento) {
		return _departamento.compareTo(departamento);
	}

	@Override
	public int hashCode() {
		return _departamento.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.movistar.latam.colportal.homephone.model.Departamento> toCacheModel() {
		return _departamento.toCacheModel();
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.Departamento toEscapedModel() {
		return new DepartamentoWrapper(_departamento.toEscapedModel());
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.Departamento toUnescapedModel() {
		return new DepartamentoWrapper(_departamento.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _departamento.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _departamento.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_departamento.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DepartamentoWrapper)) {
			return false;
		}

		DepartamentoWrapper departamentoWrapper = (DepartamentoWrapper)obj;

		if (Validator.equals(_departamento, departamentoWrapper._departamento)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Departamento getWrappedDepartamento() {
		return _departamento;
	}

	@Override
	public Departamento getWrappedModel() {
		return _departamento;
	}

	@Override
	public void resetOriginalValues() {
		_departamento.resetOriginalValues();
	}

	private Departamento _departamento;
}