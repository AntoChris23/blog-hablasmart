/*
 * 
 */

package com.movistar.latam.colportal.homephone.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.movistar.latam.colportal.homephone.NoSuchMatrizException;
import com.movistar.latam.colportal.homephone.model.Matriz;
import com.movistar.latam.colportal.homephone.service.base.MatrizLocalServiceBaseImpl;
import com.movistar.latam.colportal.homephone.service.persistence.MatrizUtil;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The implementation of the matriz local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.movistar.latam.colportal.homephone.service.MatrizLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author vass
 * @see com.movistar.latam.colportal.homephone.service.base.MatrizLocalServiceBaseImpl
 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalServiceUtil
 */
/**
 * @author vass
 *
 */
public class MatrizLocalServiceImpl extends MatrizLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.movistar.latam.colportal.homephone.service.MatrizLocalServiceUtil} to access the matriz local service.
	 */
	/** Log. */
	private static Log log = LogFactoryUtil.getLog(Matriz.class);
	
	/* (non-Javadoc)
	 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalService#getAllMatrizRows()
	 */
	public List<Matriz> getAllMatrizRows() throws SystemException{
		return matrizPersistence.findAll();
	}
	
	/*
	 * Devuelve los registros de la matriz filtrando por la ciudad (divipola)
	 * (non-Javadoc)
	 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalService#findByCiudad(java.lang.Long)
	 */
	public List<Matriz> findByCiudad(Long divipola) throws SystemException {
		
		List<Matriz> matrizRows = null;
		
		try {
			matrizRows = MatrizUtil.findByCiudad(divipola);
						
		} catch (SystemException e) {
			log.error(e.getMessage());
		}
		return matrizRows;
	}
	
	/*
	 * Devuelve los registros de la matriz filtrando por la ciudad (divipola) y el estrato
	 * 
	 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalService#findByCiudad(java.lang.Long)
	 */
	/* (non-Javadoc)
	 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalService#findBySearchMatriz(java.lang.Long, java.lang.String)
	 */
	public List<Matriz> findBySearchMatriz(Long divipola, String estrato) throws SystemException {
		
		List<Matriz> matrizRows = null;
		
		try {
			matrizRows = MatrizUtil.findBySearchMatriz(divipola, estrato);
						
		} catch (SystemException e) {
			log.error(e.getMessage());
		}
		return matrizRows;
	}
	
	
	/* 
	 * Añade un nuevo registro de matriz
	 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalService#addMatrizRow(com.movistar.latam.colportal.homephone.model.Matriz)
	 */
	/* (non-Javadoc)
	 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalService#addMatrizRow(com.movistar.latam.colportal.homephone.model.Matriz)
	 */
	public Matriz addMatrizRow(Matriz newMatrizRow) throws SystemException {
		
		long matrizId = counterLocalService.increment(Matriz.class.getName());
		
		Matriz matrizRow = matrizPersistence.create(matrizId);
				
		matrizRow.setDivipola(newMatrizRow.getDivipola());
		matrizRow.setCodigoAtis(newMatrizRow.getCodigoAtis());
		matrizRow.setEstrato(newMatrizRow.getEstrato());
		matrizRow.setTipoMX(newMatrizRow.getTipoMX());
		matrizRow.setPlanLB(newMatrizRow.getPlanLB());
		matrizRow.setCodigoLB(newMatrizRow.getCodigoLB());
		matrizRow.setValorLB(newMatrizRow.getValorLB());
		matrizRow.setPlanBA(newMatrizRow.getPlanBA());
		matrizRow.setCodigoBA(newMatrizRow.getCodigoBA());
		matrizRow.setValorBA(newMatrizRow.getValorBA());
		matrizRow.setSubsidioBA(newMatrizRow.getSubsidioBA());
		matrizRow.setPlanTV(newMatrizRow.getPlanTV());
		matrizRow.setCodigoTV(newMatrizRow.getCodigoTV());
		matrizRow.setValorTV(newMatrizRow.getValorTV());
		matrizRow.setTotalValor(newMatrizRow.getTotalValor());
		matrizRow.setTiemPromDes(newMatrizRow.getTiemPromDes());
		matrizRow.setDescuento(newMatrizRow.getDescuento());
		matrizRow.setAplicaHD(newMatrizRow.getAplicaHD());
 	
	 	matrizPersistence.update(matrizRow);
	 		 	
	 	return matrizRow;
	}
		
	/* 
	 * Borra todos los registros de la matriz
	 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalService#removeAll()
	 */
	/* (non-Javadoc)
	 * @see com.movistar.latam.colportal.homephone.service.MatrizLocalService#removeAll()
	 */
	public void removeAll() throws SystemException{
		
		if (log.isDebugEnabled()) {
			log.debug("Borrando los registros de la matriz antigua");
		}
		matrizPersistence.removeAll();
		
		if (log.isDebugEnabled()) {
			log.debug("Se han borrado los registros de la matriz antigua correctamente");
		}
	}
	
}