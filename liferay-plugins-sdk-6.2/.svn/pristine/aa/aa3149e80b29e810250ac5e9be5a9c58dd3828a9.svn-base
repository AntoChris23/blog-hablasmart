/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CiudadLocalService}.
 *
 * @author vass
 * @see CiudadLocalService
 * @generated
 */
public class CiudadLocalServiceWrapper implements CiudadLocalService,
	ServiceWrapper<CiudadLocalService> {
	public CiudadLocalServiceWrapper(CiudadLocalService ciudadLocalService) {
		_ciudadLocalService = ciudadLocalService;
	}

	/**
	* Adds the ciudad to the database. Also notifies the appropriate model listeners.
	*
	* @param ciudad the ciudad
	* @return the ciudad that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.movistar.latam.colportal.homephone.model.Ciudad addCiudad(
		com.movistar.latam.colportal.homephone.model.Ciudad ciudad)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.addCiudad(ciudad);
	}

	/**
	* Creates a new ciudad with the primary key. Does not add the ciudad to the database.
	*
	* @param id the primary key for the new ciudad
	* @return the new ciudad
	*/
	@Override
	public com.movistar.latam.colportal.homephone.model.Ciudad createCiudad(
		long id) {
		return _ciudadLocalService.createCiudad(id);
	}

	/**
	* Deletes the ciudad with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the ciudad
	* @return the ciudad that was removed
	* @throws PortalException if a ciudad with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.movistar.latam.colportal.homephone.model.Ciudad deleteCiudad(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.deleteCiudad(id);
	}

	/**
	* Deletes the ciudad from the database. Also notifies the appropriate model listeners.
	*
	* @param ciudad the ciudad
	* @return the ciudad that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.movistar.latam.colportal.homephone.model.Ciudad deleteCiudad(
		com.movistar.latam.colportal.homephone.model.Ciudad ciudad)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.deleteCiudad(ciudad);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ciudadLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.CiudadModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.CiudadModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.Ciudad fetchCiudad(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.fetchCiudad(id);
	}

	/**
	* Returns the ciudad with the primary key.
	*
	* @param id the primary key of the ciudad
	* @return the ciudad
	* @throws PortalException if a ciudad with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.movistar.latam.colportal.homephone.model.Ciudad getCiudad(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.getCiudad(id);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the ciudads.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.CiudadModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ciudads
	* @param end the upper bound of the range of ciudads (not inclusive)
	* @return the range of ciudads
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.movistar.latam.colportal.homephone.model.Ciudad> getCiudads(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.getCiudads(start, end);
	}

	/**
	* Returns the number of ciudads.
	*
	* @return the number of ciudads
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCiudadsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.getCiudadsCount();
	}

	/**
	* Updates the ciudad in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ciudad the ciudad
	* @return the ciudad that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.movistar.latam.colportal.homephone.model.Ciudad updateCiudad(
		com.movistar.latam.colportal.homephone.model.Ciudad ciudad)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.updateCiudad(ciudad);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _ciudadLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_ciudadLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _ciudadLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* Devuelve las ciudades filtrando por idDepartamento.
	*
	* @param idDepartamento
	id departamento
	* @return list
	* @throws SystemException
	system exception
	* @see com.movistar.latam.colportal.homephone.service.CiudadLocalService#findByCiudad(java.lang.Long)
	*/
	@Override
	public java.util.List<com.movistar.latam.colportal.homephone.model.Ciudad> findByIdDepartamento(
		java.lang.Long idDepartamento)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ciudadLocalService.findByIdDepartamento(idDepartamento);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CiudadLocalService getWrappedCiudadLocalService() {
		return _ciudadLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCiudadLocalService(
		CiudadLocalService ciudadLocalService) {
		_ciudadLocalService = ciudadLocalService;
	}

	@Override
	public CiudadLocalService getWrappedService() {
		return _ciudadLocalService;
	}

	@Override
	public void setWrappedService(CiudadLocalService ciudadLocalService) {
		_ciudadLocalService = ciudadLocalService;
	}

	private CiudadLocalService _ciudadLocalService;
}