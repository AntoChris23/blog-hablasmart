/*
 * 
 */
package com.movistar.latam.colportal.hook.portlet.layoutsadmin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * SitemapNode:.
 * 
 * @author VASS
 * @version 1.0
 */
public class SitemapNode {

	/** Name map. */
	private Map<Locale, String> nameMap;

	/** Title map. */
	private Map<Locale, String> titleMap;

	/** Friendly url. */
	private String friendlyURL;

	/** Image url. */
	private String imageURL;

	/** Children. */
	private List<SitemapNode> children;

	/** Priority. */
	private int priority = 0;

	/** Target. */
	private String target = null;

	/**
	 * Instancia sitemap node.
	 */
	public SitemapNode() {

	}

	/**
	 * Obtiene friendly url.
	 * 
	 * @return Friendly url
	 */
	public String getFriendlyURL() {
		return friendlyURL;
	}

	/**
	 * Asigna friendly url.
	 * 
	 * @param friendlyURL
	 *            friendly url
	 */
	public void setFriendlyURL(final String friendlyURL) {
		this.friendlyURL = friendlyURL;
	}

	/**
	 * Obtiene name map.
	 * 
	 * @return Name map
	 */
	public Map<Locale, String> getNameMap() {
		if (nameMap == null) {
			nameMap = new HashMap<Locale, String>();
		}
		return nameMap;
	}

	/**
	 * Asigna name map.
	 * 
	 * @param nameMap
	 *            name map
	 */
	public void setNameMap(final Map<Locale, String> nameMap) {
		this.nameMap = nameMap;
	}

	/**
	 * Obtiene title map.
	 * 
	 * @return Title map
	 */
	public Map<Locale, String> getTitleMap() {
		if (titleMap == null) {
			titleMap = new HashMap<Locale, String>();
		}
		return titleMap;
	}

	/**
	 * Asigna title map.
	 * 
	 * @param titleMap
	 *            title map
	 */
	public void setTitleMap(final Map<Locale, String> titleMap) {
		this.titleMap = titleMap;
	}

	/**
	 * Obtiene children.
	 * 
	 * @return Children
	 */
	public List<SitemapNode> getChildren() {
		if (children == null) {
			children = new ArrayList<SitemapNode>();
		}
		return children;
	}

	/**
	 * Asigna children.
	 * 
	 * @param children
	 *            children
	 */
	public void setChildren(final List<SitemapNode> children) {
		this.children = children;
	}

	/**
	 * Obtiene priority.
	 * 
	 * @return Priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * Asigna priority.
	 * 
	 * @param priority
	 *            priority
	 */
	public void setPriority(final int priority) {
		this.priority = priority;
	}

	/**
	 * Obtiene image url.
	 * 
	 * @return Image url
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * Asigna image url.
	 * 
	 * @param imageURL
	 *            image url
	 */
	public void setImageURL(final String imageURL) {
		this.imageURL = imageURL;
	}

	/**
	 * Asigna target.
	 * 
	 * @param target
	 *            the new target
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * Obtiene target.
	 * 
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}
}
