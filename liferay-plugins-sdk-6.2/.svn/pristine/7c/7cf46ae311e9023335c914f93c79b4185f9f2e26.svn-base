/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for LB. This utility wraps
 * {@link com.movistar.latam.colportal.homephone.service.impl.LBLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author vass
 * @see LBLocalService
 * @see com.movistar.latam.colportal.homephone.service.base.LBLocalServiceBaseImpl
 * @see com.movistar.latam.colportal.homephone.service.impl.LBLocalServiceImpl
 * @generated
 */
public class LBLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.movistar.latam.colportal.homephone.service.impl.LBLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the l b to the database. Also notifies the appropriate model listeners.
	*
	* @param lb the l b
	* @return the l b that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.LB addLB(
		com.movistar.latam.colportal.homephone.model.LB lb)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addLB(lb);
	}

	/**
	* Creates a new l b with the primary key. Does not add the l b to the database.
	*
	* @param id the primary key for the new l b
	* @return the new l b
	*/
	public static com.movistar.latam.colportal.homephone.model.LB createLB(
		long id) {
		return getService().createLB(id);
	}

	/**
	* Deletes the l b with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the l b
	* @return the l b that was removed
	* @throws PortalException if a l b with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.LB deleteLB(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLB(id);
	}

	/**
	* Deletes the l b from the database. Also notifies the appropriate model listeners.
	*
	* @param lb the l b
	* @return the l b that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.LB deleteLB(
		com.movistar.latam.colportal.homephone.model.LB lb)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLB(lb);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.LBModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.LBModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.movistar.latam.colportal.homephone.model.LB fetchLB(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchLB(id);
	}

	/**
	* Returns the l b with the primary key.
	*
	* @param id the primary key of the l b
	* @return the l b
	* @throws PortalException if a l b with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.LB getLB(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getLB(id);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the l bs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.LBModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of l bs
	* @param end the upper bound of the range of l bs (not inclusive)
	* @return the range of l bs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.movistar.latam.colportal.homephone.model.LB> getLBs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLBs(start, end);
	}

	/**
	* Returns the number of l bs.
	*
	* @return the number of l bs
	* @throws SystemException if a system exception occurred
	*/
	public static int getLBsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLBsCount();
	}

	/**
	* Updates the l b in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lb the l b
	* @return the l b that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.movistar.latam.colportal.homephone.model.LB updateLB(
		com.movistar.latam.colportal.homephone.model.LB lb)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLB(lb);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.movistar.latam.colportal.homephone.model.LB findByNombre(
		java.lang.String Nombre) {
		return getService().findByNombre(Nombre);
	}

	public static void clearService() {
		_service = null;
	}

	public static LBLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					LBLocalService.class.getName());

			if (invokableLocalService instanceof LBLocalService) {
				_service = (LBLocalService)invokableLocalService;
			}
			else {
				_service = new LBLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(LBLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(LBLocalService service) {
	}

	private static LBLocalService _service;
}