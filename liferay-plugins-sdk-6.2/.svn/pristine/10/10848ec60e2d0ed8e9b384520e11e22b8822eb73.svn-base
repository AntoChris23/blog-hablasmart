/*
 * 
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.movistar.latam.colportal.homephone.NoSuchMercadoException;
import com.movistar.latam.colportal.homephone.model.Mercado;
import com.movistar.latam.colportal.homephone.model.impl.MercadoImpl;
import com.movistar.latam.colportal.homephone.model.impl.MercadoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * The persistence implementation for the mercado service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see MercadoPersistence
 * @see MercadoUtil
 * @generated
 */
public class MercadoPersistenceImpl extends BasePersistenceImpl<Mercado>
	implements MercadoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MercadoUtil} to access the mercado persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	/** FINDER_CLASS_NAME_ENTITY. */
	public static final String FINDER_CLASS_NAME_ENTITY = MercadoImpl.class.getName();
	
	/** FINDER_CLASS_NAME_LIST_WITH_PAGINATION. */
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	
	/** FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION. */
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	
	/** FINDER_PATH_WITH_PAGINATION_FIND_ALL. */
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MercadoModelImpl.ENTITY_CACHE_ENABLED,
			MercadoModelImpl.FINDER_CACHE_ENABLED, MercadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	
	/** FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL. */
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MercadoModelImpl.ENTITY_CACHE_ENABLED,
			MercadoModelImpl.FINDER_CACHE_ENABLED, MercadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	
	/** FINDER_PATH_COUNT_ALL. */
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MercadoModelImpl.ENTITY_CACHE_ENABLED,
			MercadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Instancia mercado persistence impl.
	 */
	public MercadoPersistenceImpl() {
		setModelClass(Mercado.class);
	}

	/**
	 * Caches the mercado in the entity cache if it is enabled.
	 *
	 * @param mercado the mercado
	 */
	@Override
	public void cacheResult(Mercado mercado) {
		EntityCacheUtil.putResult(MercadoModelImpl.ENTITY_CACHE_ENABLED,
			MercadoImpl.class, mercado.getPrimaryKey(), mercado);

		mercado.resetOriginalValues();
	}

	/**
	 * Caches the mercados in the entity cache if it is enabled.
	 *
	 * @param mercados the mercados
	 */
	@Override
	public void cacheResult(List<Mercado> mercados) {
		for (Mercado mercado : mercados) {
			if (EntityCacheUtil.getResult(
						MercadoModelImpl.ENTITY_CACHE_ENABLED,
						MercadoImpl.class, mercado.getPrimaryKey()) == null) {
				cacheResult(mercado);
			}
			else {
				mercado.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all mercados.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(MercadoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(MercadoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the mercado.
	 * 
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and
	 * {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by
	 * this method.
	 * </p>
	 * 
	 * @param mercado
	 *            mercado
	 */
	@Override
	public void clearCache(Mercado mercado) {
		EntityCacheUtil.removeResult(MercadoModelImpl.ENTITY_CACHE_ENABLED,
			MercadoImpl.class, mercado.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/* (non-Javadoc)
	 * @see com.liferay.portal.service.persistence.impl.BasePersistenceImpl#clearCache(java.util.List)
	 */
	@Override
	public void clearCache(List<Mercado> mercados) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Mercado mercado : mercados) {
			EntityCacheUtil.removeResult(MercadoModelImpl.ENTITY_CACHE_ENABLED,
				MercadoImpl.class, mercado.getPrimaryKey());
		}
	}

	/**
	 * Creates a new mercado with the primary key. Does not add the mercado to the database.
	 *
	 * @param id the primary key for the new mercado
	 * @return the new mercado
	 */
	@Override
	public Mercado create(long id) {
		Mercado mercado = new MercadoImpl();

		mercado.setNew(true);
		mercado.setPrimaryKey(id);

		return mercado;
	}

	/**
	 * Removes the mercado with the primary key from the database. Also notifies
	 * the appropriate model listeners.
	 * 
	 * @param id
	 *            the primary key of the mercado
	 * @return the mercado that was removed
	 * @throws NoSuchMercadoException
	 *             no such mercado exception
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Mercado remove(long id)
		throws NoSuchMercadoException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the mercado with the primary key from the database. Also notifies
	 * the appropriate model listeners.
	 * 
	 * @param primaryKey
	 *            the primary key of the mercado
	 * @return the mercado that was removed
	 * @throws NoSuchMercadoException
	 *             no such mercado exception
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Mercado remove(Serializable primaryKey)
		throws NoSuchMercadoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Mercado mercado = (Mercado)session.get(MercadoImpl.class, primaryKey);

			if (mercado == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMercadoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(mercado);
		}
		catch (NoSuchMercadoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/* (non-Javadoc)
	 * @see com.liferay.portal.service.persistence.impl.BasePersistenceImpl#removeImpl(com.liferay.portal.model.BaseModel)
	 */
	@Override
	protected Mercado removeImpl(Mercado mercado) throws SystemException {
		mercado = toUnwrappedModel(mercado);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(mercado)) {
				mercado = (Mercado)session.get(MercadoImpl.class,
						mercado.getPrimaryKeyObj());
			}

			if (mercado != null) {
				session.delete(mercado);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (mercado != null) {
			clearCache(mercado);
		}

		return mercado;
	}

	/* (non-Javadoc)
	 * @see com.liferay.portal.service.persistence.impl.BasePersistenceImpl#updateImpl(com.liferay.portal.model.BaseModel)
	 */
	@Override
	public Mercado updateImpl(
		com.movistar.latam.colportal.homephone.model.Mercado mercado)
		throws SystemException {
		mercado = toUnwrappedModel(mercado);

		boolean isNew = mercado.isNew();

		Session session = null;

		try {
			session = openSession();

			if (mercado.isNew()) {
				session.save(mercado);

				mercado.setNew(false);
			}
			else {
				session.merge(mercado);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(MercadoModelImpl.ENTITY_CACHE_ENABLED,
			MercadoImpl.class, mercado.getPrimaryKey(), mercado);

		return mercado;
	}

	/**
	 * To unwrapped model.
	 * 
	 * @param mercado
	 *            mercado
	 * @return mercado
	 */
	protected Mercado toUnwrappedModel(Mercado mercado) {
		if (mercado instanceof MercadoImpl) {
			return mercado;
		}

		MercadoImpl mercadoImpl = new MercadoImpl();

		mercadoImpl.setNew(mercado.isNew());
		mercadoImpl.setPrimaryKey(mercado.getPrimaryKey());

		mercadoImpl.setId(mercado.getId());
		mercadoImpl.setNombre(mercado.getNombre());
		mercadoImpl.setFecha(mercado.getFecha());

		return mercadoImpl;
	}

	/**
	 * Returns the mercado with the primary key or throws a
	 * {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 * 
	 * @param primaryKey
	 *            the primary key of the mercado
	 * @return the mercado
	 * @throws NoSuchMercadoException
	 *             no such mercado exception
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Mercado findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMercadoException, SystemException {
		Mercado mercado = fetchByPrimaryKey(primaryKey);

		if (mercado == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMercadoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return mercado;
	}

	/**
	 * Returns the mercado with the primary key or throws a
	 * {@link com.movistar.latam.colportal.homephone.NoSuchMercadoException} if
	 * it could not be found.
	 * 
	 * @param id
	 *            the primary key of the mercado
	 * @return the mercado
	 * @throws NoSuchMercadoException
	 *             no such mercado exception
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Mercado findByPrimaryKey(long id)
		throws NoSuchMercadoException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the mercado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the mercado
	 * @return the mercado, or <code>null</code> if a mercado with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Mercado fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Mercado mercado = (Mercado)EntityCacheUtil.getResult(MercadoModelImpl.ENTITY_CACHE_ENABLED,
				MercadoImpl.class, primaryKey);

		if (mercado == _nullMercado) {
			return null;
		}

		if (mercado == null) {
			Session session = null;

			try {
				session = openSession();

				mercado = (Mercado)session.get(MercadoImpl.class, primaryKey);

				if (mercado != null) {
					cacheResult(mercado);
				}
				else {
					EntityCacheUtil.putResult(MercadoModelImpl.ENTITY_CACHE_ENABLED,
						MercadoImpl.class, primaryKey, _nullMercado);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(MercadoModelImpl.ENTITY_CACHE_ENABLED,
					MercadoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return mercado;
	}

	/**
	 * Returns the mercado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the mercado
	 * @return the mercado, or <code>null</code> if a mercado with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Mercado fetchByPrimaryKey(long id) throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the mercados.
	 *
	 * @return the mercados
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Mercado> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the mercados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MercadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of mercados
	 * @param end the upper bound of the range of mercados (not inclusive)
	 * @return the range of mercados
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Mercado> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the mercados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.MercadoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of mercados
	 * @param end the upper bound of the range of mercados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of mercados
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Mercado> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Mercado> list = (List<Mercado>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_MERCADO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MERCADO;

				if (pagination) {
					sql = sql.concat(MercadoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Mercado>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Mercado>(list);
				}
				else {
					list = (List<Mercado>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the mercados from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Mercado mercado : findAll()) {
			remove(mercado);
		}
	}

	/**
	 * Returns the number of mercados.
	 *
	 * @return the number of mercados
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MERCADO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/* (non-Javadoc)
	 * @see com.liferay.portal.service.persistence.impl.BasePersistenceImpl#getBadColumnNames()
	 */
	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the mercado persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.movistar.latam.colportal.homephone.model.Mercado")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Mercado>> listenersList = new ArrayList<ModelListener<Mercado>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Mercado>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	/**
	 * Destroy.
	 */
	public void destroy() {
		EntityCacheUtil.removeCache(MercadoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/** _SQL_SELECT_MERCADO. */
	private static final String _SQL_SELECT_MERCADO = "SELECT mercado FROM Mercado mercado";
	
	/** _SQL_COUNT_MERCADO. */
	private static final String _SQL_COUNT_MERCADO = "SELECT COUNT(mercado) FROM Mercado mercado";
	
	/** _ORDER_BY_ENTITY_ALIAS. */
	private static final String _ORDER_BY_ENTITY_ALIAS = "mercado.";
	
	/** _NO_SUCH_ENTITY_WITH_PRIMARY_KEY. */
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Mercado exists with the primary key ";
	
	/** _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE. */
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	
	/** _log. */
	private static Log _log = LogFactoryUtil.getLog(MercadoPersistenceImpl.class);
	
	/** _bad column names. */
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	
	/** _null mercado. */
	private static Mercado _nullMercado = new MercadoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Mercado> toCacheModel() {
				return _nullMercadoCacheModel;
			}
		};

	/** _null mercado cache model. */
	private static CacheModel<Mercado> _nullMercadoCacheModel = new CacheModel<Mercado>() {
			@Override
			public Mercado toEntityModel() {
				return _nullMercado;
			}
		};
}