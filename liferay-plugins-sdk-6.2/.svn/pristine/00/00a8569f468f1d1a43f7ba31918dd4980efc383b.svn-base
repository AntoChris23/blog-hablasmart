/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.movistar.latam.colportal.homephone.service.ClpSerializer;
import com.movistar.latam.colportal.homephone.service.PromocionLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vass
 */
public class PromocionClp extends BaseModelImpl<Promocion> implements Promocion {
	public PromocionClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Promocion.class;
	}

	@Override
	public String getModelClassName() {
		return Promocion.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("idEstrato", getIdEstrato());
		attributes.put("idGrupoCiudad", getIdGrupoCiudad());
		attributes.put("idTipoMx", getIdTipoMx());
		attributes.put("idBa", getIdBa());
		attributes.put("idLb", getIdLb());
		attributes.put("idTv", getIdTv());
		attributes.put("divipola", getDivipola());
		attributes.put("tiemPromDes", getTiemPromDes());
		attributes.put("descuento", getDescuento());
		attributes.put("descripcion", getDescripcion());
		attributes.put("fecha", getFecha());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long idEstrato = (Long)attributes.get("idEstrato");

		if (idEstrato != null) {
			setIdEstrato(idEstrato);
		}

		Long idGrupoCiudad = (Long)attributes.get("idGrupoCiudad");

		if (idGrupoCiudad != null) {
			setIdGrupoCiudad(idGrupoCiudad);
		}

		Long idTipoMx = (Long)attributes.get("idTipoMx");

		if (idTipoMx != null) {
			setIdTipoMx(idTipoMx);
		}

		Long idBa = (Long)attributes.get("idBa");

		if (idBa != null) {
			setIdBa(idBa);
		}

		Long idLb = (Long)attributes.get("idLb");

		if (idLb != null) {
			setIdLb(idLb);
		}

		Long idTv = (Long)attributes.get("idTv");

		if (idTv != null) {
			setIdTv(idTv);
		}

		Long divipola = (Long)attributes.get("divipola");

		if (divipola != null) {
			setDivipola(divipola);
		}

		Long tiemPromDes = (Long)attributes.get("tiemPromDes");

		if (tiemPromDes != null) {
			setTiemPromDes(tiemPromDes);
		}

		Float descuento = (Float)attributes.get("descuento");

		if (descuento != null) {
			setDescuento(descuento);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Date fecha = (Date)attributes.get("fecha");

		if (fecha != null) {
			setFecha(fecha);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setId", long.class);

				method.invoke(_promocionRemoteModel, id);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdEstrato() {
		return _idEstrato;
	}

	@Override
	public void setIdEstrato(long idEstrato) {
		_idEstrato = idEstrato;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdEstrato", long.class);

				method.invoke(_promocionRemoteModel, idEstrato);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdGrupoCiudad() {
		return _idGrupoCiudad;
	}

	@Override
	public void setIdGrupoCiudad(long idGrupoCiudad) {
		_idGrupoCiudad = idGrupoCiudad;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdGrupoCiudad", long.class);

				method.invoke(_promocionRemoteModel, idGrupoCiudad);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdTipoMx() {
		return _idTipoMx;
	}

	@Override
	public void setIdTipoMx(long idTipoMx) {
		_idTipoMx = idTipoMx;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdTipoMx", long.class);

				method.invoke(_promocionRemoteModel, idTipoMx);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdBa() {
		return _idBa;
	}

	@Override
	public void setIdBa(long idBa) {
		_idBa = idBa;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdBa", long.class);

				method.invoke(_promocionRemoteModel, idBa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdLb() {
		return _idLb;
	}

	@Override
	public void setIdLb(long idLb) {
		_idLb = idLb;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdLb", long.class);

				method.invoke(_promocionRemoteModel, idLb);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdTv() {
		return _idTv;
	}

	@Override
	public void setIdTv(long idTv) {
		_idTv = idTv;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setIdTv", long.class);

				method.invoke(_promocionRemoteModel, idTv);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getDivipola() {
		return _divipola;
	}

	@Override
	public void setDivipola(long divipola) {
		_divipola = divipola;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setDivipola", long.class);

				method.invoke(_promocionRemoteModel, divipola);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTiemPromDes() {
		return _tiemPromDes;
	}

	@Override
	public void setTiemPromDes(long tiemPromDes) {
		_tiemPromDes = tiemPromDes;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setTiemPromDes", long.class);

				method.invoke(_promocionRemoteModel, tiemPromDes);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public float getDescuento() {
		return _descuento;
	}

	@Override
	public void setDescuento(float descuento) {
		_descuento = descuento;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setDescuento", float.class);

				method.invoke(_promocionRemoteModel, descuento);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDescripcion() {
		return _descripcion;
	}

	@Override
	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setDescripcion", String.class);

				method.invoke(_promocionRemoteModel, descripcion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFecha() {
		return _fecha;
	}

	@Override
	public void setFecha(Date fecha) {
		_fecha = fecha;

		if (_promocionRemoteModel != null) {
			try {
				Class<?> clazz = _promocionRemoteModel.getClass();

				Method method = clazz.getMethod("setFecha", Date.class);

				method.invoke(_promocionRemoteModel, fecha);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getPromocionRemoteModel() {
		return _promocionRemoteModel;
	}

	public void setPromocionRemoteModel(BaseModel<?> promocionRemoteModel) {
		_promocionRemoteModel = promocionRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _promocionRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_promocionRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			PromocionLocalServiceUtil.addPromocion(this);
		}
		else {
			PromocionLocalServiceUtil.updatePromocion(this);
		}
	}

	@Override
	public Promocion toEscapedModel() {
		return (Promocion)ProxyUtil.newProxyInstance(Promocion.class.getClassLoader(),
			new Class[] { Promocion.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		PromocionClp clone = new PromocionClp();

		clone.setId(getId());
		clone.setIdEstrato(getIdEstrato());
		clone.setIdGrupoCiudad(getIdGrupoCiudad());
		clone.setIdTipoMx(getIdTipoMx());
		clone.setIdBa(getIdBa());
		clone.setIdLb(getIdLb());
		clone.setIdTv(getIdTv());
		clone.setDivipola(getDivipola());
		clone.setTiemPromDes(getTiemPromDes());
		clone.setDescuento(getDescuento());
		clone.setDescripcion(getDescripcion());
		clone.setFecha(getFecha());

		return clone;
	}

	@Override
	public int compareTo(Promocion promocion) {
		long primaryKey = promocion.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PromocionClp)) {
			return false;
		}

		PromocionClp promocion = (PromocionClp)obj;

		long primaryKey = promocion.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", idEstrato=");
		sb.append(getIdEstrato());
		sb.append(", idGrupoCiudad=");
		sb.append(getIdGrupoCiudad());
		sb.append(", idTipoMx=");
		sb.append(getIdTipoMx());
		sb.append(", idBa=");
		sb.append(getIdBa());
		sb.append(", idLb=");
		sb.append(getIdLb());
		sb.append(", idTv=");
		sb.append(getIdTv());
		sb.append(", divipola=");
		sb.append(getDivipola());
		sb.append(", tiemPromDes=");
		sb.append(getTiemPromDes());
		sb.append(", descuento=");
		sb.append(getDescuento());
		sb.append(", descripcion=");
		sb.append(getDescripcion());
		sb.append(", fecha=");
		sb.append(getFecha());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(40);

		sb.append("<model><model-name>");
		sb.append("com.movistar.latam.colportal.homephone.model.Promocion");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idEstrato</column-name><column-value><![CDATA[");
		sb.append(getIdEstrato());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idGrupoCiudad</column-name><column-value><![CDATA[");
		sb.append(getIdGrupoCiudad());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idTipoMx</column-name><column-value><![CDATA[");
		sb.append(getIdTipoMx());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idBa</column-name><column-value><![CDATA[");
		sb.append(getIdBa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idLb</column-name><column-value><![CDATA[");
		sb.append(getIdLb());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idTv</column-name><column-value><![CDATA[");
		sb.append(getIdTv());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>divipola</column-name><column-value><![CDATA[");
		sb.append(getDivipola());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tiemPromDes</column-name><column-value><![CDATA[");
		sb.append(getTiemPromDes());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descuento</column-name><column-value><![CDATA[");
		sb.append(getDescuento());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descripcion</column-name><column-value><![CDATA[");
		sb.append(getDescripcion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fecha</column-name><column-value><![CDATA[");
		sb.append(getFecha());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _id;
	private long _idEstrato;
	private long _idGrupoCiudad;
	private long _idTipoMx;
	private long _idBa;
	private long _idLb;
	private long _idTv;
	private long _divipola;
	private long _tiemPromDes;
	private float _descuento;
	private String _descripcion;
	private Date _fecha;
	private BaseModel<?> _promocionRemoteModel;
}