/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Promocion}.
 * </p>
 *
 * @author vass
 * @see Promocion
 * @generated
 */
public class PromocionWrapper implements Promocion, ModelWrapper<Promocion> {
	public PromocionWrapper(Promocion promocion) {
		_promocion = promocion;
	}

	@Override
	public Class<?> getModelClass() {
		return Promocion.class;
	}

	@Override
	public String getModelClassName() {
		return Promocion.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("idEstrato", getIdEstrato());
		attributes.put("idGrupoCiudad", getIdGrupoCiudad());
		attributes.put("idTipoMx", getIdTipoMx());
		attributes.put("idBa", getIdBa());
		attributes.put("idLb", getIdLb());
		attributes.put("idTv", getIdTv());
		attributes.put("divipola", getDivipola());
		attributes.put("tiemPromDes", getTiemPromDes());
		attributes.put("descuento", getDescuento());
		attributes.put("descripcion", getDescripcion());
		attributes.put("fecha", getFecha());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long idEstrato = (Long)attributes.get("idEstrato");

		if (idEstrato != null) {
			setIdEstrato(idEstrato);
		}

		Long idGrupoCiudad = (Long)attributes.get("idGrupoCiudad");

		if (idGrupoCiudad != null) {
			setIdGrupoCiudad(idGrupoCiudad);
		}

		Long idTipoMx = (Long)attributes.get("idTipoMx");

		if (idTipoMx != null) {
			setIdTipoMx(idTipoMx);
		}

		Long idBa = (Long)attributes.get("idBa");

		if (idBa != null) {
			setIdBa(idBa);
		}

		Long idLb = (Long)attributes.get("idLb");

		if (idLb != null) {
			setIdLb(idLb);
		}

		Long idTv = (Long)attributes.get("idTv");

		if (idTv != null) {
			setIdTv(idTv);
		}

		Long divipola = (Long)attributes.get("divipola");

		if (divipola != null) {
			setDivipola(divipola);
		}

		Long tiemPromDes = (Long)attributes.get("tiemPromDes");

		if (tiemPromDes != null) {
			setTiemPromDes(tiemPromDes);
		}

		Float descuento = (Float)attributes.get("descuento");

		if (descuento != null) {
			setDescuento(descuento);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Date fecha = (Date)attributes.get("fecha");

		if (fecha != null) {
			setFecha(fecha);
		}
	}

	/**
	* Returns the primary key of this promocion.
	*
	* @return the primary key of this promocion
	*/
	@Override
	public long getPrimaryKey() {
		return _promocion.getPrimaryKey();
	}

	/**
	* Sets the primary key of this promocion.
	*
	* @param primaryKey the primary key of this promocion
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_promocion.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this promocion.
	*
	* @return the ID of this promocion
	*/
	@Override
	public long getId() {
		return _promocion.getId();
	}

	/**
	* Sets the ID of this promocion.
	*
	* @param id the ID of this promocion
	*/
	@Override
	public void setId(long id) {
		_promocion.setId(id);
	}

	/**
	* Returns the id estrato of this promocion.
	*
	* @return the id estrato of this promocion
	*/
	@Override
	public long getIdEstrato() {
		return _promocion.getIdEstrato();
	}

	/**
	* Sets the id estrato of this promocion.
	*
	* @param idEstrato the id estrato of this promocion
	*/
	@Override
	public void setIdEstrato(long idEstrato) {
		_promocion.setIdEstrato(idEstrato);
	}

	/**
	* Returns the id grupo ciudad of this promocion.
	*
	* @return the id grupo ciudad of this promocion
	*/
	@Override
	public long getIdGrupoCiudad() {
		return _promocion.getIdGrupoCiudad();
	}

	/**
	* Sets the id grupo ciudad of this promocion.
	*
	* @param idGrupoCiudad the id grupo ciudad of this promocion
	*/
	@Override
	public void setIdGrupoCiudad(long idGrupoCiudad) {
		_promocion.setIdGrupoCiudad(idGrupoCiudad);
	}

	/**
	* Returns the id tipo mx of this promocion.
	*
	* @return the id tipo mx of this promocion
	*/
	@Override
	public long getIdTipoMx() {
		return _promocion.getIdTipoMx();
	}

	/**
	* Sets the id tipo mx of this promocion.
	*
	* @param idTipoMx the id tipo mx of this promocion
	*/
	@Override
	public void setIdTipoMx(long idTipoMx) {
		_promocion.setIdTipoMx(idTipoMx);
	}

	/**
	* Returns the id ba of this promocion.
	*
	* @return the id ba of this promocion
	*/
	@Override
	public long getIdBa() {
		return _promocion.getIdBa();
	}

	/**
	* Sets the id ba of this promocion.
	*
	* @param idBa the id ba of this promocion
	*/
	@Override
	public void setIdBa(long idBa) {
		_promocion.setIdBa(idBa);
	}

	/**
	* Returns the id lb of this promocion.
	*
	* @return the id lb of this promocion
	*/
	@Override
	public long getIdLb() {
		return _promocion.getIdLb();
	}

	/**
	* Sets the id lb of this promocion.
	*
	* @param idLb the id lb of this promocion
	*/
	@Override
	public void setIdLb(long idLb) {
		_promocion.setIdLb(idLb);
	}

	/**
	* Returns the id tv of this promocion.
	*
	* @return the id tv of this promocion
	*/
	@Override
	public long getIdTv() {
		return _promocion.getIdTv();
	}

	/**
	* Sets the id tv of this promocion.
	*
	* @param idTv the id tv of this promocion
	*/
	@Override
	public void setIdTv(long idTv) {
		_promocion.setIdTv(idTv);
	}

	/**
	* Returns the divipola of this promocion.
	*
	* @return the divipola of this promocion
	*/
	@Override
	public long getDivipola() {
		return _promocion.getDivipola();
	}

	/**
	* Sets the divipola of this promocion.
	*
	* @param divipola the divipola of this promocion
	*/
	@Override
	public void setDivipola(long divipola) {
		_promocion.setDivipola(divipola);
	}

	/**
	* Returns the tiem prom des of this promocion.
	*
	* @return the tiem prom des of this promocion
	*/
	@Override
	public long getTiemPromDes() {
		return _promocion.getTiemPromDes();
	}

	/**
	* Sets the tiem prom des of this promocion.
	*
	* @param tiemPromDes the tiem prom des of this promocion
	*/
	@Override
	public void setTiemPromDes(long tiemPromDes) {
		_promocion.setTiemPromDes(tiemPromDes);
	}

	/**
	* Returns the descuento of this promocion.
	*
	* @return the descuento of this promocion
	*/
	@Override
	public float getDescuento() {
		return _promocion.getDescuento();
	}

	/**
	* Sets the descuento of this promocion.
	*
	* @param descuento the descuento of this promocion
	*/
	@Override
	public void setDescuento(float descuento) {
		_promocion.setDescuento(descuento);
	}

	/**
	* Returns the descripcion of this promocion.
	*
	* @return the descripcion of this promocion
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _promocion.getDescripcion();
	}

	/**
	* Sets the descripcion of this promocion.
	*
	* @param descripcion the descripcion of this promocion
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_promocion.setDescripcion(descripcion);
	}

	/**
	* Returns the fecha of this promocion.
	*
	* @return the fecha of this promocion
	*/
	@Override
	public java.util.Date getFecha() {
		return _promocion.getFecha();
	}

	/**
	* Sets the fecha of this promocion.
	*
	* @param fecha the fecha of this promocion
	*/
	@Override
	public void setFecha(java.util.Date fecha) {
		_promocion.setFecha(fecha);
	}

	@Override
	public boolean isNew() {
		return _promocion.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_promocion.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _promocion.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_promocion.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _promocion.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _promocion.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_promocion.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _promocion.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_promocion.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_promocion.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_promocion.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PromocionWrapper((Promocion)_promocion.clone());
	}

	@Override
	public int compareTo(
		com.movistar.latam.colportal.homephone.model.Promocion promocion) {
		return _promocion.compareTo(promocion);
	}

	@Override
	public int hashCode() {
		return _promocion.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.movistar.latam.colportal.homephone.model.Promocion> toCacheModel() {
		return _promocion.toCacheModel();
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.Promocion toEscapedModel() {
		return new PromocionWrapper(_promocion.toEscapedModel());
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.Promocion toUnescapedModel() {
		return new PromocionWrapper(_promocion.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _promocion.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _promocion.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_promocion.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PromocionWrapper)) {
			return false;
		}

		PromocionWrapper promocionWrapper = (PromocionWrapper)obj;

		if (Validator.equals(_promocion, promocionWrapper._promocion)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Promocion getWrappedPromocion() {
		return _promocion;
	}

	@Override
	public Promocion getWrappedModel() {
		return _promocion;
	}

	@Override
	public void resetOriginalValues() {
		_promocion.resetOriginalValues();
	}

	private Promocion _promocion;
}