/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Producto}.
 * </p>
 *
 * @author vass
 * @see Producto
 * @generated
 */
public class ProductoWrapper implements Producto, ModelWrapper<Producto> {
	public ProductoWrapper(Producto producto) {
		_producto = producto;
	}

	@Override
	public Class<?> getModelClass() {
		return Producto.class;
	}

	@Override
	public String getModelClassName() {
		return Producto.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("idEstrato", getIdEstrato());
		attributes.put("idGrupoCiudad", getIdGrupoCiudad());
		attributes.put("idTipoMx", getIdTipoMx());
		attributes.put("idBa", getIdBa());
		attributes.put("idLb", getIdLb());
		attributes.put("idTv", getIdTv());
		attributes.put("idMercado", getIdMercado());
		attributes.put("nombre", getNombre());
		attributes.put("tarifa", getTarifa());
		attributes.put("tarifaIva", getTarifaIva());
		attributes.put("fecha", getFecha());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long idEstrato = (Long)attributes.get("idEstrato");

		if (idEstrato != null) {
			setIdEstrato(idEstrato);
		}

		Long idGrupoCiudad = (Long)attributes.get("idGrupoCiudad");

		if (idGrupoCiudad != null) {
			setIdGrupoCiudad(idGrupoCiudad);
		}

		Long idTipoMx = (Long)attributes.get("idTipoMx");

		if (idTipoMx != null) {
			setIdTipoMx(idTipoMx);
		}

		Long idBa = (Long)attributes.get("idBa");

		if (idBa != null) {
			setIdBa(idBa);
		}

		Long idLb = (Long)attributes.get("idLb");

		if (idLb != null) {
			setIdLb(idLb);
		}

		Long idTv = (Long)attributes.get("idTv");

		if (idTv != null) {
			setIdTv(idTv);
		}

		Long idMercado = (Long)attributes.get("idMercado");

		if (idMercado != null) {
			setIdMercado(idMercado);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Long tarifa = (Long)attributes.get("tarifa");

		if (tarifa != null) {
			setTarifa(tarifa);
		}

		Long tarifaIva = (Long)attributes.get("tarifaIva");

		if (tarifaIva != null) {
			setTarifaIva(tarifaIva);
		}

		Date fecha = (Date)attributes.get("fecha");

		if (fecha != null) {
			setFecha(fecha);
		}
	}

	/**
	* Returns the primary key of this producto.
	*
	* @return the primary key of this producto
	*/
	@Override
	public long getPrimaryKey() {
		return _producto.getPrimaryKey();
	}

	/**
	* Sets the primary key of this producto.
	*
	* @param primaryKey the primary key of this producto
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_producto.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this producto.
	*
	* @return the ID of this producto
	*/
	@Override
	public long getId() {
		return _producto.getId();
	}

	/**
	* Sets the ID of this producto.
	*
	* @param id the ID of this producto
	*/
	@Override
	public void setId(long id) {
		_producto.setId(id);
	}

	/**
	* Returns the id estrato of this producto.
	*
	* @return the id estrato of this producto
	*/
	@Override
	public long getIdEstrato() {
		return _producto.getIdEstrato();
	}

	/**
	* Sets the id estrato of this producto.
	*
	* @param idEstrato the id estrato of this producto
	*/
	@Override
	public void setIdEstrato(long idEstrato) {
		_producto.setIdEstrato(idEstrato);
	}

	/**
	* Returns the id grupo ciudad of this producto.
	*
	* @return the id grupo ciudad of this producto
	*/
	@Override
	public long getIdGrupoCiudad() {
		return _producto.getIdGrupoCiudad();
	}

	/**
	* Sets the id grupo ciudad of this producto.
	*
	* @param idGrupoCiudad the id grupo ciudad of this producto
	*/
	@Override
	public void setIdGrupoCiudad(long idGrupoCiudad) {
		_producto.setIdGrupoCiudad(idGrupoCiudad);
	}

	/**
	* Returns the id tipo mx of this producto.
	*
	* @return the id tipo mx of this producto
	*/
	@Override
	public long getIdTipoMx() {
		return _producto.getIdTipoMx();
	}

	/**
	* Sets the id tipo mx of this producto.
	*
	* @param idTipoMx the id tipo mx of this producto
	*/
	@Override
	public void setIdTipoMx(long idTipoMx) {
		_producto.setIdTipoMx(idTipoMx);
	}

	/**
	* Returns the id ba of this producto.
	*
	* @return the id ba of this producto
	*/
	@Override
	public long getIdBa() {
		return _producto.getIdBa();
	}

	/**
	* Sets the id ba of this producto.
	*
	* @param idBa the id ba of this producto
	*/
	@Override
	public void setIdBa(long idBa) {
		_producto.setIdBa(idBa);
	}

	/**
	* Returns the id lb of this producto.
	*
	* @return the id lb of this producto
	*/
	@Override
	public long getIdLb() {
		return _producto.getIdLb();
	}

	/**
	* Sets the id lb of this producto.
	*
	* @param idLb the id lb of this producto
	*/
	@Override
	public void setIdLb(long idLb) {
		_producto.setIdLb(idLb);
	}

	/**
	* Returns the id tv of this producto.
	*
	* @return the id tv of this producto
	*/
	@Override
	public long getIdTv() {
		return _producto.getIdTv();
	}

	/**
	* Sets the id tv of this producto.
	*
	* @param idTv the id tv of this producto
	*/
	@Override
	public void setIdTv(long idTv) {
		_producto.setIdTv(idTv);
	}

	/**
	* Returns the id mercado of this producto.
	*
	* @return the id mercado of this producto
	*/
	@Override
	public long getIdMercado() {
		return _producto.getIdMercado();
	}

	/**
	* Sets the id mercado of this producto.
	*
	* @param idMercado the id mercado of this producto
	*/
	@Override
	public void setIdMercado(long idMercado) {
		_producto.setIdMercado(idMercado);
	}

	/**
	* Returns the nombre of this producto.
	*
	* @return the nombre of this producto
	*/
	@Override
	public java.lang.String getNombre() {
		return _producto.getNombre();
	}

	/**
	* Sets the nombre of this producto.
	*
	* @param nombre the nombre of this producto
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_producto.setNombre(nombre);
	}

	/**
	* Returns the tarifa of this producto.
	*
	* @return the tarifa of this producto
	*/
	@Override
	public long getTarifa() {
		return _producto.getTarifa();
	}

	/**
	* Sets the tarifa of this producto.
	*
	* @param tarifa the tarifa of this producto
	*/
	@Override
	public void setTarifa(long tarifa) {
		_producto.setTarifa(tarifa);
	}

	/**
	* Returns the tarifa iva of this producto.
	*
	* @return the tarifa iva of this producto
	*/
	@Override
	public long getTarifaIva() {
		return _producto.getTarifaIva();
	}

	/**
	* Sets the tarifa iva of this producto.
	*
	* @param tarifaIva the tarifa iva of this producto
	*/
	@Override
	public void setTarifaIva(long tarifaIva) {
		_producto.setTarifaIva(tarifaIva);
	}

	/**
	* Returns the fecha of this producto.
	*
	* @return the fecha of this producto
	*/
	@Override
	public java.util.Date getFecha() {
		return _producto.getFecha();
	}

	/**
	* Sets the fecha of this producto.
	*
	* @param fecha the fecha of this producto
	*/
	@Override
	public void setFecha(java.util.Date fecha) {
		_producto.setFecha(fecha);
	}

	@Override
	public boolean isNew() {
		return _producto.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_producto.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _producto.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_producto.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _producto.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _producto.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_producto.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _producto.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_producto.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_producto.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_producto.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ProductoWrapper((Producto)_producto.clone());
	}

	@Override
	public int compareTo(
		com.movistar.latam.colportal.homephone.model.Producto producto) {
		return _producto.compareTo(producto);
	}

	@Override
	public int hashCode() {
		return _producto.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.movistar.latam.colportal.homephone.model.Producto> toCacheModel() {
		return _producto.toCacheModel();
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.Producto toEscapedModel() {
		return new ProductoWrapper(_producto.toEscapedModel());
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.Producto toUnescapedModel() {
		return new ProductoWrapper(_producto.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _producto.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _producto.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_producto.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProductoWrapper)) {
			return false;
		}

		ProductoWrapper productoWrapper = (ProductoWrapper)obj;

		if (Validator.equals(_producto, productoWrapper._producto)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Producto getWrappedProducto() {
		return _producto;
	}

	@Override
	public Producto getWrappedModel() {
		return _producto;
	}

	@Override
	public void resetOriginalValues() {
		_producto.resetOriginalValues();
	}

	private Producto _producto;
}