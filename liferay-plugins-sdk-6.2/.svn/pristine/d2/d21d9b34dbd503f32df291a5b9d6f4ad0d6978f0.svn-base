/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.movistar.latam.colportal.homephone.NoSuchTipoMxException;
import com.movistar.latam.colportal.homephone.model.TipoMx;
import com.movistar.latam.colportal.homephone.model.impl.TipoMxImpl;
import com.movistar.latam.colportal.homephone.model.impl.TipoMxModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the tipo mx service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see TipoMxPersistence
 * @see TipoMxUtil
 * @generated
 */
public class TipoMxPersistenceImpl extends BasePersistenceImpl<TipoMx>
	implements TipoMxPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TipoMxUtil} to access the tipo mx persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TipoMxImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
			TipoMxModelImpl.FINDER_CACHE_ENABLED, TipoMxImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
			TipoMxModelImpl.FINDER_CACHE_ENABLED, TipoMxImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
			TipoMxModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_NOMBRE = new FinderPath(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
			TipoMxModelImpl.FINDER_CACHE_ENABLED, TipoMxImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByNombre",
			new String[] { String.class.getName() },
			TipoMxModelImpl.NOMBRE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NOMBRE = new FinderPath(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
			TipoMxModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByNombre",
			new String[] { String.class.getName() });

	/**
	 * Returns the tipo mx where nombre = &#63; or throws a {@link com.movistar.latam.colportal.homephone.NoSuchTipoMxException} if it could not be found.
	 *
	 * @param nombre the nombre
	 * @return the matching tipo mx
	 * @throws com.movistar.latam.colportal.homephone.NoSuchTipoMxException if a matching tipo mx could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx findByNombre(String nombre)
		throws NoSuchTipoMxException, SystemException {
		TipoMx tipoMx = fetchByNombre(nombre);

		if (tipoMx == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("nombre=");
			msg.append(nombre);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTipoMxException(msg.toString());
		}

		return tipoMx;
	}

	/**
	 * Returns the tipo mx where nombre = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param nombre the nombre
	 * @return the matching tipo mx, or <code>null</code> if a matching tipo mx could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx fetchByNombre(String nombre) throws SystemException {
		return fetchByNombre(nombre, true);
	}

	/**
	 * Returns the tipo mx where nombre = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param nombre the nombre
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching tipo mx, or <code>null</code> if a matching tipo mx could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx fetchByNombre(String nombre, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { nombre };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_NOMBRE,
					finderArgs, this);
		}

		if (result instanceof TipoMx) {
			TipoMx tipoMx = (TipoMx)result;

			if (!Validator.equals(nombre, tipoMx.getNombre())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TIPOMX_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				List<TipoMx> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NOMBRE,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"TipoMxPersistenceImpl.fetchByNombre(String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					TipoMx tipoMx = list.get(0);

					result = tipoMx;

					cacheResult(tipoMx);

					if ((tipoMx.getNombre() == null) ||
							!tipoMx.getNombre().equals(nombre)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NOMBRE,
							finderArgs, tipoMx);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NOMBRE,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TipoMx)result;
		}
	}

	/**
	 * Removes the tipo mx where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 * @return the tipo mx that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx removeByNombre(String nombre)
		throws NoSuchTipoMxException, SystemException {
		TipoMx tipoMx = findByNombre(nombre);

		return remove(tipoMx);
	}

	/**
	 * Returns the number of tipo mxs where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching tipo mxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByNombre(String nombre) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NOMBRE;

		Object[] finderArgs = new Object[] { nombre };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TIPOMX_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_1 = "tipoMx.nombre IS NULL";
	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_2 = "tipoMx.nombre = ?";
	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_3 = "(tipoMx.nombre IS NULL OR tipoMx.nombre = '')";

	public TipoMxPersistenceImpl() {
		setModelClass(TipoMx.class);
	}

	/**
	 * Caches the tipo mx in the entity cache if it is enabled.
	 *
	 * @param tipoMx the tipo mx
	 */
	@Override
	public void cacheResult(TipoMx tipoMx) {
		EntityCacheUtil.putResult(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
			TipoMxImpl.class, tipoMx.getPrimaryKey(), tipoMx);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NOMBRE,
			new Object[] { tipoMx.getNombre() }, tipoMx);

		tipoMx.resetOriginalValues();
	}

	/**
	 * Caches the tipo mxs in the entity cache if it is enabled.
	 *
	 * @param tipoMxs the tipo mxs
	 */
	@Override
	public void cacheResult(List<TipoMx> tipoMxs) {
		for (TipoMx tipoMx : tipoMxs) {
			if (EntityCacheUtil.getResult(
						TipoMxModelImpl.ENTITY_CACHE_ENABLED, TipoMxImpl.class,
						tipoMx.getPrimaryKey()) == null) {
				cacheResult(tipoMx);
			}
			else {
				tipoMx.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all tipo mxs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TipoMxImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TipoMxImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the tipo mx.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TipoMx tipoMx) {
		EntityCacheUtil.removeResult(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
			TipoMxImpl.class, tipoMx.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(tipoMx);
	}

	@Override
	public void clearCache(List<TipoMx> tipoMxs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TipoMx tipoMx : tipoMxs) {
			EntityCacheUtil.removeResult(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
				TipoMxImpl.class, tipoMx.getPrimaryKey());

			clearUniqueFindersCache(tipoMx);
		}
	}

	protected void cacheUniqueFindersCache(TipoMx tipoMx) {
		if (tipoMx.isNew()) {
			Object[] args = new Object[] { tipoMx.getNombre() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NOMBRE, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NOMBRE, args, tipoMx);
		}
		else {
			TipoMxModelImpl tipoMxModelImpl = (TipoMxModelImpl)tipoMx;

			if ((tipoMxModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_NOMBRE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { tipoMx.getNombre() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NOMBRE, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NOMBRE, args,
					tipoMx);
			}
		}
	}

	protected void clearUniqueFindersCache(TipoMx tipoMx) {
		TipoMxModelImpl tipoMxModelImpl = (TipoMxModelImpl)tipoMx;

		Object[] args = new Object[] { tipoMx.getNombre() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NOMBRE, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NOMBRE, args);

		if ((tipoMxModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_NOMBRE.getColumnBitmask()) != 0) {
			args = new Object[] { tipoMxModelImpl.getOriginalNombre() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NOMBRE, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NOMBRE, args);
		}
	}

	/**
	 * Creates a new tipo mx with the primary key. Does not add the tipo mx to the database.
	 *
	 * @param id the primary key for the new tipo mx
	 * @return the new tipo mx
	 */
	@Override
	public TipoMx create(long id) {
		TipoMx tipoMx = new TipoMxImpl();

		tipoMx.setNew(true);
		tipoMx.setPrimaryKey(id);

		return tipoMx;
	}

	/**
	 * Removes the tipo mx with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the tipo mx
	 * @return the tipo mx that was removed
	 * @throws com.movistar.latam.colportal.homephone.NoSuchTipoMxException if a tipo mx with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx remove(long id) throws NoSuchTipoMxException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the tipo mx with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the tipo mx
	 * @return the tipo mx that was removed
	 * @throws com.movistar.latam.colportal.homephone.NoSuchTipoMxException if a tipo mx with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx remove(Serializable primaryKey)
		throws NoSuchTipoMxException, SystemException {
		Session session = null;

		try {
			session = openSession();

			TipoMx tipoMx = (TipoMx)session.get(TipoMxImpl.class, primaryKey);

			if (tipoMx == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTipoMxException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(tipoMx);
		}
		catch (NoSuchTipoMxException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TipoMx removeImpl(TipoMx tipoMx) throws SystemException {
		tipoMx = toUnwrappedModel(tipoMx);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(tipoMx)) {
				tipoMx = (TipoMx)session.get(TipoMxImpl.class,
						tipoMx.getPrimaryKeyObj());
			}

			if (tipoMx != null) {
				session.delete(tipoMx);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (tipoMx != null) {
			clearCache(tipoMx);
		}

		return tipoMx;
	}

	@Override
	public TipoMx updateImpl(
		com.movistar.latam.colportal.homephone.model.TipoMx tipoMx)
		throws SystemException {
		tipoMx = toUnwrappedModel(tipoMx);

		boolean isNew = tipoMx.isNew();

		Session session = null;

		try {
			session = openSession();

			if (tipoMx.isNew()) {
				session.save(tipoMx);

				tipoMx.setNew(false);
			}
			else {
				session.merge(tipoMx);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TipoMxModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
			TipoMxImpl.class, tipoMx.getPrimaryKey(), tipoMx);

		clearUniqueFindersCache(tipoMx);
		cacheUniqueFindersCache(tipoMx);

		return tipoMx;
	}

	protected TipoMx toUnwrappedModel(TipoMx tipoMx) {
		if (tipoMx instanceof TipoMxImpl) {
			return tipoMx;
		}

		TipoMxImpl tipoMxImpl = new TipoMxImpl();

		tipoMxImpl.setNew(tipoMx.isNew());
		tipoMxImpl.setPrimaryKey(tipoMx.getPrimaryKey());

		tipoMxImpl.setId(tipoMx.getId());
		tipoMxImpl.setNombre(tipoMx.getNombre());
		tipoMxImpl.setDescripcion(tipoMx.getDescripcion());
		tipoMxImpl.setFecha(tipoMx.getFecha());

		return tipoMxImpl;
	}

	/**
	 * Returns the tipo mx with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the tipo mx
	 * @return the tipo mx
	 * @throws com.movistar.latam.colportal.homephone.NoSuchTipoMxException if a tipo mx with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTipoMxException, SystemException {
		TipoMx tipoMx = fetchByPrimaryKey(primaryKey);

		if (tipoMx == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTipoMxException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return tipoMx;
	}

	/**
	 * Returns the tipo mx with the primary key or throws a {@link com.movistar.latam.colportal.homephone.NoSuchTipoMxException} if it could not be found.
	 *
	 * @param id the primary key of the tipo mx
	 * @return the tipo mx
	 * @throws com.movistar.latam.colportal.homephone.NoSuchTipoMxException if a tipo mx with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx findByPrimaryKey(long id)
		throws NoSuchTipoMxException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the tipo mx with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the tipo mx
	 * @return the tipo mx, or <code>null</code> if a tipo mx with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		TipoMx tipoMx = (TipoMx)EntityCacheUtil.getResult(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
				TipoMxImpl.class, primaryKey);

		if (tipoMx == _nullTipoMx) {
			return null;
		}

		if (tipoMx == null) {
			Session session = null;

			try {
				session = openSession();

				tipoMx = (TipoMx)session.get(TipoMxImpl.class, primaryKey);

				if (tipoMx != null) {
					cacheResult(tipoMx);
				}
				else {
					EntityCacheUtil.putResult(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
						TipoMxImpl.class, primaryKey, _nullTipoMx);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(TipoMxModelImpl.ENTITY_CACHE_ENABLED,
					TipoMxImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return tipoMx;
	}

	/**
	 * Returns the tipo mx with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the tipo mx
	 * @return the tipo mx, or <code>null</code> if a tipo mx with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TipoMx fetchByPrimaryKey(long id) throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the tipo mxs.
	 *
	 * @return the tipo mxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TipoMx> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tipo mxs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.TipoMxModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tipo mxs
	 * @param end the upper bound of the range of tipo mxs (not inclusive)
	 * @return the range of tipo mxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TipoMx> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the tipo mxs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.TipoMxModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tipo mxs
	 * @param end the upper bound of the range of tipo mxs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tipo mxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TipoMx> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TipoMx> list = (List<TipoMx>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TIPOMX);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TIPOMX;

				if (pagination) {
					sql = sql.concat(TipoMxModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TipoMx>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TipoMx>(list);
				}
				else {
					list = (List<TipoMx>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the tipo mxs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (TipoMx tipoMx : findAll()) {
			remove(tipoMx);
		}
	}

	/**
	 * Returns the number of tipo mxs.
	 *
	 * @return the number of tipo mxs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TIPOMX);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the tipo mx persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.movistar.latam.colportal.homephone.model.TipoMx")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<TipoMx>> listenersList = new ArrayList<ModelListener<TipoMx>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<TipoMx>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TipoMxImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_TIPOMX = "SELECT tipoMx FROM TipoMx tipoMx";
	private static final String _SQL_SELECT_TIPOMX_WHERE = "SELECT tipoMx FROM TipoMx tipoMx WHERE ";
	private static final String _SQL_COUNT_TIPOMX = "SELECT COUNT(tipoMx) FROM TipoMx tipoMx";
	private static final String _SQL_COUNT_TIPOMX_WHERE = "SELECT COUNT(tipoMx) FROM TipoMx tipoMx WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "tipoMx.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TipoMx exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TipoMx exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TipoMxPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static TipoMx _nullTipoMx = new TipoMxImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<TipoMx> toCacheModel() {
				return _nullTipoMxCacheModel;
			}
		};

	private static CacheModel<TipoMx> _nullTipoMxCacheModel = new CacheModel<TipoMx>() {
			@Override
			public TipoMx toEntityModel() {
				return _nullTipoMx;
			}
		};
}