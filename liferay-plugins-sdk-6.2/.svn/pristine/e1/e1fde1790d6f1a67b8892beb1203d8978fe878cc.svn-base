/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.movistar.latam.colportal.homephone.service.ClpSerializer;
import com.movistar.latam.colportal.homephone.service.ProductoLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vass
 */
public class ProductoClp extends BaseModelImpl<Producto> implements Producto {
	public ProductoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Producto.class;
	}

	@Override
	public String getModelClassName() {
		return Producto.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("idEstrato", getIdEstrato());
		attributes.put("idGrupoCiudad", getIdGrupoCiudad());
		attributes.put("idTipoMx", getIdTipoMx());
		attributes.put("idBa", getIdBa());
		attributes.put("idLb", getIdLb());
		attributes.put("idTv", getIdTv());
		attributes.put("idMercado", getIdMercado());
		attributes.put("nombre", getNombre());
		attributes.put("tarifa", getTarifa());
		attributes.put("tarifaIva", getTarifaIva());
		attributes.put("fecha", getFecha());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long idEstrato = (Long)attributes.get("idEstrato");

		if (idEstrato != null) {
			setIdEstrato(idEstrato);
		}

		Long idGrupoCiudad = (Long)attributes.get("idGrupoCiudad");

		if (idGrupoCiudad != null) {
			setIdGrupoCiudad(idGrupoCiudad);
		}

		Long idTipoMx = (Long)attributes.get("idTipoMx");

		if (idTipoMx != null) {
			setIdTipoMx(idTipoMx);
		}

		Long idBa = (Long)attributes.get("idBa");

		if (idBa != null) {
			setIdBa(idBa);
		}

		Long idLb = (Long)attributes.get("idLb");

		if (idLb != null) {
			setIdLb(idLb);
		}

		Long idTv = (Long)attributes.get("idTv");

		if (idTv != null) {
			setIdTv(idTv);
		}

		Long idMercado = (Long)attributes.get("idMercado");

		if (idMercado != null) {
			setIdMercado(idMercado);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Long tarifa = (Long)attributes.get("tarifa");

		if (tarifa != null) {
			setTarifa(tarifa);
		}

		Long tarifaIva = (Long)attributes.get("tarifaIva");

		if (tarifaIva != null) {
			setTarifaIva(tarifaIva);
		}

		Date fecha = (Date)attributes.get("fecha");

		if (fecha != null) {
			setFecha(fecha);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setId", long.class);

				method.invoke(_productoRemoteModel, id);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdEstrato() {
		return _idEstrato;
	}

	@Override
	public void setIdEstrato(long idEstrato) {
		_idEstrato = idEstrato;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdEstrato", long.class);

				method.invoke(_productoRemoteModel, idEstrato);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdGrupoCiudad() {
		return _idGrupoCiudad;
	}

	@Override
	public void setIdGrupoCiudad(long idGrupoCiudad) {
		_idGrupoCiudad = idGrupoCiudad;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdGrupoCiudad", long.class);

				method.invoke(_productoRemoteModel, idGrupoCiudad);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdTipoMx() {
		return _idTipoMx;
	}

	@Override
	public void setIdTipoMx(long idTipoMx) {
		_idTipoMx = idTipoMx;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdTipoMx", long.class);

				method.invoke(_productoRemoteModel, idTipoMx);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdBa() {
		return _idBa;
	}

	@Override
	public void setIdBa(long idBa) {
		_idBa = idBa;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdBa", long.class);

				method.invoke(_productoRemoteModel, idBa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdLb() {
		return _idLb;
	}

	@Override
	public void setIdLb(long idLb) {
		_idLb = idLb;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdLb", long.class);

				method.invoke(_productoRemoteModel, idLb);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdTv() {
		return _idTv;
	}

	@Override
	public void setIdTv(long idTv) {
		_idTv = idTv;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdTv", long.class);

				method.invoke(_productoRemoteModel, idTv);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdMercado() {
		return _idMercado;
	}

	@Override
	public void setIdMercado(long idMercado) {
		_idMercado = idMercado;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdMercado", long.class);

				method.invoke(_productoRemoteModel, idMercado);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNombre() {
		return _nombre;
	}

	@Override
	public void setNombre(String nombre) {
		_nombre = nombre;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setNombre", String.class);

				method.invoke(_productoRemoteModel, nombre);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTarifa() {
		return _tarifa;
	}

	@Override
	public void setTarifa(long tarifa) {
		_tarifa = tarifa;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setTarifa", long.class);

				method.invoke(_productoRemoteModel, tarifa);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTarifaIva() {
		return _tarifaIva;
	}

	@Override
	public void setTarifaIva(long tarifaIva) {
		_tarifaIva = tarifaIva;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setTarifaIva", long.class);

				method.invoke(_productoRemoteModel, tarifaIva);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getFecha() {
		return _fecha;
	}

	@Override
	public void setFecha(Date fecha) {
		_fecha = fecha;

		if (_productoRemoteModel != null) {
			try {
				Class<?> clazz = _productoRemoteModel.getClass();

				Method method = clazz.getMethod("setFecha", Date.class);

				method.invoke(_productoRemoteModel, fecha);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getProductoRemoteModel() {
		return _productoRemoteModel;
	}

	public void setProductoRemoteModel(BaseModel<?> productoRemoteModel) {
		_productoRemoteModel = productoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _productoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_productoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ProductoLocalServiceUtil.addProducto(this);
		}
		else {
			ProductoLocalServiceUtil.updateProducto(this);
		}
	}

	@Override
	public Producto toEscapedModel() {
		return (Producto)ProxyUtil.newProxyInstance(Producto.class.getClassLoader(),
			new Class[] { Producto.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ProductoClp clone = new ProductoClp();

		clone.setId(getId());
		clone.setIdEstrato(getIdEstrato());
		clone.setIdGrupoCiudad(getIdGrupoCiudad());
		clone.setIdTipoMx(getIdTipoMx());
		clone.setIdBa(getIdBa());
		clone.setIdLb(getIdLb());
		clone.setIdTv(getIdTv());
		clone.setIdMercado(getIdMercado());
		clone.setNombre(getNombre());
		clone.setTarifa(getTarifa());
		clone.setTarifaIva(getTarifaIva());
		clone.setFecha(getFecha());

		return clone;
	}

	@Override
	public int compareTo(Producto producto) {
		int value = 0;

		value = getNombre().compareTo(producto.getNombre());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProductoClp)) {
			return false;
		}

		ProductoClp producto = (ProductoClp)obj;

		long primaryKey = producto.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", idEstrato=");
		sb.append(getIdEstrato());
		sb.append(", idGrupoCiudad=");
		sb.append(getIdGrupoCiudad());
		sb.append(", idTipoMx=");
		sb.append(getIdTipoMx());
		sb.append(", idBa=");
		sb.append(getIdBa());
		sb.append(", idLb=");
		sb.append(getIdLb());
		sb.append(", idTv=");
		sb.append(getIdTv());
		sb.append(", idMercado=");
		sb.append(getIdMercado());
		sb.append(", nombre=");
		sb.append(getNombre());
		sb.append(", tarifa=");
		sb.append(getTarifa());
		sb.append(", tarifaIva=");
		sb.append(getTarifaIva());
		sb.append(", fecha=");
		sb.append(getFecha());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(40);

		sb.append("<model><model-name>");
		sb.append("com.movistar.latam.colportal.homephone.model.Producto");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idEstrato</column-name><column-value><![CDATA[");
		sb.append(getIdEstrato());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idGrupoCiudad</column-name><column-value><![CDATA[");
		sb.append(getIdGrupoCiudad());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idTipoMx</column-name><column-value><![CDATA[");
		sb.append(getIdTipoMx());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idBa</column-name><column-value><![CDATA[");
		sb.append(getIdBa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idLb</column-name><column-value><![CDATA[");
		sb.append(getIdLb());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idTv</column-name><column-value><![CDATA[");
		sb.append(getIdTv());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idMercado</column-name><column-value><![CDATA[");
		sb.append(getIdMercado());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nombre</column-name><column-value><![CDATA[");
		sb.append(getNombre());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tarifa</column-name><column-value><![CDATA[");
		sb.append(getTarifa());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tarifaIva</column-name><column-value><![CDATA[");
		sb.append(getTarifaIva());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fecha</column-name><column-value><![CDATA[");
		sb.append(getFecha());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _id;
	private long _idEstrato;
	private long _idGrupoCiudad;
	private long _idTipoMx;
	private long _idBa;
	private long _idLb;
	private long _idTv;
	private long _idMercado;
	private String _nombre;
	private long _tarifa;
	private long _tarifaIva;
	private Date _fecha;
	private BaseModel<?> _productoRemoteModel;
}