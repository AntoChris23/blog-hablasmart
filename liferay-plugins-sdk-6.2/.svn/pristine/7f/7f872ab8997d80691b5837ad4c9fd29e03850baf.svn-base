/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BA}.
 * </p>
 *
 * @author vass
 * @see BA
 * @generated
 */
public class BAWrapper implements BA, ModelWrapper<BA> {
	public BAWrapper(BA ba) {
		_ba = ba;
	}

	@Override
	public Class<?> getModelClass() {
		return BA.class;
	}

	@Override
	public String getModelClassName() {
		return BA.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("nombre", getNombre());
		attributes.put("fecha", getFecha());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Date fecha = (Date)attributes.get("fecha");

		if (fecha != null) {
			setFecha(fecha);
		}
	}

	/**
	* Returns the primary key of this b a.
	*
	* @return the primary key of this b a
	*/
	@Override
	public long getPrimaryKey() {
		return _ba.getPrimaryKey();
	}

	/**
	* Sets the primary key of this b a.
	*
	* @param primaryKey the primary key of this b a
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_ba.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this b a.
	*
	* @return the ID of this b a
	*/
	@Override
	public long getId() {
		return _ba.getId();
	}

	/**
	* Sets the ID of this b a.
	*
	* @param id the ID of this b a
	*/
	@Override
	public void setId(long id) {
		_ba.setId(id);
	}

	/**
	* Returns the nombre of this b a.
	*
	* @return the nombre of this b a
	*/
	@Override
	public java.lang.String getNombre() {
		return _ba.getNombre();
	}

	/**
	* Sets the nombre of this b a.
	*
	* @param nombre the nombre of this b a
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_ba.setNombre(nombre);
	}

	/**
	* Returns the fecha of this b a.
	*
	* @return the fecha of this b a
	*/
	@Override
	public java.util.Date getFecha() {
		return _ba.getFecha();
	}

	/**
	* Sets the fecha of this b a.
	*
	* @param fecha the fecha of this b a
	*/
	@Override
	public void setFecha(java.util.Date fecha) {
		_ba.setFecha(fecha);
	}

	@Override
	public boolean isNew() {
		return _ba.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_ba.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _ba.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ba.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _ba.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _ba.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_ba.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _ba.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_ba.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_ba.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_ba.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BAWrapper((BA)_ba.clone());
	}

	@Override
	public int compareTo(com.movistar.latam.colportal.homephone.model.BA ba) {
		return _ba.compareTo(ba);
	}

	@Override
	public int hashCode() {
		return _ba.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.movistar.latam.colportal.homephone.model.BA> toCacheModel() {
		return _ba.toCacheModel();
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.BA toEscapedModel() {
		return new BAWrapper(_ba.toEscapedModel());
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.BA toUnescapedModel() {
		return new BAWrapper(_ba.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _ba.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ba.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_ba.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BAWrapper)) {
			return false;
		}

		BAWrapper baWrapper = (BAWrapper)obj;

		if (Validator.equals(_ba, baWrapper._ba)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public BA getWrappedBA() {
		return _ba;
	}

	@Override
	public BA getWrappedModel() {
		return _ba;
	}

	@Override
	public void resetOriginalValues() {
		_ba.resetOriginalValues();
	}

	private BA _ba;
}