/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.ddlform.hook.upgrade.v1_0_0;

import com.liferay.portal.kernel.upgrade.BaseUpgradePortletPreferences;
import com.liferay.portlet.PortletPreferencesFactoryUtil;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.PortletPreferences;

/**
 * @author Marcellus Tavares
 */
public class UpgradePortletPreferences extends BaseUpgradePortletPreferences {

	public UpgradePortletPreferences() {
		_preferencesMap.put("detailDDMTemplateId", "formDDMTemplateId");
	}

	protected String[] getPortletIds() {
		return new String[] { "1_WAR_ddlformportlet_INSTANCE_%" };
	}

	protected String upgradePreferences(long companyId, long ownerId,
			int ownerType, long plid, String portletId, String xml)
			throws Exception {

		PortletPreferences preferences = PortletPreferencesFactoryUtil.fromXML(
				companyId, ownerId, ownerType, plid, portletId, xml);

		Map<String, String[]> preferencesMap = preferences.getMap();

		for (String name : _preferencesMap.keySet()) {
			String[] values = preferencesMap.get(name);

			if (values == null) {
				continue;
			}

			preferences.reset(name);

			preferences.setValues(_preferencesMap.get(name), values);
		}

		return PortletPreferencesFactoryUtil.toXML(preferences);
	}

	private Map<String, String> _preferencesMap = new HashMap<String, String>();

}