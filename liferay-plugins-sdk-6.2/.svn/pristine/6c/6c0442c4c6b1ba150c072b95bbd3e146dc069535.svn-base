/*
 * 
 */
package com.movistar.latam.colportal.cam.hook.init;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalFolder;
import com.liferay.portlet.journal.model.JournalStructure;
import com.liferay.portlet.journal.model.JournalTemplate;
import com.liferay.portlet.wiki.model.WikiNode;
import com.liferay.portlet.wiki.model.WikiPage;
import com.movistar.latam.colportal.cam.hook.init.util.RoleUtil;
import com.movistar.latam.colportal.cam.hook.init.util.UserUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ColportalInitDataAction:.
 * 
 * @author VASS
 * @version 1.0
 */
public class ColportalCAMInitDataAction extends SimpleAction {

	/** ASSET_CATEGORY_HELP. */
	private static final String ASSET_CATEGORY_HELP = "Ayuda";

	/**
	 * PortalIntance:.
	 * 
	 * @author VASS
	 * @version 1.0
	 */
	private enum PortalIntance {

		/** Panama. */
		PANAMA("panama", "movistar.com.pa"), /** Costarica. */
		COSTARICA("costarica", "movistar.cr"),
		/** Elsalvador. */
		ELSALVADOR("elsalvador", "movistar.com.sv"),
		/** Guatemala. */
		GUATEMALA("guatemala", "movistar.com.gt"),
		/** Nicaragua. */
		NICARAGUA("nicaragua", "movistar.com.ni");

		/** Web id. */
		private String webId;

		/** Domain. */
		private String domain;

		/**
		 * Instancia portal intance.
		 * 
		 * @param webId
		 *            web id
		 * @param domain
		 *            domain
		 */
		private PortalIntance(String webId, String domain) {
			this.webId = webId;
			this.domain = domain;
		}

		/**
		 * Obtiene web id.
		 * 
		 * @return Web id
		 */
		public String getWebId() {
			return webId;
		}

		/**
		 * Obtiene domain.
		 * 
		 * @return Domain
		 */
		public String getDomain() {
			return domain;
		}

	}

	/** _log. */
	private static Log _log = LogFactoryUtil
			.getLog(ColportalCAMInitDataAction.class);

	/*
	 * (non-Java-doc)
	 * 
	 * @see com.liferay.portal.kernel.events.SimpleAction#SimpleAction()
	 */
	/**
	 * Instancia colportal init data action.
	 */
	public ColportalCAMInitDataAction() {
		super();
	}

	/*
	 * (non-Java-doc)
	 * 
	 * @see com.liferay.portal.kernel.events.SimpleAction#run(String[] ids)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.liferay.portal.kernel.events.SimpleAction#run(java.lang.String[])
	 */
	@Override
	public void run(String[] ids) throws ActionException {
		for (PortalIntance instance : PortalIntance.values()) {
			try {
				Company company = CompanyLocalServiceUtil
						.getCompanyByWebId(instance.getWebId());
//				if (company != null) {
//					initCompany(company);
//				}
			} catch (PortalException e) {
				try {
					Company company = CompanyLocalServiceUtil.addCompany(
							instance.getWebId(), instance.getDomain(),
							instance.getDomain(), instance.getWebId(), false,
							0, true);
//					if (company != null) {
//						initCompany(company);
//					}
				} catch (PortalException | SystemException e1) {
					if (_log.isErrorEnabled()) {
						_log.error(
								"Error creando instancia "
										+ instance.getWebId(), e1);
					}
				}
				if (_log.isInfoEnabled()) {
					_log.info("Instancia " + instance.getWebId() + " creada");
				}
			} catch (SystemException e) {
				if (_log.isErrorEnabled()) {
					_log.error("Error inicializando instancias", e);
				}
			}
		}
	}

	private void initCompany(Company company) {
		try {
			long companyId = company.getCompanyId();
			long defaultUserId = UserLocalServiceUtil
					.getDefaultUserId(companyId);
			long groupId = GroupLocalServiceUtil.getGroup(companyId,
					GroupConstants.GUEST).getGroupId();

			PrincipalThreadLocal.setName(defaultUserId);
			setupRoles(companyId, defaultUserId);
			setupUsers(companyId);
			setupCategories(companyId, defaultUserId, groupId);
		} catch (Exception exc) {
			if (_log.isErrorEnabled()) {
				_log.error("Error inicializando los datos ", exc);
			}
		}
	}

	/**
	 * Setup roles.
	 * 
	 * @param companyId
	 *            company id
	 * @param userId
	 *            user id
	 * @return list
	 */
	protected List<Role> setupRoles(long companyId, long userId) {
		List<Role> roles = new ArrayList<Role>();
		try {
			Map<String, String[]> roleEditorPermissions = getRoleEditorPermissions();
			Role roleEditor = RoleUtil.addRole(companyId, userId,
					RoleUtil.PORTAL_CONTENT_EDITOR,
					RoleUtil.PORTAL_CONTENT_EDITOR, RoleConstants.TYPE_REGULAR,
					roleEditorPermissions);
			roles.add(roleEditor);
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error creando role "
						+ RoleUtil.PORTAL_CONTENT_EDITOR, e);
			}
		}
		/* Grupo publisher */
		try {
			Map<String, String[]> rolePublisherPermissions = getRolePublisherPermissions();
			Role rolePublisher = RoleUtil.addRole(companyId, userId,
					RoleUtil.PORTAL_CONTENT_PUBLISHER,
					RoleUtil.PORTAL_CONTENT_PUBLISHER,
					RoleConstants.TYPE_REGULAR, rolePublisherPermissions);
			roles.add(rolePublisher);
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error creando role "
						+ RoleUtil.PORTAL_CONTENT_PUBLISHER, e);
			}
		}
		/* Grupo Validador */
		try {
			Role roleReviewer = RoleLocalServiceUtil.fetchRole(companyId,
					RoleUtil.PORTAL_CONTENT_REVIEWER);
			roles.add(roleReviewer);
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error creando role "
						+ RoleUtil.PORTAL_CONTENT_PUBLISHER, e);
			}
		}
		return roles;
	}

	/**
	 * Obtiene role editor permissions.
	 * 
	 * @return Role editor permissions
	 */
	private Map<String, String[]> getRoleEditorPermissions() {
		Map<String, String[]> permissions = new HashMap<String, String[]>();
		/* Panel Control */
		permissions.put(PortletKeys.JOURNAL, new String[] {
				ActionKeys.ACCESS_IN_CONTROL_PANEL, ActionKeys.VIEW });
		permissions.put(PortletKeys.SITES_ADMIN, new String[] {
				ActionKeys.ACCESS_IN_CONTROL_PANEL, ActionKeys.VIEW });
		permissions.put(PortletKeys.WIKI_ADMIN, new String[] {
				ActionKeys.ACCESS_IN_CONTROL_PANEL, ActionKeys.VIEW });
		permissions.put(PortletKeys.DOCUMENT_LIBRARY, new String[] {
				ActionKeys.ACCESS_IN_CONTROL_PANEL, ActionKeys.VIEW });
		permissions.put(PortletKeys.BLOGS_ADMIN, new String[] {
				ActionKeys.ACCESS_IN_CONTROL_PANEL, ActionKeys.VIEW });

		/* Journal */
		permissions.put("com.liferay.portlet.journal", new String[] {
				ActionKeys.ADD_ARTICLE, ActionKeys.ADD_FEED,
				ActionKeys.ADD_FOLDER, ActionKeys.ADD_STRUCTURE,
				ActionKeys.ADD_TEMPLATE, ActionKeys.VIEW });
		/* Journal Folder */
		permissions.put(JournalFolder.class.getName(), new String[] {
				ActionKeys.ACCESS, ActionKeys.ADD_ARTICLE,
				ActionKeys.ADD_SUBFOLDER, ActionKeys.DELETE,
				ActionKeys.PERMISSIONS, ActionKeys.UPDATE, ActionKeys.VIEW });
		/* Journal Article */
		permissions.put(JournalArticle.class.getName(), new String[] {
				ActionKeys.ADD_DISCUSSION, ActionKeys.DELETE,
				ActionKeys.EXPIRE, ActionKeys.PERMISSIONS, ActionKeys.UPDATE,
				ActionKeys.VIEW });
		/* Journal Structure */
		permissions.put(JournalStructure.class.getName(), new String[] {
				ActionKeys.DELETE, ActionKeys.PERMISSIONS, ActionKeys.UPDATE,
				ActionKeys.VIEW });
		/* Journal Template */
		permissions.put(JournalTemplate.class.getName(), new String[] {
				ActionKeys.DELETE, ActionKeys.PERMISSIONS, ActionKeys.UPDATE,
				ActionKeys.VIEW });
		/* Group */
		permissions.put(Group.class.getName(), new String[] {
				ActionKeys.MANAGE_LAYOUTS, ActionKeys.ADD_LAYOUT,
				ActionKeys.CONFIGURE_PORTLETS, ActionKeys.DELETE,
				ActionKeys.UPDATE, ActionKeys.VIEW,
				ActionKeys.PREVIEW_IN_DEVICE, ActionKeys.VIEW_STAGING,
				ActionKeys.VIEW_SITE_ADMINISTRATION });
		/* Wiki */
		permissions.put("com.liferay.portlet.wiki",
				new String[] { ActionKeys.ADD_NODE });
		/* Wiki Node */
		permissions.put(WikiNode.class.getName(), new String[] {
				ActionKeys.ADD_ATTACHMENT, ActionKeys.ADD_PAGE,
				ActionKeys.DELETE, ActionKeys.IMPORT, ActionKeys.UPDATE,
				ActionKeys.PERMISSIONS, ActionKeys.VIEW });
		/* Wiki Page */
		permissions.put(WikiPage.class.getName(), new String[] {
				ActionKeys.DELETE, ActionKeys.UPDATE, ActionKeys.VIEW,
				ActionKeys.PERMISSIONS });
		/* Document Library */
		permissions.put("com.liferay.portlet.documentlibrary", new String[] {
				ActionKeys.ADD_REPOSITORY, ActionKeys.ADD_SHORTCUT,
				ActionKeys.ADD_DOCUMENT_TYPE, ActionKeys.ADD_DOCUMENT,
				ActionKeys.ADD_FOLDER, ActionKeys.ADD_STRUCTURE,
				ActionKeys.UPDATE, ActionKeys.VIEW, ActionKeys.PERMISSIONS });
		/* Document Library Folder */
		permissions.put(DLFolder.class.getName(), new String[] {
				ActionKeys.ACCESS, ActionKeys.ADD_SHORTCUT,
				ActionKeys.ADD_DOCUMENT, ActionKeys.ADD_SUBFOLDER,
				ActionKeys.DELETE, ActionKeys.UPDATE, ActionKeys.VIEW,
				ActionKeys.PERMISSIONS });
		/* Document Library File Entry */
		permissions.put(DLFileEntry.class.getName(), new String[] {
				ActionKeys.DELETE, ActionKeys.UPDATE, ActionKeys.VIEW,
				ActionKeys.PERMISSIONS });
		/* Blog */
		permissions.put("com.liferay.portlet.blogs", new String[] {
				ActionKeys.ADD_ENTRY, ActionKeys.PERMISSIONS });
		/* Blog entry */
		permissions.put(BlogsEntry.class.getName(), new String[] {
				ActionKeys.DELETE, ActionKeys.UPDATE, ActionKeys.VIEW,
				ActionKeys.PERMISSIONS });

		return permissions;
	}

	/**
	 * Obtiene role publisher permissions.
	 * 
	 * @return Role publisher permissions
	 */
	private Map<String, String[]> getRolePublisherPermissions() {
		Map<String, String[]> permissions = new HashMap<String, String[]>();
		/* Panel Control */
		permissions.put(PortletKeys.SITES_ADMIN,
				new String[] { ActionKeys.ACCESS_IN_CONTROL_PANEL });
		/* Group */
		permissions.put(Group.class.getName(), new String[] {
				ActionKeys.MANAGE_STAGING, ActionKeys.PUBLISH_STAGING,
				ActionKeys.PREVIEW_IN_DEVICE, ActionKeys.VIEW,
				ActionKeys.PUBLISH_TO_REMOTE, ActionKeys.VIEW_STAGING,
				ActionKeys.VIEW_SITE_ADMINISTRATION });
		return permissions;
	}

	/**
	 * Setup users.
	 * 
	 * @param companyId
	 *            company id
	 * @return list
	 */
	protected List<User> setupUsers(long companyId) {
		List<User> users = new ArrayList<User>();
		/* Usuario editor */
		try {
			Role roleEditor = RoleLocalServiceUtil.getRole(companyId,
					RoleUtil.PORTAL_CONTENT_EDITOR);
			User editorUser = UserUtil.addUser(companyId, "editor", "Editor",
					"Movistar", "editor", true, "Editor",
					new long[] { roleEditor.getRoleId() });
			users.add(editorUser);
		} catch (PortalException | SystemException e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error creando usuario editor", e);
			}
		}
		/* Usuario publicador */
		try {
			Role rolePublisher = RoleLocalServiceUtil.getRole(companyId,
					RoleUtil.PORTAL_CONTENT_PUBLISHER);
			User publisherUser = UserUtil.addUser(companyId, "publicador",
					"Publicador", "Movistar", "publicador", true, "Publicador",
					new long[] { rolePublisher.getRoleId() });
			users.add(publisherUser);
		} catch (PortalException | SystemException e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error creando usuario publicador", e);
			}
		}
		/* Usuario validador */
		try {
			Role roleValidator = RoleLocalServiceUtil.getRole(companyId,
					RoleUtil.PORTAL_CONTENT_REVIEWER);
			User reviewerUser = UserUtil.addUser(companyId, "validador",
					"Validador", "Movistar", "validador", true, "Validador",
					new long[] { roleValidator.getRoleId() });
			users.add(reviewerUser);
		} catch (PortalException | SystemException e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error creando usuario validador", e);
			}
		}
		return users;
	}

	/**
	 * Find category by name.
	 * 
	 * @param name
	 *            name
	 * @param groupId
	 *            group id
	 * @return asset category
	 */
	private AssetCategory findCategoryByName(String name, long groupId) {
		AssetCategory assetCategory = null;
		ClassLoader classLoader = PortalClassLoaderUtil.getClassLoader();
		DynamicQuery query = DynamicQueryFactoryUtil
				.forClass(AssetCategory.class, classLoader)
				.add(PropertyFactoryUtil.forName("name").eq(name))
				.add(PropertyFactoryUtil.forName("groupId").eq(groupId));
		try {
			List<AssetCategory> assetCategories = AssetCategoryLocalServiceUtil
					.dynamicQuery(query);
			if (!assetCategories.isEmpty()) {
				assetCategory = assetCategories.get(0);
			}
		} catch (SystemException e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error getting category by name " + name, e);
			}
		}
		return assetCategory;
	}

	/**
	 * Setup categories.
	 * 
	 * @param companyId
	 *            company id
	 * @param userId
	 *            user id
	 * @param groupId
	 *            group id
	 * @throws SystemException
	 *             system exception
	 */
	private void setupCategories(long companyId, long userId, long groupId)
			throws SystemException {
		try {

			List<AssetVocabulary> companyVocabularies = AssetVocabularyLocalServiceUtil
					.getGroupVocabularies(groupId);
			AssetVocabulary vocabulary = companyVocabularies.get(0);
			AssetCategory helCategory = findCategoryByName(ASSET_CATEGORY_HELP,
					groupId);
			if (helCategory == null) {
				ServiceContext serviceContext = new ServiceContext();
				serviceContext.setScopeGroupId(groupId);
				AssetCategoryLocalServiceUtil.addCategory(userId,
						ASSET_CATEGORY_HELP, vocabulary.getVocabularyId(),
						serviceContext);
			}
		} catch (PortalException e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error creando categoría Ayuda", e);
			}
		}
	}
}