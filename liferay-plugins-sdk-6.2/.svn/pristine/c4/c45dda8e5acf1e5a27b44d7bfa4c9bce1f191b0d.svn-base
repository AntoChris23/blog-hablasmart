/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author vass
 * @generated
 */
public class TvSoap implements Serializable {
	public static TvSoap toSoapModel(Tv model) {
		TvSoap soapModel = new TvSoap();

		soapModel.setId(model.getId());
		soapModel.setNombre(model.getNombre());
		soapModel.setFecha(model.getFecha());

		return soapModel;
	}

	public static TvSoap[] toSoapModels(Tv[] models) {
		TvSoap[] soapModels = new TvSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TvSoap[][] toSoapModels(Tv[][] models) {
		TvSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TvSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TvSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TvSoap[] toSoapModels(List<Tv> models) {
		List<TvSoap> soapModels = new ArrayList<TvSoap>(models.size());

		for (Tv model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TvSoap[soapModels.size()]);
	}

	public TvSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public Date getFecha() {
		return _fecha;
	}

	public void setFecha(Date fecha) {
		_fecha = fecha;
	}

	private long _id;
	private String _nombre;
	private Date _fecha;
}