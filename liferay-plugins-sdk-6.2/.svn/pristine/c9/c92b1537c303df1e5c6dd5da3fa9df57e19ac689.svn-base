/*
 * 
 */
package com.movistar.latam.colportal.homephone.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.movistar.latam.colportal.homephone.model.Ciudad;
import com.movistar.latam.colportal.homephone.model.Deco;
import com.movistar.latam.colportal.homephone.model.DecoValor;
import com.movistar.latam.colportal.homephone.model.Departamento;
import com.movistar.latam.colportal.homephone.model.Matriz;
import com.movistar.latam.colportal.homephone.model.Sva;
import com.movistar.latam.colportal.homephone.model.SvaValor;
import com.movistar.latam.colportal.homephone.model.SvaXBa;
import com.movistar.latam.colportal.homephone.service.BALocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.CiudadLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.DecoLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.DecoValorLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.DepartamentoLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.MatrizLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.SvaLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.SvaValorLocalServiceUtil;
import com.movistar.latam.colportal.homephone.service.SvaXBaLocalServiceUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

// TODO: Auto-generated Javadoc
/**
 * HomePhoneUtil:.
 * 
 * @author VASS
 * @version 1.0
 */
public class HomePhoneUtil {
	
	/** Log. */
	private static Log log = LogFactoryUtil.getLog(HomePhoneUtil.class);
	
	
	/**
	 * Devuelve un json con el listado de departamentos.
	 * 
	 * @return Departments
	 * @throws SystemException
	 *             system exception
	 */
	public static JSONArray getDepartments() throws SystemException {
		List<Departamento> allDepartamentos = DepartamentoLocalServiceUtil.getAllDepartamentos();
		

		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();

		for (Departamento d : allDepartamentos) {
			

			JSONObject jsonObj = JSONFactoryUtil.createJSONObject();

			jsonObj.put("id", d.getId());
			jsonObj.put("nombre", d.getNombre());

			jsonArray.put(jsonObj);
		}
		
		return jsonArray;
	}

	/**
	 * Devuelve un json con el listado de ciudades por departamento.
	 * 
	 * @param idDepartment
	 *            id department
	 * @return Cities
	 */
	public static JSONArray getCities(long idDepartment) {
		List<Ciudad> cities = null;
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		try {
			cities = CiudadLocalServiceUtil.findByIdDepartamento(idDepartment);
		} catch (SystemException e) {
			log.error(e.getMessage());
		}
		for (Ciudad city : cities) {
			

			JSONObject jsonObj = JSONFactoryUtil.createJSONObject();

			jsonObj.put("idDepartamento", city.getIdDepartamento());
			jsonObj.put("divipola", city.getDivipola());
			jsonObj.put("nombre", city.getNombre());

			jsonArray.put(jsonObj);
		}
		
		return jsonArray;
	}

	/**
	 * Método que devuelve un json con los estratos de una ciudad.
	 * 
	 * @param divipola
	 *            divipola
	 * @return Stratums
	 */
	public static JSONArray getStratums(long divipola) {
		List<Matriz> matrizRows = null;
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		TreeSet<String> estratos = new TreeSet<String>();
		try {
			//Buscamos los registros de la matriz filtrando por divipola
			matrizRows = MatrizLocalServiceUtil.findByCiudad(divipola);
		} catch (SystemException e) {
			log.error(e.getMessage());
		}
		for (Matriz row : matrizRows) {
			
			estratos.add(row.getEstrato());
			
		}
			
		for (String estrato : estratos){
			JSONObject jsonObj = JSONFactoryUtil.createJSONObject();

			jsonObj.put("estrato", estrato);
			
			jsonArray.put(jsonObj);
		}
		
		return jsonArray;
	}
	
	
	/**
	 * Devuelve un json con los productos.
	 * 
	 * @param divipola
	 *            divipola
	 * @param estrato
	 *            estrato
	 * @return Products
	 */
	public static JSONArray getProducts(long divipola, String estrato) {
		List<Matriz> matrizRows = null;
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		try {
			matrizRows = MatrizLocalServiceUtil.findBySearchMatriz(divipola, estrato);
		} catch (SystemException e) {
			log.error(e.getMessage());
		}
			
		for (Matriz row : matrizRows) {
			JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
			
			jsonObj.put("id", row.getId());
			jsonObj.put("divipola", row.getDivipola());
			jsonObj.put("codigoAtis", row.getCodigoAtis());
			jsonObj.put("estrato", row.getEstrato());
			jsonObj.put("tipoMX", row.getTipoMX());
			jsonObj.put("planLB", row.getPlanLB());
			jsonObj.put("codigoLB", row.getCodigoLB());
			jsonObj.put("valorLB", row.getValorLB());
			jsonObj.put("planBA", row.getPlanBA());
			jsonObj.put("codigoBA", row.getCodigoBA());
			jsonObj.put("valorBA", row.getValorBA());
			jsonObj.put("planTV", row.getPlanTV());
			jsonObj.put("codigoTV", row.getCodigoTV());
			jsonObj.put("valorTV", row.getValorTV());
			jsonObj.put("totalValor", row.getTotalValor());
			jsonObj.put("tiemPromDes", row.getTiemPromDes());
			jsonObj.put("descuento", row.getDescuento());
			jsonObj.put("aplicaHD", row.getAplicaHD());
			jsonObj.put("subsidioBA", row.getSubsidioBA());
			
			
			jsonArray.put(jsonObj);
		}
		
		return jsonArray;
	}
	
	/**
	 * Devuelve un json con los svas de internet.
	 * 
	 * @return Sva intenet
	 */
	public static JSONArray getSvaIntenet(){
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		List<SvaXBa> svas = null;
		try {
			svas = SvaXBaLocalServiceUtil.getAllSvaXBa();
			
			for(SvaXBa svaXBa : svas){
				JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
				
				jsonObj.put("id",String.valueOf(svaXBa.getId()));
				
				//Obtenemos el nombre del BA asociado al id
				String BA = BALocalServiceUtil.getBA(svaXBa.getIdBa()).getNombre();
				jsonObj.put("ba",BA);
				
				//Obtenemos el nombre del SVA asociado al idSva
				String sva = SvaLocalServiceUtil.getSva(svaXBa.getIdSva()).getNombre();
				jsonObj.put("sva", sva);
				
				jsonObj.put("idSva",String.valueOf(svaXBa.getIdSva()));
				jsonObj.put("incluye",svaXBa.getIncluye());
				
				jsonArray.put(jsonObj);
				
				
			}
			
		} catch (SystemException e) {
			log.error(e.getMessage());
		} catch (PortalException e) {
			log.error(e.getMessage());
		}

		return jsonArray;
		
	}
	
	/**
	 * Devuelve un json con los svas de tv.
	 * 
	 * @param aplicaHD
	 *            aplica hd
	 * @return Sva television
	 */
	public static JSONArray getSvaTelevision(String aplicaHD){
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		List<Sva> svas = null;
		List<Sva> svasHD = null;
		try {
			
			//Buscamos los svas de TV
			svas = SvaLocalServiceUtil.findByIdSvaTipo(Constants.SVA_TV);
			

			for(Sva sva : svas){
				JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
				
				jsonObj.put("id",String.valueOf(sva.getId()));
				jsonObj.put("idSvaTipo",String.valueOf(sva.getIdSvaTipo()));
				jsonObj.put("nombre",sva.getNombre());
				
				jsonArray.put(jsonObj);

			}
			
			//Si aplicaHD buscdamos los SVAs de tipo HD
			if(aplicaHD.equals(Constants.APLICA_HD)){
				svasHD = SvaLocalServiceUtil.findByIdSvaTipo(Constants.SVA_TV_HD);
			
				for(Sva svaHD : svasHD){
					JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
					
					jsonObj.put("id",String.valueOf(svaHD.getId()));
					jsonObj.put("idSvaTipo",String.valueOf(svaHD.getIdSvaTipo()));
					jsonObj.put("nombre",svaHD.getNombre());
					
					jsonArray.put(jsonObj);
	
				}
			}
			
		} catch (SystemException e) {
			log.error(e.getMessage());
		}

		return jsonArray;
		
	}
	
	
	/**
	 * Devuelve un json con la información de los decodificadores.
	 * 
	 * @param aplicaHD
	 *            aplica hd
	 * @return Decos
	 */
	public static JSONArray getDecos(Boolean aplicaHD){
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		List<Deco> decosSD = new LinkedList<Deco>();
		List<Deco> decosHD = new LinkedList<Deco>();
		List<Deco> decosHDPVR = new LinkedList<Deco>();
		try {
			decosSD = DecoLocalServiceUtil.findByTipoDeco(Constants.TIPO_DECO_SD);
			
			for(Deco decoSD : decosSD){
				JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
				
				jsonObj.put("id",decoSD.getId());
				jsonObj.put("nombre",decoSD.getNombre());
							
				jsonArray.put(jsonObj);
			}
			
			if (aplicaHD){
				decosHD = DecoLocalServiceUtil.findByTipoDeco(Constants.TIPO_DECO_HD);
				for(Deco decoHD : decosHD){
					JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
					
					jsonObj.put("id",decoHD.getId());
					jsonObj.put("nombre",decoHD.getNombre());
								
					jsonArray.put(jsonObj);
				}
				decosHDPVR = DecoLocalServiceUtil.findByTipoDeco(Constants.TIPO_DECO_HD_PVR);
				for(Deco decoHDPVR : decosHDPVR){
					JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
					
					jsonObj.put("id",decoHDPVR.getId());
					jsonObj.put("nombre",decoHDPVR.getNombre());
								
					jsonArray.put(jsonObj);
				}
			}
			
			
		} catch (SystemException e) {
			log.error(e.getMessage());
		}

		return jsonArray;
		
	}
	
	/**
	 * Devuelve un json con los valores de svas.
	 * 
	 * @return Valor svas
	 */
	public static JSONArray getValorSVAS(){
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		List<SvaValor> svasValor = null;
		try {
			svasValor = SvaValorLocalServiceUtil.getAllSvaValor();
			
			for(SvaValor svaValor : svasValor){
				JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
				
				jsonObj.put("id",String.valueOf(svaValor.getId()));
				jsonObj.put("divipola",String.valueOf(svaValor.getDivipola()));
				jsonObj.put("idSva",String.valueOf(svaValor.getIdSva()));
				jsonObj.put("valor",String.valueOf(svaValor.getValor()));
				jsonObj.put("descuento",String.valueOf(svaValor.getDescuento()));
				jsonObj.put("descuentoDescripcion",svaValor.getDescuentoDescripcion());
				jsonObj.put("fecha",String.valueOf(svaValor.getFecha()));
				
				jsonArray.put(jsonObj);
				
			}
			
		} catch (SystemException e) {
			log.error(e.getMessage());
		}

		return jsonArray;
		
	}
	
	/**
	 * Devuelve un json con los valores de los decos.
	 * 
	 * @return Valor decos
	 */
	public static JSONArray getValorDecos(){
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		List<DecoValor> decosValor = null;
		try {
			decosValor = DecoValorLocalServiceUtil.getAllDecoValor();
			
			for(DecoValor decoValor : decosValor){
				JSONObject jsonObj = JSONFactoryUtil.createJSONObject();
				
				jsonObj.put("id",String.valueOf(decoValor.getId()));
				jsonObj.put("divipola",String.valueOf(decoValor.getDivipola()));
				jsonObj.put("idTipoMX","");
				jsonObj.put("idDeco",String.valueOf(decoValor.getIdDeco()));
				jsonObj.put("nombre",String.valueOf(decoValor.getNombre()));
				jsonObj.put("valor",String.valueOf(decoValor.getValor()));
				
				jsonArray.put(jsonObj);
			}
			
		} catch (SystemException e) {
			log.error(e.getMessage());
		}

		return jsonArray;
		
	}
	
	
}