/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.movistar.latam.colportal.homephone.model.DecoValor;

/**
 * The persistence interface for the deco valor service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see DecoValorPersistenceImpl
 * @see DecoValorUtil
 * @generated
 */
public interface DecoValorPersistence extends BasePersistence<DecoValor> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DecoValorUtil} to access the deco valor persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the deco valors where idDeco = &#63; and divipola = &#63;.
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @return the matching deco valors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.DecoValor> findByDecosPorCiudad(
		long idDeco, long divipola)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the deco valors where idDeco = &#63; and divipola = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.DecoValorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @param start the lower bound of the range of deco valors
	* @param end the upper bound of the range of deco valors (not inclusive)
	* @return the range of matching deco valors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.DecoValor> findByDecosPorCiudad(
		long idDeco, long divipola, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the deco valors where idDeco = &#63; and divipola = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.DecoValorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @param start the lower bound of the range of deco valors
	* @param end the upper bound of the range of deco valors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching deco valors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.DecoValor> findByDecosPorCiudad(
		long idDeco, long divipola, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first deco valor in the ordered set where idDeco = &#63; and divipola = &#63;.
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching deco valor
	* @throws com.movistar.latam.colportal.homephone.NoSuchDecoValorException if a matching deco valor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor findByDecosPorCiudad_First(
		long idDeco, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchDecoValorException;

	/**
	* Returns the first deco valor in the ordered set where idDeco = &#63; and divipola = &#63;.
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching deco valor, or <code>null</code> if a matching deco valor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor fetchByDecosPorCiudad_First(
		long idDeco, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last deco valor in the ordered set where idDeco = &#63; and divipola = &#63;.
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching deco valor
	* @throws com.movistar.latam.colportal.homephone.NoSuchDecoValorException if a matching deco valor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor findByDecosPorCiudad_Last(
		long idDeco, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchDecoValorException;

	/**
	* Returns the last deco valor in the ordered set where idDeco = &#63; and divipola = &#63;.
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching deco valor, or <code>null</code> if a matching deco valor could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor fetchByDecosPorCiudad_Last(
		long idDeco, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the deco valors before and after the current deco valor in the ordered set where idDeco = &#63; and divipola = &#63;.
	*
	* @param id the primary key of the current deco valor
	* @param idDeco the id deco
	* @param divipola the divipola
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next deco valor
	* @throws com.movistar.latam.colportal.homephone.NoSuchDecoValorException if a deco valor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor[] findByDecosPorCiudad_PrevAndNext(
		long id, long idDeco, long divipola,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchDecoValorException;

	/**
	* Removes all the deco valors where idDeco = &#63; and divipola = &#63; from the database.
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @throws SystemException if a system exception occurred
	*/
	public void removeByDecosPorCiudad(long idDeco, long divipola)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of deco valors where idDeco = &#63; and divipola = &#63;.
	*
	* @param idDeco the id deco
	* @param divipola the divipola
	* @return the number of matching deco valors
	* @throws SystemException if a system exception occurred
	*/
	public int countByDecosPorCiudad(long idDeco, long divipola)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the deco valor in the entity cache if it is enabled.
	*
	* @param decoValor the deco valor
	*/
	public void cacheResult(
		com.movistar.latam.colportal.homephone.model.DecoValor decoValor);

	/**
	* Caches the deco valors in the entity cache if it is enabled.
	*
	* @param decoValors the deco valors
	*/
	public void cacheResult(
		java.util.List<com.movistar.latam.colportal.homephone.model.DecoValor> decoValors);

	/**
	* Creates a new deco valor with the primary key. Does not add the deco valor to the database.
	*
	* @param id the primary key for the new deco valor
	* @return the new deco valor
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor create(
		long id);

	/**
	* Removes the deco valor with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the deco valor
	* @return the deco valor that was removed
	* @throws com.movistar.latam.colportal.homephone.NoSuchDecoValorException if a deco valor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor remove(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchDecoValorException;

	public com.movistar.latam.colportal.homephone.model.DecoValor updateImpl(
		com.movistar.latam.colportal.homephone.model.DecoValor decoValor)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the deco valor with the primary key or throws a {@link com.movistar.latam.colportal.homephone.NoSuchDecoValorException} if it could not be found.
	*
	* @param id the primary key of the deco valor
	* @return the deco valor
	* @throws com.movistar.latam.colportal.homephone.NoSuchDecoValorException if a deco valor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchDecoValorException;

	/**
	* Returns the deco valor with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the deco valor
	* @return the deco valor, or <code>null</code> if a deco valor with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.DecoValor fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the deco valors.
	*
	* @return the deco valors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.DecoValor> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the deco valors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.DecoValorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of deco valors
	* @param end the upper bound of the range of deco valors (not inclusive)
	* @return the range of deco valors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.DecoValor> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the deco valors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.DecoValorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of deco valors
	* @param end the upper bound of the range of deco valors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of deco valors
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.DecoValor> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the deco valors from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of deco valors.
	*
	* @return the number of deco valors
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}