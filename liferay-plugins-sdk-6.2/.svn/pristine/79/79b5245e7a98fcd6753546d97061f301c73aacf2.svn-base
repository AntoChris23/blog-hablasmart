/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author vass
 * @generated
 */
public class TipoMxSoap implements Serializable {
	public static TipoMxSoap toSoapModel(TipoMx model) {
		TipoMxSoap soapModel = new TipoMxSoap();

		soapModel.setId(model.getId());
		soapModel.setNombre(model.getNombre());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setFecha(model.getFecha());

		return soapModel;
	}

	public static TipoMxSoap[] toSoapModels(TipoMx[] models) {
		TipoMxSoap[] soapModels = new TipoMxSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TipoMxSoap[][] toSoapModels(TipoMx[][] models) {
		TipoMxSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TipoMxSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TipoMxSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TipoMxSoap[] toSoapModels(List<TipoMx> models) {
		List<TipoMxSoap> soapModels = new ArrayList<TipoMxSoap>(models.size());

		for (TipoMx model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TipoMxSoap[soapModels.size()]);
	}

	public TipoMxSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public Date getFecha() {
		return _fecha;
	}

	public void setFecha(Date fecha) {
		_fecha = fecha;
	}

	private long _id;
	private String _nombre;
	private String _descripcion;
	private Date _fecha;
}