/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.movistar.latam.colportal.homephone.NoSuchSvaTipoException;
import com.movistar.latam.colportal.homephone.model.SvaTipo;
import com.movistar.latam.colportal.homephone.model.impl.SvaTipoImpl;
import com.movistar.latam.colportal.homephone.model.impl.SvaTipoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the sva tipo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see SvaTipoPersistence
 * @see SvaTipoUtil
 * @generated
 */
public class SvaTipoPersistenceImpl extends BasePersistenceImpl<SvaTipo>
	implements SvaTipoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SvaTipoUtil} to access the sva tipo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SvaTipoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
			SvaTipoModelImpl.FINDER_CACHE_ENABLED, SvaTipoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
			SvaTipoModelImpl.FINDER_CACHE_ENABLED, SvaTipoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
			SvaTipoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public SvaTipoPersistenceImpl() {
		setModelClass(SvaTipo.class);
	}

	/**
	 * Caches the sva tipo in the entity cache if it is enabled.
	 *
	 * @param svaTipo the sva tipo
	 */
	@Override
	public void cacheResult(SvaTipo svaTipo) {
		EntityCacheUtil.putResult(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
			SvaTipoImpl.class, svaTipo.getPrimaryKey(), svaTipo);

		svaTipo.resetOriginalValues();
	}

	/**
	 * Caches the sva tipos in the entity cache if it is enabled.
	 *
	 * @param svaTipos the sva tipos
	 */
	@Override
	public void cacheResult(List<SvaTipo> svaTipos) {
		for (SvaTipo svaTipo : svaTipos) {
			if (EntityCacheUtil.getResult(
						SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
						SvaTipoImpl.class, svaTipo.getPrimaryKey()) == null) {
				cacheResult(svaTipo);
			}
			else {
				svaTipo.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all sva tipos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(SvaTipoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(SvaTipoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the sva tipo.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(SvaTipo svaTipo) {
		EntityCacheUtil.removeResult(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
			SvaTipoImpl.class, svaTipo.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<SvaTipo> svaTipos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (SvaTipo svaTipo : svaTipos) {
			EntityCacheUtil.removeResult(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
				SvaTipoImpl.class, svaTipo.getPrimaryKey());
		}
	}

	/**
	 * Creates a new sva tipo with the primary key. Does not add the sva tipo to the database.
	 *
	 * @param id the primary key for the new sva tipo
	 * @return the new sva tipo
	 */
	@Override
	public SvaTipo create(long id) {
		SvaTipo svaTipo = new SvaTipoImpl();

		svaTipo.setNew(true);
		svaTipo.setPrimaryKey(id);

		return svaTipo;
	}

	/**
	 * Removes the sva tipo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the sva tipo
	 * @return the sva tipo that was removed
	 * @throws com.movistar.latam.colportal.homephone.NoSuchSvaTipoException if a sva tipo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SvaTipo remove(long id)
		throws NoSuchSvaTipoException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the sva tipo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the sva tipo
	 * @return the sva tipo that was removed
	 * @throws com.movistar.latam.colportal.homephone.NoSuchSvaTipoException if a sva tipo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SvaTipo remove(Serializable primaryKey)
		throws NoSuchSvaTipoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			SvaTipo svaTipo = (SvaTipo)session.get(SvaTipoImpl.class, primaryKey);

			if (svaTipo == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSvaTipoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(svaTipo);
		}
		catch (NoSuchSvaTipoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected SvaTipo removeImpl(SvaTipo svaTipo) throws SystemException {
		svaTipo = toUnwrappedModel(svaTipo);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(svaTipo)) {
				svaTipo = (SvaTipo)session.get(SvaTipoImpl.class,
						svaTipo.getPrimaryKeyObj());
			}

			if (svaTipo != null) {
				session.delete(svaTipo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (svaTipo != null) {
			clearCache(svaTipo);
		}

		return svaTipo;
	}

	@Override
	public SvaTipo updateImpl(
		com.movistar.latam.colportal.homephone.model.SvaTipo svaTipo)
		throws SystemException {
		svaTipo = toUnwrappedModel(svaTipo);

		boolean isNew = svaTipo.isNew();

		Session session = null;

		try {
			session = openSession();

			if (svaTipo.isNew()) {
				session.save(svaTipo);

				svaTipo.setNew(false);
			}
			else {
				session.merge(svaTipo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
			SvaTipoImpl.class, svaTipo.getPrimaryKey(), svaTipo);

		return svaTipo;
	}

	protected SvaTipo toUnwrappedModel(SvaTipo svaTipo) {
		if (svaTipo instanceof SvaTipoImpl) {
			return svaTipo;
		}

		SvaTipoImpl svaTipoImpl = new SvaTipoImpl();

		svaTipoImpl.setNew(svaTipo.isNew());
		svaTipoImpl.setPrimaryKey(svaTipo.getPrimaryKey());

		svaTipoImpl.setId(svaTipo.getId());
		svaTipoImpl.setNombre(svaTipo.getNombre());
		svaTipoImpl.setFecha(svaTipo.getFecha());

		return svaTipoImpl;
	}

	/**
	 * Returns the sva tipo with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the sva tipo
	 * @return the sva tipo
	 * @throws com.movistar.latam.colportal.homephone.NoSuchSvaTipoException if a sva tipo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SvaTipo findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSvaTipoException, SystemException {
		SvaTipo svaTipo = fetchByPrimaryKey(primaryKey);

		if (svaTipo == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSvaTipoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return svaTipo;
	}

	/**
	 * Returns the sva tipo with the primary key or throws a {@link com.movistar.latam.colportal.homephone.NoSuchSvaTipoException} if it could not be found.
	 *
	 * @param id the primary key of the sva tipo
	 * @return the sva tipo
	 * @throws com.movistar.latam.colportal.homephone.NoSuchSvaTipoException if a sva tipo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SvaTipo findByPrimaryKey(long id)
		throws NoSuchSvaTipoException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the sva tipo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the sva tipo
	 * @return the sva tipo, or <code>null</code> if a sva tipo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SvaTipo fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		SvaTipo svaTipo = (SvaTipo)EntityCacheUtil.getResult(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
				SvaTipoImpl.class, primaryKey);

		if (svaTipo == _nullSvaTipo) {
			return null;
		}

		if (svaTipo == null) {
			Session session = null;

			try {
				session = openSession();

				svaTipo = (SvaTipo)session.get(SvaTipoImpl.class, primaryKey);

				if (svaTipo != null) {
					cacheResult(svaTipo);
				}
				else {
					EntityCacheUtil.putResult(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
						SvaTipoImpl.class, primaryKey, _nullSvaTipo);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(SvaTipoModelImpl.ENTITY_CACHE_ENABLED,
					SvaTipoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return svaTipo;
	}

	/**
	 * Returns the sva tipo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the sva tipo
	 * @return the sva tipo, or <code>null</code> if a sva tipo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SvaTipo fetchByPrimaryKey(long id) throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the sva tipos.
	 *
	 * @return the sva tipos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SvaTipo> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sva tipos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.SvaTipoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sva tipos
	 * @param end the upper bound of the range of sva tipos (not inclusive)
	 * @return the range of sva tipos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SvaTipo> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the sva tipos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.SvaTipoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sva tipos
	 * @param end the upper bound of the range of sva tipos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of sva tipos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SvaTipo> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<SvaTipo> list = (List<SvaTipo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_SVATIPO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SVATIPO;

				if (pagination) {
					sql = sql.concat(SvaTipoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<SvaTipo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<SvaTipo>(list);
				}
				else {
					list = (List<SvaTipo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the sva tipos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (SvaTipo svaTipo : findAll()) {
			remove(svaTipo);
		}
	}

	/**
	 * Returns the number of sva tipos.
	 *
	 * @return the number of sva tipos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SVATIPO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the sva tipo persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.movistar.latam.colportal.homephone.model.SvaTipo")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<SvaTipo>> listenersList = new ArrayList<ModelListener<SvaTipo>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<SvaTipo>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(SvaTipoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_SVATIPO = "SELECT svaTipo FROM SvaTipo svaTipo";
	private static final String _SQL_COUNT_SVATIPO = "SELECT COUNT(svaTipo) FROM SvaTipo svaTipo";
	private static final String _ORDER_BY_ENTITY_ALIAS = "svaTipo.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SvaTipo exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(SvaTipoPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static SvaTipo _nullSvaTipo = new SvaTipoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<SvaTipo> toCacheModel() {
				return _nullSvaTipoCacheModel;
			}
		};

	private static CacheModel<SvaTipo> _nullSvaTipoCacheModel = new CacheModel<SvaTipo>() {
			@Override
			public SvaTipo toEntityModel() {
				return _nullSvaTipo;
			}
		};
}