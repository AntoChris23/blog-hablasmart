/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.movistar.latam.colportal.homephone.model.Solicitud;

/**
 * The persistence interface for the solicitud service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author vass
 * @see SolicitudPersistenceImpl
 * @see SolicitudUtil
 * @generated
 */
public interface SolicitudPersistence extends BasePersistence<Solicitud> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SolicitudUtil} to access the solicitud persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the solicitud in the entity cache if it is enabled.
	*
	* @param solicitud the solicitud
	*/
	public void cacheResult(
		com.movistar.latam.colportal.homephone.model.Solicitud solicitud);

	/**
	* Caches the solicituds in the entity cache if it is enabled.
	*
	* @param solicituds the solicituds
	*/
	public void cacheResult(
		java.util.List<com.movistar.latam.colportal.homephone.model.Solicitud> solicituds);

	/**
	* Creates a new solicitud with the primary key. Does not add the solicitud to the database.
	*
	* @param id the primary key for the new solicitud
	* @return the new solicitud
	*/
	public com.movistar.latam.colportal.homephone.model.Solicitud create(
		long id);

	/**
	* Removes the solicitud with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the solicitud
	* @return the solicitud that was removed
	* @throws com.movistar.latam.colportal.homephone.NoSuchSolicitudException if a solicitud with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Solicitud remove(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchSolicitudException;

	public com.movistar.latam.colportal.homephone.model.Solicitud updateImpl(
		com.movistar.latam.colportal.homephone.model.Solicitud solicitud)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the solicitud with the primary key or throws a {@link com.movistar.latam.colportal.homephone.NoSuchSolicitudException} if it could not be found.
	*
	* @param id the primary key of the solicitud
	* @return the solicitud
	* @throws com.movistar.latam.colportal.homephone.NoSuchSolicitudException if a solicitud with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Solicitud findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.movistar.latam.colportal.homephone.NoSuchSolicitudException;

	/**
	* Returns the solicitud with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the solicitud
	* @return the solicitud, or <code>null</code> if a solicitud with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.movistar.latam.colportal.homephone.model.Solicitud fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the solicituds.
	*
	* @return the solicituds
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Solicitud> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the solicituds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.SolicitudModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of solicituds
	* @param end the upper bound of the range of solicituds (not inclusive)
	* @return the range of solicituds
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Solicitud> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the solicituds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.movistar.latam.colportal.homephone.model.impl.SolicitudModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of solicituds
	* @param end the upper bound of the range of solicituds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of solicituds
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.movistar.latam.colportal.homephone.model.Solicitud> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the solicituds from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of solicituds.
	*
	* @return the number of solicituds
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}