/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.movistar.latam.colportal.homephone.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LB}.
 * </p>
 *
 * @author vass
 * @see LB
 * @generated
 */
public class LBWrapper implements LB, ModelWrapper<LB> {
	public LBWrapper(LB lb) {
		_lb = lb;
	}

	@Override
	public Class<?> getModelClass() {
		return LB.class;
	}

	@Override
	public String getModelClassName() {
		return LB.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("nombre", getNombre());
		attributes.put("fecha", getFecha());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Date fecha = (Date)attributes.get("fecha");

		if (fecha != null) {
			setFecha(fecha);
		}
	}

	/**
	* Returns the primary key of this l b.
	*
	* @return the primary key of this l b
	*/
	@Override
	public long getPrimaryKey() {
		return _lb.getPrimaryKey();
	}

	/**
	* Sets the primary key of this l b.
	*
	* @param primaryKey the primary key of this l b
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_lb.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this l b.
	*
	* @return the ID of this l b
	*/
	@Override
	public long getId() {
		return _lb.getId();
	}

	/**
	* Sets the ID of this l b.
	*
	* @param id the ID of this l b
	*/
	@Override
	public void setId(long id) {
		_lb.setId(id);
	}

	/**
	* Returns the nombre of this l b.
	*
	* @return the nombre of this l b
	*/
	@Override
	public java.lang.String getNombre() {
		return _lb.getNombre();
	}

	/**
	* Sets the nombre of this l b.
	*
	* @param nombre the nombre of this l b
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_lb.setNombre(nombre);
	}

	/**
	* Returns the fecha of this l b.
	*
	* @return the fecha of this l b
	*/
	@Override
	public java.util.Date getFecha() {
		return _lb.getFecha();
	}

	/**
	* Sets the fecha of this l b.
	*
	* @param fecha the fecha of this l b
	*/
	@Override
	public void setFecha(java.util.Date fecha) {
		_lb.setFecha(fecha);
	}

	@Override
	public boolean isNew() {
		return _lb.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_lb.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _lb.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_lb.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _lb.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _lb.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_lb.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _lb.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_lb.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_lb.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_lb.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LBWrapper((LB)_lb.clone());
	}

	@Override
	public int compareTo(com.movistar.latam.colportal.homephone.model.LB lb) {
		return _lb.compareTo(lb);
	}

	@Override
	public int hashCode() {
		return _lb.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.movistar.latam.colportal.homephone.model.LB> toCacheModel() {
		return _lb.toCacheModel();
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.LB toEscapedModel() {
		return new LBWrapper(_lb.toEscapedModel());
	}

	@Override
	public com.movistar.latam.colportal.homephone.model.LB toUnescapedModel() {
		return new LBWrapper(_lb.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _lb.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _lb.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_lb.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LBWrapper)) {
			return false;
		}

		LBWrapper lbWrapper = (LBWrapper)obj;

		if (Validator.equals(_lb, lbWrapper._lb)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public LB getWrappedLB() {
		return _lb;
	}

	@Override
	public LB getWrappedModel() {
		return _lb;
	}

	@Override
	public void resetOriginalValues() {
		_lb.resetOriginalValues();
	}

	private LB _lb;
}