!function($) {
	'use strict';
	var LightboxModal = function () {
		this.ele = '.js-lightbox-modal';
	};

	LightboxModal.prototype.init = function()
	{
		$(".manana-radio-toggle").click(function(){
			$(".tarde-radio-value").removeAttr('checked');
			$(".manana-radio-value").attr('checked', 'checked');
		});
		$(".tarde-radio-toggle").click(function(){
			$(".manana-radio-value").removeAttr('checked');
			$(".tarde-radio-value").attr('checked', 'checked');
		});
		
	};


$.LightboxModal = new LightboxModal();
	$.LightboxModal.Constructor = LightboxModal;
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.LightboxModal.init();
});// jshint ignore:line