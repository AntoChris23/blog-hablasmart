!function($) {
	'use strict';
	var BenefitsSlick = function () {
		this.ele = '.js-slider_benefits';
	};


	BenefitsSlick.prototype.init = function (){

		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

		var self = this;

		this.$benefitsItems = $('.js-slider_benefits');

		var defaults = {
			$previousDot : null,
			$activeDot : null,
			time : 0,
			isPause : false,
			tick : false,
			percentTime : 0,
			$lickItem : this,
			self : this
		};


		this.$slicks.each(function() {

		 	var $itemsToShow = $( this ).find('.personalizacion-advantages__item').length;

		 	$( this ).slick({
				mobileFirst: true,
				infinite: true,
				autoplay: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: true,
				adaptiveHeight: true,
				swipeToSlide: true,
				arrows: false,
				accessibility: false,
				dotsClass: 'progress-dots',
					customPaging : function(/*slider, i*/) {
			        return '<div class="progress-dots__item">' +
			        	   		'<div class="progress-dots__progress"><div class="progress-dots__progress-inner"></div></div>'+
			        	   '</div>';
			    },
				responsive : [
					{
						breakpoint: 1023,
						settings: {
							slidesToShow: $itemsToShow,
							slidesToScroll: 0,
							autoplay: false,
							dots: false
						}
					}
				]			
			});

		});


		this.$benefitsItems.on('init beforeChange', function(event, slick, currentSlide, nextSlide)
    	{

	    	if ($(window).width() < 767) {		


		  		if(event.type === 'init') {
		  			defaults.$activeDot = slick.$dots.find('.slick-active');
		  		}else {
		  			defaults.$previousDot = slick.$dots.children().eq(currentSlide);
		  			defaults.$activeDot = slick.$dots.children().eq(nextSlide);
		  		}

		  		if(slick.options.autoplay) {
		  			defaults.time = slick.options.autoplaySpeed / 1000;
		  			self._startProgressbar(defaults);
		  		}

		  	}
	  		
	  	});


	  	$('.progress-dots').wrap("<div class='hero__dots'></div>");


		};


	BenefitsSlick.prototype._startProgressbar = function (defaults)
	{
		this._resetProgressbar(defaults);
		defaults.isPause = false;
		defaults.percentTime = 0;
		defaults.tick = setInterval(this._interval, 10, defaults);
	};

	BenefitsSlick.prototype._resetProgressbar = function (defaults)
	{
		if(defaults.$previousDot) {
			defaults.$previousDot.find('.progress-dots__progress-inner').css({
				width: 0+'%'
			});
			clearTimeout(defaults.tick);
		}
	};

	BenefitsSlick.prototype._interval = function (defaults)
	{
		if(defaults.isPause === false) {
			defaults.percentTime += 1 / (defaults.time+0.1);
			defaults.$activeDot.find('.progress-dots__progress-inner').css({
				width: defaults.percentTime+"%"
			});
			/*if(defaults.percentTime >= 100)
			{

				defaults.$lickItem.slick('slickNext');
				defaults.self._startProgressbar(defaults);

				return false;
			}*/
		}
	};	


	$.BenefitsSlick = new BenefitsSlick();
	$.BenefitsSlick.Constructor = BenefitsSlick;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.BenefitsSlick.init();
});// jshint ignore:line