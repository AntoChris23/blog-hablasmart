
!function($) {
	'use strict';
	// Footer mobile collapse
	var FooterCollapse = function () {
		this.ele = '.js-mobile-only-collapsible';
	};

	FooterCollapse.prototype.init = function()
	{

		this.$accordion = $(this.ele);
		if(!this.$accordion.length) return false;

		var self = this;

		this.$accordion.each(function() {
			var $this = $(this);

      		 $this.on('click', '> li > .collapsible-header',{self:self},self._accordionToggle);

		});

	};

	FooterCollapse.prototype._accordionToggle = function(e)
	{
		if($(window).width() > 993) return;
		var object = $(this);
		var self = e.data.self;

		var $active = object.parents(self.ele).find('.collapsible-header.active');

		if(!$active.is(object.parent())){
			self._accordionClose($active);
		}

		if (object.hasClass('active')) {
          self._accordionClose(object);
        }else {
        	//abre
        	self._accordionOpen(object);
        }
	};

	FooterCollapse.prototype._accordionOpen = function(object)
	{
		object.siblings('.collapsible-body').stop(true,false).slideDown({ duration: 350, easing: "easeOutQuart", queue: false, complete: function() {
			$(this).siblings('.collapsible-header').addClass('active');
			$(this).parent().addClass('active');
			$(this).attr('style', '');
		}});
	};

	FooterCollapse.prototype._accordionClose = function(object)
	{
		object.siblings('.collapsible-body').stop(true,false).slideUp({ duration: 350, easing: "easeOutQuart", queue: false, complete: function() {
			$(this).siblings('.collapsible-header').removeClass('active');
			$(this).parent().removeClass('active');
			$(this).attr('style', '');
		}});
	};




	$.FooterCollapse = new FooterCollapse();
	$.FooterCollapse.Constructor = FooterCollapse;
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.FooterCollapse.init();
});// jshint ignore:line
