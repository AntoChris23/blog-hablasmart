!function($) {
	'use strict';
	// Footer mobile collapse
	var PricingAccordion = function () {
		this.ele = '.js-pricing-accordion';
	};

	PricingAccordion.prototype.init = function()
	{
		this.$acccordion = $(this.ele);

		this.$acccordion.each(this._accordionFn(this));

	};

	PricingAccordion.prototype._accordionFn = function(self)
	{
		//function(index, element)
	    return function(){
	    	var $this = $(this);

	    	var $panel_headers = $(this).find('.c-plan__header');

	    	//console.log($panel_headers);


			// Add click handler to only direct collapsible header children
			$this.on('click.collapse', '> .c-plan .c-plan__header', function(e) {

				if($(window).width() > 993) return false;
				var element = $(e.target);

		        if (self._isChildrenOfPanelHeader(element)) {
		          element = self._getPanelHeader(element);
		        }

				if(element.parent().hasClass('c-plan--isActive')) 
				{
					//self._panelClose(element);
					self._restoreToDefaultSequence($panel_headers);
				} else 
				{
					self._panelOpen(element);
					self._shrinkHeadersSequence($panel_headers.not(element));
					//self._panelClose($panel_headers.not(element).parent().filter('.c-plan--isActive').find('.c-plan__header'));

				}

				// Close previously open  elements.
				///self._panelClose($panel_headers.not(element).parent().filter('.c-plan--isActive').find('.c-plan__header'));

			});

	    };
	};


	PricingAccordion.prototype._panelOpen = function (panel)
	{

		this._openPanelSequence(panel);
	};

	PricingAccordion.prototype._panelClose = function (panel)
	{
		this._closePanelSequence(panel);
		
	};


	PricingAccordion.prototype._openPanelSequence = function (panel) 
	{

		var $panelHeader = panel;
		var $panelCaret = $panelHeader.find('.c-plan__caret');
		var $panelBody = $panelHeader.siblings('.c-plan__body-container');
		var $detail = $panelHeader.find('.c-plan__detail');
		var $detailText = $panelHeader.find('.c-plan__detail-text');
		var $priceText = $panelHeader.find('.c-plan__price-text');
		


		var openPanelSequence = [
			{ elements: $panelHeader, properties: { backgroundColor: "#F4F4F3" }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
	        { elements: $panelCaret, properties: { rotateZ: "-180deg", opacity:1 }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
	        { elements: $detailText, properties: { opacity: 1 }, options: { display:'block',duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
	        { elements: $priceText, properties: { opacity: 1 }, options: { display:'block',duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
	        { elements: $detail, properties: { fontSize: '56px', lineHeight: '45px', letterSpacing : '-5px' }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
	        { elements: $panelBody, properties: 'slideDown', options: {duration: 350, easing: "easeOutQuart", sequenceQueue: false} }
	    ];

	    $.when( this._deferredRunSequence(openPanelSequence) ).then(
			  function() 
			  {
			  	$panelHeader.parent().addClass('c-plan--isActive');
				$panelHeader.css({'background-color':''});
				$panelBody.css({'height':'', 'display':''});
				$detail.css({fontSize: '', lineHeight: '', letterSpacing : ''});
				$detailText.css({opacity: '', display:''});
				$priceText.css({opacity: '', display:''});
			  }
		  );
	};

	PricingAccordion.prototype._closePanelSequence = function (panel) 
	{

		var $panelHeader = panel;
		var $panelCaret = $panelHeader.find('.c-plan__caret');
		var $panelBody = $panelHeader.siblings('.c-plan__body-container');
		


		var closePanelSequence = [
	        { elements: $panelHeader, properties: { backgroundColor: '#ffffff' }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false }},
	        { elements: $panelCaret, properties: { rotateZ: "0" }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
	        { elements: $panelBody, properties: 'slideUp',options: {duration: 350, easing: "easeOutQuart", sequenceQueue: false}}
	    ];

	    $.when( this._deferredRunSequence(closePanelSequence) ).then(
			  function() 
			  {
			  	$panelHeader.parent().removeClass('c-plan--isActive');
				$panelHeader.css({'background-color':''});
				$panelBody.css({'height':'', 'display':''});
			  }
		  );
	};

	PricingAccordion.prototype._shrinkHeadersSequence = function (panels) 
	{
		var $panels = $(panels);
		var $detail = $panels.find('.c-plan__detail');
		var $detailText = $panels.find('.c-plan__detail-text');
		var $priceText = $panels.find('.c-plan__price-text');
		var $panelCaret = $panels.find('.c-plan__caret');
		var $panelBody = $panels.siblings('.c-plan__body-container');
		var  self= this;


		$.each( $panels, function( i ){
            var shrinkHeadersSequence = [
				{ elements: $panels.eq(i), properties: { backgroundColor: '#fff' }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false }},
				{ elements: $panelBody.eq(i), properties: 'slideUp', options: {duration: 350, easing: "easeOutQuart", sequenceQueue: false} },
				{ elements: $panelCaret.eq(i), properties: { opacity: "0" }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
				{ elements: $detailText.eq(i), properties: { opacity: "0", height: 0 }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
				{ elements: $priceText.eq(i), properties: { opacity: "0", height: 0 }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
				{ elements: $detail.eq(i),properties: { fontSize: '26px', lineHeight: '24px', letterSpacing : 0 }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false }}
			];

			$.when( self._deferredRunSequence(shrinkHeadersSequence) ).then(
				  function() 
				  {
				  	$panels.eq(i).parent().removeClass('c-plan--isActive');
				  	self.$acccordion.addClass('pricing-accordion--isOpen');
				  	$panels.eq(i).css({ backgroundColor: '' });
				    $detailText.eq(i).css({opacity: '', height: '', display:''});
				    $priceText.eq(i).css({opacity: '', height: '', display:''});
				    $detail.eq(i).css({fontSize: '', lineHeight: '', letterSpacing : ''});
				    $panelBody.eq(i).css({'height':'', 'display':''});

				  }
			  );
        });
	};


	PricingAccordion.prototype._restoreToDefaultSequence = function (panels) 
	{
		var $panels = $(panels);
		var $panelCaret = $panels.find('.c-plan__caret');
		var $detail = $panels.find('.c-plan__detail');
		var $detailText = $panels.find('.c-plan__detail-text');
		var $priceText = $panels.find('.c-plan__price-text');
		var $panelBody = $panels.siblings('.c-plan__body-container');
		var self = this;


		$.each( $panels, function( i ){
            var restoreToDefaultSequence = [
				{ elements: $panels.eq(i), properties: { backgroundColor: "#FFF" }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
		        { elements: $panelCaret.eq(i), properties: { rotateZ: "0", opacity: 1 }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
		        { elements: $detailText.eq(i), properties: { opacity: 1 }, options: { display:'block',duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
		        { elements: $priceText.eq(i), properties: { opacity: 1 }, options: { display:'block',duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
		        { elements: $detail.eq(i), properties: { fontSize: '56px', lineHeight: '45px', letterSpacing : '-5px' }, options: { duration: 350, easing: "easeOutQuart", sequenceQueue: false } },
		        { elements: $panelBody.eq(i), properties: 'slideUp', options: {duration: 350, easing: "easeOutQuart", sequenceQueue: false} }
			];

			$.when( self._deferredRunSequence(restoreToDefaultSequence) ).then(
				  function() 
				  {
				  	self.$acccordion.removeClass('pricing-accordion--isOpen');
				  	$panels.eq(i).parent().removeClass('c-plan--isActive');
				  	$panels.eq(i).css({'background-color':''});
				    $detailText.eq(i).css({opacity: "", height: '', display: ''});
				    $priceText.eq(i).css({opacity: "", height: ''});
				    $detail.eq(i).css({fontSize: '', lineHeight: '', letterSpacing : ''});
				    $panelBody.eq(i).css({'height':'', 'display':''});
				  }
			  );
        });
		
	};

	/**
	* Check if object is children of panel header
	* @param  {Object}  object Jquery object
	* @return {Boolean} true if it is children
	*/
	PricingAccordion.prototype._isChildrenOfPanelHeader = function (object) 
	{

		var panelHeader = this._getPanelHeader(object);

		return panelHeader.length > 0;
	};



	/**
	* Get panel header from a children element
	* @param  {Object} object Jquery object
	* @return {Object} panel header object
	*/
	PricingAccordion.prototype._getPanelHeader = function (object) 
	{
		return object.closest('.c-plan .c-plan__header');
	};



	/**
	* Add promise to velocity runSequence
	* @param  {Object} velocity sequence object
	*/
	PricingAccordion.prototype._deferredRunSequence = function (sequence) 
	{
		var dfd = jQuery.Deferred();
		var total = 0;
		var done = 0;
            
        $.each( sequence, function( i, animation ){
            total++;
            if( !animation.options ) animation.options = {};
            var old = animation.options.complete;
            animation.options.complete = function(){
                
                if( old && old instanceof Function ){
                    old.apply( this, arguments );
                }
                
                if( ++done === total ){
                    return dfd.resolve();
                }
            };
        });

	 $.Velocity.RunSequence( sequence );
	  // Return the Promise so caller can't change the Deferred
	  return dfd.promise();
};






	$.PricingAccordion = new PricingAccordion();
	$.PricingAccordion.Constructor = PricingAccordion;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.PricingAccordion.init();
});// jshint ignore:line
