!function($, Liferay) {
	'use strict';
	// Multi Level Navigation Menu
	var MLMenu = function () {
		this.ele = '#mlnavigation';
		
		this.options = {
			itemsDelayInterval : 30
		};

		this.current = 0;
		this.backPath = [];

		this.searchOpen = false;
	};

	MLMenu.prototype.init = function() 
	{

		this.$wraper = $(this.ele);
		if(!this.$wraper.length) return false;

		this.$body = $('body');
		this.$openSearch = $('.js-open-top-search');
		this.$closeSearch = $('.js-close-top-search');
		this.$navigationHeader = $('.navigation__header');

		this.$menuHeader = this.$wraper.find('.mlmenu__header');
		this.$back = this.$menuHeader.find('.mlmenu__back');


		this.$menus = $('#navigation').find('.mlmenu__sub-menu');
		//set current menu
		$(this.$menus[this.current]).addClass('mlmenu__sub-menu--current');

		//this._createMenuStructure();
		
		this._initEvents();

	};

	/**
	* Event binding
	*/
	MLMenu.prototype._initEvents = function() 
	{
		var self = this;

		//open search
		self.$openSearch.on('click', function(e) {
			if($(this).data('search-modal') === true) {
				e.preventDefault();

				if(!self.searchOpen){
					self.$menuHeader.addClass('mlmenu__search--open');
					self.searchOpen = !self.searchOpen;
				}
			}
			
		});

		//close search
		self.$closeSearch.on('click', function() {
			self.$menuHeader.removeClass('mlmenu__search--open');
			self.searchOpen = false;
		});



		self.$wraper.find('.mlmenu__link').on('click', function(e){
			var $this = $(this),
				submenu = $this.data('submenu'),
				$subMenuEl = self.$wraper.find('.'+submenu),
				clickPosition = $this.parent().index(),
				subMenuName = $this.find('.mlmenu__link-text').text();

			//check if there´s a submenu for this item
			if(submenu && $subMenuEl.length>0){
				e.preventDefault();
				//open it
				self._openSubMenu($subMenuEl,clickPosition,subMenuName);
			}
		});

		self.$back.on('click', function(e){
				e.preventDefault();
				self._back();
		});


		this.lastScroll = parseInt($(window).scrollTop(), 10);
		$(window).on('scroll',{self:this},_throttle(this._headerScrollAnimation, 50));
		

		//Liferay Fix - opens the modal containing the portlet's configuration dropdown (ONLY FOR LOGED IN USERS)
		if(typeof Liferay !== 'undefined'){
			if(Liferay.ThemeDisplay.isSignedIn()){
				self.$wraper.find('#p_p_id_clientsettings_WAR_clientsettingsportlet_').find('#_clientsettings_WAR_clientsettingsportlet_kldx_null_null_menu').on('click', function(){
					$('#clientsettings-portlet-configuration-modal').modal('open');
				});
			}
		}
		
	};


	/**
	* Opens sumenus
	*/
	MLMenu.prototype._openSubMenu = function($subMenu, clickPosition, subMenuName) 
	{
		if(this.isAnimating) return false;
		this.isAnimating = true;

		// current menu slides out
		this._menuOut(clickPosition);

		// next menu (submenu) slides in
		this._menuIn($subMenu,clickPosition,subMenuName);
	};

	MLMenu.prototype._back = function() 
	{
		if(this.isAnimating) return false;
		if(this.backPath.length <= 0) return false;

		this.isAnimating = true;
		var $backMenuEl = $(this.$menus[this.backPath[this.backPath.length - 1].position]);

		// current menu slides out
		this._menuOut();
		// next menu (submenu) slides in
		this._menuIn($backMenuEl);
	};

	MLMenu.prototype._menuOut = function(clickPosition) 
	{
		var self = this,
			isBackNavigation = typeof clickPosition == 'undefined' ? true : false;

		//animate menu
		self._animateInOut('out',clickPosition,isBackNavigation, $(self.$menus[self.current]));
		
	};

	MLMenu.prototype._menuIn = function($nextMenu, clickPosition,subMenuName) 
	{
		var self = this,
			isBackNavigation = typeof clickPosition == 'undefined' ? true : false;
		
		//build navigation
		self._navigation(isBackNavigation, subMenuName);

		//animate menu
		self._animateInOut('in',clickPosition,isBackNavigation, $nextMenu);
		
	};
	
	MLMenu.prototype._navigation = function (isBackNavigation, subMenuName) {
		var self = this;

		if(!isBackNavigation) {
			self.backPath.push({position : self.current, title : subMenuName});
			self.$wraper.addClass('mlnavigation--active-back');
		}else {
			self.backPath.splice(-1,1);
		}

		if(self.backPath.length > 0) {
			$('.mlmenu__back-text').text(self.backPath[self.backPath.length - 1].title);
		}else {
			self.$wraper.removeClass('mlnavigation--active-back');
		}
	};

	MLMenu.prototype._animateInOut = function (direction,clickPosition,isBackNavigation,$menu) {
		var self = this,
			$items = $menu.find('.mlmenu__level'),
			itemsTotal = $items.length,
			farthestIdx = clickPosition <= itemsTotal/2 || isBackNavigation ? itemsTotal - 1 : 0;

		$items.each(function(pos,item) {
			//set animation delay for each item
			$(item).css({'animation-delay': isBackNavigation ? parseInt(pos * self.options.itemsDelayInterval) + 'ms' : parseInt(Math.abs(clickPosition - pos) * self.options.itemsDelayInterval) + 'ms'});

			if( pos === farthestIdx ) {
				$(item).one('animationend', function() {
					if(direction === 'in') {
						$menu.removeClass(!isBackNavigation ? 'animate-inFromRight' : 'animate-inFromLeft');
						self.current = $menu.index();
						self.isAnimating = false;
					}else if(direction === 'out') {
						$menu.removeClass('mlmenu__sub-menu--current');
						$menu.removeClass(!isBackNavigation ? 'animate-outToLeft' : 'animate-outToRight');
					}	
					$(this).off('animationend');
				});
			}
		});

		if(direction === 'in') {
			if(self.backPath.length > 0){
				self.$body.addClass('full-width-menu');
			}else {
				self.$body.removeClass('full-width-menu');
			}

			$menu.addClass('mlmenu__sub-menu--current');
			$menu.addClass(!isBackNavigation ? 'animate-inFromRight' : 'animate-inFromLeft');
		}else if(direction === 'out') {
			$menu.addClass(!isBackNavigation ? 'animate-outToLeft' : 'animate-outToRight');
		}	
	};

	MLMenu.prototype._headerScrollAnimation = function(e) 
	{
		if (e && e.data && e.data.self){
			var self = e.data.self;
		}else {
			return false;
		}
		
		var currentTop = parseInt($(window).scrollTop(), 10);

	    if(currentTop === self.lastScroll) return false;

	    if(currentTop === 0){
	    	$('html').removeClass('scrolling--down');
	    }else {
	    	if (currentTop < self.lastScroll) {
				$('html').removeClass('scrolling--down');
			} else {
				$('html').addClass('scrolling--down');
			}
	    }
		
		self.lastScroll = currentTop;
	};


	$.MLMenu = new MLMenu();
	$.MLMenu.Constructor = MLMenu;
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.MLMenu.init();

});// jshint ignore:line
