!function($) {
	'use strict';
	var quickLinksHeader = function () {
		this.ele = '.js-slider-quick-links';
	};


	quickLinksHeader.prototype.init = function (){

		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

		var self = this;

		var items = this.$slicks.find('.quick-links-item').length;
		/*console.log(items);*/

		var $size = $('.quick-links-header-container').width();
		/*console.log($size);*/

		this.$slicks.slick({
				infinite: false,
				autoplay:false,
				slidesToShow: 1.6,
				slidesToScroll: 1,
				dots: false,
				centerMode: false,
				adaptiveHeight: true,
				variableWidth: true,
				swipeToSlide: true,
				mobileFirst: true,
				accessibility: false,
				arrows:false,
				responsive : [
					{
						breakpoint: 450,
						settings: {
							slidesToShow:2.4
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow:2.8
						}
					},
					{
						breakpoint: 767,
						settings: {
							arrows: true
						}
					},
					{
						breakpoint: 992,
						settings: {
							infinite: true,
							slidesToShow:3,
							arrows: true,
							variableWidth: items < 3 ? true : false
						}			
					},
					{
						breakpoint: 1190,
						settings: {
							infinite: true,
							slidesToShow:5,
							arrows: true,
							variableWidth: items < 5 ? true : false		
						}
					}
				]

			});	


			if ( $(window).width() > 992 ) {
				if (items <= 5) {
					this.$slicks.find('.quick-links-item').css({
						"width" : ($size / items) + "px"
					});	
				} else if (items > 5) {
					this.$slicks.find('.quick-links-item').css({
						"width" : ($size / 5) + "px"
					});
				}	
			}
		};

	$.quickLinksHeader = new quickLinksHeader();
	$.quickLinksHeader.Constructor = quickLinksHeader;

	
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.quickLinksHeader.init();

	jQuery(window).resize(function(){
		$.quickLinksHeader.init();
	});


});// jshint ignore:line