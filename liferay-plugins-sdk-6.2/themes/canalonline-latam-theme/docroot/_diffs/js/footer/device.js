!function($) {
	'use strict';
	var carouselDevicesPage = function () {
		this.ele = '.js-device-page-caruosel';
	};


	carouselDevicesPage.prototype.init = function (){

		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

		var self = this;

		this.$slicks.each(this._multiInit());

		this.$categoryTabs = this.$slicks.parents('.category-tabs-container').find('.js-category-tabs');



		this.$categoryTabs.tabs({ 
			onShow: function(tab) 
			{ 
				$(tab).find(self.ele).slick('setPosition');
			} 
		});
	};


	carouselDevicesPage.prototype._multiInit = function () 
	{
		return function(index, element){

			var $this = $(element);

			$this.slick({
				mobileFirst:true,
				slidesToShow: 1,
  				slidesToScroll: 1,
        		variableWidth: true,
  				swipeToSlide: true,
  				centerMode: true,
  				accessibility: false,
  				arrows: false,
  				appendArrows: $this.siblings('.device-carousel__arrows'),
  				dots:true,
				dotsClass: 'progress-dots',
				customPaging : function(/*slider, i*/) {
	                return '<div class="progress-dots__item">' +
	                	   		'<div class="progress-dots__progress"><div class="progress-dots__progress-inner"></div></div>'+
	                	   '</div>';
	            }
			});

	    };
	};




	$.carouselDevicesPage = new carouselDevicesPage();
	$.carouselDevicesPage.Constructor = carouselDevicesPage;
}(window.jQuery);// jshint ignore:line


!function($) {
	'use strict';
	var chooseDevicePref = function () {
		//this.ele = '.device-color-button';
		this.Color_block_ob = '.js-color-choose';
		this.GB_block_ob = '.js-gb-choose';
		this.Next_ob = '.device-goToNextStep';
		this.Cancel_ob = '.device-cancel-button';
		this.Toggler_ob = '.device-toggler-container';
		this.Plan_mode_ob = '.plan-or-free-radios';
		this.Plan_ob = '.devices-plan__input-container';
	};

	chooseDevicePref.prototype.init = function()
	{

		// creamos objeto de javascript con la clase del contenedor de los elementos
		this.$colorChoice = $(this.Color_block_ob);

		// comprobamos que se encuentra en la pagina y si no, salimos
		if(!this.$colorChoice.length) return false;

		// Creamos un array de objetos con todos los elementos
		this.$color_choices = this.$colorChoice.find('.color-choice');

		// Ejecutamos el evento click, especificando que se pulse el elemento (no todo el contenedor), 
		// le pasamos una variable con el this actual y ejecutamos la funcion 
		this.$colorChoice.on('click', '.color-choice', {elementoPadre:this}, this.chooseColor);

		// Creamos un objeto (para el padre) del input y el span que rellenaremos con el dato seleccionado 
		this.$colorInput = $('#color-input');
		this.$prefColor = $('#color-pref');
		
		// Filtramos en el array cual esta seleccionado al inicio
		var firstDataColor = this.$color_choices.filter('.color-choice--active').data('color');
		
		// Y lo llevamos al formulario y el span
		this.$colorInput.val(firstDataColor);
		this.$prefColor.html(firstDataColor);

		
		// Repetimos los pasos para el selector de GB
		this.$gbChoice = $(this.GB_block_ob);
		if(!this.$gbChoice.length) return false;
		this.$gb_choices = this.$gbChoice.find('.gb-choice');
		this.$gbChoice.on('click', '.gb-choice', {elementoPadre:this}, this.chooseGB);
		this.$gbInput = $('#gb-input');
		this.$prefGB = $('#gb-pref');
		var firstDataGB = this.$gb_choices.filter('.gb-choice--active').data('gb');
		this.$gbInput.val(firstDataGB);
		this.$prefGB.html(firstDataGB);


		this.$nextStep = $(this.Next_ob);
		this.$previousStep = $(this.Cancel_ob);
		this.$Toggler = $(this.Toggler_ob);
		if(!this.$nextStep.length) return false;
		if(!this.$previousStep.length) return false;
		if(!this.$Toggler.length) return false;

		this.$nextStep.on('click', '.btn', {elementoPadre:this}, this.toggleNext);
		this.$previousStep.on('click','.btn', {elementoPadre:this}, this.togglePrev);


		// PLAN
		this.$planModeChoice = $(this.Plan_mode_ob);
		this.$planChoice = $(this.Plan_ob);
		if(!this.$planModeChoice.length) return false;
		if(!this.$planChoice.length) return false;

		// Plan inicial
		this.$plan_mode_choices = this.$planModeChoice.find('.radio-plan-mode input');
		this.$plan_choices = this.$planChoice.find('.plan-item input');
		this.$plan_choices_block = this.$planChoice.find('.plan-item');
		var firstDataPlanMode = this.$plan_mode_choices.filter(':checked').val();
		var firstDataPlan = this.$plan_choices.filter(':checked').val();
		this.$PlanModeInput = $('#plan-mode-input');
		this.$PlanInput = $('#plan-input');
		this.$PlanModeInput.val(firstDataPlanMode);
		this.$PlanInput.val(firstDataPlan);

		// Actualizar Plan Mode
		this.$noPlan = $('#noPlan');
		this.$withPlan = $('#withPlan');
		this.$Libre = $('#libre');
		this.$firstPlan = $(".plan-item.main-plan input");

		this.$noPlan.on('click', {elementoPadre:this}, this.changeToNoPlan);
		this.$withPlan.on('click', {elementoPadre:this}, this.changeToWithPlan);
		

		this.$plan_choices.on('change', {elementoPadre:this}, this.choosePlan);

	};

	chooseDevicePref.prototype.chooseColor = function(e)
	{
		// this ahora es el objetivo del evento (e) click
		// Declaramos una variable para recoger el objeto this (elemento padre) que enviamos en el on click
		var padre = e.data.elementoPadre;
		
		// Rellenar input
		padre.$colorInput.val($(this).data('color'));
		padre.$prefColor.html($(this).data('color'));

		//Quitar active
		padre.$color_choices.filter('.color-choice--active').removeClass('color-choice--active');
		
		//Añadir active
		$(this).addClass("color-choice--active");
	};

	chooseDevicePref.prototype.chooseGB = function(e)
	{
		// Repetimos los pasos de la funcion chooseColor
		var padre = e.data.elementoPadre;
		padre.$gbInput.val($(this).data('gb'));
		padre.$prefGB.html($(this).data('gb'));
		padre.$gb_choices.filter('.gb-choice--active').removeClass('gb-choice--active');
		$(this).addClass("gb-choice--active");
	};

	chooseDevicePref.prototype.toggleNext = function(e)
	{
		var padre = e.data.elementoPadre;
		padre.$Toggler.addClass("next-step");
	};

	chooseDevicePref.prototype.togglePrev = function(e)
	{
		var padre = e.data.elementoPadre;
		padre.$Toggler.removeClass("next-step");
	};

	chooseDevicePref.prototype.choosePlan = function(e){
		var padre = e.data.elementoPadre;
		if($(this).hasClass("free-plan")){
			padre.$withPlan.removeAttr("checked");
			padre.$noPlan.prop("checked","checked");
		}
		else{

			padre.$noPlan.removeAttr("checked");
			padre.$withPlan.prop("checked","checked");
		}
		padre.$PlanModeInput.val(padre.$plan_mode_choices.filter(':checked').val());
		padre.$PlanInput.val(padre.$plan_choices.filter(':checked').val());
	};

	chooseDevicePref.prototype.changeToNoPlan = function(e)
	{
		var padre = e.data.elementoPadre;
		// quitar posible checked
		padre.$plan_choices.filter(':checked').removeAttr("checked");
		// checked a libre
		padre.$Libre.prop("checked","checked");
		// Rellenar inputs
		padre.$PlanModeInput.val(padre.$plan_mode_choices.filter(':checked').val());
		padre.$PlanInput.val(padre.$plan_choices.filter(':checked').val());

	};

	chooseDevicePref.prototype.changeToWithPlan = function(e)
	{
		var padre = e.data.elementoPadre;
		if (padre.$Libre.prop("checked")){
			padre.$Libre.removeAttr("checked");
			padre.$firstPlan.prop("checked","checked")
		}
		padre.$PlanModeInput.val(padre.$plan_mode_choices.filter(':checked').val());
		padre.$PlanInput.val(padre.$plan_choices.filter(':checked').val());
	};


	$.chooseDevicePref = new chooseDevicePref();
	$.chooseDevicePref.Constructor = chooseDevicePref;
}(window.jQuery, window.Liferay);// jshint ignore:line


//********** document.ready

jQuery(document).ready(function(){
	'use strict';
	$.carouselDevicesPage.init();
	$.chooseDevicePref.init();
});// jshint ignore:line

