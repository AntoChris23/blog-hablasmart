
jQuery(document).ready(function() {
	
	jQuery(".advanceSearcherInput").keyup(function(event) {
	    if (event.keyCode === 13) {
	        $(".advanceSearcherIcon").click();
	    }
	});
	
});
	
function advanceSearcher(searcherURL, target) {
	var myUrl = searcherURL + '?q=' + jQuery('#autocomplete-input').val();
	if (target == "_blank") {
		window.open(myUrl);
	} else {
		window.location.href = myUrl;
	}
}
