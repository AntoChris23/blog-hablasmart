!function($) {
	'use strict';
	// Footer mobile collapse
	var packBuilder = function () {
		this.ele = '.js-pack-builder';
	};

	packBuilder.prototype.init = function()
	{

		this.$wraper = $(this.ele);
		if(!this.$wraper.length) return false;

		this.$scrollWraper = $('.pack-builder-wraper');

		this.$startButton = $('.js-pack-builder-start');
		this.$navigation = $('.pack-builder__footer-wraper');
		this.$header = $('.pack-builder__header');

		this.$progressNext = $('.js-progress-next');


		this.$showDeletePanelButton = $('.js-pack-delete');
		this.$hideDeletePanelButton = $('.js-pack-delete-cancel');
		this.$deletePanelButton = $('.js-pack-delete-accept');


		//summary
		this.$resumePanel = $('.js-pack-resume');

		this._initEvent();
	};

	packBuilder.prototype._initEvent = function()
	{
		var self = this;
		this.$startButton.on('click', function () {
			$.ProgressTabs._goToTab('next');
		});

		this.$wraper.on('show.progress-tab', function (e) {
			if(e.progressTab.active !== 0) {
				self.$startButton.hide();
				self.$navigation.addClass('pack-builder__footer-wraper--visible');
			}else {
				self.$startButton.show();
				self.$navigation.removeClass('pack-builder__footer-wraper--visible');
			}

			//summary
			if(e.progressTab.active !== 0) {
				self.$resumePanel.addClass('pack-resume--isactive');
			}else {
				self.$resumePanel.removeClass('pack-resume--isactive');
			}

			//summary mobile only for last step
			if(e.progressTab.active >= e.progressTab.$progress.length -1) {
				self.$resumePanel.addClass('pack-resume--islaststep');
			}else {
				self.$resumePanel.removeClass('pack-resume--islaststep');
			}
		});

		this.$wraper.on('shown.progress-tab', function (e) {

			if(e.progressTab.active === 0 || e.progressTab.active >= e.progressTab.$progress.length -1) {
				self.$progressNext.hide();
			}else {
				self.$progressNext.show();
			}
		});

		this.$showDeletePanelButton.on('click', this._showDeletePanel);

		this.$hideDeletePanelButton.on('click', this._hideDeletePanel);
	};

	packBuilder.prototype._showDeletePanel = function()
	{
		$(this).siblings('.pack-builder__delete-panel-container').velocity({top: 0,  opacity: 1 }, { display: "block",duration: 300, easing: 'easeOutCubic',queue: false });
	};

	packBuilder.prototype._hideDeletePanel = function()
	{
		$(this).parents('.pack-builder__delete-panel-container').velocity({top: '100%',  opacity: 0 }, { display: "none",duration: 300, easing: 'easeOutCubic',queue: false });
	};

	$.packBuilder = new packBuilder();
	$.packBuilder.Constructor = packBuilder;
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.packBuilder.init();
});// jshint ignore:line
