!function($) {
	'use strict';
	var carouselDevices = function () {
		this.ele = '.js-device-caruosel';
	};


	carouselDevices.prototype.init = function (){


		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

		var self = this;

		this.$slicks.each(this._multiInit());

		this.$categoryTabs = this.$slicks.parents('.category-tabs-container').find('.js-category-tabs');
		this.$categoryTabsItems = this.$categoryTabs.find('.tab');

		this.$categoryTabs.tabs({
			onShow: function(tab)
			{
				centerNav();
				$(tab).find(self.ele).slick('setPosition');
			}
		});

		function centerNav () {
			var $active = self.$categoryTabsItems.find('a.active').parent();
			var index = self.$categoryTabsItems.index($active);
			var position = ($active.width() * index) - (($(window).width() - $active.width()) / 2);
			self.$categoryTabs.animate({scrollLeft: position}, 300);
		}

	};



	carouselDevices.prototype._multiInit = function ()
	{

		return function(index, element){ 

			var $this = $(element);

			$this.slick({
				mobileFirst:true,
				slidesToShow: 1,
				  slidesToScroll: 1,
				  swipeToSlide: true,
				  centerMode: true,
				  accessibility: false,
				  arrows: false,
				  appendArrows: $this.siblings('.device-carousel__arrows'),
				  dots:true,
				dotsClass: 'progress-dots',
				customPaging : function(/*slider, i*/) {
					return '<div class="progress-dots__item">' +
								   '<div class="progress-dots__progress"><div class="progress-dots__progress-inner"></div></div>'+
						   '</div>';
				},
				  responsive : [
					  {
						breakpoint: 600,
						settings: {
							slidesToShow: 2
						}

					},
					{
						breakpoint: 993,
						settings: {
							arrows: true,
							infinite: false,
							centerMode: false,
							slidesToShow: 3,
							variableWidth: false,
							centerPadding: 0,
							dots:true

						}

					}
				]
			});

			$this.each(function(){
				var items = $this.find('.device-carousel__item').length;
				if ( (($(window).width() < 600) && (items === 1)) || (($(window).width() > 992) && (items < 4)) ){
					$this.find('.progress-dots').hide();
				}else{
					$this.find('.progress-dots').show();
				}
			});
			
		};
	};

	$.carouselDevices = new carouselDevices();
	$.carouselDevices.Constructor = carouselDevices;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.carouselDevices.init();
});// jshint ignore:line

