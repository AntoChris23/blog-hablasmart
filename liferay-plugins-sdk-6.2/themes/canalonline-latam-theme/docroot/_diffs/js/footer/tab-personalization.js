$( document ).ready(function() {

	// mobile
	$(".customer").click(function(){
     $(".npmenu-panel__phone").css("display","block");
	});
	$(".nocustomer").click(function(){
     $(".npmenu-panel__phone").css("display","none");
	});

	// desktop
	var boxWidth = $(".personalizacion-panel__phone").width();
	$(".personalizacion-panel .customer").click(function(){
     $(".personalizacion-panel__phone").css("display","block").animate({
                width: boxWidth
            }).css("opacity","1");;
       
	});
	$(".personalizacion-panel .nocustomer").click(function(){
     $(".personalizacion-panel__phone").animate({
                width: 0
            }).css("opacity","0");   
	});	

});