!function($) {
	'use strict';
	// collapse mobile - tabs deskpot
	var CollapseTabs = function () {
		this.ele = '.js-collapse-tab';
	};

	CollapseTabs.prototype.init = function()
	{
		this.$wraper = $(this.ele);
		if(!this.$wraper.length) return false;

		this.$collapse = $('.js-collapsible__collapse');
		this.$tab = $('.js-collapsible__tab');

		this.active;

		var self = this;

		var $firstTab= this.$tab.find('> li:first');
		var $firstCollapse = this.$collapse.find('> li:first');

		if($(window).width() > 993){
			$firstTab.addClass('active');
			$firstCollapse.addClass('active');
			this.$collapse.find('.collapsible-header:first').addClass('active');
		}

		this.$collapse.each(function() {
			var $this = $(this);
			$this.on('click', '> li > .collapsible-header',{self:self},self._collapseToggle);
		});

		this.$tab.each(function() {
			var $this = $(this);
			$this.on('click', '> li',{self:self},self._tabToggle);
		});

	};

	//Collapse - Mobile
	CollapseTabs.prototype._collapseToggle = function(e)
	{
		var $object = $(this);
		var self = e.data.self;

		var $active = $object.parents(self.ele).find('.collapsible-header.active');

		if(!$active.is($object.parent())){
			self._collapseClose($active);
		}

		if ($object.hasClass('active')) {
		  self._collapseClose($object);
		}else {
			//abre
			self._collapseOpen($object);
		}
	};

	CollapseTabs.prototype._collapseOpen = function($object)
	{
		$object.siblings('.collapsible-body').stop(true,false).slideDown({ duration: 350, easing: "easeOutQuart", queue: false, complete: function() 	{

			var $id = $(this).attr("id");
			var $href = $('.js-collapsible__tab .tab a[href="'+'#'+''+$id+'"]');

			$(this).siblings('.collapsible-header').addClass('active');
			$(this).parent().addClass('active');
			$(this).attr('style', '');


			$href.parent().addClass('active');

		}});
	};

	CollapseTabs.prototype._collapseClose = function($object)
	{
		$object.siblings('.collapsible-body').stop(true,false).slideUp({ duration: 350, easing: "easeOutQuart", queue: false, complete: function() {

			var $id = $(this).attr("id");
			var $href = $('.js-collapsible__tab .tab a[href="'+'#'+''+$id+'"]');

			$(this).siblings('.collapsible-header').removeClass('active');
			$(this).parent().removeClass('active');
			$(this).attr('style', '');

			$href.parent().removeClass('active');

		}});
	};

	//Tabs - Desktop

	CollapseTabs.prototype._tabToggle = function(e)
	{
		e.preventDefault();
		var self = e.data.self;
		var $active = $(this).siblings('.tab.active');


		if ($(this).hasClass('active')){
			self._tabOpen($(this));
		}else{
			self._tabClose($(this));
		}


		$($active).each(function() {
			self._tabClose($(this));
		});


		if(!$active.is($(this).parent())){
			self._tabOpen($(this));
		}

	};

	CollapseTabs.prototype._tabOpen = function($object)
	{
			$object.addClass('active');
			var id = $object.find('a').attr('href');
			var $collapse = this.$collapse.find(id);

			$collapse.siblings('.collapsible-header').addClass('active');
			$collapse.parent().addClass('active');

	};

	CollapseTabs.prototype._tabClose = function($object)
	{
			$object.removeClass('active');
			var id = $object.find('a').attr('href');
			var $collapse = this.$collapse.find(id);

			$collapse.siblings('.collapsible-header').removeClass('active');
			$collapse.parent().removeClass('active');
	};



	$.CollapseTabs = new CollapseTabs();
	$.CollapseTabs.Constructor = CollapseTabs;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.CollapseTabs.init();
});// jshint ignore:line
