!function($) {
	'use strict';
	var carouselShows = function () {
		this.ele = '.js-slider-shows';
	};


	carouselShows.prototype.init = function (){

		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

		var self = this;

		this.$slicks.slick({
				infinite: false,
				autoplay:false,
				slidesToShow: 2.2,
				slidesToScroll: 1,
				dots: false,
				centerMode: false,
				adaptiveHeight: true,
				swipeToSlide: true,
				arrows: false,
				mobileFirst: true,
				accessibility: false,
				responsive : [
					{
						breakpoint: 600,
						settings: {
							slidesToShow:4,
						}
					},
					{
						breakpoint: 1020,
						settings: {
							slidesToShow:5,
							nav:true,
							arrows: true
						}
					}
				]			
			});
		};


	$.carouselShows = new carouselShows();
	$.carouselShows.Constructor = carouselShows;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.carouselShows.init();
});// jshint ignore:line