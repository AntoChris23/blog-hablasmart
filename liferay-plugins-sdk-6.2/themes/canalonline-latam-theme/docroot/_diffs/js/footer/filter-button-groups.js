!function($) {
	'use strict';
	// Filter button groupss
	var Fbg = function () {
		this.ele = '.js-button-group';
	};

	Fbg.prototype.init = function() 
	{
		this.$ele = $(this.ele);
		if(!this.$ele.length) return false;

		var self = this;

		this.$ele.each(function(){
			var $this = $(this),
				$links = $this.find('li.pill a'),
				$active = $links.filter('.active');
			if(!$active.length){
				$active = $links.first().addClass('active');
			}

			var prev_index,
				index = $links.index($active);

			// append indicator then set indicator width to tab width
			if (!$this.find('.indicator').length) {
				$this.append('<li class="indicator"></li>');
			}
			var $indicator = $this.find('.indicator');

			//set indicator initial position
			$indicator.css({"left": self._calcLeftPos($active,$this),"right": self._calcRightPos($active,$this)});
			// we make sure that the indicator is at the end of the tabs
			$this.append($indicator);


			$this.off('click.pill').on('click.pill', 'a', function(e) {
				e.preventDefault();
		        prev_index = index;
		        index = $links.index($(this));
		        $links.removeClass('active');
		        $active = $(this);
		        $active.addClass('active');
		        // Update indicator
        		self._animateIndicator($indicator,$active,$this, index, prev_index);

        		var data = $active.data('category');
        		$carousel.slick('slickUnfilter');
        		if(data && data !=='all'){
        			$carousel.slick('slickFilter',"[data-category='" + data + "']");
        		}
        		
			});

			$(window).on('resize',{self:self, $indicator:$indicator, $this:$this},_throttle(function(e) {
				var self = e.data.self,
					$active = e.data.$this.find('li.pill a.active');
				e.data.$indicator.css({"left": self._calcLeftPos($active,e.data.$this),"right": self._calcRightPos($active,e.data.$this)});
			}, 50));

			var $container = $this.parents('.js-filter-carousel-container'),
				$carousel = $container.find('.js-filter-carousel');
		});
	};

	Fbg.prototype._calcRightPos = function(el,$group) 
	{
		return Math.ceil($group.width() - el.position().left - el[0].getBoundingClientRect().width - $group.scrollLeft());
	};

	Fbg.prototype._calcLeftPos = function(el,$group) 
	{
		return Math.floor(el.position().left + $group.scrollLeft());
	};


	Fbg.prototype._animateIndicator = function($indicator,$active,$group, index, prev_index) 
	{
		if ((index - prev_index) >= 0) {
			$indicator.velocity({"right": this._calcRightPos($active,$group) }, { duration: 300, queue: false, easing: 'easeOutQuad'});
			$indicator.velocity({"left": this._calcLeftPos($active,$group) }, {duration: 300, queue: false, easing: 'easeOutQuad', delay: 90});
		} else {
			$indicator.velocity({"left": this._calcLeftPos($active,$group) }, { duration: 300, queue: false, easing: 'easeOutQuad'});
			$indicator.velocity({"right": this._calcRightPos($active,$group) }, {duration: 300, queue: false, easing: 'easeOutQuad', delay: 90});
		}
	};

	/**
	* Event binding
	*/
	Fbg.prototype._initEvents = function() 
	{
		var self = this;
	};


	$.Fbg = new Fbg();
	$.Fbg.Constructor = Fbg;
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.Fbg.init();
});// jshint ignore:line
