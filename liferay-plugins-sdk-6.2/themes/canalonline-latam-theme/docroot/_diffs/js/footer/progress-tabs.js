!function($) {
	'use strict';
	// Footer mobile collapse
	var ProgressTabs = function () {
		this.ele = '.js-progress-tabs';
	};

	ProgressTabs.prototype.init = function()
	{

		this.$ProgressTabsContainer = $(this.ele);
		if(!this.$ProgressTabsContainer.length) return false;

		this.$ProgressTabs = this.$ProgressTabsContainer.find('.tabs.tabs--progress');
		this.$progress = this.$ProgressTabs.find('.tab a');
		this.$next = this.$ProgressTabsContainer.find('.js-progress-next');
		this.$prev = this.$ProgressTabsContainer.find('.js-progress-back');
		this.active = this.$progress.index(this.$progress.filter('.active'));


		this.$progress.on('click',this._removeTabsEvent);

		this.$next.on('click',{self:this},this._nextTab);
		this.$prev.on('click',{self:this},this._previousTab);

		this.$ProgressTabsContainer.on('show.progress-tab', function(e,callback) {
			if (typeof callback === "function") {
				callback.call();
			}
		});

		this.$ProgressTabsContainer.on('shown.progress-tab', function(e,callback) {
			if (typeof callback === "function") {
				callback.call();
			}
		});

	};

	ProgressTabs.prototype._removeTabsEvent = function(e)
	{
		//remove material tabs default functionality
		if(typeof(e.originalEvent) !== 'undefined') {
			e.preventDefault();
	  		e.stopPropagation();
		}
	  	
	};

	ProgressTabs.prototype._nextTab = function(e)
	{	
			e.data.self._goToTab('next');
	};

	ProgressTabs.prototype._previousTab = function(e)
	{
		e.data.self._goToTab('prev');
	};

	ProgressTabs.prototype._goToTab = function(step)
	{
		var tabId;
		var self = this;
		var size = this.$progress.length - 1;
		var contentPage = $('html');

		if(step === 'next') this.active++;
		if(step === 'prev') this.active--;

		if(this.active >= size) {
			this.active = size;
		}else if(this.active <= 0) {
			this.active = 0;
		}

		var _relatedTarget = this.$progress.eq(this.active);

		this.$ProgressTabsContainer.trigger($.Event('show.progress-tab',{ progressTab: this, relatedTarget: _relatedTarget }));

		if(step === 'next'){
			this.$prev.show();
			if ( this.active >= this.$progress.length -1 ){
				this.$next.hide();
			}else {
				contentPage.animate( { scrollTop : 0 }, 500 );
				this.$next.show();
			}
		}

		if(step === 'prev'){
			this.$next.show();
			if ( this.active <= 0 ){
				this.$prev.hide();
			}else {
				contentPage.animate( { scrollTop : 0 }, 500 );
				this.$prev.show();
			}
		}


		tabId = this.$progress.eq(this.active).attr('href').substring(1);
		this.$ProgressTabs.tabs('select_tab', tabId);

		this.$progress.each(function(index, element ){
			if(index < self.active) {
				$(element).addClass('previousActive');
			}else {
				$(element).removeClass('previousActive');
			}
		});

		this.$ProgressTabsContainer.trigger($.Event('shown.progress-tab',{ progressTab: this, relatedTarget: _relatedTarget }));

	};

	$.ProgressTabs = new ProgressTabs();
	$.ProgressTabs.Constructor = ProgressTabs;
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.ProgressTabs.init();

	/*$('.js-progress-tabs').on('show.progress-tab', function(e){
		console.log('Código a ejecutar antes de enseñar el siguiente paso');
		console.log(e);
	});

	$('.js-progress-tabs').on('shown.progress-tab', function(e){
		console.log('Código a ejecutar después de enseñar el siguiente paso');
		console.log(e);
	});*/

});// jshint ignore:line
