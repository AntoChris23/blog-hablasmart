!function($) {
	'use strict';
	var carouselVideos = function () {
		this.wrapper = '.slider-videos-wrapper';
		this.ele = '.js-video-scroll';
		this.featured = '.js-video-featured';
	};


	carouselVideos.prototype.init = function (){

		this.$videoScrollContainer = $(this.ele);
		if(!this.$videoScrollContainer.length) return false;

		this.$videos = this.$videoScrollContainer.find('.js-youtube');
		this.$featured = $(this.featured);

		this.$featuredVideo = this.$featured.find('.js-youtube');


		this.$videoScrollContainer.scrollbar({
			"scrollx": $('.js-video-scroll_x'),
        	"scrolly": $('.js-video-scroll_y')
		});

		this.$videos.each(this._loadYoutubeAsync);

		this.$featuredVideo.each(this._loadYoutubeAsync);

	};

	carouselVideos.prototype._loadYoutubeAsync = function ( index, element )
	{

		var $this = $(element),
			thumbnailSrc = "https://img.youtube.com/vi/"+ $this.data('embed') +"/sddefault.jpg",
			videoSrc = 'https://www.youtube.com/embed/'+ $this.data('embed') +'?rel=0&showinfo=0&autoplay=1';


		var $image = $(new Image()).attr('src', thumbnailSrc);

		$image.on('load', function(){
			$this.append(this);
		});

		var $iframe = $('<iframe>', {
		   src: videoSrc,
		   frameborder: 0,
		   allowfullscreen: ''
	   });

		$this.on('click', function(e){
			e.preventDefault();
			var $this = $(this);

			/*if($this.parent().hasClass('active')) return;*/

			if($this.parents('.js-video-scroll').length > 0) {
				$this.parents('.js-video-scroll').find('.embed-container').removeClass('active');
				$this.parent().addClass('active');
				$this.parents('.slider-videos-wrapper').find('.js-video-featured').find('.video-list__title').text($this.parent().siblings('.video-list__title').text());
			}

			

			if($(window).width() < 992){
				$image.hide();
				$this.find('.play-button').hide();
				$this.append($iframe);
			}else {
				$this.parents('.slider-videos-wrapper').find('.js-video-featured .js-youtube').empty().append($iframe);

			}
			
		});
	};

	$.carouselVideos = new carouselVideos();
	$.carouselVideos.Constructor = carouselVideos;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.carouselVideos.init();
});// jshint ignore:line