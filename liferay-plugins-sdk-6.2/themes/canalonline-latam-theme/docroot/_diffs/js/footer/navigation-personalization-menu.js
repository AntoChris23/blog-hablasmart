//navigation-personalization-menu.js
!function($) {
	'use strict';
	// Navigation Personalization Menu
	var NPMenu = function () {
		this.ele = '.js-npmenu';
		
		this.options = {
			itemsDelayInterval : 30
		};

	};

	NPMenu.prototype.init = function() {
		this.$wraper = $(this.ele);
		if(!this.$wraper.length) return false;

		var self = this;

		this.$settingsToggle = this.$wraper.find('.js-settings-toggle');
		this.$panelCheckBoxes = this.$wraper.find('.js-npmenu__panel-checkbox');


		this.$settingOffPanel = this.$wraper.find('.npmenu__settings--off');
		this.$settingOnPanel = this.$wraper.find('.npmenu__settings--on');

		this.$settingPanelContainer = this.$wraper.find('.npmenu__panel-container');
		this.$settingPanels = this.$wraper.find('.checkbox__panel');


		this.$settingsToggle.on('change', function(){
			if(this.checked){
				self.$settingOffPanel.css({opacity: 1});
				self.$settingOnPanel.css({opacity: 0});

				self.$settingPanels.velocity({ translateY: "20px", opacity: 0},{ duration: 0 });

				self.$wraper.addClass('npmenu__settings--isActive');

				self.$settingOffPanel.velocity({opacity: 0}, {
			      duration: 550,
			      queue: false,
			      easing: 'easeOutSine'
			    });

				self.$settingOnPanel.velocity({opacity: 1}, {
			      duration: 550,
			      queue: false,
			      easing: 'easeOutSine'
			    });

			    var time = 0;
			    $(this).parents(self.ele).find('.checkbox__panel').each(function(){
		      		$(this).velocity(
				        { opacity: "1", translateY: "0"},
				        { duration: 225, delay: time, easing: [0.4, 0.0, 0.2, 1] });//[60, 10] 
				      time += 120;
				  });

			}else {
				self.$settingOffPanel.css({opacity: 0});
				self.$settingOnPanel.css({opacity: 1});

				self.$wraper.removeClass('npmenu__settings--isActive');

				self.$settingOffPanel.velocity({opacity: 1}, {
			      duration: 550,
			      queue: false,
			      easing: 'easeOutSine'
			    });

				self.$settingOnPanel.velocity({opacity: 0}, {
			      duration: 550,
			      queue: false,
			      easing: 'easeOutSine'
			    });
			}
		});

	};

	

	$.NPMenu = new NPMenu();
	$.NPMenu.Constructor = NPMenu;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	//Initialization of the Navigation Personalization Menu navigation-personalization-menu.js
	$.NPMenu.init();
});// jshint ignore:line

