!function($) {
	'use strict';
	var carouselHeroHome = function () {
		this.ele = '.js-hero-slider';
	};


	carouselHeroHome.prototype.init = function (){

		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

		this.$slicks.each(this._multiInit(this));

		$('.js-hero-slider-background')
		.on('init', function(slick) {
		    if ($.GtmManager && $.GtmManager.onHeroInit) { 
		 	  $.GtmManager.onHeroInit(slick);
		    }
	    })
		.slick({
				dots: false,
				arrows:false,
				mobileFirst:true,
				infinite: true,
				autoplay:false,
				accessibility:false,
				asNavFor: '.js-hero-slider',
			});

	};

	carouselHeroHome.prototype._multiInit = function (self)
	{
		return function(index, element){

			var $this = $(element);

			var defaults = {
				$previousDot : null,
				$activeDot : null,
				time : 0,
				isPause : false,
				tick : false,
				percentTime : 0,
				$lickItem : $this,
				self : self
			};


			//console.log("$this", $this, $this.hasClass('slick-initialized'))
			$this.on('init beforeChange', function(event, slick, currentSlide, nextSlide){
				//console.log(slick);
				//console.log(slick.$dots);
				if(event.type === 'init') {
					defaults.$activeDot = slick.$dots.find('.slick-active');
				}else {
					defaults.$previousDot = slick.$dots.children().eq(currentSlide);
					defaults.$activeDot = slick.$dots.children().eq(nextSlide);
					//debugger;
				}

				if(slick.options.autoplay && slick.$slides.length > 1) {
					defaults.time = slick.options.autoplaySpeed / 1000;
					self._startProgressbar(defaults);
				}
			});

			$this.on('mouseenter.slick', function(e){
				defaults.isPause = true;
			});

			$this.on('mouseleave.slick', function(e){
				defaults.isPause = false;
			})

			$this.on('afterChange', function(event, slick, currentSlide){
				if($.GtmManager && $.GtmManager.newSlide){
					$.GtmManager.newSlide(slick.$slides[currentSlide], currentSlide);
				}
			});

			$this.slick({
				dots: true,
				arrows:false,
				mobileFirst:true,
				infinite: true,
				autoplay:true,
				accessibility:false,
				autoplaySpeed: 6000,
				appendDots: $this.siblings('.hero__dots'),
				dotsClass: 'progress-dots',
				appendArrows: $this.siblings('.hero__arrows'),
				asNavFor: '.js-hero-slider-background',
				customPaging : function(/*slider, i*/) {
					return '<div class="progress-dots__item">' +
						'<div class="progress-dots__progress"><div class="progress-dots__progress-inner"></div></div>'+
						'</div>';
				},
				responsive: [
					{
						breakpoint: 993,
						settings: {
							arrows: true
						}
					}
				]
			});

		};
	};

	carouselHeroHome.prototype._startProgressbar = function (defaults)
	{
		this._resetProgressbar(defaults);
		defaults.isPause = false;
		defaults.percentTime = 0;
		defaults.tick = setInterval(this._interval, 10, defaults);
	};

	carouselHeroHome.prototype._resetProgressbar = function (defaults)
	{
		if(defaults.$previousDot) {
			defaults.$previousDot.find('.progress-dots__progress-inner').css({
				width: 0+'%'
			});
			clearTimeout(defaults.tick);
		}
	};

	carouselHeroHome.prototype._interval = function (defaults)
	{
		if(defaults.isPause === false) {
			defaults.percentTime += 1 / (defaults.time+0.1);
			defaults.$activeDot.find('.progress-dots__progress-inner').css({
				width: defaults.percentTime+"%"
			});
			if(defaults.percentTime >= 100)
			{
				defaults.$lickItem.slick('slickNext');
				defaults.self._startProgressbar(defaults);

				return false;
			}
		}
	};


	$.carouselHeroHome = new carouselHeroHome();
	$.carouselHeroHome.Constructor = carouselHeroHome;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.carouselHeroHome.init();
});// jshint ignore:line
