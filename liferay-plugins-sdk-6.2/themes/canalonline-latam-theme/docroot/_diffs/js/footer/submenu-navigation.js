//Submenu navigation
!function($) {
	'use strict';
	var SubNavigation = function () {
		this.ele = '.js-submenu-navigation';
	};

	SubNavigation.prototype.init = function()
	{

		this.$rail = $(this.ele);
		if(!this.$rail.length) return false;

		var $submenu__item = $('.dnavigation__item__no-parent.selected-children').parents('.dnavigation li');

		$(document).ready(function(){
			if ( $('.dnavigation__item__no-parent').hasClass('selected-children') ){
				$submenu__item.addClass('selected')
			}
		});

	};


	$.SubNavigation = new SubNavigation();
	$.SubNavigation.Constructor = SubNavigation;
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.SubNavigation.init();
});// jshint ignore:line
