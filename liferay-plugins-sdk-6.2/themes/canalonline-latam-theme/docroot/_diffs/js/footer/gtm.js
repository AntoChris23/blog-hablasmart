// ***********************************************
// *                                             *
// *                GTM CAM CUSTOM               *
// *                                             *
// ***********************************************


try { !function($) {
	
	var GtmManager = function () {
		this.dl = window['dataLayer'];
		this.ckPhone = 'CANALONLINE_PERSONALIZATION';
		this.ckUserType = 'CANALONLINE_PROFILE';
		this.promosShown = [];
		this.plansShown = [];
		this.isAutomaticSlide = true;
	};
	
	GtmManager.prototype.init = function() {
		var that = this;	
		that._checkUserId();
		that._heroSlide();
		that._plansAndDevices();
		that._impresionDevices();
		that._checkCommonOnClick();
		that._productDetail();
		that._checkMakerecargaOnClick();
		that._checkOcpVisible();
	}
	
	/* ***********************
	 * Public methods
	 * *********************** */
	
	GtmManager.prototype.settingSelected = function(settingName, settingId) {
		var that = this;
		
		var gtmEntry = {
			'event': 'trackEvent',
			'eventCategory': 'Personalización',
			'eventAction': 'No Clientes',
			'eventLabel': settingName,
			'eventValue': settingId.toString()
		}
		that.dl.push(gtmEntry);
		
	}
	
	GtmManager.prototype.heroNextSlideAuto = function(isAuto) {
		this.isAutomaticSlide = isAuto;
	}
	
	GtmManager.prototype.newSlide = function(slideEl, pos) {
		var that = this;
		
		var $el = $(slideEl);
		that._processHeroSlide($el);
		
	}
	
	GtmManager.prototype.onPlanListInit = function(slick) {

		var that = this;
		var target = $(slick.currentTarget);
		var activePlans = target.find('[data-gtm-type="listadoPlanes"]:visible');
		
		/*
		that._sendPlanesToGTM(
			activePlans,
			that.plansShown, activePlans.size() == 0, false
		);
		*/
		
	}
	
	GtmManager.prototype.onHeroInit = function(slick) {
		var that = this;
		var target = $(slick.currentTarget);
		var firstSlide = target.closest('.hero').find('div[data-gtm-type="hero-slide"][data-slick-index="0"]');
		that._processHeroSlide(firstSlide);
		
		// Seteamos los onClick para forzar que el siguiente
		// evento beforechange sea forzado 
		
		$('.hero .progress-dots,.hero .slick-arrow').click(function() {
			$.GtmManager.heroNextSlideAuto(false);
		});
		
	}
	
	GtmManager.prototype.onPlanListTab = function(tab) {
		
		var that = this;
		var target = $(tab);
		
		// tap selected
		
		var selectedTab = target.parents('.c-plan-container').find('a[href="#' + target.prop("id") + '"]');
		that._sendClickToGTM(selectedTab);
		
		// Visible plans
		
		that._sendPlanesToGTM(
			target.find('div[data-gtm-type="listadoPlanes"]:visible'),
			that.plansShown, false, false
		);
	}
	
	GtmManager.prototype.onDeviceListInit = function(slick) {
		
		var that = this;
		var target = $(slick.currentTarget);
		var activeDevices = target.find('a.slick-active[data-gtm-type="listadoEquipos"]:visible');
		
		that._sendPlanesToGTM(
			target.find('a[data-gtm-type="listadoEquipos"]:visible'),
			that.plansShown, activeDevices.size() == 0, true
		);
		
	}
	
	GtmManager.prototype.onDeviceListTab = function(tab) {
		
		var that = this;
		var target = $(tab);
		
		// tap selected
		
		var selectedTab = target.parents('.category-tabs-container').find('a[href="#' + target.prop("id") + '"]');
		that._sendClickToGTM(selectedTab);
		
		// Visible devices
		
		//var activeDevices = target.find('a.slick-active[data-gtm-type="listadoEquipos"]');
		var activeDevices = target.find('a.slick-active[data-gtm-type="listadoEquipos"]:visible');
		
		that._sendPlanesToGTM(
				target.find('a[data-gtm-type="listadoEquipos"]'),
			that.plansShown, activeDevices.size() == 0, true
		);
	}
	
	/* ***********************
	 * Private methods
	 * *********************** */
	
	GtmManager.prototype._checkUserId = function() {
		
		var that = this;
		
		var phone = that._cookieVal(that.ckPhone);
		var userType = that._cookieVal(that.ckUserType);
		
		if (phone || userType) {
			var gtmEntry = {}
			if (phone) gtmEntry['UserID'] = that._toB64(phone);
			if (userType) gtmEntry['tipoCliente'] = userType;
			that.dl.push(gtmEntry);
		}
		
	}
	
	GtmManager.prototype._heroSlide = function() {
		
		var that = this;
		
		// onClick
		
		$('div[data-gtm-type="hero-slide"]').click(function(ev){
			
			var $target = $(ev.target);
			
			if ($target.data('gtm-type') !== 'commonClick' && 
					$target.parents('.hero__item-freehtml').size() == 0) {
				
				var $el = $(ev.target).closest('.hero__item');
				var wcid = $el.data('wcid').toString(); 
				var creative = $el.data('creative');
				var gname = $el.data('gname');
				var pos = $el.data('slick-index');
				
				var newPromo = {	
					'id': wcid,
					'name': gname,
					'creative': creative,
					'position': pos.toString()
				}
				var gtmEntry = {
					'promos': [newPromo],
					'event': 'trackClickProm'
				}
				
				that.dl.push(gtmEntry);
			}
			
		});
	}
	
	GtmManager.prototype._processHeroSlide = function($el) {
		
		var that = this;
		var wcid = $el.data('wcid');
		var creative = $el.data('creative');
		var gname = $el.data('gname');
		var pos = $el.data('slick-index') || '0';
		
		wcid = wcid ? wcid.toString() : undefined;
		pos = pos ? pos.toString() : undefined;

		if ((that.isAutomaticSlide && $.inArray(wcid, that.promosShown) < 0) || !that.isAutomaticSlide) {
			that.promosShown.push(wcid);
			var newPromo = {	
				'id': wcid,
				'name': gname,
				'creative': creative,
				'position': pos
			}
			var gtmEntry = {
				'promos': [newPromo],
				'event': that.promosShown.length == 1 ? 'trackImpProm' :'trackEventSliderProm'
			}
			that.dl.push(gtmEntry);
		}
		that.isAutomaticSlide = true;
		
	}
	
	GtmManager.prototype._productDetail = function() {
		
		var that = this;
		var $buyBtn = $('[data-gtm-type="productDetailBuy"]');
		
		// Init
		
		$('[data-gtm-type="productDetailClient"]').each(function(i, el) {
			var $el = $(el);
			var $buyBtn = $('[data-gtm-type="productDetailClient"]');
			$buyBtn.data('gtm-price', $el.data('gtm-price'));
			$buyBtn.data('gtm-client', $el.data('gtm-client'));
			
		});
		
		if ($buyBtn.size() > 0) {
			var $el = $($buyBtn.get(0));
			that.dl.push({
				'productos': [that._createObjFromData($el,[
					"name", "id", "price", "brand", "category"
				])],
				'event': 'productdetailview'
			});
		}
		
		// onClick
		
		$('[data-gtm-type="productDetailClient"],[data-gtm-type="productDetailNotClient"]').click(function(ev) {
			var $el = $(ev.target).closest('[data-gtm-type="productDetailClient"],[data-gtm-type="productDetailNotClient"]');
			$buyBtn.data('gtm-price', $el.data('gtm-price'));
			$buyBtn.data('gtm-client', $el.data('gtm-client'));
		});
		
		$buyBtn.click(function(ev) {
			var obj = that._createObjFromData($buyBtn,[
				"name", "id", "price", "brand", "category", "variant", "list", "position", "quantity"
			]);
			
			that.dl.push({
				'productos': [obj],
				'event': 'trackaddtocart'
			});
			
		});
		
	}
	
	GtmManager.prototype._sendPlanesToGTM = function(list, plansShown, force, areDevices) {
		
		var that = this;
		var planesEquipos = [];
		
		list.each(function(i, el) {
			var $el = $(el);
			
			if ($el.hasClass('slick-active') || force) {
				
				var elId = $el.data('gtm-id');
				
				if ($.inArray(elId, that.plansShown) < 0) {
					that.plansShown.push(elId);
					
					var newPlanEquipo = that._createObjFromData($el,[
						"name", "id", "price", "brand", "category", "list", "position"
					]);
					
					
					// Si se ven mas de 1 equipo a la vez es que no es mobile
					// y hay que arreglar las posiciones

					if (!force && areDevices && $(window).width() > 600) {
						try {
							var indx = parseInt($el.data('slick-index')) + 1;
							newPlanEquipo.position = indx;
						} catch (e) { console.log(e); }
					}
					else {
						newPlanEquipo.position = parseInt(newPlanEquipo.position);
					}
					
					newPlanEquipo.position = ( newPlanEquipo.position || newPlanEquipo.position == 0) ? 
							newPlanEquipo.position.toString() : undefined;
					
					// Agregamos al listado
					
					planesEquipos.push(newPlanEquipo);
										
				}
				
			}
		});
		
		if (planesEquipos.length > 0) {
			
			// No se cargan bien las positions si no hay suficientes elementos
			// como para generar paginacion
			if (force) {
				$.each(planesEquipos, function(i, arrEl) {
					arrEl.position = i.toString();
				});
			}
			

			// Finalmente
			that.dl.push({
				'productos': planesEquipos,
				'event': 'trackProdImp'
			});

		}
		
	}
	

	GtmManager.prototype._sendClickToGTM = function(el) {
		
		var that = this;
		var $el = $(el);
		var obj = { 'event': 'trackEvent' };
		
		obj['eventCategory'] = $el.data('gtm-category');
		obj['eventAction'] = $el.data('gtm-action');
		obj['eventLabel'] = $el.data('gtm-label');
		
		that.dl.push(obj);
		
	}
	
	GtmManager.prototype._plansAndDevices = function() {
		
		var that = this;
		
		// -------------- Planes
		
		var slickPlanes = $('.js-pricing-slider');
		
		//on Load
		
		slickPlanes.on('init', function(slick) {
			
			var $target = $(slick.currentTarget);
			
			that._sendPlanesToGTM(
					$target.find("div.slick-active[data-gtm-type='listadoPlanes']:visible"),
					that.plansShown, true, false
			);
			
		});
		
		
		// Event on swipe or on next
		
		slickPlanes.on('afterChange', function(slick){
			
			var $target = $(slick.currentTarget);
			
			that._sendPlanesToGTM(
				$target.find('div[data-gtm-type="listadoPlanes"]:visible'),
				that.plansShown, false, false
			);
			
		});
		
		// on click "Ver mas" planes
		
		$('.js-pricing-slider .c-plan .c-plan__view-detail').click(function(ev) {
			var $target = $(ev.target);
			var elPlan = $target.parents('.c-plan');
			var plan = that._createObjFromData(elPlan,[
				"name", "id", "price", "brand", "category", "list", "position"
			]); 
			
			that.dl.push({
				'productos': [plan],
				'event': 'trackClickProd'
			});
			
		});
		
		// on click "Lo quiero" planes
		
		$('.js-pricing-slider .c-plan .c-plan__get').click(function(ev) {
			var $target = $(ev.target);
			var elPlan = $target.parents('.c-plan');
			var plan = that._createObjFromData(elPlan,[
				"name", "id", "price", "brand", "category", "list", "position"
			]);
			plan['quantity'] = 1;
			
			that.dl.push({
				'productos': [plan],
				'event': 'trackaddtocart'
			});
			
		});
		
		// -------------- Dispositivos
		
		var slickDispositivos = $('.js-device-caruosel');
		
		//on Load 
		
		slickDispositivos.on('init', function(slick) {
			
			var $target = $(slick.currentTarget);
			
			that._sendPlanesToGTM(
					$target.find("a.device-carousel__item.slick-active[data-gtm-type='listadoEquipos']:visible"),
					that.plansShown, true, false
			);
			
		});
		
		
		// Event on swipe or on next
		
		slickDispositivos.on('afterChange', function(slick){

			var $target = $(slick.currentTarget);
			that._sendPlanesToGTM(
				$target.find('a.slick-active[data-gtm-type="listadoEquipos"]:visible'),
				that.plansShown, false, true
			);
			
		});
		
		// on click dispositivos
		
		slickDispositivos.find('.device-carousel__item').click(function(ev) {
			var $target = $(ev.target);
			var elDevice = $target.closest('.device-carousel__item');
			var wrapper = elDevice.parents('.js-device-caruosel');
			
			var position = elDevice.data('gtm-position');
			if (wrapper.find('a.slick-active[data-gtm-type="listadoEquipos"]:visible').size() > 1 
					&& $(window).width() > 600) {
				try {
					position = parseInt(elDevice.data('slick-index')) + 1;
				} catch (e) { console.log(e); }
			}
			position = position || position == 0 ? position.toString() : undefined;

			if ($target.hasClass('device-carousel__featured')) {
				
				var device = that._createObjFromData(elDevice,[
					"name", "id", "price", "brand", "category", "variant", "list", "position", "quantity"
				]);
				device.quantity = 1;
				device.position = position;				
				
				that.dl.push({
					'productos': [device],
					'event': 'trackaddtocart'
				});
				
			}
			else {
				
				var device = that._createObjFromData(elDevice,[
					"name", "id", "price", "brand", "category", "list", "position"
				]);
				device.position = position;
				
				that.dl.push({
					'productos': [device],
					'event': 'trackClickProd'
				});
			}			
			
		});
		
	}
	
	/* ***********************
	 * Dispositivos (no Slick)
	 * *********************** */
	
	GtmManager.prototype._impresionDevices = function() {
		
		var that = this;
		
		// -------------- Dispositivos
		
		var dispositivos = $('.wrapperDevices');
		var that = this;
		
		dispositivos.find('.boxDevice').click(function(ev) {
			var $target = $(ev.target);
			var elDevice = $target.closest('.boxDevice');
			var wrapper = elDevice.parents('.wrapperDevices');
			
			var position = elDevice.data('data-gtm-position');
			if (wrapper.find('div.boxDevice[data-gtm-category="Celulares"]').size() > 1) {
				try {
					position = parseInt(elDevice.data('data-gtm-position')) + 1;
				} catch (e) { console.log(e); }
			}
			position = position || position == 0 ? position.toString() : undefined;

			var device = that._createObjFromData(elDevice,[
				"name", "id", "price", "brand", "category", "list", "position"
			]);
			device.position = position;
			
			that.dl.push({
				'productos': [device],
				'event': 'trackClickProd'
			});
		
		});
		
	}
	
	/* ***********************
****** Dispositivos (no Slick)
	 * *********************** */
	
	
	/* ***********************************
	 * *medicion OCP visible
	 */							
	GtmManager.prototype._checkOcpVisible = function() {			
		
		var that = this;			
		var slickocp = $('.js-pricing-slider');
         		
        slickocp.on('afterChange', function(slick){
			
			var $target = $(slick.currentTarget);
			that._sendPlanesToGTM($target.find('div[data-gtm-type="PaqueteOCP"]:visible'),
				that.plansShown, false, false
			);
		
		});

          
      	// on click "comprar"
  		
  		$('.js-pricing-slider .c-plan .c-plan__get').click(function(ev) {
  			var $target = $(ev.target);
  			var elPlan = $target.parents('.c-plan');
  			var plan = that._createObjFromData(elPlan,[
  				"name", "id", "price", "brand", "category", "list", "position"
  			]);
  			plan['quantity'] = 1;
  			
  			that.dl.push({
  				'productos': [plan],
  				'event': 'trackClickProd'
  			});
  			
  		});
  		
  	
	
		// Event on swipe or on next
		
		var slickDispositivos = $('.js-device-caruosel');
		var that = this;
		
		slickDispositivos.on('afterChange', function(slick){
	
			var $target = $(slick.currentTarget);
			that._sendPlanesToGTM(
				$target.find('div[data-gtm-type="listadoOCP"]:visible'),
				that.plansShown, false, true
			);
			
		});
		
	}
	
	
	
	
	/* ***********************************
	 * *medicion Recragas 
	 */
							
		GtmManager.prototype._checkMakerecargaOnClick = function() {		
					
			
			var that = this;							

				$('[data-gtm-type="checkMakerecargaOnClick"]').click(function(ev) {
				
					
					var $el = $(ev.target).closest('[data-gtm-type="checkMakerecargaOnClick"]');     
					var obj = {
							'event': 'trackEvent',
							'eventCategory': $el.data('gtm-category'),
							'eventAction': $el.data('gtm-action'),
							'eventLabel': $('#rechargesForm').find('ul.dropdown-content.select-dropdown li.active.selected').text(),
							'eventValue': $('.recharge-process__item-cam').find('.process-rounded[type="radio"]:checked').val(),
							'dimension1':  $('#movistarNumber').val(),
						}
					
					that.dl.push(obj);
					
				});
				
			}
		
	
	
	GtmManager.prototype._checkCommonOnClick = function() {
		
		var that = this;
		
		$('[data-gtm-type="commonClick"]').click(function(ev) {
			
			var $el = $(ev.target).closest('[data-gtm-type="commonClick"]');
			
			var obj = {
				'event': 'trackEvent',
				'eventCategory': $el.data('gtm-category'),
				'eventAction': $el.data('gtm-action'),
				'eventLabel': $el.data('gtm-label'),
			}
			
			that.dl.push(obj);
			
		});
		
	}
	
	/* ***********************
	 * Utils
	 * *********************** */
	
	GtmManager.prototype._createObjFromData = function(el, propsArr) {
		var result = {}
		
		$.each(propsArr, function(i, prop) {
			var val = el.data("gtm-" + prop);
			if (val || val === 0) {
				result[prop] = String(val).replace(/^\s+|\s+$/g, '');
			}
		});
		
		return result;
	}
	
	GtmManager.prototype._toB64 = function(str) {
		return window.btoa(unescape(encodeURIComponent( str )));
	}
	
	GtmManager.prototype._cookieVal = function(cName) {
		var nameEQ = cName + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) === ' ') {
				c = c.substring(1, c.length);
			}
			if (c.indexOf(nameEQ) === 0) {
				return c.substring(nameEQ.length, c.length);
			}
		}
		return null;
	}
	
	/* ***********************
	 * Init
	 * *********************** */
	
	
	$.GtmManager = new GtmManager();
	$.GtmManager.Constructor = GtmManager;
	
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.GtmManager.init();
});// jshint ignore:line

} catch(e) { console.log(e); } 