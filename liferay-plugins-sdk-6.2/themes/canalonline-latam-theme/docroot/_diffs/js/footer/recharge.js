!function($) {
	'use strict';
	var rechargeProcess = function () {
		this.process = '.recharge-process';
		this.dataMobileNumber = '.data-mobile-number';
		this.dataMobileEmail = '.data-mobile-email';
		this.dataMobilePrice = '.data-mobile-price';
		this.dataMobilePaymentMethod = '.data-mobile-paymentmethod';
		this.editPrice_ob = '.data-mobile-price-edit';
		this.editEmail_ob = '.data-mobile-email-edit';
		this.editNumber_ob = '.data-mobile-number-edit';
		this.check_ob = '.recharge-process .process-rounded';
		this.textPrice_ob = '.process-rounded__inner[type="text"]';
		this.buttonPrice_ob = '.buttonPrice';
		this.moviNumber_ob = '#movistarNumber';
		this.moviEmail_ob = '#movistarEmail';
		this.moviPayment_ob = '#movistarPaymentmethod';

	};

	rechargeProcess.prototype.init = function()
	{
		this.$inputPrice = $(this.dataMobilePrice);
		this.$inputNumber = $(this.dataMobileNumber);
		this.$inputEmail = $(this.dataMobileEmail);
		this.$inputPayment = $(this.dataMobilePaymentMethod);
		this.$editEmailClick = $(this.editEmail_ob);
		this.$editPriceClick = $(this.editPrice_ob);
		this.$editNumberClick = $(this.editNumber_ob);
		this.$checkedPrice = $(this.check_ob).filter(":checked");
		this.$prices = $(this.check_ob);
		this.$textPrice = $(this.textPrice_ob);
		this.$textPriceParent = $(this.textPrice_ob).parent().siblings("input");
		this.$buttonPrice = $(this.buttonPrice_ob);
		this.$moviNumber = $(this.moviNumber_ob);
		this.$moviEmail = $(this.moviEmail_ob);
		this.$moviPayment = $(this.moviPayment_ob);
		var self = this;
		
		// obtenemos la opcion seleccionada por default del metodo de pago y se la asignamos
		//al input de resumen
		var movPaymentSelected = $(this.$moviPayment.selector + ' option:selected').text();
		this.$inputPayment.val(movPaymentSelected);
		
		// obtenemos valor inicial del precio y lo añadimos en los campos de los siguientes pasos
		var initialPrice = this.$checkedPrice.val();
		this.$inputPrice.val(initialPrice);
//		this.$buttonPrice.text(initialPrice);
		
		// Llamamos a la funcion cuando el precio cambie o se modifique el input
		this.$prices.on('change', {elementoPadre:this}, this.changePrice);
		this.$textPrice.on('paste keyup focus',{elementoPadre:this}, this.changeValue);

		//Llamamos a la funcion cuando el email se modifique
		this.$moviEmail.on('click change paste keyup blur focus',{elementoPadre:this}, this.changeEmail);
		
		//Llamamos a la funcion cuando el metodo de pago se modifique
		this.$moviPayment.on('click change paste keyup blur focus',{elementoPadre:this}, this.changePayment);
		
		//Llamamos a la funcion cuando el numero se modifique
		this.$moviNumber.on('click change paste keyup blur focus',{elementoPadre:this}, this.changeNumber);

		// Habilitar y modificar number
		this.$editNumberClick.click(function(){
			self.$inputNumber.removeAttr("readonly");
			self.$inputNumber.focus();
		});

		// Habilitar y modificar email
		this.$editEmailClick.click(function(){
			self.$inputEmail.removeAttr("readonly");
			self.$inputEmail.focus();
		});

		// Deshabilitar y modificar datos number
		this.$inputEmail.on('blur',{elementoPadre:this}, this.editEmail);
		
		// Habilitar y modificar price
		this.$editPriceClick.click(function(){
			self.$inputPrice.removeAttr("readonly");
			self.$inputPrice.focus();
		});

		// Deshabilitar y modificar datos price
		this.$inputPrice.on('blur',{elementoPadre:this}, this.editPrice);
		
//		//colocamos como valor por defecto para el input del metodo de pago el primero del select
//		var movPaymentDefault = $(this.moviPayment.selector + ' option:selected').text();
//		alert(movPaymentDefault);
//		this.$inputPayment.val(movPaymentDefault);
//		var movPayment = $(padre.$moviPayment.selector + ' option:selected').text();
//		padre.$inputPayment.val(movPayment);
	};
	rechargeProcess.prototype.changeNumber = function (e){
		var padre = e.data.elementoPadre;
		var movNumber = padre.$moviNumber.val();
		padre.$inputNumber.val(movNumber);
	}
	rechargeProcess.prototype.changeEmail = function (e){
		var padre = e.data.elementoPadre;
		var movEmail = padre.$moviEmail.val();
		padre.$inputEmail.val(movEmail);
	}
	rechargeProcess.prototype.changePayment = function (e){
		var padre = e.data.elementoPadre;
		var movPayment = $(padre.$moviPayment.selector + ' option:selected').text();
		padre.$inputPayment.val(movPayment);
	}
	rechargeProcess.prototype.changeValue = function (e){
		var padre = e.data.elementoPadre;
		var newValue = padre.$textPrice.val();
		padre.$textPriceParent.val(newValue);

		padre.$inputPrice.val(newValue);
//		padre.$buttonPrice.text(newValue);
	}
	rechargeProcess.prototype.changePrice = function (e){

		var padre = null;
		if (typeof e !== 'undefined') {
			padre = e.data.elementoPadre;
		}else {
			padre = this;
		}
		padre.$checkedPrice = $(padre.check_ob).filter(":checked");

		var newPrice = padre.$checkedPrice.val();
		
		padre.$inputPrice.val(newPrice);
//		padre.$buttonPrice.text(newPrice);
	}
	rechargeProcess.prototype.editNumber = function (e){
		var padre = e.data.elementoPadre;
		var movNumber = padre.$inputNumber.val();
		padre.$moviNumber.val(movNumber);
		padre.$inputNumber.attr("readonly", true);
	}
	rechargeProcess.prototype.editPrice = function (e){
		var padre = e.data.elementoPadre;
		var editingPrice = padre.$inputPrice.val();
//		padre.$buttonPrice.text(editingPrice);
		padre.$textPrice.val(editingPrice);
		padre.$textPriceParent.val(editingPrice);
		padre.$inputPrice.attr("readonly", true);
		padre.$textPriceParent.click();
	}
	
	$.rechargeProcess = new rechargeProcess();
	$.rechargeProcess.Constructor = rechargeProcess;
}(window.jQuery, window.Liferay);// jshint ignore:line


//********** document.ready

jQuery(document).ready(function(){
	'use strict';
	$.rechargeProcess.init();
});// jshint ignore:line

