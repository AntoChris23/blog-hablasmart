!function($) {
	'use strict';
	// Canal Online Header
	var CLHeader = function () {
		this.ele = '#navigation';
		this.svgLogo = '#logo-movistar-svg';
		this.logoGradientMap = [];
	};

	CLHeader.prototype.init = function() 
	{
		this.$wraper = $(this.ele);
		if(!this.$wraper.length) return false;

		this.$body = $('body');
		this.$toogle = this.$wraper.find('.js-toogle-top-navigation');
		this.$desktopSearchButton = this.$wraper.find('.js-desktop-search-button');
		this.$desktopSearchDismiss = this.$wraper.find('.js-dismiss-desktop-search-modal');

		this.$logo = $(this.svgLogo);

		this.logoGradientMap = this._setGradientMap('#ffffff', '#53bf02', this.$wraper.height());

		this._initEvents();

	};

	/**
	* Event binding
	*/
	CLHeader.prototype._initEvents = function() 
	{
		var self = this;

		//menu toogle function
		this.$toogle.on('click', function() {
			self.$body.toggleClass('mlnavigation--open');
		});

		this.$desktopSearchButton.on('click', function(e){
			if($(this).data('search-modal') === true) {
				e.preventDefault();
				self.$body.addClass('desktopSearch--open');
				$('#desktop-search-modal').modal('open');
			}
			
		});

		this.$desktopSearchDismiss.on('click', function(){
			self.$body.removeClass('desktopSearch--open');
			$('#desktop-search-modal').modal('close');
		});

		//Adds fade / unfade effect to the header bar on scroll
		/*
		$(window).scroll(function() {
			if($(window).width() < 993){
				var shadow = 0.14;
				var bgOpacity = Math.round($(this).scrollTop() * 100 / self.$wraper.height()) / 100 ;
				var shadowOpacity = bgOpacity * 100  * shadow / 100;

				if(bgOpacity > 1){
					self.$wraper.css({'background':'rgb(255,255,255)','box-shadow':'0 2px 2px 0 rgba(0,0,0,'+shadow+')'});
				}else if(bgOpacity > 0 && bgOpacity < 1 ){
					self.$wraper.css({'background':'rgba(255,255,255,'+bgOpacity+')','box-shadow':'0 2px 2px 0 rgba(0,0,0,'+shadowOpacity+')'});
				}else if(bgOpacity >= 1 ){
					self.$wraper.css({'background':'rgba(255,255,255,1)','box-shadow':'0 2px 2px 0 rgba(0,0,0,'+shadow+')'});
				}else {
					self.$wraper.css({'background':'','box-shadow':''});
				}

				if($(this).scrollTop() === 0){
					self.$logo.css({color: self.logoGradientMap[0]});
				}else if($(this).scrollTop() < self.$wraper.height()){
					self.$logo.css({color: self.logoGradientMap[Math.round($(this).scrollTop())]});
				}else if ($(this).scrollTop() >= self.$wraper.height()){
					self.$logo.css({color: self.logoGradientMap[self.logoGradientMap.length - 1]});
				}
			}else {
				self.$wraper.css({'background':'','box-shadow':''});
				self.$logo.css({color: ''});
			}
			
		});
		*/




	};

	/**
	* Returns array of colors between to colors
		@param: initColor      => the first color, hex (ie: #000000)
		@param: endColor      => the second color, hex (ie: #ffffff)
		@param: steps 		  => amount of color blends
		@returns: Array    => array of color in between the initColor and the endColor
	*/
	CLHeader.prototype._setGradientMap = function(initColor,endColor,steps) 
	{
		steps = Math.round(steps);
		var gradient = [];

		for (var i = 0; i < steps; i++) {
			if(i !== 0 && i !== steps-1){
				var percentage = Math.round(i * 100 / steps) / 100;
				gradient[i] = this._blendColors(initColor,endColor,percentage);
			}else if(i === 0){
				gradient[i] = initColor;
			}else if (i === steps-1){
				gradient[i] = endColor;
			}
		}

		return gradient;
	};

	/*
	    blend two colors to create the color that is at the percentage away from the first color
	    @param: initColor      => the first color, hex (ie: #000000)
	    @param: endColor      => the second color, hex (ie: #ffffff)
	    @param: percentage  => the distance from the first color, as a decimal between 0 and 1 (ie: 0.5)
	    @returns: string    => the third color, hex, represenatation of the blend between color1 and color2 at the given percentage
	*/
	CLHeader.prototype._blendColors = function(initColor, endColor, percentage) 
	{
		//remove # char
		initColor = initColor.substring(1);  
		endColor = endColor.substring(1);

	    //convert hex to rgb
	    initColor = [parseInt(initColor[0] + initColor[1], 16), parseInt(initColor[2] + initColor[3], 16), parseInt(initColor[4] + initColor[5], 16)];
	    endColor = [parseInt(endColor[0] + endColor[1], 16), parseInt(endColor[2] + endColor[3], 16), parseInt(endColor[4] + endColor[5], 16)];

	    //blend colors
	    var blendedColor = [
	    	(1 - percentage) * initColor[0] + percentage * endColor[0], 
	        (1 - percentage) * initColor[1] + percentage * endColor[1], 
	        (1 - percentage) * initColor[2] + percentage * endColor[2]
	    ];

	    // convert to hex
	    blendedColor = '#' + this._intToHex(blendedColor[0]) + this._intToHex(blendedColor[1]) + this._intToHex(blendedColor[2]);


	    // return hex
	    return blendedColor;
	};

	/**
	* Rgb String to Rgb Array
	* 
	* @param: String rgbColor	=> 'rgb(83, 191, 2)'
	* @returns: Array 			=> [83,191,2]
	*/
	CLHeader.prototype._rgbStringToArray = function(rgbColor) 
	{
		var rgbRegExp = /rgb\((\d{1,3}), (\d{1,3}), (\d{1,3})\)/;
		return rgbRegExp.exec(rgbColor);
	};

	/**
	* Rgb to hexadecimal
	* 
	* [83,191,2] => #53bf02
	*/
	CLHeader.prototype._rgbToHex = function(R,G,B) 
	{
		return '#'+this._intToHex(R)+this._intToHex(G)+this._intToHex(B);
	};

	/**
	* Integer to Hexadecimal
	* 
	* 83 => 53
	*/
	CLHeader.prototype._intToHex = function(number) 
	{
		number = parseInt(number,10);
		if (isNaN(number)) return "00";
		number = Math.max(0,Math.min(number,255));
		return "0123456789ABCDEF".charAt((number-number%16)/16) + "0123456789ABCDEF".charAt(number%16);
	};

	$.CLHeader = new CLHeader();
	$.CLHeader.Constructor = CLHeader;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.CLHeader.init();
});// jshint ignore:line