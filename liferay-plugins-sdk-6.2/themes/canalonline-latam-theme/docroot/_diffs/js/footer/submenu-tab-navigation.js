//Submenu tab navigation
!function($) {
	'use strict';
	// Multi Level Navigation Menu
	var SMTNavigation = function () {
		this.ele = '.js-navigation-tabs';
	};

	SMTNavigation.prototype.init = function()
	{

		this.$rail = $(this.ele);
		if(!this.$rail.length) return false;

		$('html').addClass('navigation-tabs--active');

		var $subemnu2__item = $('.dnavigation__submenu__2 .selected-children').parents('.dnavigation li');

		$(document).ready(function(){
			if ( $('.navigation-tabs__item').hasClass('selected-children') ) {
				$subemnu2__item.addClass('selected');
			}

			$('.navigation-tabs__link').on('click',function(){
				window.sessionStorage.setItem('tnv::hdtb-pos', $('.navigation-tabs__rail').scrollLeft());
			});
		});


		(function() {
			var a = document.getElementById("hdtb-sc"),
				b = "rtl" == window.getComputedStyle(a).direction,
				c = a.querySelector(".hdtb-msel");
			if (c) {
				var d = 0;
				try {
					d = parseInt(window.sessionStorage.getItem("tnv::hdtb-pos"), 10);
				} catch (k) {}
				var e = b ? a.scrollWidth - c.clientWidth - c.offsetLeft : c.offsetLeft,
					f = a.clientWidth,
					g = Math.max(Math.min(a.scrollWidth - f, e + c.clientWidth - f + 24), Math.min(isNaN(d) ? 0 : d, e));

				a.scrollLeft = g;
			}
			a.style.opacity = "1";
		}).call(this);

	};


	$.SMTNavigation = new SMTNavigation();
	$.SMTNavigation.Constructor = SMTNavigation;
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.SMTNavigation.init();
});// jshint ignore:line
