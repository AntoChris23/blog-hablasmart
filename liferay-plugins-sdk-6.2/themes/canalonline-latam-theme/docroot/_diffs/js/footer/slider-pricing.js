!function($) {
	'use strict';
	var carouselPricing = function () {
		this.ele = '.js-pricing-slider';
		this.modal = '#modal-products-filter';
	};


	carouselPricing.prototype.init = function (){

		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

		var self = this;


		//init all slick carousels
		this.$slicks.each(this._multiInit());

		this.$categoryTabs = this.$slicks.parents('.category-tabs-container').find('.js-category-tabs');
		this.$categoryTabsItems = this.$categoryTabs.find('.tab');

		this.$categoryTabs.tabs({
			onShow: function(tab)
			{
				centerNav();
				$(tab).find(self.ele).slick('setPosition');
				self.$filterModal.find('#filter-' + $(tab).attr('id')).prop('checked', true);
				if ($.GtmManager.onPlanListTab) {
					$.GtmManager.onPlanListTab(tab);
				}
			}
		});

		function centerNav () {
			var $active = self.$categoryTabsItems.find('a.active').parent();
			var index = self.$categoryTabsItems.index($active);
			var position = ($active.width() * index) - (($(window).width() - $active.width()) / 2);
			self.$categoryTabs.animate({scrollLeft: position}, 300);
		}

		this.$filterModal= $(this.modal);

		if(this.$filterModal.length) {
			this.applyFilterButton = this.$filterModal.find('.js-filter-apply');
			this.discardFilterButton = this.$filterModal.find('.js-filter-erase');


			this.applyFilterButton.on('click', {self:self}, this._setFilter);
			this.discardFilterButton.on('click', {self:self}, this._discardFilter);
		}


	};

	carouselPricing.prototype._multiInit = function ()
	{
		return function(index, element){

			var $this = $(element);

			$this
			  .on('destroy', reInitCarousel)
			  .on('init', function(slick) {
				  if ($.GtmManager.onPlanListInit) { 
					  $.GtmManager.onPlanListInit(slick);
				  }
			  })
			.slick({
			  infinite: false,
			  slidesToShow: 4,
			  slidesToScroll: 4,
			  swipeToSlide: true,
			  accessibility: false,
			  arrows: true,
			  appendArrows: $this.siblings('.c-pricing__arrows'),
			  responsive: [
				{
				  breakpoint: 1200,
				  settings: {
						slidesToShow: 3,
						  slidesToScroll: 3
					}
				},
				{
				  breakpoint: 993,
				  settings: "unslick"
				}
			  ]
			});

		};
	};


	carouselPricing.prototype._setFilter = function (e)
	{
		var self = e.data.self;
		var tabId = self.$filterModal.find('input').filter(':checked').attr('id').replace('filter-','');
		self.$categoryTabs.tabs('select_tab', tabId);
		//self.$filterModal.modal('close');
	};

	carouselPricing.prototype._discardFilter = function (e)
	{
		var self = e.data.self;
		var tabId = self.$categoryTabs.find('a').filter('.active').attr('href').substring(1);
		self.$filterModal.find('#filter-' + tabId).prop('checked', true);
	};





	$.carouselPricing = new carouselPricing();
	$.carouselPricing.Constructor = carouselPricing;
}(window.jQuery);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.carouselPricing.init();
});// jshint ignore:line
