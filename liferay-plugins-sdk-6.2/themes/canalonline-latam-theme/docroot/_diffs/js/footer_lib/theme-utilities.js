function _debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function _throttle(fn, threshhold, scope) 
{
  if (threshhold === undefined) threshhold = 250;
  var last,deferTimer;

  return function ()
  {
    var context = scope === undefined ? scope : this;

    var now = +new Date(),
          args = arguments;

      if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
      }else {
        last = now;
      fn.apply(context, args);
      }

  };
}

function reInitCarousel(ev, slick)
{
		let carousel = $(this),
    reinit = _debounce(function () {
      /*
       * slick.activeBreakpoint will tell us the breakpoint
       * at which the carousel was destroyed.
       */

       if(slick.options.mobileFirst && slick.activeBreakpoint < window.innerWidth || !slick.options.mobileFirst && slick.activeBreakpoint > window.innerWidth){
       		return;
       }

      // Re-initialize with the old settings.
      carousel.slick(slick.options);

      // Remove this event listener.
      window.removeEventListener('resize',reinit);
    }, 500);

    // Assign our debounced callback to window.resize.
    window.addEventListener('resize', reinit);
}