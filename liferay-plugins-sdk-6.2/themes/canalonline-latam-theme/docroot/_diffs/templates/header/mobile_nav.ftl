<div id="mlnavigation" class="mlnavigation">
   <div class="mlmenu__header">
		<div class="mlmenu__header-inner">
			<div class="mlmenu__nav">
				<div class="mlmenu__nav-inner">
					<div class="mlmenu__nav-slide">

						<div class="input-field mlmenu__nav__select">
							<select class="md" onchange="location = this.options[this.selectedIndex].value;">
								<#if headerLayout?? && headerLayout.hasChildren()>
									<#list headerLayout.getAllChildren() as layout>
										<option value="${portalUtil.getLayoutFriendlyURL(layout, themeDisplay)}">${layout.getName(locale)}</option>
									</#list>
								</#if>
							</select>
						</div>

						<div class="mlmenu__back-container">
							<a href="#" class="mlmenu__back waves-effect waves-light">
								<span class="icon-navigate_before"></span>
								<span class="mlmenu__back-text"></span>
							</a>
						</div>

					</div>
				</div>
			</div>
			<#if !validator.equals(stUrlSetting, '#!')>
			<div class="mlmenu__search">
				<div class="mlmenu__search__inner">
					<div class="mlmenu__search-button-container">
						<a class="mlmenu__search-button js-open-top-search" data-search-modal="${isStModal?string}" href="${stUrlSetting}">
							<span class="icon-search"></span>
						</a>
					</div>
					<div class="mlmenu__search-input-container">
						<input class="mlmenu__search-input" type="text">
					</div>
					<div class="mlmenu__search-close-container">
						<button class="mlmenu__search-close js-close-top-search">Cancelar</button>
					</div>
				</div>
			</div>
			</#if>
		</div>
	</div>
	<div class="mlmenu__container">
		
		<nav id="navigationMobile" class="mlmenu">

			<#-- * LEVEL 1 * -->
		
			<ul role="menubar" data-menu="main" class="mlmenu__sub-menu">
			
				<#assign navItemI = 0>
			
				<#list navItems as nav_item>
					
					<#if nav_item.getLayout().getFriendlyURL(locale) != homeLayoutFurl>

						<li class="mlmenu__level">					
									<a data-submenu="submenu-${navItemI}" href="${nav_item.getURL()}" class="mlmenu__link" data-gtm-type="commonClick" data-gtm-category="opciones menu" data-gtm-action="click" data-gtm-label="${nav_item.getName()}" data-gtm-value="">
								<span class="mlmenu__link-text">${nav_item.getName()}</span>
								<#if nav_item.hasChildren() || nav_item.getLayout().getFriendlyURL(locale) == clientSettingsLayoutFurl>
									<span class="icon-navigate_next"></span>
								</#if>
							</a>				
								
						</li>
						
					</#if>
				
					<#assign navItemI = navItemI + 1>
				
				</#list>
				
			</ul>
			<#-- * LEVEL 2 *  -->
	
			<#assign navItemI = 0>
			
			<#list navItems as nav_item>
			
				<#if nav_item.getLayout().getFriendlyURL(locale) != homeLayoutFurl && nav_item.hasChildren()>
					
					<ul data-menu="submenu-${navItemI}" class="mlmenu__sub-menu submenu-${navItemI}">
					
						<#assign navChildI = 0>
					
						<#list nav_item.getChildren() as nav_child>
						
							<li class="mlmenu__level">
									<a data-submenu="submenu-${navItemI}-${navChildI}" href="${nav_child.getURL()}" class="mlmenu__link"  data-gtm-type="commonClick" data-gtm-category="opciones menu" data-gtm-action="click" data-gtm-label="${nav_child.getName()}" data-gtm-value="">
									<span class="mlmenu__link-text">${nav_child.getName()}</span>
									<#if nav_child.hasChildren()>
										<span class="icon-navigate_next"></span>
									</#if>	
								</a>	
								
							</li>
							
							<#assign navChildI = navChildI + 1>
						
						</#list>
						
					</ul>

				<#elseif nav_item.getLayout().getFriendlyURL(locale) == clientSettingsLayoutFurl>

					<#-- ********* Client Settings ********* -->
			
					<ul data-menu="submenu-${navItemI}" class="mlmenu__sub-menu submenu-${navItemI}">
						<li class="mlmenu__level">
							
							<#-- *********** START : Client settings Portlet *********** -->
							
							<#attempt>
							
								<#assign locPortletId = "clientsettings_WAR_clientsettingsportlet_INSTANCE_menu" />
								<#assign PortletPreferencesFactoryUtil = staticUtil["com.liferay.portlet.PortletPreferencesFactoryUtil"] />
								<#assign portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(layout, locPortletId) />
								<#assign temp = portletSetup.setValue("portletSetupShowBorders", "false") />
								<#assign temp = portletSetup.setValue("view", "menu") />
								${theme.runtime(locPortletId, "", portletSetup.toString())}
								 
							<#recover>
							</#attempt>
							
							<#-- *********** END : Client settings Portlet *********** -->
							
						</li>
					</ul>

				
				</#if>
			
				<#assign navItemI = navItemI + 1>
			
			</#list>


			<#-- * LEVEL 3 * -->
	
			<#assign navItemI = 0>
			
			<#list navItems as nav_item>
			
				<#if nav_item.getLayout().getFriendlyURL(locale) != homeLayoutFurl && nav_item.hasChildren()>
					
					<#assign navChildI = 0>
					
					<#list nav_item.getChildren() as nav_child>
						
						<#if nav_child.hasChildren()>
						
							<ul data-menu="submenu-${navItemI}-${navChildI}" class="mlmenu__sub-menu submenu-${navItemI}-${navChildI}">
							
								<#list nav_child.getChildren() as nav_grandchild>
									<li class="mlmenu__level">
											<a href="${nav_grandchild.getURL()}" class="mlmenu__link"  data-gtm-type="commonClick"  data-gtm-category="opciones menu" data-gtm-action="click" data-gtm-label="${nav_grandchild.getName()}" data-gtm-value="">
											<span class="mlmenu__link-text">${nav_grandchild.getName()}</span>
										</a>
									</li>
								
								</#list>
								
							</ul>
							
						</#if>
						
						<#assign navChildI = navChildI + 1>
					
					</#list>
				
				</#if>
			
				<#assign navItemI = navItemI + 1>
			
			</#list>


			<#-- * Embedded mobile menu web content * -->
			
			<div class="mlmenu--login">
				<#if hasEmbeddedWcMenuMobile>
					${embeddedWcMenuMobileContent}
				</#if>
			</div>

		</nav>

	</div>
	<div class="mlmenu__close js-toogle-top-navigation"></div>
</div>
