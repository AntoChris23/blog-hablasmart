<nav class="navigation-tabs">
	<div>
		<div id="hdtb-sc" class="navigation-tabs__rail js-navigation-tabs">
			<div>
				<ul id="hdtb-msb" class="navigation-tabs__list">
					<#list navItems as nav_item>
						<#list nav_item.getChildren() as nav_child>
							<#list nav_child.getChildren() as nav_grandchild>
								<#if nav_grandchild.isSelected()>
									<#list nav_child.getChildren() as nav_grandchild>
										<#assign
											nav_grandchild_attr_selected = ""
											nav_grandchild_css_class = ""
										/>
									
										<#if nav_grandchild.isSelected()>
											<#assign
												nav_grandchild_attr_selected = "aria-selected='true'"
												nav_grandchild_css_class = "selected-children"
											/>
											
											<li ${nav_grandchild_attr_selected} class="${nav_grandchild_css_class} navigation-tabs__item hdtb-msel">
												<a href="${nav_grandchild.getURL()}" class="navigation-tabs__link">${nav_grandchild.getName()}</a>
											</li>
											
										<#else>
											<li ${nav_grandchild_attr_selected} class="${nav_grandchild_css_class} navigation-tabs__item">
												<a href="${nav_grandchild.getURL()}" class="navigation-tabs__link">${nav_grandchild.getName()}</a>
											</li>
										
										</#if>
										
									</#list>
								</#if>
							</#list>	
						</#list>	
					</#list>
				</ul>
			</div>
		</div>
	</div>
</nav>
