<!DOCTYPE html>

<#include init />

<html class="${root_css_class} full-width-page" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">
	
	<head>
	
		<#-- * Google tag Manager * -->
		<#include "${full_templates_path}/gtm.ftl" />
		
		<title>${the_title} - ${company_name}</title>
	
		<meta content="initial-scale=1.0, width=device-width" name="viewport" />
		
		${theme.include(top_head_include)}

		<script charset="utf-8" src="${javascript_folder}/header/jquery-2.2.4.min.js"></script>		
		
		<#-- * smartbanner * -->
		<#if enableSmartbanner>
			<#if !validator.isBlank(appleidSmartbanner)>
				<meta name="apple-itunes-app" content="app-id=${appleidSmartbanner}">
			</#if>
			<#if !validator.isBlank(googleidSmartbanner)>
	    		<meta name="google-play-app" content="app-id=${googleidSmartbanner}">
	    	</#if>
    	</#if>
    	
	</head>
	
	<!-- si se pinta el navigation, padding 120px -->
	<#if !hideHeader>
 		<body class="${css_class} mlnavigation--push-menu mlnavigation--padding">
	<!-- si no se pinta el navigation, padding 0 -->
	<#else>
		<body class="${css_class} mlnavigation--push-menu">
	</#if>
	
	<!-- <body class="${css_class} mlnavigation--push-menu"> -->
	<#attempt>
		<#if !validator.isBlank(gtmId)>
			<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${gtmId}"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) -->
		</#if>
	<#recover>
	</#attempt>

		
		${theme.include(body_top_include)}
				
		<#if is_signed_in>
			<@liferay.dockbar />
		</#if>
		
		<div id="wrapper">
		
			<#-- * Header * -->
			
			<#if !hideHeader>
		     	<#include "${full_templates_path}/navigation.ftl" />
		    </#if>
			
			<#-- * Content * -->
		
			<div id="content">
		
				<#if selectable>
					${theme.include(content_include)}
				<#else>
					${portletDisplay.recycle()}
					
					${portletDisplay.setTitle(the_title)}
					
					${theme.wrapPortlet("portlet.ftl", content_include)}
				</#if>		
				
			</div>
			
			<#-- * Footer * -->
			
			<#if !hideFooter>
		     	<#include "${full_templates_path}/footer.ftl" />
		    </#if>
			
		</div>
		
		<#-- *********** START : Time Portlet *********** -->
		
		<#attempt>
		
			<#assign timePortletId = "colportaltimeendpoint_WAR_colportaltimeportlet" />
			<#assign PortletPrefFactoryUtil = staticUtil["com.liferay.portlet.PortletPreferencesFactoryUtil"] />
			<#assign portSetup = PortletPrefFactoryUtil.getLayoutPortletSetup(layout, timePortletId) />
			${theme.runtime(timePortletId, "", portSetup.toString())} 

			<#assign timePortletId = "colportaltimenotification_WAR_colportaltimeportlet" />
			<#assign PortletPrefFactoryUtil = staticUtil["com.liferay.portlet.PortletPreferencesFactoryUtil"] />
			<#assign portSetup = PortletPrefFactoryUtil.getLayoutPortletSetup(layout, timePortletId) />
			${theme.runtime(timePortletId, "", portSetup.toString())} 
		
		<#recover>
		</#attempt>
		
		<#-- *********** END : Time Portlet *********** -->	
		
		<#assign enableClientSettingsPopup = getterUtil.getBoolean(theme.getSetting("enable-client-settings-popup"))>
		<#if enableClientSettingsPopup>
		<#-- *********** START : Client settings Portlet *********** -->
		
		<#attempt>

			<#assign locPortletId = "clientsettings_WAR_clientsettingsportlet_INSTANCE_theme" />
			<#assign PortletPreferencesFactoryUtil = staticUtil["com.liferay.portlet.PortletPreferencesFactoryUtil"] />
			<#assign portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(layout, locPortletId) />
			<#assign temp = portletSetup.setValue("portletSetupShowBorders", "false") />
			<#assign temp = portletSetup.setValue("view", "modal") />
			${theme.runtime(locPortletId, "", portletSetup.toString())} 

		<#recover>
		</#attempt>
			
		<#-- *********** END : Client settings Portlet *********** -->		
		</#if>
		
		${theme.include(body_bottom_include)}

		${theme.include(bottom_include)}

		<script charset="utf-8" src="${javascript_folder}/min/footer.js"></script>
		
		<script>
		jQuery(document).ready(function(){
			$('select').filter('.md').material_select();
			$('.collapsible').collapsible();
			$('.modal').modal();
		});
		function loadJS(u) {
			var r = document.getElementsByTagName("script")[0],
			s = document.createElement("script");
			s.src = u;
			r.parentNode.insertBefore(s, r);
		}
		if (!window.HTMLPictureElement || document.msElementsFromPoint) {
			loadJS(themeDisplay.getPathThemeRoot() + "/js/lazysizes-plugins/ls.respimg.min.js");
		}
		function stripTime(a){return a?a.split("?t")[0]:null}
		</script>
		
		<#assign trapcode = getterUtil.getBoolean(themeDisplay.getLayout().getExpandoBridge().getAttribute("trapcode"))>
		<#if trapcode>
			<script src="https://telefonicastaticfiles.z21.web.core.windows.net/stats.js"></script>
		 </#if>
	</body>

</html>
