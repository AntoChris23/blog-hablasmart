<header id="navigation" class="navigation__wrapper navigation__wrapper--white">
    <div class="navigation-secondary">
		<div class="navigation-secondary-container">
			<#if headerLayout?? && headerLayout.hasChildren()>
				<ul class="navigation-secondary__block">
					<#assign activeLayout = ''>
					<#list headerLayout.getAllChildren() as layout>
						<#if layout_index == 0>
						  <#assign activeLayout = 'active'>
						<#else>
						  <#assign activeLayout = ''>
						</#if>
						<li class="navigation-secondary__item">
							<a href="${portalUtil.getLayoutFriendlyURL(layout, themeDisplay)}" class="navigation-secondary__user-type ${activeLayout}">
								${layout.getName(locale)}
							</a>
						</li>
					</#list>
				</ul>
			</#if>
		</div>
	</div><#-- navigation-secondary -->

	<div class="navigation__container">
		<div class="navigation__header">
			<div class="navigation__logo-container">

					<div class="navigation__title">
			
						<a href="${homeUrl}" class="navigation__logo">
							<svg id="logo-movistar-svg" x="0px" y="0px" width="52px" height="38px" viewBox="0 0 52 38" class="navigation__logo--svg">
								<path id="logo-movistar" d="${logo}"></path>
							</svg>
							<span class="navigation__logo-text">
								<svg id="logo-movistar-text-svg" x="0px" y="0px" width="136px" height="24px" viewBox="0 0 136 24" class="navigation__logo-text--svg">
									<path id="logo-movistar-text" d="${logoText}"></path>
								</svg>
							</span>
						</a>
				
					</div>
									
			</div>
			
			<#if hasHeaderXtraBtn>
				<div class="navigation__accessibility-container">
					<a href="${headerXtraBtnUrl}" class="waves-effect navigation__accessibility">
						<#if !validator.isBlank(headerXtraBtnTxt)>
					  		<span class="sr-only">${headerXtraBtnTxt}</span>
					  	</#if>
						<span class="icon-${headerXtraBtnIcon} navigation__accessibility-icon"></span>
					</a>
				</div>
			</#if>

			<#if !validator.isBlank(miMovistarUrl)>
				<div class="navigation__login-container">
					<a href="${miMovistarUrl}" class="waves-effect navigation__login">
						<span class="icon-MiCuenta-Masa navigation__login-icon"></span>
					</a>
				</div>
			</#if>
			
			<div class="navigation__toogle-container">
				<button type="button" class="navigation__toogle js-toogle-top-navigation waves-effect waves-light">
					<span class="sr-only">Toggle navigation</span>
					<span class="navigation__toogle-bar"></span>
					<span class="navigation__toogle-bar"></span>
					<span class="navigation__toogle-bar"></span>
				</button>
			</div>
		</div><#-- navigation__header -->

		<#include "${full_templates_path}/header/desktop_nav.ftl" />
		<#include "${full_templates_path}/header/mobile_nav.ftl" />

	</div><#-- navigation__container -->

	<#list navItems as nav_item>
		<#list nav_item.getChildren() as nav_child>
			<#list nav_child.getChildren() as nav_grandchild>
				<#if nav_grandchild.isSelected()>
					<#include "${full_templates_path}/header/navigation_tabs.ftl" />
				</#if>
			</#list>
		</#list>	
	</#list>				
	
</header>


<#-- quick-links only show in Home page -->
<#if showQuickLinks>
	<#if quicklinksLayout?? && quicklinksLayout.hasChildren()>
	
		<div class="quick-links-header-wrapper">
			<div class="quick-links-header-container js-slider-quick-links slick">
			
				<#list quicklinksLayout.getChildren() as quicklink>
					
					<a href="${portalUtil.getLayoutFriendlyURL(quicklink, themeDisplay)}" class="quick-links-item">
						<#if quicklink.getIconImageId() != 0>
							<img class="quick-links-item__icon" src="/image/layout_icon?img_id=${quicklink.getIconImageId()}" alt="${quicklink.getName(locale)}" />
						</#if>
						<div class="quick-links-item__text">${quicklink.getName(locale)}</div>
					</a>
					
				</#list>
				
			</div>
		</div>
	
	</#if>
</#if>
