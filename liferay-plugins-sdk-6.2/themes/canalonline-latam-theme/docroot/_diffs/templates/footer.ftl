
<#if hasEmbeddedWcFooter>
	${embeddedWcFooterContent}
</#if>

<footer>
		
		
		<#if !footerMinified>
		
		
			<div class="footer-large-wrapper">
				
				<div class="footer-large-wrapper-logo">
					<div class="container">
						<div class="row">
							<div class="col s12">
								<div class="footer-large__container">
									<div class="footer-large__logo"><img src="${themeDisplay.getPathThemeImages()}/logo-footer.png" alt="Logo-Telefonica" class="footer-large__logo-img"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="footer-large-wrapper-links">
					<div class="container-full">
						<div class="row">
							<div class="col s12">
								<#if footerLayout?? && footerLayout.hasChildren()>
								
									<ul class="footer-large-links js-mobile-only-collapsible">
									
										<#list footerLayout.getChildren() as layout>
										
											<li class="footer-large-links-block">
												<div class="collapsible-header">
													
													<div class="footer-large__title">
														<span>${layout.getName(locale)}</span>
														<span class="icon-keyboard_arrow_down footer-large__icon"></span>
													</div>
													
												</div>
												
												<ul class="collapsible-body">
												
													<#list layout.getChildren() as childLayout>
												
														<li>
															<a href="${portalUtil.getLayoutFriendlyURL(childLayout, themeDisplay)}" class="footer-large__url">
																<span class="footer-large__url-title">${childLayout.getName(locale)}</span>
															</a>
														</li>
														
													</#list>
													
												</ul>
											</li>
											
										</#list>
										
									</ul>
									
								</#if>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		
		
		<#else>
		
		
			<div class="footer-wrapper">
				<div class="container">
					<div class="row">
					
						<div class="col s12 l4">
							<div class="footer__logo"><img src="${themeDisplay.getPathThemeImages()}/logo-footer.png" alt="Logo-Telefonica" class="footer__logo-img"></div>
						</div>
						
						<div class="col s12 l8">
							<#if footerLayout?? && footerLayout.hasChildren()>
								<ul class="footer__links">
									<#list footerLayout.getChildren() as layout>
										<li class="footer__links-block">
											<a href="${portalUtil.getLayoutFriendlyURL(layout, themeDisplay)}" class="footer__url">
												<div class="footer__url-title">${layout.getName(locale)}</div>
											</a>
										</li>
									</#list>
								</ul>
							</#if>
						</div>
						
					</div>
				</div>
			</div>
		
		
		</#if>
		
	
</footer>
