<#-- Include creado para facilitar el GTM en cada pais -->

<a href="${miMovistarUrl}" class="mi-movistar-login__button waves-effect waves-light" 
		data-gtm-type="commonClick" data-gtm-category="Acceso Mi Movistar" data-gtm-action="click" 
		data-gtm-label="${themeDisplay.getLayout().getName(themeDisplay.getLocale())}">
	Mi Movistar
</a>