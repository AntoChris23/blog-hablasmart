<#-- * Service definitions * -->
<#assign LayoutLocalService = serviceLocator.findService("com.liferay.portal.service.LayoutLocalService")  />
<#-- * URL Logo Home * -->
<#assign homeUrlLogo = theme.getSetting("home-url-logo")>
<#-- * Root Layouts * -->
<#assign footerLayoutFurl = theme.getSetting("footer-layout-furl")>
<#attempt>
	<#assign footerLayout = LayoutLocalService.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, footerLayoutFurl)>
<#recover>
</#attempt>
<#-- * URLs Redes Sociales * -->
<#assign socialFacebookUrl = theme.getSetting("social-facebook-url")>
<#assign socialTwitterUrl = theme.getSetting("social-twitter-url")>
<#assign socialInstagramUrl = theme.getSetting("social-instagram-url")>
<#assign socialYoutubeUrl = theme.getSetting("social-youtube-url")>
<#-- * Facebook metatags * -->
<#assign showFacebookMetatags = getterUtil.getBoolean(theme.getSetting("show-facebook-metatags"))>
<#attempt>  
	<#assign socialTitle = themeDisplay.getLayout().getExpandoBridge().getAttribute("social-title")>
	<#assign socialDescription = themeDisplay.getLayout().getExpandoBridge().getAttribute("social-description")>
<#recover>
	<#assign socialTitle = ''>
	<#assign socialDescription = ''>
</#attempt>
<#assign facebookTypeMetatag = theme.getSetting("facebook-type-metatag")>
<#assign facebookImageMetatag = theme.getSetting("facebook-image-metatag")>
<#-- * TWITTER metatags * -->
<#assign showTwitterMetatags = getterUtil.getBoolean(theme.getSetting("show-twitter-metatags"))>
<#assign twitterImageMetatag = theme.getSetting("twitter-image-metatag")>
<#-- * Search Portlet * -->
<#macro vass_search>
	<div class="navbar__search">
        <div class="search--content">
            <form class="search--form" method="POST">
                <div class="search--group">
                    <input class="search__input" type="text" value="" id="inputSearch" size="30" placeholder="¿Qué estas buscando?" title="¿Qué estas buscando?" maxlength="2000" onkeypress="return doSearch(event);">
                    <button class="search__button" type="button" aria-label="Buscar" onclick="initSearch();">
                        <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg" role="img">
                        <title>Buscar</title>
                        <path d="M11.3054 20.9213C16.7056 20.9213 21.0834 16.5435 21.0834 11.1433C21.0834 5.743 16.7056 1.36523 11.3054 1.36523C5.90511 1.36523 1.52734 5.743 1.52734 11.1433C1.52734 16.5435 5.90511 20.9213 11.3054 20.9213Z" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M23.5277 23.3656L18.2109 18.0488" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                </div>
            </form>
            <div class="button button--navbar button--search" id="btnSearch" aria-label="Buscar" onclick="dataLayer.push({'event': 'hv.event','eventName': 'header_buscar_click','eventCategory': 'header - buscar','eventAction': 'click','eventLabel': 'boton buscar'});">
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg" role="img">
                <title>Buscar</title>
                <path d="M11.3054 20.9213C16.7056 20.9213 21.0834 16.5435 21.0834 11.1433C21.0834 5.743 16.7056 1.36523 11.3054 1.36523C5.90511 1.36523 1.52734 5.743 1.52734 11.1433C1.52734 16.5435 5.90511 20.9213 11.3054 20.9213Z" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M23.5277 23.3656L18.2109 18.0488" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <div class="button button--navbar button--arrow button--return" id="btnSearchReturn" aria-label="Cerrar buscador" onclick="dataLayer.push({'event': 'hv.event','eventName': 'header_buscar_click','eventCategory': 'header - buscar','eventAction': 'click','eventLabel': 'cerrar'});">
                <div class="button__inner">&nbsp;</div>
            </div>
        </div>
        <#attempt>
            <#assign searchPortletId = "customsearch_WAR_movistarblogssearchportlet" />
            <#assign PortletPrefFactoryUtil = staticUtil["com.liferay.portlet.PortletPreferencesFactoryUtil"] />
            <#assign portSetup = PortletPrefFactoryUtil.getLayoutPortletSetup(layout, searchPortletId) />
            ${theme.runtime(searchPortletId, "", portSetup.toString())}
            <#recover>
        </#attempt>
    </div>
</#macro>