<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<#include "${full_templates_path}/gtm.ftl" />
	<title>${the_title} | Habla Smart</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />
	<link rel="alternate" hreflang="es-pe" href="https://www.movistar.com.pe/" />    
	<link rel="alternate" hreflang="es-cl" href="https://www.movistar.cl/" />
	<link rel="alternate" hreflang="es-ar" href="https://www.movistar.com.ar/" />
	<link rel="alternate" hreflang="es-co" href="https://www.movistar.co/" />
	<link rel="alternate" hreflang="es-cr" href="https://movistar.cr/" />
	<link rel="alternate" hreflang="es-ec" href="https://www.movistar.com.ec/" />
	<link rel="alternate" hreflang="es-es" href="https://www.movistar.es/" />
	<link rel="alternate" hreflang="es-gt" href="https://www.movistar.com.gt/" />
	<link rel="alternate" hreflang="es-mx" href="https://www.movistar.com.mx/" />
	<link rel="alternate" hreflang="es-ni" href="https://www.movistar.com.ni/" />
	<link rel="alternate" hreflang="es-pa" href="https://www.movistar.com.pa/" />
	<link rel="alternate" hreflang="es-uy" href="https://www.movistar.com.uy/" />
	<link rel="alternate" hreflang="es-ve" href="http://www.movistar.com.ve/" />
	<link rel="alternate" hreflang="es-sv" href="https://www.movistar.com.sv/" />
	<link rel="alternate" hreflang="x-default" href="https://www.movistar.com" />
	<#if showFacebookMetatags>
		<meta property="og:site_name" content="Habla Smart | Movistar">
		<#if validator.isBlank(socialTitle)>
			<meta property="og:title" content="${the_title} | Habla Smart" />
		<#else>
			<meta property="og:title" content="${socialTitle}" />
		</#if>
		<#if validator.isBlank(socialDescription)>
			<meta property="og:description" content="${themeDisplay.getLayout().getDescription(locale)}" />
		<#else>
			<meta property="og:description" content="${socialDescription}" />
		</#if>
		<meta property="og:type" content="${facebookTypeMetatag}" />
		<meta property="og:url" content="${themeDisplay.getURLPortal()}${themeDisplay.getURLCurrent()}" />
		<#if validator.isBlank(facebookImageMetatag)>
			<meta property="og:image" content="${images_folder}/movistar/open-graph.jpg" />
		<#else>
			<meta property="og:image" content="${facebookImageMetatag}" />
		</#if>
	</#if>
	<#if showTwitterMetatags>
		<meta name="twitter:card" content="summary_large_image">
		<#if validator.isBlank(socialTitle)>
			<meta name="twitter:title" content="${the_title} | Habla Smart" />
		<#else>
			<meta name="twitter:title" content="${socialTitle}" />
		</#if>
		<#if validator.isBlank(socialDescription)>
			<meta name="twiter:description" content="${themeDisplay.getLayout().getDescription(locale)}" />
		<#else>
			<meta name="twiter:description" content="${socialDescription}" />
		</#if>
		<#if validator.isBlank(twitterImageMetatag)>
			<meta name="twitter:image" content="${images_folder}/movistar/open-graph.jpg" />
		<#else>
			<meta name="twitter:image" content="${twitterImageMetatag}" />
		</#if>
    </#if>
	<#attempt>
		<#assign canonicalUrl = themeDisplay.getLayout().getExpandoBridge().getAttribute("canonical-url")>
	<#recover>
		<#assign canonicalUrl = ''>
	</#attempt>
	<#if !validator.isBlank(canonicalUrl)>
		${canonicalUrl}
	</#if>
	${theme.include(top_head_include)}
	<script charset="utf-8" src="${javascript_folder}/movistar/jquery-3.4.1.min.js"></script>
</head>

<body class="body ${css_class}">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9Q7JFD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<a href="#main-content" id="skip-to-content"><@liferay.language key="skip-to-content" /></a>

${theme.include(body_top_include)}

<#if is_signed_in>
	<@liferay.dockbar />
</#if>

<header class="nat-header nat-header--nav" id="natHeaderNavigation">
    <div class="container">
		<#if has_navigation>
			<#include "${full_templates_path}/navigation.ftl" />
		</#if>
	</div>
</header>
<main role="main">
    <div id="content">
        <#if selectable>
            ${theme.include(content_include)}
        <#else>
            ${portletDisplay.recycle()}
    
            ${portletDisplay.setTitle(the_title)}
    
            ${theme.wrapPortlet("portlet.ftl", content_include)}
        </#if>
        <div class="mat-life">
			<div class="modal nat-modal-video nat-modal-video--search" id="modalSearchVideo">
			    <div class="container">
			        <div class="modal-header">
			            <a class="modal-action modal-close" href="#!">
			                <span class="icon icon-nat_close">
		                        <svg class="svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
		                            <title>Cerrar</title>
		                            <path d="M1 1l16 16M17 1L1 17" stroke="currentColor" stroke-width="2" stroke-linecap="round"/>
		                        </svg>
		                    </span>
			            </a>
			        </div>
			        <div class="modal-content">
			        	<div class="embed--responsive embed--responsive-16by9">
			            	<iframe class="embed--responsive__item" width="560" height="315" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
</main>

<#include "${full_templates_path}/footer.ftl" />

${theme.include(body_bottom_include)}

${theme.include(bottom_include)}

<script charset="utf-8" src="${javascript_folder}/movistar.min.js"></script>

</body>

</html>