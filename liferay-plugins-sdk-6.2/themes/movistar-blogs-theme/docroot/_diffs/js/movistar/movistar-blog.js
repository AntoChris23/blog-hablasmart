/* Header navigation */
let bodyOverlay = document.createElement('div');
bodyOverlay.classList.add('nat-header--overlay');
document.querySelector('body').appendChild(bodyOverlay);

let htmlBody = document.querySelector("body");
let navBtn = document.querySelector("#btnNavToggle");
let navHeader = document.querySelector("#natHeaderNavigation");
let navBtnSearch = document.querySelector("#btnSearch");
let navSearchReturn = document.querySelector("#btnSearchReturn");

function overlayClose() {
    htmlBody.classList.remove("nat-navbar--active");
}

function overlayClick(e) {
    if (!e.target.closest(".nat-header--nav")) {
        overlayClose();
    }
}

function overlayKeyDown(e) {
    if (e.keyCode === 27 && htmlBody.classList.contains("nat-navbar--active")) {
        overlayClose();
    }
}

function navbarToggle() {
    if (htmlBody.classList.contains('nat-navbar--active')) {
        htmlBody.classList.remove('nat-navbar--active');
    } else {
        htmlBody.classList.add('nat-navbar--active');
    }
}

function navSearch() {
    navHeader.classList.add('nav--search-open');
}

function navSearchClose() {
    navHeader.classList.remove('nav--search-open');
}

navBtn.addEventListener("click", navbarToggle);
navBtnSearch.addEventListener("click", navSearch);
navSearchReturn.addEventListener("click", navSearchClose);

document.addEventListener('DOMContentLoaded', function() {
    var elemCollapsible = document.querySelectorAll('.collapsible');
    var optionCollapsible = {};
    var instanceCollapsible = M.Collapsible.init(elemCollapsible, optionCollapsible);

    var elemModal = document.querySelectorAll('.modal');
    var optionModal = {};
    var instanceModal = M.Modal.init(elemModal, optionModal);

    var eMostCollapse1 = document.querySelectorAll('.nat-the-most-read .nat-the-most-read__accordeon.collapsible');
    if (eMostCollapse1.length > 0){
        var elemMostRead1 = document.querySelector('.nat-the-most-read .nat-the-most-read__accordeon.collapsible');
        var instanceMostRead1 = M.Collapsible.init(elemMostRead1, {
            accordion: true
        });
        instanceMostRead1.open();
    }

    var elemNatSlider1 = document.querySelectorAll('.nat-featured-notes--slider .swiper-container .nat-featured-notes__item');
    if(elemNatSlider1.length > 1){
        var swiper1 = new Swiper('.nat-featured-notes--slider .swiper-container', {
            slidesPerView: 'auto',
            loop: false,
            centeredSlides: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                dynamicBullets: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                992: {
                    slidesPerView: 3,
                }
            }
        });
    }
    
    var elemNatSlider2 = document.querySelectorAll('.nat-planes--section .swiper-container .planes--item');
    if(elemNatSlider2.length > 0){
        var swiper2 = new Swiper('.nat-planes--section .swiper-container', {
            slidesPerView: 'auto',
            loop: false,
            centeredSlides: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                dynamicBullets: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                },
                992: {
                    slidesPerView: 3,
                },
                1199: {
                    slidesPerView: 4,
                }
            }
        });
    }

    var elemNatSlider3 = document.querySelectorAll('.nat-note-gallery .swiper-container .gallery__item');
    if(elemNatSlider3.length > 0){
        var swiper3 = new Swiper('.nat-note-gallery .swiper-container', {
            slidesPerView: 'auto',
            centeredSlides: true,
            spaceBetween: 20,
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                768: {
                    spaceBetween: 45,
                },
                992: {
                    spaceBetween: 95,
                }
            }
        });
    }

    var elemNatEquipment1 = document.querySelectorAll('.nat-equipment-carousel .swiper-container .equipment__item');
    if(elemNatEquipment1.length > 0){
        var swiper4 = new Swiper('.nat-equipment-carousel .equipment__gallery', {
            effect: 'coverflow',
            slidesPerView: 'auto',
            centeredSlides: true,
            loop: true,
            loopedSlides: 3,
            spaceBetween: 5,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            coverflowEffect: {
                rotate: -40,
                stretch: 100,
                depth: 160,
                modifier: 1,
                slideShadows : false,
            },
            breakpoints: {
                768: {
                    spaceBetween: 10,
                    coverflowEffect: {
                        rotate: -35,
                        stretch: 0,
                        depth: 160,
                        modifier: 1,
                        slideShadows : false,
                    }
                }
            }
        });
    }

    var elemNatEquipment2 = document.querySelectorAll('.nat-equipment-carousel .swiper-container .device__item');
    if(elemNatEquipment2.length > 0){
        var swiper5 = new Swiper('.nat-equipment-carousel .equipment__gallery-2', {
            slidesPerView: 'auto',
            centeredSlides: true,
            spaceBetween: 20,
            loop: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                dynamicBullets: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                992: {
                    spaceBetween: 95,
                },
                768: {
                    spaceBetween: 45,
                }
            }
        });
    }
});

$(document).ready(function(){
	// Ver mas / menos
    if ($('.accordeon__notes.collapsible-body').length) {
        function accordeoNote() {
            if ($('.accordeon__notes .box.box--note').length > 3) {
                $('.accordeon__notes .box.box--note:gt(2)').hide();
                $('.accordeon__notes .button--more').show();
            }
        }
        
        accordeoNote();
        $('.accordeon__notes .button--more').on('click', function() {
            $('.accordeon__notes .box.box--note:gt(2)').slideToggle(200);
            $(this).text() === 'Ver más' ? $(this).text('Ver menos') : $(this).text('Ver más');
        });

        $(window).resize(function() {
            var $theWindowSize = $(this).width();
            if($theWindowSize < 992) {
                accordeoNote();
                if ($('.accordeon__notes .button--more').text() === 'Ver menos') {
                    $('.accordeon__notes .button--more').text('Ver más');
                }
            }
        })
    }
    
    // Modal Video
    if ($('.nat-modal-video').length) {
	    $('.nat-modal-video').modal({
	        onOpenStart: function(modal, trigger) {
	        	var $triggerVideo = $(trigger);
	        	var $modalVideo = $(modal);
	            var urlVideo = $triggerVideo.data('videourl');
	            var classVideo = $triggerVideo.data('videoclass');
	            if(classVideo != undefined){
	            	$modalVideo.addClass(classVideo);
	            }else{
	            	$modalVideo.addClass('nat-modal-video--search');
	            }
	            var htmlContent;
	            $modalVideo.find('div.modal-content').empty();
	            if(urlVideo.indexOf('www.youtube.com') >= 0){
	            	htmlContent = '<div class="embed--responsive embed--responsive-16by9"><iframe class="embed--responsive__item" width="560" height="315" src="' + urlVideo
	            		+ '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
	            	$modalVideo.find('div.modal-content').html(htmlContent);
	            }else{
	            	htmlContent = '<video class="video-gallery--item video--fluid" controls><source src="' + urlVideo + '" type="video/mp4"></video>';
	            	$modalVideo.find('div.modal-content').html(htmlContent);
	            	// Play al abrir modal
	                $(".nat-modal-video .video--fluid").each(function(){
	                    this.play();
	                });
	            }
	        },
	        onCloseStart: function(modal, trigger){
	        	var $modalVideo = $(modal);
	        	
	        	$modalVideo.find('iframe').removeAttr('src');
                // Pause al cerrar modal
                $(".nat-modal-video .video--fluid").each(function(){
                    this.pause();
                });
                $modalVideo.removeClass('nat-modal-video--search nat-modal-video--gallery');
	        }
	    });
    }

    // Planes
    $(".plan--box .box--footer .footer__button").click(function(e) {
        e.preventDefault();
        if ($(".plan--box").hasClass("active")) {
            $(".plan--box").removeClass("active");
            $(".plan--box .box--body .body__detalles").slideUp();
            $(".plan--box .box--footer .footer__button .button__texto").text("Ver detalle");
        } else {
            $(".plan--box").addClass("active");
            $(".plan--box .box--body .body__detalles").slideDown();
            $(".plan--box .box--footer .footer__button .button__texto").text("Ocultar detalle");
        }
    });
})