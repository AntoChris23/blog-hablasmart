<footer class="footer footer--nav" id="footer" role="contentinfo">
    <div class="footer__brand">
        <div class="container">
            <div class="footer__logo">
                <img src="${themeDisplay.getPathThemeImages()}/logo-footer-telefonica.png" alt="Logo-Telefonica" class="footer__logo-img">
            </div>
        </div>
    </div>
    <div class="footer__nav">
        <#if footerLayout?? && footerLayout.hasChildren()>
        <div class="container">
            <ul class="collapsible" id="footerCollapse" data-collapsible="accordion">
                <#list footerLayout.getChildren() as layout>
                <li class="collapsible--item">
                    <div class="collapsible-header">
                        <p class="list--title">${layout.getName(locale)}</p><span class="nat-icomoon icon-keyboard_arrow_down"></span>
                    </div>
                    <#if layout.hasChildren()>
                    <div class="collapsible-body">
                        <ul class="nav">
                            <#list layout.getChildren() as childLayout>
                            <li class="nav__item"><a class="nav__link" href="${portalUtil.getLayoutFriendlyURL(childLayout, themeDisplay)}">${childLayout.getName(locale)}</a></li>
                            </#list>
                        </ul>
                    </div>
                    </#if>
                </li>
                </#list>
            </ul>
        </div>
        </#if>
    </div>
    <div class="footer__claims">
        <div class="container">
            <#assign footerConsultEnable = getterUtil.getBoolean(theme.getSetting("footer-consult-links-enable"))>
			<#if footerConsultEnable>
            <ul class="nav nav--claims">
                <#assign footerConsultJsonString = theme.getSetting("footer-consult-links-json") >
				<#attempt>
					<#assign footerConsultMap = jsonFactoryUtil.looseDeserializeSafe(footerConsultJsonString)>
				<#recover>
					<#assign footerConsultMap = {'principal': {'title': 'Libro de Reclamaciones', 'url': 'https://www.movistar.com.pe/libro-de-reclamaciones', 'alt': 'Libro de Reclamaciones'}, 'consult': [{'title': 'Reclamos', 'url': 'https://www.movistar.com.pe/atencion-al-cliente/reclamos/registro-averias'}, {'title': 'Consulta de reclamos', 'url': 'https://www.movistar.com.pe/atencion-al-cliente/reclamos/reporte-virtual'}, {'title': 'InformaciÃ³n abonados y usuarios', 'url': 'https://www.movistar.com.pe/informacion-a-abonados-y-usuarios'}]}>
				</#attempt>
                <#if footerConsultMap.principal??>
                    <#attempt>
                    <li class="nav__item">
                        <a class="nav__link" href="${footerConsultMap.principal.url}">
                            <svg  class="svg--libro-reclamacion svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 27">
                            <title>${footerConsultMap.principal.title}</title>
                            <path fill="currentColor" fill-rule="evenodd" clip-rule="evenodd" d="M22.508 19.354l-.84 2.64a.264.264 0 00.033.227.24.24 0 00.084.076.226.226 0 00.183.015l2.506-.874a.235.235 0 00.082-.05l.083-.078c.65-.608 4.668-4.363 4.935-4.641a1.48 1.48 0 00.427-1.036c0-.386-.139-.757-.388-1.04a1.417 1.417 0 00-.954-.483c-.283-.022-.701.038-1.086.444-.218.23-3.364 3.17-4.997 4.691a.252.252 0 00-.068.109zm.74.438l-.41 1.29 1.257-.438a2227.696 2227.696 0 004.046-3.793 177.126 177.126 0 00.824-.785l.017-.017.005-.005a.58.58 0 00.18-.41.67.67 0 00-.165-.443.599.599 0 00-.404-.204.48.48 0 00-.191.022.593.593 0 00-.243.166c-.239.252-3.272 3.085-4.915 4.617zm.963.811z"/>
                            <path fill="currentColor" fill-rule="evenodd" clip-rule="evenodd" d="M6.492.844l.002.001 5.483 3.838.937.941h-.02v19.953l-6.902-4.786-.004-.003c-.85-.564-1.261-.958-1.493-1.48-.24-.543-.315-1.284-.315-2.63V5.648h-.834v11.028c0 1.334.064 2.262.393 3.004.339.762.927 1.268 1.8 1.849l7.355 5.1v.134h-8.72c-1.014 0-2.063-.162-2.864-.78C.482 25.343 0 24.282 0 22.686V9.68c0-1.904.685-2.965 1.56-3.517.616-.389 1.285-.497 1.757-.527V2.52c-.055-.881.24-1.663.887-2.03C4.853.12 5.68.267 6.492.844zm7.236 25.92h9.074c.383 0 1.346 0 2.22-.506.45-.26.878-.654 1.19-1.242.31-.585.493-1.339.493-2.3V18.26l-2.429 2.384-1.46.503.552-1.62 3.337-3.067V9.737c0-1.593-.48-2.651-1.307-3.29-.8-.617-1.848-.779-2.86-.779h-8.81v21.096zm2.128-16.261c0-.243.187-.44.418-.44h6.655c.23 0 .417.197.417.44 0 .242-.187.44-.417.44h-6.655a.429.429 0 01-.418-.44zm.418 4.878a.429.429 0 00-.418.44c0 .242.187.439.418.439h6.655c.23 0 .417-.197.417-.44a.429.429 0 00-.417-.439h-6.655zm-.46 5.757c0-.243.187-.44.418-.44h4.193c.23 0 .417.197.417.44s-.186.44-.417.44h-4.193a.429.429 0 01-.417-.44zM6.86 5.737a.405.405 0 00-.576.135.454.454 0 00.128.607l4.632 3.098c.194.13.452.07.576-.135a.454.454 0 00-.129-.607L6.86 5.737zm-.656 10.237a.405.405 0 01.578-.125l3.61 2.505a.454.454 0 01.118.609.405.405 0 01-.578.125l-3.609-2.505a.454.454 0 01-.119-.609zm.656-5.447a.405.405 0 00-.576.136.454.454 0 00.128.606l4.632 3.099c.194.13.452.07.576-.136a.454.454 0 00-.129-.606L6.86 10.527z"/>
                            </svg>
                            ${footerConsultMap.principal.title}
                        </a>
                    </li>
                    <#recover>
                    <li><div class="alert alert-error">Error</div></li>
                    </#attempt>
                </#if>
                <#if footerConsultMap.consult??>
                    <#attempt>
                        <#list footerConsultMap.consult as item>
                        <li class="nav__item"><a class="nav__link" href="${item.url}">${item.title}</a></li>
                        </#list>
                        <#recover>
                        <li><div class="alert alert-error">Error</div></li>
                    </#attempt>
                </#if>
            </ul>
            </#if>
            <ul class="nav nav--rrss">
                <#if !validator.isBlank(socialFacebookUrl)>
                <li class="nav__item">
                    <a class="nav__link" href="${socialFacebookUrl}" target="_blank" rel="noopener noreferrer" aria-label="Facebook">
                        <svg class="link__icon svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                        <title>Facebook</title>
                        <path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z" class=""></path>
                        </svg>
                    </a>
                </li>
                </#if>
                <#if !validator.isBlank(socialTwitterUrl)>
                <li class="nav__item">
                    <a class="nav__link" href="${socialTwitterUrl}" target="_blank" rel="noopener noreferrer" aria-label="Twitter">
                        <svg class="link__icon svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <title>Twitter</title>
                        <path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" class=""></path>
                        </svg>
                    </a>
                </li>
                </#if>
                <#if !validator.isBlank(socialInstagramUrl)>
                <li class="nav__item">
                    <a class="nav__link" href="${socialInstagramUrl}" target="_blank" rel="noopener noreferrer" aria-label="Instagram">
                        <svg class="link__icon svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <title>Instagram</title>
                        <path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z" class=""></path>
                        </svg>
                    </a>
                </li>
                </#if>
                <#if !validator.isBlank(socialYoutubeUrl)>
                <li class="nav__item">
                    <a class="nav__link" href="${socialYoutubeUrl}" target="_blank" rel="noopener noreferrer" aria-label="YouTube">
                        <svg class="link__icon svg--inline" aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <title>YouTube</title>
                        <path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z" class=""></path>
                        </svg>
                    </a>
                </li>
                </#if>
            </ul>
        </div>
    </div>
</footer>