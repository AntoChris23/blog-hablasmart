document.addEventListener('DOMContentLoaded', function() {
    //collapse materialize
    var elems = document.querySelectorAll('.collapsible');
    var collapsible = M.Collapsible.init(elems);

    //Hamburguer navMenu
    // selector
    var menu = document.querySelector('.hamburger');

    // evento
    menu.addEventListener('click', toggleMenu, false);

    //Banners
    var eNatPlant1 = document.querySelectorAll('.natBanner .swiper-slide');
    if(eNatPlant1.length > 1){
        var swiper = new Swiper('.natBanner', {
            keyboardControl: true,
            loop: true,
            speed: 2000,
            autoplay: {
                delay: 6000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                dynamicBullets: true,
            },
        });
    }
});

$(document).ready(function(){
    // Modal Video
    if ($('.nat-modal-video').length) {
	    $('.nat-modal-video').modal({
	        onOpenStart: function(modal, trigger) {
	        	var $triggerVideo = $(trigger);
	        	var $modalVideo = $(modal);
	            var urlVideo = $triggerVideo.data('videourl');
	            var classVideo = $triggerVideo.data('videoclass');
	            if(classVideo != undefined){
	            	$modalVideo.addClass(classVideo);
	            }else{
	            	$modalVideo.addClass('nat-modal-video--search');
	            }
	            var htmlContent;
	            $modalVideo.find('div.modal-content').empty();
	            if(urlVideo.indexOf('www.youtube.com') >= 0){
	            	htmlContent = '<div class="embed--responsive embed--responsive-16by9"><iframe class="embed--responsive__item" width="560" height="315" src="' + urlVideo
	            		+ '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
	            	$modalVideo.find('div.modal-content').html(htmlContent);
	            }else{
	            	htmlContent = '<video class="video-gallery--item video--fluid" controls><source src="' + urlVideo + '" type="video/mp4"></video>';
	            	$modalVideo.find('div.modal-content').html(htmlContent);
	            	// Play al abrir modal
	                $(".nat-modal-video .video--fluid").each(function(){
	                    this.play();
	                });
	            }
	        },
	        onCloseStart: function(modal, trigger){
	        	var $modalVideo = $(modal);
	        	
	        	$modalVideo.find('iframe').removeAttr('src');
                // Pause al cerrar modal
                $(".nat-modal-video .video--fluid").each(function(){
                    this.pause();
                });
                $modalVideo.removeClass('nat-modal-video--search nat-modal-video--gallery');
	        }
	    });
    }

    var eNatPlant1 = document.querySelectorAll('.mobile-first .swiper-slide');
    //Swiper Slider first Mobile
    var breakpoint = window.matchMedia('(min-width:992px)');
    // keep track of swiper instances to destroy later
    var mySwiper = void 0;
    if(eNatPlant1.length > 1){
        var breakpointChecker = function breakpointChecker() {
            // if larger viewport and multi-row layout needed
            if (breakpoint.matches === true) {
                // clean up old instances and inline styles when available
                if (mySwiper !== undefined){
                    // mySwiper.destroy(true, true);
                    $(".mobile-first").each(function(){
                        this.swiper.destroy(true, true);
                    });
                } 
                // or/and do nothing
                return;
            // else if a small viewport and single column layout needed
            } else if (breakpoint.matches === false) {
                // fire small viewport version of swiper
                return enableSwiper();
            }
        };

        var enableSwiper = function enableSwiper() {
            mySwiper = new Swiper('.mobile-first', {
                loop: true,
                slidesPerView: 'auto',
                centeredSlides: true,
                a11y: true,
                keyboardControl: true,
                grabCursor: true,
                // observer: true,
                // pagination
                pagination: {
                    el: '.swiper-pagination',
                    dynamicBullets: true,
                },
            });
        };
        // keep an eye on viewport size changes
        breakpoint.addListener(breakpointChecker);
        // kickstart
        breakpointChecker();
    }

    if($('.tabs').length) {
        var el = $('.tabs')
        var options = {
            onShow: function () {
                if ($('.mobile-first').length > 0) {
                    var breakpoint = window.matchMedia('(min-width:992px)');

                    if (breakpoint.matches === false){
                        if (mySwiper !== undefined){
                            $(".mobile-first").each(function(){
                                this.swiper.update();
                            });
                        }
                    }
                }
            }
        }
        var tabs = M.Tabs.init(el, options);
    }
});

//resize function
var phaterSection = $('.see-more').parent().parent().parent();
$(window).on('resize', function(){
    if($('.dropdown-content').length > 0){ 
        $('.nav-dropdown-trigger').on('click', function(e){
            e.preventDefault();
        })     
        //init dropdown menu
        $('.nav-dropdown-trigger').dropdown();
        //Resize
        if($(window).outerWidth() < 992){
            //navMenu      
            $('.nav-dropdown-trigger').dropdown('destroy', true);        
            $('#nav-mobile').addClass('collapsible');
            $('#nav-mobile .item .link').addClass('collapsible-header');
            $('#nav-mobile .item ul').addClass('collapsible-body');        
            $('#nav-mobile.collapsible').collapsible();      
        }else{
            $('#nav-mobile').removeClass('collapsible');
            $('#nav-mobile .item .link').removeClass('collapsible-header');
            $('#nav-mobile .item ul').removeClass('collapsible-body');
        }
    }
});
$(window).resize();

// Hamburguer
function toggleMenu (event) {
    this.classList.toggle('is-active');
    $( "#nav-mobile" ).slideToggle();
    event.preventDefault();
}