!function($) {
	'use strict';
	var carouselPlansPe = function () {
		this.ele = '.plans .plans__list';
	};

	carouselPlansPe.prototype.init = function (){
		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

	  	this.$slicks.each(this._multiInit(this));
	  	
	  	$('.plans .plan__footer .btn-flat').bind('click', function(e) {
	        e.preventDefault();
	        $('.plans .plan__item').toggleClass('item--open');
	        $('.plans .benefit__list--compress').slideToggle();
	        $('.plans .benefit__list--complete').slideToggle();
	    });
	};

	carouselPlansPe.prototype._multiInit = function (self) {
		return function(index, element){
			var $this = $(element);
			$this.slick({
		        slidesToShow: 4,
		        slidesToScroll: 1,
		        responsive: [{
		                breakpoint: 1350,
		                settings: {
		                    slidesToShow: 3,
		                    slidesToScroll: 1,
		                    dots: false,
		                }
		            },
		            {
		                breakpoint: 1199,
		                settings: {
		                    slidesToShow: 3,
		                    slidesToScroll: 1,
		                    dots: true,
		                    arrows: false
		                }
		            },
		            {
		                breakpoint: 1024,
		                settings: {
		                    slidesToShow: 2,
		                    slidesToScroll: 1,
		                    dots: true,
		                    arrows: false
		                }
		            },
		            {
		                breakpoint: 650,
		                settings: {
		                    slidesToShow: 1,
		                    slidesToScroll: 1,
		                    dots: true,
		                    arrows: false
		                }
		            }
		        ]
		    });
	    };
	};

	$.carouselPlansPe = new carouselPlansPe();
	$.carouselPlansPe.Constructor = carouselPlansPe;
}(window.jQuery);// jshint ignore:line

jQuery(document).ready(function(){
	'use strict';
	$.carouselPlansPe.init();
});