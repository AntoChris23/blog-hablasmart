!function($) {
	'use strict';
	var carouselHeroPe = function () {
		this.ele = '.banner-hero .slider';
	};

	carouselHeroPe.prototype.init = function (){
		this.$slicks = $(this.ele);
		if(!this.$slicks.length) return false;

	  	this.$slicks.each(this._multiInit(this));
	  	
	  	var win = $(window);
		if (win.width() <= 1024) {
			this._mobileOperatingSystem();
	    }

	    var userAgent, ieReg, ie;
	    userAgent = window.navigator.userAgent;
	    ieReg = /msie|Trident.*rv[ :]*11\./gi;
	    ie = ieReg.test(userAgent);

	    if(ie) {
	        $(".slider__item").each(function () {
	            var $container = $(this);
	            $(window).on('resize', function(){
	                var imgUrl = $container.find("img").prop("src");
	                if (imgUrl) {
	                    $container.css({
	                        "backgroundImage" : 'url(' + imgUrl + ')'
	                    }).addClass("custom-object-fit");
	                }
	            });
	            $(window).resize();        
	        });
	    }
	};
	
	carouselHeroPe.prototype._mobileOperatingSystem = function() {
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;

	    if (/android/i.test(userAgent)) {
	        $('.app__item--ios').addClass('hide');
	        return "Android";
	    }
	    
	    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
	        $('.app__item--android').addClass('hide');
	        return "iOS";
	    }
	    return "unknown";
	};

	carouselHeroPe.prototype._multiInit = function (self) {
		return function(index, element){
			var $this = $(element);
			$this.slick({
		        dots: true,
		        arrows: false,
		        slidesToShow: 1,
		        infinite: true,
		        speed: 600,
		        autoplay: true,
		        autoplaySpeed: 10000,
		        pauseOnFocus: true,
		        pauseOnHover: true
		    });
	    };
	};

	$.carouselHeroPe = new carouselHeroPe();
	$.carouselHeroPe.Constructor = carouselHeroPe;
}(window.jQuery);// jshint ignore:line

jQuery(document).ready(function(){
	'use strict';
	$.carouselHeroPe.init();
});