// ***********************************************
// *                                             *
// *                  GTM PERU                   *
// *                                             *
// ***********************************************

try { !function($) {
	
	var GtmManager = function () {
		this.dl = window['dataLayer'];
		this.ckPhone = 'CANALONLINE_PERSONALIZATION';
		this.ckUserType = 'CANALONLINE_PROFILE';
		this.promosShown = [];
		this.plansShown = [];
		this.isAutomaticSlide = true;
	};
	
	GtmManager.prototype.init = function() {
		var that = this;
		
		that._checkUserId();
		that._heroSlide();
		that._plansAndDevices();
		that._checkCommonOnClick();
		that._productDetail();
		that._checkTerceros();
		that._checkFichaEquipo();
		
		// Especificos peru
		that._checkDistribucion();
		
	}
	
	/* ***********************
	 * Public methods
	 * *********************** */
	
	GtmManager.prototype.settingSelected = function(settingName, settingId) {
		var that = this;
		
		var gtmEntry = {
			'event': 'trackEvent',
			'eventCategory': 'Personalización',
			'eventAction': 'No Clientes',
			'eventLabel': settingName,
			'eventValue': settingId.toString()
		}
		that.dl.push(gtmEntry);
		
	}
	
	GtmManager.prototype.heroNextSlideAuto = function(isAuto) {
		this.isAutomaticSlide = isAuto;
	}
	
	GtmManager.prototype.newSlide = function(slideEl, pos) {
		var that = this;
		
		var $el = $(slideEl);
		that._processHeroSlide($el);
		
	}
	
	GtmManager.prototype.onPlanListInit = function(slick) {
		var that = this;
		var target = $(slick.currentTarget);
		var activePlans = target.find('[data-gtm-type="listadoPlanes"]:visible');
		
		that._sendPlanesToGTM(
			activePlans,
			that.plansShown, activePlans.length == 0, false
		);
		
	}
	
	GtmManager.prototype.onHeroInit = function(slick) {
		var that = this;
		var target = $(slick.currentTarget);
		var firstSlide = target.closest('.hero').find('div[data-gtm-type="hero-slide"][data-slick-index="0"]');
		that._processHeroSlide(firstSlide);
		
		// Seteamos los onClick para forzar que el siguiente
		// evento beforechange sea forzado 
		
		$('.hero .progress-dots,.hero .slick-arrow').click(function() {
			$.GtmManager.heroNextSlideAuto(false);
		});
		
	}
	
	GtmManager.prototype.onPlanListTab = function(tab) {

		var that = this;
		var target = $(tab);
		
		// tap selected
		
		var selectedTab = target.parents('.c-plan-container').find('a[href="#' + target.prop("id") + '"]');
		that._sendClickToGTM(selectedTab);
		
		// Visible plans
		
		that._sendPlanesToGTM(
			target.find('div[data-gtm-type="listadoPlanes"]:visible'),
			that.plansShown, false, false
		);
	}
	
	GtmManager.prototype.onDeviceListInit = function(slick) {
		var that = this;
		var target = $(slick.currentTarget);
		var activeDevices = target.find('a.slick-active[data-gtm-type="listadoEquipos"]:visible');
		
		that._sendPlanesToGTM(
			target.find('a[data-gtm-type="listadoEquipos"]:visible'),
			that.plansShown, activeDevices.length == 0, true
		);
		
	}
	
	GtmManager.prototype.onDeviceListTab = function(tab) {

		var that = this;
		var target = $(tab);
		
		// tap selected
		
		var selectedTab = target.parents('.category-tabs-container').find('a[href="#' + target.prop("id") + '"]');
		that._sendClickToGTM(selectedTab);
		
		// Visible devices
		
		var activeDevices = target.find('a.slick-active[data-gtm-type="listadoEquipos"]');
		
		that._sendPlanesToGTM(
				target.find('a[data-gtm-type="listadoEquipos"]'),
			that.plansShown, activeDevices.length == 0, true
		);
	}
	
	
	GtmManager.prototype.onVideoChange = function(yEvent) {
		
		var that = this;		
		var action = '';
		
		if (yEvent.data == 0) {
			action = 'ended ';
		}
		else if (yEvent.data == 1) {
			action = 'play';
		}
		else if (yEvent.data == 2) {
			action = 'pause';
		}
		
		if (action) {
			var obj = {
				'eventCategory' : 'Videos',
				'eventAction' : action,
				'eventLabel' : yEvent.target.getVideoData().title
			}
			
			that.dl.push(obj);
		}
		
	}
	
	/* ***********************
	 * Private methods
	 * *********************** */
	
	GtmManager.prototype._checkUserId = function() {
		
		var that = this;
		
		var phone = that._cookieVal(that.ckPhone);
		var userType = that._cookieVal(that.ckUserType);
		
		if (phone || userType) {
			var gtmEntry = {}
			if (phone) gtmEntry['celular'] = that._toB64(phone);
			if (userType) gtmEntry['tipoCliente'] = userType;
			that.dl.push(gtmEntry);
		}
		
	}
	
	GtmManager.prototype._heroSlide = function() {
		
		var that = this;
		
		// onClick
		
		$('div[data-gtm-type="hero-slide"]').click(function(ev) {
			
			var $target = $(ev.target);
			var gtmType = $target.data('gtm-type');			
			
			if (gtmType !== 'commonClick' && 
					$target.parents('.hero__item-freehtml').length == 0) {
				
				var $el = $(ev.target).closest('.hero__item');
				var wcid = $el.data('wcid').toString(); 
				var creative = $el.data('creative');
				var gname = $el.data('gname');
				var pos = $el.data('slick-index');
				
				var newPromo = {	
					'id': wcid,
					'name': gname,
					'creative': creative,
					'position': pos.toString()
				}
				var gtmEntry = {
					'promos': [newPromo],
					'event': 'trackClickProm'
				}
				
				that.dl.push(gtmEntry);
			}
			
		});
	}
	
	GtmManager.prototype._processHeroSlide = function($el) {
		
		var that = this;
		var wcid = $el.data('wcid');
		var creative = $el.data('creative');
		var gname = $el.data('gname');
		var pos = $el.data('slick-index') || '0';
		
		wcid = wcid ? wcid.toString() : undefined;
		pos = pos ? pos.toString() : undefined;

		if ((that.isAutomaticSlide && $.inArray(wcid, that.promosShown) < 0) || !that.isAutomaticSlide) {
			that.promosShown.push(wcid);
			var newPromo = {	
				'id': wcid,
				'name': gname,
				'creative': creative,
				'position': pos
			}
			var gtmEntry = {
				'promos': [newPromo],
				'event': that.promosShown.length == 1 ? 'trackImpProm' :'trackEventSliderProm'
			}
			that.dl.push(gtmEntry);
		}
		that.isAutomaticSlide = true;
		
	}
	
	GtmManager.prototype._productDetail = function() {
		
		var that = this;
		var $buyBtn = $('[data-gtm-type="productDetailBuy"]');
		
		// Init
		
		$('[data-gtm-type="productDetailClient"]').each(function(i, el) {
			var $el = $(el);
			var $buyBtn = $('[data-gtm-type="productDetailClient"]');
			$buyBtn.data('gtm-price', $el.data('gtm-price'));
			$buyBtn.data('gtm-client', $el.data('gtm-client'));
			
		});
		
		if ($buyBtn.length > 0) {
			var $el = $($buyBtn.get(0));
			that.dl.push({
				'productos': [that._createObjFromData($el,[
					"name", "id", "price", "brand", "category"
				])],
				'event': 'productdetailview'
			});
		}
		
		// onClick
		
		$('[data-gtm-type="productDetailClient"],[data-gtm-type="productDetailNotClient"]').click(function(ev) {
			var $el = $(ev.target).closest('[data-gtm-type="productDetailClient"],[data-gtm-type="productDetailNotClient"]');
			$buyBtn.data('gtm-price', $el.data('gtm-price'));
			$buyBtn.data('gtm-client', $el.data('gtm-client'));
		});
		
		$buyBtn.click(function(ev) {
			that.dl.push({
				'productos': [that._createObjFromData($buyBtn,[
					"name", "id", "price", "brand", "category", "variant", "list", "position", "quantity"
				])],
				'event': 'trackaddtocart'
			});
			
		});
		
	}
	
	GtmManager.prototype._sendPlanesToGTM = function(list, plansShown, force, areDevices) {

		var that = this;
		var planesEquipos = [];
		
		list.each(function(i, el) {
			var $el = $(el);
			if ($el.hasClass('slick-active') || force) {
				
				var elId = $el.data('gtm-id');
				
				if ($.inArray(elId, that.plansShown) < 0) {
					that.plansShown.push(elId);
					
					var newPlanEquipo = that._createObjFromData($el,[
						"name", "id", "price", "brand", "category", "list", "position"
					]);
					
					// Si se ven mas de 1 equipo a la vez es que no es mobile
					// y hay que arreglar las posiciones

					if (!force && areDevices && $(window).width() > 600) {
						try {
							var indx = parseInt($el.data('slick-index')) + 1;
							newPlanEquipo.position = indx;
						} catch (e) { console.log(e); }
					}
					else {
						newPlanEquipo.position = parseInt(newPlanEquipo.position);
					}
					
					// Agregamos al listado
					
					planesEquipos.push(newPlanEquipo);
					
				}
				
			}
		});
		
		if (planesEquipos.length > 0) {
			
			// No se cargan bien las positions si no hay suficientes elementos
			// como para generar paginacion
			if (force) {
				$.each(planesEquipos, function(i, arrEl) {
					arrEl.position = i;
				});
			}
			
			
			
			// Finalmente
			that.dl.push({
				'productos': planesEquipos,
				'event': 'trackProdImp'
			});

		}
		
	}
	

	GtmManager.prototype._sendClickToGTM = function(el) {
		
		var that = this;
		var $el = $(el);
		var obj = { 'event': 'trackEvent' };
		
		obj['eventCategory'] = $el.data('gtm-category');
		obj['eventAction'] = $el.data('gtm-action');
		obj['eventLabel'] = $el.data('gtm-label');
		
		that.dl.push(obj);
		
	}
	
	GtmManager.prototype._plansAndDevices = function() {
		
		var that = this;
		
		// -------------- Planes
		
		// Event on swipe or on next
		
		var slickPlanes = $('.js-pricing-slider');
		var that = this;
		
		
		slickPlanes.on('afterChange', function(slick){
			
			var $target = $(slick.currentTarget);
			that._sendPlanesToGTM(
				$target.find('div[data-gtm-type="listadoPlanes"]:visible'),
				that.plansShown, false, false
			);
			
		});
		
		// on click "Ver mas" planes
		
		$('.js-pricing-slider .c-plan .c-plan__view-detail').click(function(ev) {
			var $target = $(ev.target);
			var elPlan = $target.parents('.c-plan');
			var plan = that._createObjFromData(elPlan,[
				"name", "id", "price", "brand", "category", "list", "position"
			]);
			
			that.dl.push({
				'productos': [plan],
				'event': 'trackClickProd'
			});
			
		});
		
		// on click "Lo quiero" planes
		
		$('.js-pricing-slider .c-plan .c-plan__get').click(function(ev) {
			var $target = $(ev.target);
			var elPlan = $target.parents('.c-plan');
			var plan = that._createObjFromData(elPlan,[
				"name", "id", "price", "brand", "category", "list", "position"
			]);
			plan['quantity'] = 1;
			
			that.dl.push({
				'productos': [plan],
				'event': 'trackaddtocart'
			});
			
		});
		
		// -------------- Dispositivos
		
		// Event on swipe or on next
		
		var slickDispositivos = $('.js-device-caruosel');
		var that = this;
		
		slickDispositivos.on('afterChange', function(slick){

			var $target = $(slick.currentTarget);
			that._sendPlanesToGTM(
				$target.find('a[data-gtm-type="listadoEquipos"]:visible'),
				that.plansShown, false, true
			);
			
		});
		
		// on click dispositivos
		
		slickDispositivos.find('.device-carousel__item').click(function(ev) {
			var $target = $(ev.target);
			var elDevice = $target.closest('.device-carousel__item');

			if ($target.hasClass('device-carousel__featured')) {
				
				var device = that._createObjFromData(elDevice,[
					"name", "id", "price", "brand", "category", "variant", "list", "position", "quantity"
				]);
				device.quantity = 1;
				device.position = parseInt(device.position);
				
				that.dl.push({
					'productos': [device],
					'event': 'trackaddtocart'
				});
				
			}
			else {
				
				var device = that._createObjFromData(elDevice,[
					"name", "id", "price", "brand", "category", "list", "position"
				]);
				
				that.dl.push({
					'productos': [device],
					'event': 'trackClickProd'
				});
			}			
			
		});
		
	}
	
	GtmManager.prototype._checkCommonOnClick = function() {
		
		var that = this;
		
		$('[data-gtm-type="commonClick"]').click(function(ev) {
			
			
			var $el = $(ev.target).closest('[data-gtm-type="commonClick"]');
			
			var gtmSubType = $el.data('gtm-subtype');
			
			if (gtmSubType !== 'mainDistribucion' && gtmSubType !== 'disableCommonClick') {
				var obj = {
					'event': 'trackEvent',
					'eventCategory': $el.data('gtm-category'),
					'eventAction': $el.data('gtm-action'),
					'eventLabel': $el.data('gtm-label'),
				}
				
				that.dl.push(obj);
			}
			
		});
		
	}
	
	GtmManager.prototype._checkDistribucion = function() {
		
		var that = this;
		var bannersDist = $('[data-gtm-subtype="mainDistribucion"]');
		
		bannersDist.each(function(i, node) {
			
			var $node = $(node);
			
			var obj = {
				'id': $node.data('gtm-id') ? $node.data('gtm-id').toString(): '',
				'name': $node.data('gtm-desc'),
				'creative': 'Distribucion',
				'position': 'Distribucion - ' + $node.data('gtm-action') 
			}
			
			that.dl.push({
				'promos': [obj],
				'event': 'trackImpProm'
			});
			
		});
		
		bannersDist.click(function(ev) {
			
			var $node = $(ev.target).closest('[data-gtm-type="commonClick"]');
			
			var obj = {
				'id': $node.data('gtm-id') ? $node.data('gtm-id').toString(): '',
				'name': $node.data('gtm-desc'),
				'creative': 'Distribucion',
				'position': 'Distribucion - ' + $node.data('gtm-action') 
			}
			
			that.dl.push({
				'promos': [obj],
				'event': 'trackClickProm'
			});
			
		});
		
	}
	
	GtmManager.prototype._checkTerceros = function() {
		
		var that = this;
		var serviciosTerceros = $('[data-gtm-type="terceros"] [data-gtm-type="commonClick"]');
		
		serviciosTerceros.each(function(i, node) {
			
			var $node = $(node);
			
			$node.data("gtm-subtype", "disableCommonClick");
			
		});
		
		serviciosTerceros.click(function(ev) {
			
			var $node = $(ev.target).closest('[data-gtm-type="commonClick"]');
			var action = $node.parents('[data-gtm-type="terceros"]').data('gtm-action');
			var label = $node.data('gtm-label');
			
			var obj = {
				'eventCategory': 'Vista de Detalle',
				'eventAction': action,
				'eventLabel': label
			}
			
			that.dl.push(obj);
			
		});
		
	}
	
	GtmManager.prototype._checkFichaEquipo = function() {
		
		var that = this;
		var ficha = $('[data-gtm-type="fichaEquipo"]');
		
		// Visualization
		
		if (ficha.length > 0) {
			var singleFicha = $(that.get(0));
			
			var obj = that._createObjFromData(ficha, ['name', 
				'id', 'price', 'brand', 'category']);
			
			that.dl.push({
				'productos': [obj],
				'event': 'productdetailview'
			});
		}
		
		// add to cart
		
		ficha.click(function(ev){
			
			var clickFicha = $(ev.target).closest('[data-gtm-type="fichaEquipo"]');
			
			var obj = that._createObjFromData(ficha, ['name', 
				'id', 'price', 'brand', 'category']);
			
			obj.variant = $('.js-color-choose .color-choice--active').data('color');
			obj.list = 'Ficha equipo';
			obj.position = '0';
			obj.quantity = '1';
				
			that.dl.push({
				'productos': [obj],
				'event': 'trackaddtocart'
			});
			
		});
		
	}
	
	
	/* ***********************
	 * Utils
	 * *********************** */
	
	GtmManager.prototype._createObjFromData = function(el, propsArr) {
		var result = {}
		
		$.each(propsArr, function(i, prop) {
			var val = el.data("gtm-" + prop);
			if (val || val === 0) {
				result[prop] = String(val).replace(/^\s+|\s+$/g, '');
			}
		});
		
		return result;
	}
	
	GtmManager.prototype._toB64 = function(str) {
		return window.btoa(unescape(encodeURIComponent( str )));
	}
	
	GtmManager.prototype._cookieVal = function(cName) {
		var nameEQ = cName + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) === ' ') {
				c = c.substring(1, c.length);
			}
			if (c.indexOf(nameEQ) === 0) {
				return c.substring(nameEQ.length, c.length);
			}
		}
		return null;
	}
	
	/* ***********************
	 * Init
	 * *********************** */
	
	
	$.GtmManager = new GtmManager();
	$.GtmManager.Constructor = GtmManager;
	
}(window.jQuery, window.Liferay);// jshint ignore:line


jQuery(document).ready(function(){
	'use strict';
	$.GtmManager.init();
});// jshint ignore:line

} catch(e) { console.log(e); } 