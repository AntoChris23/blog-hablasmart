document.addEventListener('DOMContentLoaded', function() {
	var elemsNatCarouselEquipment = document.querySelectorAll('.nat-equipment-carousel .equipment--carousel .swiper-container');
	var elemsNatCatalogEquipment = document.querySelectorAll('.nat-equipment-carousel .equipment--carousel .swiper-container');
	if(elemsNatCarouselEquipment.length > 0){
	    var swiper1 = new Swiper('.nat-equipment-carousel .equipment--carousel .swiper-container', {
			speed: 1000,
	        effect: 'coverflow',
	        slidesPerView: 3,
	        spaceBetween: -90,
	        centeredSlides: true,
	        loop: true,
	        coverflowEffect: {
	            rotate: -80,
	            stretch: -80,
	            depth: 160,
	            modifier: 1,
	            slideShadows : false,
	        },
	        navigation: {
	            nextEl: '.swiper-button-next',
	            prevEl: '.swiper-button-prev',
	        },
	        breakpoints: {
	            992: {
	                spaceBetween: 5,
	                coverflowEffect: {
	                    rotate: -30,
	                    stretch: 5
	                }
	            }
	        }
	    });
	}
	if(elemsNatCatalogEquipment.length > 0){
		var swiper2 = new Swiper('.nat-equipment-carousel .equipment--block .swiper-container',{   
			speed: 1000,         
	        centeredSlides: true,
	        slidesPerView: 1,
	        spaceBetween: 5,
	        pagination: {
	            el: '.swiper-pagination',
	            dynamicBullets: true,
	        },
	        navigation: {
	            nextEl: '.swiper-button-next',
	            prevEl: '.swiper-button-prev',
	        },
	    });
	}
});