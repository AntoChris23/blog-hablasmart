<!DOCTYPE html>

<#include init />

<html class="${root_css_class} full-width-page" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">
	
	<head>
		<#include "${full_templates_path}/gtm.ftl" />
		
		<title>${the_title} - ${company_name}</title>
	
		<meta content="initial-scale=1.0, width=device-width" name="viewport" />
		
		<link rel="alternate" hreflang="es-pe" href="https://www.movistar.com.pe/" />    
    	<link rel="alternate" hreflang="es-cl" href="https://www.movistar.cl/" />
    	<link rel="alternate" hreflang="es-ar" href="https://www.movistar.com.ar/" />
    	<link rel="alternate" hreflang="es-co" href="https://www.movistar.co/" />
    	<link rel="alternate" hreflang="es-cr" href="https://movistar.cr/" />
    	<link rel="alternate" hreflang="es-ec" href="https://www.movistar.com.ec/" />
    	<link rel="alternate" hreflang="es-es" href="https://www.movistar.es/" />
    	<link rel="alternate" hreflang="es-gt" href="https://www.movistar.com.gt/" />
    	<link rel="alternate" hreflang="es-mx" href="https://www.movistar.com.mx/" />
    	<link rel="alternate" hreflang="es-ni" href="https://www.movistar.com.ni/" />
    	<link rel="alternate" hreflang="es-pa" href="https://www.movistar.com.pa/" />
    	<link rel="alternate" hreflang="es-uy" href="https://www.movistar.com.uy/" />
    	<link rel="alternate" hreflang="es-ve" href="http://www.movistar.com.ve/" />
    	<link rel="alternate" hreflang="es-sv" href="https://www.movistar.com.sv/" />
    	<link rel="alternate" hreflang="x-default" href="https://www.movistar.com" />
		<#if showFacebookMetatags>
			<#if validator.isBlank(socialTitle)>
				<meta property="og:title" content="${the_title} | ${company_name}" />
			<#else>
				<meta property="og:title" content="${socialTitle}" />
			</#if>
			<#if validator.isBlank(socialDescription)>
				<meta property="og:description" content="${themeDisplay.getLayout().getDescription(locale)}" />
			<#else>
				<meta property="og:description" content="${socialDescription}" />
			</#if>
			<meta property="og:type" content="${facebookTypeMetatag}" />
			<meta property="og:url" content="${themeDisplay.getURLPortal()}${themeDisplay.getURLCurrent()}" />
			<#if validator.isBlank(facebookImageMetatag)>
				<meta property="og:image" content="/canalonline-peru-theme/images/peru/open-graph-general.jpg" />
			<#else>
				<meta property="og:image" content="${facebookImageMetatag}" />
			</#if>
		</#if>
		<#assign canonicalUrl = themeDisplay.getLayout().getExpandoBridge().getAttribute("canonical-url")>
		<#if !validator.isBlank(canonicalUrl)>
			${canonicalUrl}
		</#if>
		${theme.include(top_head_include)}

		<script charset="utf-8" src="${javascript_folder}/header/jquery-3.5.0.min.js"></script>	
		<#assign tealeafJs = theme.getSetting("tealeaf-js")>
		<#if !validator.isBlank(tealeafJs)>
			<script src="${tealeafJs}" async="async"></script>
		</#if>
		<#if enableSmartbanner>
			<#if !validator.isBlank(appleidSmartbanner)>
				<meta name="apple-itunes-app" content="app-id=${appleidSmartbanner}">
			</#if>
			<#if !validator.isBlank(googleidSmartbanner)>
	    		<meta name="google-play-app" content="app-id=${googleidSmartbanner}">
	    	</#if>
    	</#if>
    	<#assign headerJs = theme.getSetting("header-js")>
		<#if !validator.isBlank(headerJs)>
			${headerJs}
		</#if>
	</head>
	<#if !hideHeader>
 		<body class="${css_class} mlnavigation--push-menu mlnavigation--padding">
	<#else>
		<body class="${css_class} mlnavigation--push-menu">
	</#if>
	<#attempt>
		<#if !validator.isBlank(gtmId)>
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${gtmId}"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		</#if>
	<#recover>
	</#attempt>

		
		${theme.include(body_top_include)}
				
		<#if is_signed_in>
			<@liferay.dockbar />
		</#if>
		
		<div id="wrapper">
		
			<#-- * Header * -->
			
			<#if !hideHeader>
		     	<#include "${full_templates_path}/navigation.ftl" />
		    </#if>
			<#-- * Content * -->
		
			<div id="content">
				<#-- * Breadcrumbs * -->
				<#include "${full_templates_path}/breadcrumbs.ftl" />
				
				<#if selectable>
					${theme.include(content_include)}
				<#else>
					${portletDisplay.recycle()}
					
					${portletDisplay.setTitle(the_title)}
					
					${theme.wrapPortlet("portlet.ftl", content_include)}
				</#if>		
				
			</div>
			
			<#-- * Footer * -->
			
			<#if !hideFooter>
		     	<#include "${full_templates_path}/footer.ftl" />
		    </#if>
			
		</div>
		

		
		<#assign enableCookiePortlet = getterUtil.getBoolean(theme.getSetting("enable-cookie-portlet"))>
		<#if enableCookiePortlet>
		<#-- *********** START : Time Portlet *********** -->
		
		<#attempt>
		
		<script>
		jQuery(document).ready(function(){
			$('#_privacycookies_WAR_privacycookiesportlet_privacy-info-message-v2').show();
		});
		</script>
		
		<#recover>
		</#attempt>
		</#if>
		<#-- *********** END : Time Portlet *********** -->	
		
		
			
		<#assign enableTimePortlet = getterUtil.getBoolean(theme.getSetting("enable-time-portlet"))>
		<#if enableTimePortlet>
		<#-- *********** START : Time Portlet *********** -->
		
		<#attempt>
		
			<#assign timePortletId = "colportaltimeendpoint_WAR_colportaltimeportlet" />
			<#assign PortletPrefFactoryUtil = staticUtil["com.liferay.portlet.PortletPreferencesFactoryUtil"] />
			<#assign portSetup = PortletPrefFactoryUtil.getLayoutPortletSetup(layout, timePortletId) />
			${theme.runtime(timePortletId, "", portSetup.toString())} 

			<#assign timePortletId = "colportaltimenotification_WAR_colportaltimeportlet" />
			<#assign PortletPrefFactoryUtil = staticUtil["com.liferay.portlet.PortletPreferencesFactoryUtil"] />
			<#assign portSetup = PortletPrefFactoryUtil.getLayoutPortletSetup(layout, timePortletId) />
			${theme.runtime(timePortletId, "", portSetup.toString())} 
		
		<#recover>
		</#attempt>
		</#if>
		<#-- *********** END : Time Portlet *********** -->	
		
		<#assign enableClientSettingsPopup = getterUtil.getBoolean(theme.getSetting("enable-client-settings-popup"))>
		<#if enableClientSettingsPopup>
		<#-- *********** START : Client settings Portlet *********** -->
		
		<#attempt>

			<#assign locPortletId = "clientsettings_WAR_clientsettingsportlet_INSTANCE_theme" />
			<#assign PortletPreferencesFactoryUtil = staticUtil["com.liferay.portlet.PortletPreferencesFactoryUtil"] />
			<#assign portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(layout, locPortletId) />
			<#assign temp = portletSetup.setValue("portletSetupShowBorders", "false") />
			<#assign temp = portletSetup.setValue("view", "modal") />
			${theme.runtime(locPortletId, "", portletSetup.toString())} 

		<#recover>
		</#attempt>
			
		<#-- *********** END : Client settings Portlet *********** -->		
		</#if>
		
		<!-- Modal Mobile Mi Movistar -->
		<div class="modal modal-despreocupate" id="popup_impulsoApp">
		    <div class="modal-content">
		    	<#if hasEmbeddedWcMovistarPopUp>
					${embeddedWcMovistarPopUpContent}
				</#if>
		    </div>
		</div>
    
		${theme.include(body_bottom_include)}

		${theme.include(bottom_include)}

		<script charset="utf-8" src="${javascript_folder}/min/footer.js"></script>
		
		<script>
		jQuery(document).ready(function(){
			$('select').filter('.md').material_select();
			$('.collapsible').collapsible();
			$('.modal').modal();
		});
		</script>
		<#attempt>
		<#assign trapcode = getterUtil.getBoolean(themeDisplay.getLayout().getExpandoBridge().getAttribute("trapcode"))>
		<#if trapcode>
			<script type="text/javascript" src="${javascript_folder}/header/stats.js"></script>
		 </#if>
		 <#recover>
		</#attempt>
	</body>

</html>
