<#assign footerBookJsonString = theme.getSetting("footer-book-json") >
<#attempt>
	<#assign footerBookMap = jsonFactoryUtil.looseDeserializeSafe(footerBookJsonString)>
<#recover>
	<#assign footerBookMap = {'principal': {'title': 'Libro de Reclamaciones', 'url': 'https://www.movistar.com.pe/libro-de-reclamaciones', 'image': '/canalonline-peru-theme/images/libro2.svg', 'alt': 'libro reclamaciones'}, 'consult': [{'title': 'Reclamos', 'url': 'https://www.movistar.com.pe/atencion-al-cliente/reclamos/registro-averias'}, {'title': 'Consulta de reclamos', 'url': 'https://www.movistar.com.pe/atencion-al-cliente/reclamos/reporte-virtual'}, {'title': 'Información abonados y usuarios', 'url': 'https://www.movistar.com.pe/informacion-a-abonados-y-usuarios'}], 'social':[{'url': 'https://www.facebook.com/movistarperu', 'alt': 'facebook', 'image': '/canalonline-peru-theme/images/facebook.svg'}, {'url': 'https://twitter.com/MovistarPeru', 'alt': 'twitter', 'image': '/canalonline-peru-theme/images/twitter.svg'}, {'url': 'https://www.instagram.com/movistarperu', 'alt': 'instagram', 'image': '/canalonline-peru-theme/images/instagram.svg'}, {'url': 'https://www.youtube.com/user/CanalMovistarPeru', 'alt': 'youtube', 'image': '/canalonline-peru-theme/images/youtube.svg'} ] }>
</#attempt>
<div class="reclamaciones">
    <div class="container">
        <div class="row">
            <div class="col s12 l10">
                <ul class="reclamaciones--consulta">
                	
                    <#if footerBookMap.consult??>
	                    <#attempt>
	                    <#list footerBookMap.consult as item>
						    <li class="consulta-item"><a href="${item.url}">${item.title}</a></li>
						</#list>
						<#recover>
							<li><div class="alert alert-error">Error</div></li>
						</#attempt>
					</#if>
					<#if footerBookMap.principal??>
	                	<#attempt>
	                    <li class="consulta-item"><a href="${footerBookMap.principal.url}"><img src="${footerBookMap.principal.image}" alt="${footerBookMap.principal.alt}"><strong>${footerBookMap.principal.title}</strong></a></li>
	                    <#recover>
	                    <li><div class="alert alert-error">Error</div></li>
						</#attempt>
                    </#if>
                </ul>
            </div>
            <#if footerBookMap.social??>
            <div class="col s12 l2">
                <ul class="reclamaciones--social">
                <#attempt>
                <#list footerBookMap.social as item>
				    <li class="social-item"><a href="${item.url}"><img src="${item.image}" alt="${item.alt}"></a></li>
				</#list>
				<#recover>
					<li><div class="alert alert-error">Error</div></li>
				</#attempt>
                </ul>
            </div>
            </#if>
        </div>
    </div>
</div>