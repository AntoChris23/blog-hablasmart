<#-- Include creado para facilitar el GTM en cada pais -->

<a href="${miMovistarUrl}" class="mi-movistar-login__button waves-effect waves-light" 
		data-gtm-type="commonClick" data-gtm-category="Mi Movistar" data-gtm-action="${themeDisplay.getLayout().getName(themeDisplay.getLocale())}" 
		data-gtm-label="Ingresar">
	${miMovistarButtonText}
</a>