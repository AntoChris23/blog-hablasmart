<div id="desktopnavigation">

	<div class="dnavigation__slider">

		<ul role="menubar" data-menu="main" class="dnavigation">

			<#list navItems as nav_item>

				<#if nav_item.getLayout().getFriendlyURL(locale) != homeLayoutFurl && nav_item.getName() != "footer" && nav_item.getName() != "toplink" && nav_item.getName() != "rrss">
				
					<#assign
						nav_item_attr_selected = ""
						nav_item_css_class = ""
					/>
				
					<#if nav_item.isSelected()>
						<#assign
							nav_item_attr_selected = "aria-selected='true'"
							nav_item_css_class = "selected"
						/>
					</#if>
					
					<li ${nav_item_attr_selected} class="${nav_item_css_class} dnavigation__item">
	
						<#if nav_item.hasChildren()>
			
							<div class="dnavigation__link dnavigation__children">
								<span>${nav_item.getName()}</span>
								<span class="icon-keyboard_arrow_down dnavigation__icon"></span>
							</div>
							<div class="dnavigation__submenu-container">
								<ul class="dnavigation__submenu">
			
									<#list nav_item.getChildren() as nav_child>
			
										<#if nav_child.hasChildren()>
			
											<li>
												<div class="dnavigation__title">
													<span>${nav_child.getName()}</span>
													<a href="${nav_child.getURL()}" class="sr-only">${nav_child.getName()}</a>
												</div>
												<ul class="dnavigation__submenu__2">
			
													<#list nav_child.getChildren() as nav_grandchild>
														<#assign
															nav_grandchild_attr_selected = ""
															nav_grandchild_css_class = ""
														/>
													
														<#if nav_grandchild.isSelected()>
															<#assign
																nav_grandchild_attr_selected = "aria-selected='true'"
																nav_grandchild_css_class = "selected-children"
															/>
														</#if>
														<li ${nav_grandchild_attr_selected} class="${nav_grandchild_css_class}">
															<a href="${nav_grandchild.getURL()}" class="dnavigation__link">
																<span>${nav_grandchild.getName()}</span>
															</a>
														</li>
													
													</#list>
			
												</ul>
											</li>								
			
										<#else>
										
											<#assign
												nav_child_attr_selected = ""
												nav_child_css_class = ""
											/>
										
											<#if nav_child.isSelected()>
												<#assign
													nav_child_attr_selected = "aria-selected='true'"
													nav_child_css_class = "selected-children"
												/>
											</#if>
			
											<li ${nav_child_attr_selected} class="${nav_child_css_class}">
												<a href="${nav_child.getURL()}" class="dnavigation__link">
													<span>${nav_child.getName()}</span>
												</a>
											</li>
			
										</#if>
			
									</#list>
			
								</ul>
							</div>
			
						<#else>
			
							<a href="${nav_item.getURL()}" class="dnavigation__link dnavigation__no-children">
								<span>${nav_item.getName()}</span>
							</a>
			
						</#if>
			
					</li>


				</#if>

			</#list>

		</ul><#-- dnavigation -->
		
		<div class="dnavigation__container-icons">
			<#if !validator.equals(stUrlSetting, '#!')>
			<div class="dnavigation__search">
				<a class="waves-effect dnavigation__search-button js-desktop-search-button" data-search-modal="${isStModal?string}" href="${stUrlSetting}" 
						data-gtm-type="commonClick" data-gtm-category="Buscador" data-gtm-action="click" 
						data-gtm-label="${themeDisplay.getLayout().getName(themeDisplay.getLocale())}">
					<span class="sr-only">Busca en Movistar.com</span>
					<span class="icon-search search-lg"></span>
				</a>
				<input type="text" placeholder="Busca en Movistar.com" class="dnavigation__search-input">
			</div>
          	</#if>
          	<#if hasHeaderXtraBtn>
	            <div class="dnavigation__accessibility">
	              	<a href="${headerXtraBtnUrl}" class="waves-effect dnavigation__accessibility-button" 
	              			data-gtm-type="commonClick" data-gtm-category="Movistar Accesible" data-gtm-action="click" 
							data-gtm-label="${themeDisplay.getLayout().getName(themeDisplay.getLocale())}">
	              		<#if !validator.isBlank(headerXtraBtnTxt)>
	            	  		<span class="sr-only">${headerXtraBtnTxt}</span>
	            	  	</#if>
	              		<span class="icon-${headerXtraBtnIcon}"></span>
	              	</a>
				</div>
			</#if>
			
        </div>
	</div><#-- dnavigation__slider -->

</div><#-- desktopnavigation -->


<div class="mi-movistar-login">
	<#include "${full_templates_path}/includes/mi_movistar.ftl" />
</div>

<div id="desktop-search-modal" class="modal bottom-sheet search-modal">
	<div class="modal-header">
		<a href="#!" class="js-dismiss-desktop-search-modal waves-effect search-modal__dismiss">
			<span class="sr-only">Dismiss Search Modal</span>
			<span class="icon-close"></span>
		</a>
	</div>
	<div class="modal-content">
		<div class="container">
			<div class="row">
				<div class="link-list">
					<div class="link-list__title">section title</div>
					<div class="link-list__item"><a href="#" class="link-list__link">Cambiar nombre WiFi y contraseña</a></div>
					<div class="link-list__item"><a href="#" class="link-list__link">Coste adicional Roaming</a></div>
					<div class="link-list__item"><a href="#" class="link-list__link">Bloquear el celular en caso de robo</a></div>
				</div>
			</div>
		</div>
	</div>
</div>
