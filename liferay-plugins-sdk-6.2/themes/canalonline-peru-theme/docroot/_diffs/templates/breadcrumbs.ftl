<#assign friendlyUrl = themeDisplay.getLayout().getFriendlyURL()>
<#assign breadcrumbList = []>
<#assign breadUrl = "/">
<#assign breadStep = 1>
<#if !validator.isBlank(homeUrl) && !friendlyUrl?ends_with("/home")>
	<#assign breadcrumbItem>
		<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="breadcrumb--item">
			<a itemprop="item" class="breadcrumb--link" class="breadcrumb--link" href="${homeUrl}">
				<span itemprop="name">Inicio</span>
			</a>
			<meta itemprop="position" content="${breadStep}" />
		</li>
	</#assign>
	<#assign breadcrumbList = breadcrumbList + [breadcrumbItem]>
	<#assign breadStep = breadStep + 1>
</#if>
<#list friendlyUrl?split("/") as pathUrl>
	<#if !validator.isBlank(pathUrl)>
		<#assign breadUrl = breadUrl + pathUrl>
		<#if !(homeUrl?ends_with(breadUrl) || breadUrl?ends_with("/home"))>
			<#attempt>
				<#assign layoutBread>
				${LayoutLocalService.fetchLayoutByFriendlyURL(themeDisplay.getScopeGroupId(), false, breadUrl)!""}
				</#assign>			
				<#if !validator.isBlank(layoutBread)>
					<#if !validator.isBlank(LayoutLocalService.fetchLayoutByFriendlyURL(themeDisplay.getScopeGroupId(), false, breadUrl))>									   
						<#assign layoutBread = LayoutLocalService.fetchLayoutByFriendlyURL(themeDisplay.getScopeGroupId(), false, breadUrl)>
						<#if validator.equals(layoutBread.getType(), "portlet")>
							<#if validator.equals(friendlyUrl, layoutBread.getFriendlyURL())>
							<#assign breadcrumbItem>
								<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="breadcrumb--item active">
									<span itemprop="name">${layoutBread.getName(locale)}</span>
									<meta itemprop="position" content="${breadStep}" />
								</li>
							</#assign>
							<#else>
							<#assign breadcrumbItem>
								<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="breadcrumb--item">
									<a itemprop="item" class="breadcrumb--link" class="breadcrumb--link" href="${portalUtil.getLayoutFriendlyURL(layoutBread, themeDisplay)}">
										<span itemprop="name">${layoutBread.getName(locale)}</span>
									</a>
									<meta itemprop="position" content="${breadStep}" />
								</li>
							</#assign>
							</#if>
							<#assign breadcrumbList = breadcrumbList + [breadcrumbItem]>
							<#assign breadStep = breadStep + 1>
						</#if>
					</#if>
				</#if>
			<#recover>
			</#attempt>
		</#if>
		<#assign breadUrl = breadUrl + "/">
	</#if>
</#list>
<#if breadcrumbList?size gt 0>
	<#assign breadInit = breadcrumbList?size - 3>
	<#assign breadStep = 1>
	<div class="nateevo-breadcrumb">
		<div class="breadcrumb--content">
			<ul class="breadcrumb--list-box" itemscope itemtype="https://schema.org/BreadcrumbList">
			<#list breadcrumbList as breadcrumbItem>
				<#if !(breadStep lte breadInit)>
				${breadcrumbItem}
				</#if>
				<#assign breadStep = breadStep + 1>
			</#list>
			</ul>
		</div>
	</div>
</#if>