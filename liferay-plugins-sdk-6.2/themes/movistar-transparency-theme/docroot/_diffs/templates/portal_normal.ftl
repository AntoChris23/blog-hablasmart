<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key='lang.dir'/>" lang="${w3c_language_id}">

<head>
	${google_tag_manager_header_script}

	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />
	<link rel="alternate" hreflang="es-pe" href="https://www.movistar.com.pe/" />    
	<link rel="alternate" hreflang="es-cl" href="https://www.movistar.cl/" />
	<link rel="alternate" hreflang="es-ar" href="https://www.movistar.com.ar/" />
	<link rel="alternate" hreflang="es-co" href="https://www.movistar.co/" />
	<link rel="alternate" hreflang="es-cr" href="https://movistar.cr/" />
	<link rel="alternate" hreflang="es-ec" href="https://www.movistar.com.ec/" />
	<link rel="alternate" hreflang="es-es" href="https://www.movistar.es/" />
	<link rel="alternate" hreflang="es-gt" href="https://www.movistar.com.gt/" />
	<link rel="alternate" hreflang="es-mx" href="https://www.movistar.com.mx/" />
	<link rel="alternate" hreflang="es-ni" href="https://www.movistar.com.ni/" />
	<link rel="alternate" hreflang="es-pa" href="https://www.movistar.com.pa/" />
	<link rel="alternate" hreflang="es-uy" href="https://www.movistar.com.uy/" />
	<link rel="alternate" hreflang="es-ve" href="http://www.movistar.com.ve/" />
	<link rel="alternate" hreflang="es-sv" href="https://www.movistar.com.sv/" />
	<link rel="alternate" hreflang="x-default" href="https://www.movistar.com" />
	<#if showFacebookMetatags>
		<#if validator.isBlank(socialTitle)>
			<meta property="og:title" content="${the_title} | ${company_name}" />
		<#else>
			<meta property="og:title" content="${socialTitle}" />
		</#if>
		<#if validator.isBlank(socialDescription)>
			<meta property="og:description" content="${themeDisplay.getLayout().getDescription(locale)}" />
		<#else>
			<meta property="og:description" content="${socialDescription}" />
		</#if>
		<meta property="og:type" content="${facebookTypeMetatag}" />
		<meta property="og:url" content="${themeDisplay.getURLPortal()}${themeDisplay.getURLCurrent()}" />
		<#if validator.isBlank(facebookImageMetatag)>
			<meta property="og:image" content="${images_folder}/movistar/open-graph-general.jpg" />
		<#else>
			<meta property="og:image" content="${facebookImageMetatag}" />
		</#if>
	</#if>
	<#attempt>
		<#assign canonicalUrl = themeDisplay.getLayout().getExpandoBridge().getAttribute("canonical-url")>
	<#recover>
		<#assign canonicalUrl = ''>
	</#attempt>
	<#if !validator.isBlank(canonicalUrl)>
		${canonicalUrl}
	</#if>
	${theme.include(top_head_include)}
	<script charset="utf-8" src="${javascript_folder}/movistar/jquery-3.4.1.min.js"></script>
</head>

<body class="body ${css_class} nat-navbar--push-menu">
${google_tag_manager_body_script}

<a href="#main-content" id="skip-to-content"><@liferay.language key="skip-to-content" /></a>

${theme.include(body_top_include)}

<#if is_signed_in>
	<@liferay.dockbar />
</#if>
<#include "${full_templates_path}/navigation.ftl" />
<main role="main">
    <div id="content">
    	<#if breadcrumbsEnable>
    		<#include "${full_templates_path}/breadcrumbs.ftl" />
    	</#if>
        <#if selectable>
            ${theme.include(content_include)}
        <#else>
            ${portletDisplay.recycle()}
    
            ${portletDisplay.setTitle(the_title)}
    
            ${theme.wrapPortlet("portlet.ftl", content_include)}
        </#if>
    </div>
</main>

<#include "${full_templates_path}/footer.ftl" />

${theme.include(body_bottom_include)}

${theme.include(bottom_include)}
<script charset="utf-8" src="${javascript_folder}/movistar.min.js"></script>
</body>

</html>