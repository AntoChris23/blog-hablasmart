<#assign breadcrumbList = []>
<#macro getParent layoutBread>
	<#assign breadcrumbList = breadcrumbList + [layoutBread]>
	<#if layoutBread.getParentLayoutId() != 0>
		<#assign parentLayout = LayoutLocalService.getParentLayout(layoutBread)>
		<@getParent parentLayout />
	</#if>
</#macro>

<#assign currentLayout = themeDisplay.getLayout()>
<@getParent currentLayout />

<#if breadcrumbList?size gt 0>
<div class="nat-breadcrumbs nat-bg-white" aria-label="breadcrumb">
    <div class="container">
        <ol class="breadcrumb__list">
        	<#list breadcrumbList?reverse as breadcrumbItem>
            <li class="breadcrumb__item">
	            <#if breadcrumbItem.getLayoutId() != currentLayout.getLayoutId()>
	            	<a href="${portalUtil.getLayoutFriendlyURL(breadcrumbItem, themeDisplay)}">
	            	${breadcrumbItem.getName(locale)}
	            	</a>
	            <#else>
	            	${breadcrumbItem.getName(locale)}
	            </#if>
            </li>
            </#list>
        </ol>
    </div>
</div>
</#if>