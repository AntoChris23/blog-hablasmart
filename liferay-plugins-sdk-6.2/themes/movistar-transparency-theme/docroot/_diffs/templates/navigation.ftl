 <header class="nat-header nat-header--nav" id="natHeaderNavigation">
        <div class="container">
            <nav class="navbar__menu" aria-labelledby="primary-navigation">
                <a class="navbar__brand" href="${site_default_url}">
                	<img id="logo-movistar" src="${images_folder}/logo-movistar.svg" alt="Centro de Transparencia"
                    title="Centro de Transparencia">
                </a>
                <button class="navbar__hamburger button--elastic" id="btnNavToggle" type="button" aria-label="Toggle navigation">
                    <span class="button__inner">&nbsp;</span>
                </button>
                <div class="navbar__scroll">
                	<!-- DESKTOP-->
                    <ul class="navbar__nav">
                    <#list nav_items as nav_item>
		                <#assign nav_item_css_class = "" />
		
		                <#if nav_item.isSelected()>
		                    <#assign nav_item_css_class = "active" />
		                </#if>
		                <#if nav_item.hasChildren()>
		                <li class="nav__item ${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}">
		                	<a class="nav__link" href="${nav_item.getURL()}" ${nav_item.getTarget()} aria-labelledby="layout_${nav_item.getLayoutId()}">
		                		${nav_item.icon()} ${nav_item.getName()}
		                		<span class="nat-icomoon icon-keyboard_arrow_down"></span>
		                	</a>
		                	<div class="nav__submenu-wrapper">
                                <div class="submenu__content">
                                    <ul class="submenu__list">
                                    <#list nav_item.getChildren() as layoutChildren>
                                        <li class="submenu__item">
                                        	<a class="nav__link" ${layoutChildren.getTarget()} href="${layoutChildren.getURL()}">${layoutChildren.getName()}</a>
                                        </li>
                                    </#list>
                                    </ul>
                                </div>
                            </div>
		                </li>
		                <#else>
		                <li class="nav__item ${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}">
		                	<a class="nav__link" href="${nav_item.getURL()}" ${nav_item.getTarget()} aria-labelledby="layout_${nav_item.getLayoutId()}">${nav_item.icon()} ${nav_item.getName()}</a>
		                </li>
		                </#if>
		            </#list>
                    </ul>
                    <!-- MOBILE-->
                    <ul class="navbar__nav-mobile">
                    <#assign navbarMobList = []>
                    <#list nav_items as nav_item>
                    	<#if nav_item.hasChildren()>
                        <li class="nav__item">
                        	<a class="nav__link" href="#" data-submenu="submenu-${nav_item.getLayoutId()}">
                        	${nav_item.getName()} <span class="nat-icomoon icon-navigate_next"></span></a>
                        	<#assign subMenuItem>
                        		<ul class="navbar__submenu-mobile" data-menu="submenu-${nav_item.getLayoutId()}">
                        			<li class="nav__item"><a class="submenu__link nav__link-return" href="#" tabindex="-1"><span class="nat-icomoon icon-navigate_before"></span> ${nav_item.getName()}</a></li>
								<#list nav_item.getChildren() as layoutChildren>
		                            <li class="nav__item"><a class="submenu__link" href="${layoutChildren.getURL()}">${layoutChildren.getName()}</a></li>
		                        </#list>
		                        </ul>
							</#assign>
							<#assign navbarMobList = navbarMobList + [subMenuItem]>
                        </li>
                        <#else>
                        <li class="nav__item"><a class="nav__link" href="${nav_item.getURL()}">${nav_item.getName()}</a></li>
                        </#if>
                    </#list>
                    </ul>
                    <#list navbarMobList as navbarMobItem>
						${navbarMobItem}
					</#list>
                </div>
            </nav>
        </div>
    </header>