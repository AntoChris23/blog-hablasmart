<#-- * Service definitions * -->
<#assign LayoutLocalService = serviceLocator.findService("com.liferay.portal.service.LayoutLocalService")  />
<#-- * Root Layouts * -->
<#assign footerLayoutFurl = theme.getSetting("footer-layout-furl")>
<#attempt>
	<#assign footerLayout = LayoutLocalService.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, footerLayoutFurl)>
<#recover>
</#attempt>
<#-- * URLs Redes Sociales * -->
<#assign socialFacebookUrl = theme.getSetting("social-facebook-url")>
<#assign socialTwitterUrl = theme.getSetting("social-twitter-url")>
<#assign socialInstagramUrl = theme.getSetting("social-instagram-url")>
<#assign socialYoutubeUrl = theme.getSetting("social-youtube-url")>
<#-- * Facebook metatags * -->
<#assign showFacebookMetatags = getterUtil.getBoolean(theme.getSetting("show-facebook-metatags"))>
<#attempt>  
	<#assign socialTitle = themeDisplay.getLayout().getExpandoBridge().getAttribute("social-title")>
	<#assign socialDescription = themeDisplay.getLayout().getExpandoBridge().getAttribute("social-description")>
<#recover>
	<#assign socialTitle = ''>
	<#assign socialDescription = ''>
</#attempt>
<#assign facebookTypeMetatag = theme.getSetting("facebook-type-metatag")>
<#assign facebookImageMetatag = theme.getSetting("facebook-image-metatag")>
<#-- * Breadcrumbs * -->
<#assign breadcrumbsEnable = getterUtil.getBoolean(theme.getSetting("breadcrumbs-enable"))>
<#-- * Google tag manager * -->
<#assign google_tag_manager_header_script = getterUtil.getString (themeDisplay.getThemeSetting ("google.tag.manager.header.script")) />
<#assign google_tag_manager_body_script = getterUtil.getString (themeDisplay.getThemeSetting ("google.tag.manager.body.script")) />