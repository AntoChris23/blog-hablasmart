// HEADER NAVIGATION
let bar = document.createElement('div');
bar.classList.add('nat-header--overlay');
document.querySelector('body').appendChild(bar);

let overlayBody = document.querySelector("body");
let navBtn = document.querySelector("#btnNavToggle");
let navbar = document.querySelector("body");
let navHeader = document.querySelector(".nat-header.nat-header--nav");

function overlayClose() {
    overlayBody.classList.remove("nat-navbar--active");
    navBtn.classList.remove('active');
}

function overlayClick(e) {
    if (!e.target.closest(".nat-header--nav")) {
        overlayClose();
    }
}

function overlayKeyDown(e) {
    if (e.keyCode === 27 && overlayBody.classList.contains("nat-navbar--active")) {
        overlayClose();
    }
}

function navbarToggle() {
    this.classList.toggle("active");
    if (navBtn.classList.contains('active')) {
        navbar.classList.add('nat-navbar--active');
    } else {
        navbar.classList.remove('nat-navbar--active');
    }
}

document.addEventListener("click", overlayClick);
window.addEventListener("keydown", overlayKeyDown);
navBtn.addEventListener("click", navbarToggle);

// HEADER

$(document).on('click', 'a.nav__link[data-submenu]', function(){
    var dataSubMenu = $(this).attr('data-submenu');
    
    $('.nat-navbar--active').addClass('nat-navbar--push-menu-full');
    $(this).parents('.navbar__nav-mobile').addClass('open');
    $(this).parents('.navbar__submenu-mobile').addClass('submenu--hidden');
    $('.navbar__submenu-mobile[data-menu='+ dataSubMenu +']').addClass('submenu--open');

    return false;
});

$(document).on('click', 'a.submenu__link[data-submenu]', function(){
    var dataSubMenu = $(this).attr('data-submenu');
    
    $(this).parents('.navbar__nav-mobile').addClass('open');
    $(this).parents('.navbar__submenu-mobile').addClass('submenu--hidden');
    $('.navbar__submenu-mobile[data-menu='+ dataSubMenu +']').addClass('submenu--open');

    return false;
});


$(document).on('click', '.nav__link-return', function(){
    var dataMenuReturn = $(this).attr('data-menu-return');

    $('.nat-navbar--active').removeClass('nat-navbar--push-menu-full');

    $(this).closest('.navbar__submenu-mobile').removeClass('submenu--open');
    if ($(this).parents('.navbar__submenu-mobile').hasClass('submenu--hidden')) {
        $(this).closest('.navbar__submenu-mobile').removeClass('submenu--hidden');
    }

    $('.navbar__nav-mobile').removeClass('open');

    $('.navbar__submenu-mobile[data-menu='+ dataMenuReturn +']').addClass('submenu--open');
    return false;
});

$(document).on('click', '.nav__link-return-2', function(){
    var dataMenuReturn = $(this).attr('data-menu-return');

    $(this).closest('.navbar__submenu-mobile').removeClass('submenu--open');
    $(this).closest('.navbar__submenu-mobile').removeClass('submenu--hidden');

    $('.navbar__submenu-mobile[data-menu='+ dataMenuReturn +']').removeClass('submenu--hidden');
    return false;
});

// BANNER HERO
var elemsNatBanner = document.querySelectorAll('.nat-banner-hero .swiper-container');
if(elemsNatBanner.length > 0){
	var swiper = new Swiper('.nat-banner-hero .swiper-container', {
	    loop: true,
	    speed: 1500,
	    autoplay: {
	        delay: 6000,
	        disableOnInteraction: false
	    },
	    pagination: {
	        el: '.swiper-pagination',
	        clickable: true
	    },
	    navigation: {
	        nextEl: '.swiper-button-next',
	        prevEl: '.swiper-button-prev'
	    }
	});
}
// COLLAPSIBLE
document.addEventListener('DOMContentLoaded', function() {
    var elemsCollapsible = document.querySelectorAll('.collapsible');
    if(elemsCollapsible.length > 0){
	    var instanceCollapsible = M.Collapsible.init(elemsCollapsible, {
	        accordion: true
	    });
    }
    var elemsModal = document.querySelectorAll('.modal');
    if(elemsModal.length > 0){
    	var instanceModal = M.Modal.init(elemsModal);
    }
});